<?php
// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';

defined('ENV') or define('ENV', getenv('ENV')?getenv('ENV'):"production");
defined('LOGEnabled') or define('LOGEnabled', getenv('LOGEnabled')?getenv('LOGEnabled'):'false');
defined('REAL_SND_PATH') or define('REAL_SND_PATH', dirname(__FILE__).'/snds/');
defined('REAL_SNDARCH_PATH') or define('REAL_SNDARCH_PATH', dirname(__FILE__).'/sndsarch/');
defined('SND_PATH') or define('SND_PATH', '/snds/');
defined('QUEUES_SND_PATH') or define('QUEUES_SND_PATH', '/queues/sounds/');
defined('SND_EXT') or define('SND_EXT', '.mp3');

$config = dirname(__FILE__).'/protected/config/'.ENV.'.php';

defined('HOME') or define('HOME',  dirname(__FILE__));
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
if(ENV == 'development') defined('YII_DEBUG') or define('YII_DEBUG',true);
require_once($yii); //initialize yii

$conifgArray = require $config; 
if(LOGEnabled == 'true') $conifgArray['preload'] = CMap::mergeArray($conifgArray['preload'],array('log'));
$app=Yii::createWebApplication($conifgArray);

Yii::$enableIncludePath = false;
require_once 'PAMI/Autoloader/Autoloader.php'; // Include PAMI autoloader.
require_once 'l4php/Logger.php';
\PAMI\Autoloader\Autoloader::register(); // Call autoloader register for PAMI autoloader.
AManager::setOptions(require Yii::getPathOfAlias('config').'/'.'pami.php');
//include bootstrap file with functions
include_once Yii::getPathOfAlias('application.components').DIRECTORY_SEPARATOR.'bootstrap.php';
$app->run();
