<?php

// change the following paths if necessary
$yiit = dirname(__FILE__) . '/../../../framework/yiit.php';
$config = dirname(__FILE__) . '/../../config/test.php';

require_once($yiit);
require_once(dirname(__FILE__) . '/CommonTestCase.php');

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

Yii::createConsoleApplication($config);
Yii::$enableIncludePath = false;
Yii::getLogger()->autoFlush = 1;
Yii::getLogger()->autoDump = true;

include_once Yii::getPathOfAlias('application.components') . DIRECTORY_SEPARATOR . 'bootstrap.php';

function exception_error_handler($errno, $errstr, $errfile, $errline) {
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}
