<?php

/**
 * CommonTestCase is base abstract class for common test suit.
 *
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
abstract class CommonTestCase extends CDbTestCase {

    public $fixtures = array(
        'CallInfo' => ':CallInfo',
        'ChannelOwners' => ':ChannelOwners',
        'Channels' => ':Channels',
        'Groups' => ':Groups',
        'Modems' => ':Modems',
        'Providers' => ':Providers',
        'Queue' => ':Queue',
        'QueueLog' => ':QueueLog',
        'SIP' => ':SIP',
        'cdr' => ':cdr',
        'queue_log' => ':queue_log'
    );

    public function setUp() {
        $this->getFixtureManager()->basePath = Yii::getPathOfAlias('application.tests.unit.fixtures.common');
        parent::setUp();
    }

}

?>
