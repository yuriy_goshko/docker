<?php

namespace {

    /**
     * Test class for CompletedCallObserver class.
     */
    class CcoTest extends TestCase {

        public function testProcessForEmptyTableWithoutListeners() {
            $dataProvider = new parser\models\DataProvider();
            $parserMock = $this->getMock("parser\CompletedCallObserver", array('read', 'notify'), array(0 => $dataProvider));
            $this->processIdle($parserMock);
        }

        public function testProcessForEmptyTableWithListeners() {

            $dataProvider = new parser\models\DataProvider();
            $parserMock = $this->getMock("parser\CompletedCallObserver", array('read', 'notify'), array(0 => $dataProvider));
            $parserMock->addListener(
                    $this->getMock("parser\abstracts\Handler"), parser\models\Request::getInstance("CcoTest\MockModel0"));
            $parserMock->addListener(
                    $this->getMock("parser\abstracts\Handler"), parser\models\Request::getInstance("CcoTest\MockModel0"));
            $this->processIdle($parserMock);
        }

        public function testProcessWithoutListeners() {
            $model0 = CcoTest\MockModel0::model();
            $fixture = array(get_class($model0) => array($model0));
            $dataProvider = new parser\models\DataProvider();
            $parserMock = Phake::partialMock("parser\CompletedCallObserver", $dataProvider);

            Phake::when($parserMock)->read()->thenReturn($fixture);

            $parserMock->process();

            Phake::verify($parserMock, Phake::never())->notify($this->isType('array'), $this->isInstanceOf("\models\events\CompletedCalls"));
        }

        public function testProcessWithListeners() {

            $dataProvider = new parser\models\DataProvider();
            $parserMock = Phake::partialMock("parser\CompletedCallObserver", $dataProvider);
            $model0 = parser\models\Request::getInstance("CcoTest\MockModel0");
            $model1 = parser\models\Request::getInstance("CcoTest\MockModel1");
            $handler0 = Phake::partialMock("CcoTest\MockHandler0");
            $handler1 = Phake::partialMock("CcoTest\MockHandler1");
            $parserMock->addListener($handler0, $model0);
            $parserMock->addListener($handler1, $model1);
            $parserMock->addListener($handler0, $model1);

            Phake::when($parserMock)->notify($this->isType('array'), $this->isInstanceOf("\models\events\CompletedCalls"))->thenReturn(null); //method would not invoked
            Phake::when($parserMock)->read()->thenReturn(
                    array($model0->getModelName() => array(CcoTest\MockModel0::model(), CcoTest\MockModel0::model()), 
                        $model1->getModelName() => array(CcoTest\MockModel1::model()))
            );

            $parserMock->process();

            Phake::verify($parserMock, Phake::times(2))->notify(array($handler0), $this->isInstanceOf("CcoTest\MockModel0"));
            Phake::verify($parserMock, Phake::times(1))->notify($this->logicalAnd(
                            $this->isType('array'), $this->contains($handler0), $this->contains($handler1), $this->callback(function($listeners) {
                                return count($listeners) == 2;
                            })
                    ), $this->isInstanceOf($model1->getModelName()));
            Phake::verify($parserMock, Phake::times(3))->notify($this->isType('array'), $this->isInstanceOf("\models\events\CompletedCalls"));

            Phake::verify($handler0, Phake::never())->notify($this->isInstanceOf("\models\events\CompletedCalls"));
            Phake::verify($handler1, Phake::never())->notify($this->isInstanceOf("\models\events\CompletedCalls"));
        }

        public function testNotifyManyListeners() {
            $dataProvider = new parser\models\DataProvider();
            $cco = new parser\CompletedCallObserver($dataProvider);
            $handler0 = Phake::partialMock("parser\abstracts\Handler");
            $handler1 = Phake::partialMock("parser\abstracts\Handler");
            $model0 = Phake::mock("\models\events\CompletedCalls"); //::model();


            $method = new ReflectionMethod('parser\CompletedCallObserver', 'notify');
            $method->setAccessible(true);

            $method->invokeArgs($cco, array(array($handler0, $handler1), $model0));

            Phake::verify($model0, Phake::times(1))->setInProcess();
        }

        /**
         * Test @method process if nothing to notify.
         * 
         * @param type $parserMock
         */
        protected function processIdle($parserMock) {
            $this->assertInstanceOf("parser\CompletedCallObserver", $parserMock);
            $parserMock->expects($this->once())
                    ->method('read')
                    ->will($this->returnValue(array()));
            $parserMock->expects($this->never())
                    ->method('notify');
            $parserMock->process();
        }

        protected function getTableModel() {
            return models\events\CompletedCalls::model();
        }

    }

}
namespace CcoTest {

    class MockHandler0 extends \parser\abstracts\Handler {

        public function notify(\models\events\CompletedCalls $data) {
            
        }

    }

    class MockHandler1 extends \parser\abstracts\Handler {

        public function notify(\models\events\CompletedCalls $data) {
            
        }

    }

    class MockHandler2 extends \parser\abstracts\Handler {

        public function notify(\models\events\CompletedCalls $data) {
            
        }

    }

    class MockModel0 extends \models\events\CompletedCalls {

        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

    }

    class MockModel1 extends \models\events\CompletedCalls {

        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

    }

}