<?php

namespace {

    class DataProviderTest extends TestCase {

        public function testAddChild() {
            $dp = new parser\models\DataProvider();
            $dp->addChild(parser\models\Request::getInstance("DataProviderTest\Model0"));
            $dp->addChild(parser\models\Request::getInstance("DataProviderTest\Model0"));
            $this->assertCount(1, $this->getProperty('_childs',$dp));
        }

        public function testGetData() {
            $dp = new parser\models\DataProvider();
            $dp->addChild(parser\models\Request::getInstance("DataProviderTest\Model0"));
            $dp->addChild(parser\models\Request::getInstance("DataProviderTest\Model1"));
            $this->assertEquals(array("DataProviderTest\Model1" => array(), "DataProviderTest\Model0" => array()), $dp->getData());
        }

    }

}

namespace DataProviderTest {



    Class Model0 extends \CActiveRecord {

        public function findAll($condition = '', $params = array()) {
            return array();
        }

    }

    Class Model1 extends \CActiveRecord {

        public function findAll($condition = '', $params = array()) {
            return array();
        }

    }

}

