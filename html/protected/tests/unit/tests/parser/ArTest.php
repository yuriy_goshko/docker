<?php

class ArTest extends CDbTestCase {

    /**
     * Initialize all fixtures.
     *
     * @var array
     */
    public $fixtures = array('ecl' => 'models\events\CompletedCalls');

    public function setUp() {
        $this->getFixtureManager()->basePath = Yii::getPathOfAlias('application.tests.unit.fixtures.parser');
        parent::setUp();
    }

    public function testAccessAfterDelete() {
        $rowObject = $this->ecl('row0');
        $rowObject->delete();
        $this->assertTrue($rowObject->rid == 6 && $rowObject->lid == 1393929537);
    }

    public function testSaveAfterDelete() {
        $this->ecl('row0')->delete();
        $this->assertTrue(!$this->ecl('row0')->refresh());

        $this->ecl('row0')->save();
        $this->assertTrue(!$this->ecl('row0')->refresh());
    }

    public function testArSingleTon() {

        $model = CActiveRecord::model('models\events\CompletedCalls');

        $this->assertFalse($model === new models\events\CompletedCalls(null));

        $this->assertFalse(new models\events\CompletedCalls(null) === new models\events\CompletedCalls(null));

        $this->assertTrue($model === CActiveRecord::model('models\events\CompletedCalls'));
    }

}
