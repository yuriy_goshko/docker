<?php

namespace {

    class RequestTest extends TestCase {

        public function testConstructInstance() {
            $obj = parser\models\Request::getInstance("RequestTest\Model0");
            $this->assertTrue($obj === parser\models\Request::getInstance("RequestTest\Model0"));
            $this->assertFalse(parser\models\Request::getInstance("RequestTest\Model1") === parser\models\Request::getInstance("RequestTest\Model0"));
            $this->assertTrue(parser\models\Request::getInstance("RequestTest\Model1") === parser\models\Request::getInstance("RequestTest\Model1"));
            $this->assertInstanceOf("parser\models\Request", $obj);
        }

        public function testUnsetByLink() {
            $obja = $objb = parser\models\Request::getInstance("RequestTest\Model0");
            unset($obja);
            $this->assertTrue($objb === parser\models\Request::getInstance("RequestTest\Model0"));
        }

        /**
         * 
         * @expectedException parser\exceptions\ClassNotExistsException
         */
        public function testNotExistedModel() {
            $obj = parser\models\Request::getInstance("notExistedClass");
        }

        /**
         * 
         * @expectedException parser\exceptions\ModelTypeException
         */
        public function testModelType() {
            $obj = parser\models\Request::getInstance("stdClass");
        }

        public function testGetName() {
            $obj = parser\models\Request::getInstance("RequestTest\Model0");
            $this->assertInstanceOf("parser\models\Request", $obj);
            $this->assertEquals("RequestTest\Model0", $obj->getModelName());
        }

        public function testGetAllRows() {
            $model = parser\models\Request::getInstance("RequestTest\Model0");
            $this->setProperty('_model', $mocked = Phake::mock('RequestTest\Model0'), $model);
            Phake::when($mocked)->findAll()->thenReturn(array()); //method would not invoked
            $result = $model->getAllRows();
            $this->assertInternalType('array', $result);
            $this->assertCountSync(0, $result);
            Phake::verify($mocked, Phake::times(1))->findAll();
        }

    }

}

namespace RequestTest {

    Class Model0 extends \CActiveRecord {
        
    }

    Class Model1 extends \CActiveRecord {
        
    }

}
