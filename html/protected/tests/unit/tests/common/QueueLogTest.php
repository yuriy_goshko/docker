<?php

/**
 * Testing QueueLog table and queue_log trigger
 */
class QueueLogTest extends CommonTestCase {

    public $fixtures = array(
        'groups' => 'Groups',
        'queue' => 'Queue',
        'channels' => 'Channels',
        'agetns' => 'SIP',
    );

    public function connectRnaDataProvider() {
        return array(
            array(
                array(
                    'time' => '2013-08-19 11:52:21.511372', 'callid' => '1376902341.4705',
                    'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                    'data1' => '', 'data2' => '0633958275', 'data3' => '1'
                )
            ),
            array(
                array(
                    'time' => '2013-08-19 11:52:30.356534', 'callid' => '1376902341.4705',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/501', 'event' => 'CONNECT',
                    'data1' => '9', 'data2' => '1376902341.4706', 'data3' => '8'
                )
            ),
            array(
                array(
                    'time' => '2013-08-19 11:52:39.595345', 'callid' => '1376902359.4712',
                    'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                    'data1' => '', 'data2' => '0633461469', 'data3' => '1'
                )
            ),
            array(
                array(
                    'time' => '2013-08-19 11:52:50.168610', 'callid' => '1376902359.4712',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/500', 'event' => 'RINGNOANSWER',
                    'data1' => '10000', 'data2' => '', 'data3' => ''
                ), false
            ),
            array(
                array(
                    'time' => '2013-08-19 11:53:13.122341', 'callid' => '1376902359.4712',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/501', 'event' => 'RINGNOANSWER',
                    'data1' => '10000', 'data2' => '', 'data3' => ''
                ), false
            ),
            array(
                array(
                    'time' => '2013-08-19 11:53:41.320626', 'callid' => '1376902359.4712',
                    'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'EXITWITHTIMEOUT',
                    'data1' => '1', 'data2' => '1', 'data3' => '62'
                )
            ),
            array(
                array(
                    'time' => '2013-08-19 11:53:49.747747', 'callid' => '1376902341.4705',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/501', 'event' => 'COMPLETECALLER',
                    'data1' => '9', 'data2' => '79', 'data3' => '1'
                )
            ),
        );
    }

    public function donotdisturbProvider() {
        return array(
            array(
                array(
                    'time' => '2013-08-19 12:11:12.806785', 'callid' => '1376903472.4807',
                    'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                    'data1' => '', 'data2' => '0637685999', 'data3' => ''
                ),
            ), array(
                array(
                    'time' => '2013-08-19 12:11:12.848665', 'callid' => '1376903472.4807',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/506', 'event' => 'RINGNOANSWER',
                    'data1' => '0', 'data2' => '', 'data3' => ''
                ), false
            ),
            array(
                array(
                    'time' => '2013-08-19 12:12:11.242767', 'callid' => '1376903472.4807',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/500', 'event' => 'CONNECT',
                    'data1' => '59', 'data2' => '1376903516.4816', 'data3' => '13'
                )),
            array(
                array(
                    'time' => '2013-08-19 13:11:12.806785', 'callid' => '1376903472.4899',
                    'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                    'data1' => '', 'data2' => '0637685999', 'data3' => ''
                ),
            ),
            array(
                array(
                    'time' => '2013-08-19 13:11:12.848665', 'callid' => '1376903472.4899',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/506', 'event' => 'RINGNOANSWER',
                    'data1' => '0', 'data2' => '', 'data3' => ''
                ), false
            ),
            array(
                array(
                    'time' => '2013-08-19 13:12:11.242767', 'callid' => '1376903472.4899',
                    'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'EXITWITHTIMEOUT',
                    'data1' => '59', 'data2' => '1376903516.4816', 'data3' => '13'
                ))
        );
    }

    public function MrnaDataProvider() {
        return array(
            array(
                array('time' => '2013-07-31 03:24:45.432797', 'callid' => '1375230272.541',
                    'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                    'data1' => '', 'data2' => '380639536613', 'data3' => '')
            ),
            array(
                array('time' => '2013-07-31 03:24:49.599192', 'callid' => '1375230272.541',
                    'queuname' => 'cleverty_4', 'agent' => 'SIP/500', 'event' => 'RINGNOANSWER',
                    'data1' => '4000', 'data2' => '', 'data3' => ''), false
            ),
            array(
                array('time' => '2013-07-31 03:24:57.471187', 'callid' => '1375230272.541',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/500', 'event' => 'RINGNOANSWER',
                    'data1' => '3000', 'data2' => '', 'data3' => ''), false
            ),
            array(
                array('time' => '2013-07-31 03:25:04.959335', 'callid' => '1375230272.541',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/500', 'event' => 'CONNECT',
                    'data1' => '19', 'data2' => '1375230302.548', 'data3' => '2')
            ),
            array(
                array('time' => '2013-07-31 03:25:06.987617', 'callid' => '1375230272.541',
                    'queuename' => 'cleverty_4', 'agent' => 'SIP/500', 'event' => 'COMPLETEAGENT',
                    'data1' => '19', 'data2' => '2', 'data3' => '1'))
        );
    }

    public function RnaConnectDataProvider() {
        return array(
            array(
                array(
                    array('time' => '2013-08-09 09:34:37.497485', 'callid' => '1376030077.1043',
                        'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                        'data1' => '', 'data2' => '380633958275', 'data3' => '1'),
                    array('time' => '2013-08-09 09:34:48.160686', 'callid' => '1376030077.1043',
                        'queuename' => 'cleverty_4', 'agent' => 'SIP/500', 'event' => 'RINGNOANSWER',
                        'data1' => '10000', 'data2' => '', 'data3' => ''),
                    array('time' => '2013-08-09 09:34:48.185152', 'callid' => '1376030077.1043',
                        'queuename' => 'cleverty_4', 'agent' => 'SIP/505', 'event' => 'RINGNOANSWER',
                        'data1' => '10000', 'data2' => '', 'data3' => ''),
                    array('time' => '2013-08-09 09:35:02.676547', 'callid' => '1376030077.1043',
                        'queuename' => 'cleverty_4', 'agent' => 'SIP/505', 'event' => 'CONNECT',
                        'data1' => '25', 'data2' => '1376030100.1047', 'data3' => '1'),
                    array('time' => '2013-08-09 09:35:11.075920', 'callid' => '1376030077.1043',
                        'queuename' => 'cleverty_4', 'agent' => 'SIP/505', 'event' => 'COMPLETECALLER',
                        'data1' => '25', 'data2' => '9', 'data3' => '1'),
                ),
                '1376030077.1043',
                array(
                    array('qlEvent' => 'ENTERQUEUE', 'qlAgent' => '-1'),
                    array('qlEvent' => 'CONNECT', 'qlAgent' => '15'),
                    array('qlEvent' => 'COMPLETECALLER', 'qlAgent' => '15'),
                ),
            )
        );
    }

    /**
     * Data provider for ABANDON event
     * 
     * @return array
     */
    public function abandonDataProvider() {
        return array(
            //1 member "SIP/203" failed call in queue. Probably there is only 1 agent registered in queue "uklon_1"
            array(
                array(
                    array(
                        'time' => '2013-07-31 00:09:50.672556', 'callid' => '1375218590.485',
                        'queuename' => 'uklon_1', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                        'data1' => '', 'data2' => '380935508894', 'data3' => '1'
                    ),
                    array(
                        'time' => '2013-07-31 00:10:02.640106', 'callid' => '1375218590.485',
                        'queuename' => 'uklon_1', 'agent' => 'SIP/203', 'event' => 'RINGNOANSWER',
                        'data1' => '12000', 'data2' => '', 'data3' => ''
                    ),
                    array(
                        'time' => '2013-07-31 00:10:07.106539', 'callid' => '1375218590.485',
                        'queuename' => 'uklon_1', 'agent' => 'NONE', 'event' => 'ABANDON',
                        'data1' => '1', 'data2' => '1', 'data3' => '17'
                    )
                ),
                '1375218590.485',
                array(
                    'waitTime' => '17',
                    'callTime' => false,
                    'agentId' => array('-1', '3')
                )
            ),
            //no memebers in queue "uklon_1"
            array(
                array(
                    array(
                        'time' => '2013-07-30 19:44:36.985845', 'callid' => '1375202676.406',
                        'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                        'data1' => '', 'data2' => '380639536613', 'data3' => '1'
                    ),
                    array(
                        'time' => '2013-07-30 19:44:38.996832', 'callid' => '1375202676.406',
                        'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ABANDON',
                        'data1' => '1', 'data2' => '1', 'data3' => '2'
                    )
                ),
                '1375202676.406',
                array(
                    'waitTime' => '2',
                    'callTime' => false,
                    'agentId' => array('-1')
                )
            ),
                //More than 1 member in queue
        );
    }

    /**
     * Data provider for event "EXITWITHTIMEOUT"
     * 
     * @return array
     */
    public function ewtDataProvider() {
        return array(
            //1 member "SIP/500" failed call in queue. Probably there is only 1 agent registered in queue "cleverty_4"
            array(
                array(
                    array(
                        'time' => '2013-07-30 13:26:09.588006', 'callid' => '1375179969.93',
                        'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                        'data1' => '', 'data2' => '380979184533', 'data3' => ''
                    ),
                    array(
                        'time' => '2013-07-30 13:26:51.978616', 'callid' => '1375179969.93',
                        'queuename' => 'cleverty_4', 'agent' => 'SIP/500', 'event' => 'RINGNOANSWER',
                        'data1' => '40000', 'data2' => '', 'data3' => ''
                    ),
                    array(
                        'time' => '2013-07-30 13:26:52.019779', 'callid' => '1375179969.93',
                        'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'EXITWITHTIMEOUT',
                        'data1' => '1', 'data2' => '1', 'data3' => '43'
                    )
                ),
                '1375179969.93',
                array(
                    'waitTime' => '43',
                    'callTime' => false,
                    'agentId' => array('-1'),
                )
            ),
            //no memebers in queue "uklon_1"
            array(
                array(
                    array(
                        'time' => '2013-07-31 19:51:03.560583', 'callid' => '1375289463.963',
                        'queuename' => 'uklon_1', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                        'data1' => '', 'data2' => '380639526424', 'data3' => '1'
                    ),
                    array(
                        'time' => '2013-07-31 19:51:45.943918', 'callid' => '1375289463.963',
                        'queuename' => 'uklon_1', 'agent' => 'NONE', 'event' => 'EXITWITHTIMEOUT',
                        'data1' => '1', 'data2' => '1', 'data3' => '42'
                    )
                ),
                '1375289463.963',
                array(
                    'waitTime' => '42',
                    'callTime' => false,
                    'agentId' => array('-1'),
                )
            ),
            //More than 1 member in queue
            array(
                array(
                    array(
                        'time' => '2013-08-30 13:26:09.588006', 'callid' => '1375179999.93',
                        'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                        'data1' => '', 'data2' => '380979184533', 'data3' => ''
                    ),
                    array(
                        'time' => '2013-08-30 13:26:51.978616', 'callid' => '1375179999.93',
                        'queuename' => 'cleverty_4', 'agent' => 'SIP/500', 'event' => 'RINGNOANSWER',
                        'data1' => '40000', 'data2' => '', 'data3' => ''
                    ),
                    array(
                        'time' => '2013-08-30 13:26:51.978616', 'callid' => '1375179999.93',
                        'queuename' => 'cleverty_4', 'agent' => 'SIP/501', 'event' => 'RINGNOANSWER',
                        'data1' => '40000', 'data2' => '', 'data3' => ''
                    ),
                    array(
                        'time' => '2013-08-30 13:26:52.019779', 'callid' => '1375179999.93',
                        'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'EXITWITHTIMEOUT',
                        'data1' => '1', 'data2' => '1', 'data3' => '43'
                    )
                ),
                '1375179999.93',
                array(
                    'waitTime' => '43',
                    'callTime' => false,
                    'agentId' => array('-1', '12'),
                )
            ),
        );
    }

    /**
     * Data provider with final event "COMPLETECALLER"
     * 
     * @return array
     */
    public function compCallerDataProvider() {
        return array(
            //1 member "SIP/206" handle call in queue. Probably there is only 1 agent registered in queue "uklon_1"
            array(
                array(
                    array(
                        'time' => '2013-07-31 15:37:21.980311', 'callid' => '1375274241.807',
                        'queuename' => 'uklon_1', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                        'data1' => '', 'data2' => '380503234665', 'data3' => '1'
                    ),
                    array(
                        'time' => '2013-07-31 15:37:24.940403', 'callid' => '1375274241.807',
                        'queuename' => 'uklon_1', 'agent' => 'SIP/206', 'event' => 'CONNECT',
                        'data1' => '3', 'data2' => '1375274242.808', 'data3' => '2'
                    ),
                    array(
                        'time' => '2013-07-31 15:38:03.128354', 'callid' => '1375274241.807',
                        'queuename' => 'uklon_1', 'agent' => 'SIP/206', 'event' => 'COMPLETECALLER',
                        'data1' => '3', 'data2' => '39', 'data3' => '1'
                    )
                ),
                '1375274241.807',
                array(
                    'waitTime' => '3',
                    'callTime' => '39',
                    'agentId' => array('-1', '11'),
                )
            ),
        );
    }

    /**
     * Data provider with final event "COMPLETEAGENT"
     * 
     * @return array
     */
    public function compAgentDataProvider() {
        return array(
            //1 member "SIP/501" handle call in queue. Probably there is only 1 agent registered in queue "cleverty_4"
            array(
                array(
                    array(
                        'time' => '2013-07-31 17:48:34.294759', 'callid' => '1375282097.948',
                        'queuename' => 'cleverty_4', 'agent' => 'NONE', 'event' => 'ENTERQUEUE',
                        'data1' => '', 'data2' => '380633461469', 'data3' => '1'
                    ),
                    array(
                        'time' => '2013-07-31 17:48:37.658089', 'callid' => '1375282097.948',
                        'queuename' => 'cleverty_4', 'agent' => 'SIP/501', 'event' => 'CONNECT',
                        'data1' => '3', 'data2' => '1375282114.951', 'data3' => '3'
                    ),
                    array(
                        'time' => '2013-07-31 17:48:39.421079', 'callid' => '1375282097.948',
                        'queuename' => 'cleverty_4', 'agent' => 'SIP/501', 'event' => 'COMPLETEAGENT',
                        'data1' => '3', 'data2' => '2', 'data3' => '1'
                    )
                ),
                '1375282097.948',
                array(
                    'waitTime' => '3',
                    'callTime' => '2',
                    'agentId' => array('-1', '12'),
                )
            ),
        );
    }

    public function allDataProviders() {
        $ab = $this->abandonDataProvider();
        $ewt = $this->ewtDataProvider();
        $compa = $this->compAgentDataProvider();
        $compc = $this->compCallerDataProvider();
        return array_merge($ab, $ewt, $compa, $compc);
    }

    public function rowExistsDataProvider() {
        return array_merge(
                $this->donotdisturbProvider(), $this->MrnaDataProvider(), $this->connectRnaDataProvider()
        );
    }

    /**
     * Check if rows insert after key event.
     * 
     * @dataProvider RnaConnectDataProvider
     */
    public function testRnaConnect($rows, $callid, $expected) {
        foreach ($rows as $row) {
            $sql = 'INSERT INTO `queue_log` (`time`, `callid`, `queuename`, `agent`, `event`, `data1`, `data2`, `data3`) VALUES(' .
                    implode(', ', Yii::app()->db->quoteValueSet($row)) . ')';
            $e = Yii::app()->db->createCommand($sql)->query();
        }
        $ar = Yii::app()->db->createCommand('SELECT qlEvent, qlAgent FROM `QueueLog` WHERE `qlCallInfo_cUniqueid` = ' . $callid)->queryAll();
        $this->assertEquals($ar, $expected);
    }

    /**
     * Check if one call hasn't more than 1 record for agent with event "RINGNOANSWER"
     * 
     * @dataProvider rowExistsDataProvider
     */
    public function testExistsRow($values, $exists = true) {
        $tableStatus = Yii::app()->db->createCommand("SHOW TABLE STATUS LIKE  'QueueLog'")->queryRow();
        $values = Yii::app()->db->quoteValueSet($values);
        $sql = 'INSERT INTO `queue_log` (`time`, `callid`, `queuename`, `agent`, `event`, `data1`, `data2`, `data3`) VALUES(' . implode(', ', $values) . ')';
        $r = Yii::app()->db->createCommand($sql)->query();
        $ar = Yii::app()->db->createCommand('SELECT * FROM `QueueLog` WHERE `qlID` = ' . $tableStatus['Auto_increment'])->query();
        $this->assertTrue(count($ar) == $exists);
    }

    /**
     * Checking waitTime table attrib
     * 
     * @dataProvider allDataproviders
     */
    public function testQueueParams($rows, $callid, $expected) {
        foreach ($rows as $row) {
            $sql = 'INSERT INTO `queue_log` (`time`, `callid`, `queuename`, `agent`, `event`, `data1`, `data2`, `data3`) VALUES(' .
                    implode(', ', Yii::app()->db->quoteValueSet($row)) . ')';
            $e = Yii::app()->db->createCommand($sql)->query();
        }
        $call = Yii::app()->db->createCommand('SELECT IFNULL(SUM(`qlWaittime` ), 0) AS waitTime FROM `QueueLog` WHERE `qlCallInfo_cUniqueid` = ' . $callid)->query();
        $waittime = Yii::app()->db->createCommand('SELECT `qlWaittime` AS waitTime FROM `QueueLog` WHERE `qlCallInfo_cUniqueid` = ' . $callid .
                        ' AND (`qlEvent` = "CONNECT" OR `qlEvent` = "ABANDON" OR `qlEvent` = "EXITWITHTIMEOUT"  )')->query();
        $callttime = Yii::app()->db->createCommand('SELECT `qlCalltime` AS callTime FROM `QueueLog` WHERE `qlCallInfo_cUniqueid` = ' . $callid .
                        ' AND (`qlEvent` = "COMPLETECALLER" OR `qlEvent` = "COMPLETEAGENT" )')->query();
        $agents = Yii::app()->db->createCommand('SELECT DISTINCT(`qlAgent`) AS agentId FROM `QueueLog` WHERE `qlCallInfo_cUniqueid` = ' . $callid)->queryAll();
        $agentss = array();
        if (is_array($agents))
            foreach ($agents as $agent) {
                $agentss['agentId'][] = $agent['agentId'];
            }

        if (!$callttime = $callttime->read())
            $callttime = array('callTime' => false);
        if (!$waittime = $waittime->read())
            $waittime = array('waitTime' => false);

        $this->assertEquals(array_merge($waittime, $callttime, $agentss), $expected);
    }

}