<?php

/**
 * UpgardeTest tests UpgradeCommand class's migrations.
 */
class UpgradeTest extends CommonTestCase {

    /**
     * Test StaticPages.spHtmlClass existance.
     */
    public function testMigrationTo112() {

        $model = StaticPages::model()->getTableSchema()->getColumn('spHtmlClass');
        $this->assertInstanceOf('CDbColumnSchema', $model);
    }

    /**
     * Test new CallInfo.cDirection enum set;
     */
    public function testMigrationTo124() {
        $phpunit = $this;
        $res = Yii::app()->db->createCommand('select distinct(`cDirection`) from `CallInfo`')->queryAll();
        array_walk($res, function($item, $key) use($phpunit) {
            $phpunit->assertContains($item['cDirection'], array(
                'outbound',
                'inbound',
                'local',
                'callback'));
        });

        $res = Yii::app()->db->createCommand('select distinct(`pbBlacked`) from `PhoneBook`')->queryAll();
        array_walk($res, function($item, $key) use($phpunit) {
            $phpunit->assertContains($item['pbBlacked'], array(
                'yes',
                'no'));
        });
    }

}
