<?php

/**
 * Testing CallInfo table and cdr trigger
 */
class CdrLogTest extends CommonTestCase {
        
    private $cdrAttrCount = 20;
    /**
     * Handled in queue callback data provider
     * 
     * @return array
     */
    public function callbackAnsweredRows(){
        return array(
            array(
                array(
                    'calldate'=>'2013-08-02 11:04:10',
                    'clid'=>'380633958275',
                    'src'=>'380633958275',
                    'dst'=>'380980197739',
                    'src_num_in'=>'380633958275',
                    'dst_num_in'=>'380980197739',
                    'dcontext'=>'incomming_calls',
                    'channel'=>'SIP/channel3-0000052e',
                    'lastapp'=>'Hangup',
                    'duration'=>'0',
                    'billsec'=>'0',
                    'disposition'=>'ANSWERED',
                    'amaflags'=>'3',
                    'uniqueid'=>'1375430650.1450'
                ),
                false
            ),
            array(
                array(
                    'calldate'=>'2013-08-02 11:04:17',
                    'clid'=>'380633958275',
                    'src'=>'380633958275',
                    'dst'=>'380633958275',
                    'src_num_out'=>'380633958275',
                    'dst_num_out'=>'380633958275',
                    'dcontext'=>'otzvon',
                    'channel'=>'Local/380633958275@otzvon-ae4b;2',
                    'dstchannel'=>'SIP/channel4-0000052f',
                    'lastapp'=>'Dial',
                    'lastdata'=>'SIP/channel4/30633958275',
                    'duration'=>'11',
                    'billsec'=>'0',
                    'disposition'=>'ANSWERED',
                    'amaflags'=>'3',
                    'uniqueid'=>'1375430657.1452'
                ),
                false
            ),
            array(
                array(
                    'calldate'=>'2013-08-02 11:04:28',
                    'clid'=>'380633958275',
                    'src'=>'380633958275',
                    'dst'=>'s',
                    'dcontext'=>'queue',
                    'channel'=>'SIP/channel4-0000052f',
                    'dstchannel'=>'SIP/503-00000530',
                    'lastapp'=>'Queue',
                    'lastdata'=>'cleverty_4,t,,,40',
                    'duration'=>'13',
                    'billsec'=>'7',
                    'disposition'=>'ANSWERED',
                    'amaflags'=>'3',
                    'userfield'=>'callback',
                    'uniqueid'=>'1375430657.1453'
                ),
                false
            ),
            array(
                array(
                    'calldate'=>'2013-08-02 11:04:34',
                    'clid'=>'380633958275',
                    'src'=>'380633958275',
                    'dst'=>'s',
                    'dcontext'=>'queue',
                    'channel'=>'SIP/channel4-0000052f',
                    'dstchannel'=>'SIP/503',
                    'duration'=>'7',
                    'billsec'=>'7',
                    'disposition'=>'ANSWERED',
                    'amaflags'=>'3',
                    'userfield'=>'callback',
                    'uniqueid'=>'1375430657.1451'
                ),
                true,
                array(
                    'cSrc'=>'380633958275',
                    'cDst'=>'503',
                    'cDuration'=>'7',
                    'cBillsec'=>'8',
                    'cDisposition'=>'ANSWERED',
                    'cUniqueid'=>'1375430657.1451',
                    'cDirection'=>'callback',
                    'cChannelChannels_cID'=>'14',
                    'cDstchannelChannels_cID'=>'13',
                    'cOutchanChannels_cID'=>'14',
                    'cQueue'=>'cleverty_4'
                )
            ),
        );
    }
    
    public function callDirectionDataProvider(){
        return array(
            array(
                array(
                    'calldate'=>'2013-08-31 21:37:52',
                    'clid'=>'"204" <204>',
                    'src'=>'204',
                    'dst'=>'0654320364',
                    'src_num_out'=>'204',
                    'dst_num_out'=>'380654320364',
                    'dcontext'=>'office_1',
                    'channel'=>'SIP/203-00001a86',
                    'dstchannel'=>'SIP/channel4-00001a87',
                    'lastapp'=>'Dial',
                    'lastdata'=>'SIP/channel4/080654320364',
                    'duration'=>'33',
                    'billsec'=>'17',
                    'disposition'=>'ANSWERED',
                    'amaflags'=>'3',
                    'uniqueid'=>'1375430657.1452'
                ),
                '1375430657.1452',
                array(
                    'cGroups_gID'=>'1',
                    'cActiveAgent'=>'3'
                )
            ),
            
            array(
                array(
                    'calldate'=>'2013-09-02 10:28:25',
                    'clid'=>'"VOLNA" <channel2>',
                    'src'=>'channel2',
                    'dst'=>'380668859090',
                    'src_num_in'=>'380993729155',
                    'dst_num_in'=>'380668859090',
                    'dcontext'=>'incoming_calls',
                    'channel'=>'SIP/channel4-0000332d',
                    'dstchannel'=>'SIP/505-00003330',
                    'lastapp'=>'Queue',
                    'lastdata'=>'volna_1,t,,,60',
                    'duration'=>'99',
                    'billsec'=>'9',
                    'disposition'=>'ANSWERED',
                    'amaflags'=>'3',
                    'uniqueid'=>'1378106905.13184'
                ),
                '1378106905.13184',
                array(
                    'cGroups_gID'=>'4',
                    'cActiveAgent'=>'15'
                )
            ),
            array(
                array(
                    'calldate'=>'2013-08-05 13:16:58',
                    'clid'=>'"204" <204>',
                    'src'=>'204',
                    'dst'=>'201',
                    'dcontext'=>'office_1',
                    'channel'=>'SIP/203-0000addb',
                    'dstchannel'=>'SIP/201-0000addc',
                    'lastapp'=>'Dial',
                    'lastdata'=>'SIP/201',
                    'duration'=>'16',
                    'billsec'=>'10',
                    'disposition'=>'ANSWERED',
                    'amaflags'=>'3',
                    'uniqueid'=>'1375697818.44792'
                ),
                '1375697818.44792',
                array(
                    'cGroups_gID'=>'1',
                    'cActiveAgent'=>'3'
                )
            ),
            
        );
    }
    
    public function allCallsDataProvider() {
        return array_merge($this->callDirectionDataProvider());
    }
    
    /**
     * 
     * @param array $row Row attributes
     * @param bool $exists Is will exists in CallInfo?
     * @param array $check What params need to check?
     * @dataProvider callbackAnsweredRows
     */
    public function testCallback($row, $exists, $check = array()){
        //$this->assertTrue(count($row) === $this->cdrAttrCount);
        $row = Yii::app()->db->quoteValueSet($row);
        $keys = Yii::app()->db->quoteColumnSet(array_keys($row));
        $sql = 'INSERT INTO `cdr` ('. implode(', ', $keys).') VALUES(' . implode(', ', $row) . ')';
        $r = Yii::app()->db->createCommand($sql)->query();
        $ar = Yii::app()->db->createCommand('SELECT * FROM `CallInfo` WHERE `cUniqueid` = ' . $row['uniqueid'] . ' 
            AND `cCalldate` = ' . $row['calldate'] . ' LIMIT 1')->query();
        
        $res = $ar->read();
        if(is_array($res)) {
            $callExists = true;
            foreach ($res as $attr=>$val) {
                if(isset($check[$attr]))
                $this->assertTrue($check[$attr] == $val);
            }
        } else {
            $callExists = $res;
        }
        
        $this->assertTrue($exists === $callExists);
        
    }
    
    /**
     * Testing attribute values after trigger
     * 
     * @param array $row
     * @param integer $unid
     * @param array $expected
     * @dataProvider allCallsDataProvider
     */
    public function testCallParams($row, $unid, $expected){
        $row = Yii::app()->db->quoteValueSet($row);
        $keys = Yii::app()->db->quoteColumnSet(array_keys($row));
        $sql = 'INSERT INTO `cdr` ('. implode(', ', $keys).') VALUES(' . implode(', ', $row) . ')';
        $r = Yii::app()->db->createCommand($sql)->query();
        $ar = Yii::app()->db->createCommand('SELECT * FROM `CallInfo` WHERE `cUniqueid` = ' . $unid)->queryRow();
        $this->assertTrue(array(
           'cGroups_gID'  => $ar['cGroups_gID'], 
           'cActiveAgent' => $ar['cActiveAgent']
                ) === $expected);
    }
}