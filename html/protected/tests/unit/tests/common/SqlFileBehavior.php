<?php

/**
 * Testing SqlFileBehavior class
 */
class SqlFileBehaviorTest extends CTestCase {

    protected $_testFilesDir = 'sqlfile';
    protected $_file1Data = array(
        "drop table if exists CallInfo",
        "create table Groups
(
   gID                  int(11) not null auto_increment,
   gName                varchar(255),
   primary key (gID),
   unique key AK_Key_2 (gName)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8");
    protected $_file2Data = array(
        "create table Groups
(
   gID                  int(11) not null auto_increment,
   gName                varchar(255),
   primary key (gID),
   unique key AK_Key_2 (gName)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8",
        "DROP PROCEDURE IF EXISTS drop_index_if_exists ;
CREATE PROCEDURE drop_index_if_exists(in theTable varchar(128), in theIndexName varchar(128) )
BEGIN
 IF((SELECT COUNT(*) AS index_exists FROM information_schema.statistics WHERE TABLE_SCHEMA = DATABASE() and table_name =
theTable AND index_name = theIndexName) > 0) THEN
   SET @s = CONCAT('DROP INDEX ' , theIndexName , ' ON ' , theTable);
   PREPARE stmt FROM @s;
   EXECUTE stmt;
 END IF;
END",
        "drop table if exists CallInfo"
    );
    protected $_file3Data = array(
        "alter table CallInfo add constraint FK_Reference_12 foreign key (cChannelChannels_cID)
references Channels (cID) on delete cascade on update cascade",
        "DROP TRIGGER IF EXISTS `cdr.data-flow`",
        "CREATE TRIGGER `cdr.data-flow` AFTER INSERT ON `cdr`
FOR EACH ROW BEGIN
 CALL getChannelID(NEW.channel, @cChannelChannels_cID)$
 CALL getChannelID(NEW.dstchannel, @cDstchannelChannels_cID)$
 CALL getParamsByDirection(
    NEW.channel, 
    NEW.dstchannel, 
    NEW.src, 
    NEW.dst, 
    NEW.src_num_out,
    NEW.dst_num_out,
    NEW.src_num_in,
    NEW.dst_num_in,
    @cChannelChannels_cID, 
    @cDstchannelChannels_cID, 
    @cDirection, 
    @cOutChan, 
    @src, 
    @dst)$
END "
    );
    protected $_file4Data = array(
        '/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */',
        '/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */',
        'drop table if exists CallInfo',
        '/*!40101 SET NAMES utf8 */',
        'create table Groups
(
   gID                  int(11) not null auto_increment,
   gName                varchar(255),
   primary key (gID),
   unique key AK_Key_2 (gName)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8',
    );
    protected $_file5Data = array("/*!50003 CREATE*/ /*!50017 DEFINER=`asterisk`@`localhost`*/ /*!50003 TRIGGER `queue.data-flow` AFTER INSERT ON `queue_log`
FOR EACH ROW BEGIN
SET @qlAgent = IFNULL((SELECT `cID` FROM `Channels` WHERE cName = NEW.agent), -1);
IF (SELECT(NEW.event = 'CONNECT'))
    THEN SET @qlWaittime = new.data1;
ELSEIF (SELECT(NEW.event = 'EXITWITHTIMEOUT' OR NEW.event = 'ABANDON'))
    THEN  SET @qlWaittime = new.data3;
ELSE  SET @qlWaittime =NULL;
END IF;
IF (SELECT(NEW.event = 'COMPLETEAGENT' OR NEW.event = 'COMPLETECALLER'))
    THEN SET @qlCalltime = new.data2;
    DELETE FROM `QueueLog` WHERE qlCallInfo_cUniqueid = NEW.callid AND qlEvent = 'RINGNOANSWER'; 
ELSE SET @qlCalltime = NULL;
END IF;
SELECT IF(COUNT(*), 1, 0) FROM `QueueLog` WHERE `qlCallInfo_cUniqueid` = new.callid 
    AND qlEvent = 'RINGNOANSWER' AND qlAgent = @qlAgent AND new.event = 'RINGNOANSWER' INTO @isDuplicateRNA;
IF (SELECT (new.data1 = '0' AND new.event = 'RINGNOANSWER'))
    THEN SET @doNotDisturb = 1;
    ELSE SET @doNotDisturb = 0;
END IF;
IF (SELECT (new.event = 'RINGNOANSWER' AND (SELECT  IF(`qlEvent` = 'CONNECT',1 ,0) 
    FROM `QueueLog` WHERE `qlAgent` = @qlAgent ORDER BY `qlCalldate` DESC LIMIT 1)))
    THEN SET @iAmBusy = 1;
    ELSE SET @iAmBusy = 0;
END IF;
IF (SELECT( (NOT @isDuplicateRNA) AND (NOT @doNotDisturb) AND (NOT @iAmBusy)))
THEN
    IF(SELECT IF(COUNT(*), 1, 0) FROM `Queue` WHERE `name` = NEW.queuename ) THEN  
                            INSERT INTO `QueueLog` Set  
                            qlCallInfo_cUniqueid = NEW.callid, 
                            qlAgent = @qlAgent, 
                            qlCalldate = NEW.time,  
                            qlQueuename = NEW.queuename,
                            qlCalltime = @qlCalltime,
                            qlWaittime = @qlWaittime,
                            qlEvent = NEW.event;
    END IF;
    IF(SELECT(NEW.event = 'EXITWITHTIMEOUT' OR NEW.event = 'ABANDON'))
    THEN SET @cDsiposition = 'NO ANSWER';
        ELSEIF(SELECT(NEW.event = 'COMPLETEAGENT' OR NEW.event = 'COMPLETECALLER'))
    THEN SET @cDsiposition= 'ANSWERED';
        ELSE SET @cDsiposition='BUSY';
    END IF;
    IF(SELECT IF(COUNT(*), 1, 0) FROM `CallInfo` WHERE `cUniqueid` = NEW.callid ) THEN
        UPDATE `CallInfo` SET   
                        cDisposition = @cDsiposition, 
                        cBillsec  = @qlCalltime,
                        cQueue = NEW.queuename
        WHERE `cUniqueid` = NEW.callid;
    END IF;
END IF;
END */");

    public function sqlFilesProvider() {
        return array(
            array(
                $this->_file1Data,
                'file1.sql',
            ),
            array(
                $this->_file2Data,
                'file2.sql',
            ),
            array(
                $this->_file3Data,
                'file3.sql',
                '$'
            ),
            array(
                $this->_file4Data,
                'file4.sql',
            ),
            array(
                $this->_file5Data,
                'file5.sql',
            )
        );
    }

    /**
     * 
     * @param type $data
     * @param type $file
     * @param type $delimiter
     * @dataProvider sqlFilesProvider
     */
    public function testParseFile($data, $file, $delimiter = ';') {
        $obj = new SqlFileBehavior();
        $res = $obj->parseSqlFile(Yii::getPathOfAlias('dataTest') . DIRECTORY_SEPARATOR . $this->_testFilesDir . DIRECTORY_SEPARATOR . $file, $delimiter);
        $this->assertInternalType('array', $res);
        $this->assertEquals($res, $data);
    }

}
