<?php

return array(
    'channel1' => array(
        'cID' => 1,
        'cType' => '0',
        'cName' => 'SIP/201'
    ),
    'channel2' => array(
        'cID' => 2,
        'cType' => '0',
        'cName' => 'SIP/202'
    ),
    'channel3' => array(
        'cID' => 3,
        'cType' => '0',
        'cName' => 'SIP/203'
    ),
    'channel4' => array(
        'cID' => 4,
        'cType' => '0',
        'cName' => 'SIP/301'
    ),
    'channel5' => array(
        'cID' => 5,
        'cType' => '0',
        'cName' => 'SIP/302'
    ),
    'SIP/500' => array(
        'cID' => 10,
        'cType' => '0',
        'cName' => 'SIP/500'
    ),
    'SIP/505' => array(
        'cID' => 15,
        'cType' => '0',
        'cName' => 'SIP/505'
    ),
    'SIP/206' => array(
        'cID' => 11,
        'cType' => '0',
        'cName' => 'SIP/206'
    ),
    'SIP/501' => array(
        'cID' => 12,
        'cType' => '0',
        'cName' => 'SIP/501'
    ),
    'SIP/503' => array(
        'cID' => 13,
        'cType' => '0',
        'cName' => 'SIP/503'
    ),
    'sipout6' => array(
        'cID' => 6,
        'cType' => '1',
        'cName' => 'SIP/somesip1',
        'cDescription' => 'somesip1_descriptions'
    ),
    'sipout8' => array(
        'cID' => 14,
        'cType' => '1',
        'cName' => 'SIP/channel4',
        'cDescription' => 'somesip4_descriptions'
    ),
    'sipout7' => array(
        'cID' => 7,
        'cType' => '1',
        'cName' => 'SIP/somesip2',
        'cDescription' => 'somesip2_descriptions'
    ),
    'sipoutSIP/380633555157'=>array(
            'cID'=>16,
            'cType'=>'1',
            'cName'=>'SIP/380633555157',
    ),
    'dongle0' => array(
        'cID' => 8,
        'cType' => '1',
        'cName' => 'Dongle/dongle0',
        'cDescription' => 'dongle0_descriptions'
    ),
    'dongle1' => array(
        'cID' => 9,
        'cType' => '1',
        'cName' => 'Dongle/dongle1',
        'cDescription' => 'dongle1_descriptions'
    ),
);
?>