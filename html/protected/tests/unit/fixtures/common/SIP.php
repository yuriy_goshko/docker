<?php
return array(
    'agent1' => array(
        'sChannels_cID' => 1,
        'name'=>201,
    ),
    'agent2' => array(
        'sChannels_cID' => 2,
        'name'=>202,
    ),
    'agent3' => array(
        'sChannels_cID' => 3,
        'name'=>203,
    ),
    'agent6' => array(
        'sChannels_cID' => 11,
        'name'=>206,
    ),
    'agent7' => array(
        'sChannels_cID' => 12,
        'name'=>501,
    ),
    'agent4' => array(
        'sChannels_cID' => 4,
        'name'=>301,
    ),
    'agent5' => array(
        'sChannels_cID' => 5,
        'name'=>302,
    ),
    '500' => array(
        'sChannels_cID' => 10,
        'name'=>500,
    ),
    '503' => array(
        'sChannels_cID' => 13,
        'name'=>503,
    ),
    '505'=>array(
        'sChannels_cID' => 15,
        'name'=>505,
    ),
    'out6'=>array(
        'sChannels_cID' => 6,
        'name'=>'somesip1',
    ),
    'out7'=>array(
        'sChannels_cID' => 7,
        'name'=>'somesip2',
    )
);
?>