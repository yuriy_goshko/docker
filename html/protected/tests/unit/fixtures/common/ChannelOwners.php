<?php
return array(
    'agent1' => array(
        'coGroups_gID' => 1,
        'coChannels_cID'=>1,
    ),
    'agent2' => array(
        'coGroups_gID' => 1,
        'coChannels_cID'=>2,
    ),
    'agent3' => array(
        'coGroups_gID' => 1,
        'coChannels_cID'=>3,
    ),
    'agent4' => array(
        'coGroups_gID' => 2,
        'coChannels_cID'=>4,
        'name'=>301,
    ),
    'agent5' => array(
        'coGroups_gID' => 2,
        'coChannels_cID'=>5,
    ),
    'agent505' => array(
        'coGroups_gID' => 4,
        'coChannels_cID'=>15,
    ),
    'agent503' => array(
        'coGroups_gID' => 2,
        'coChannels_cID'=>13,
    ),
    'coOut6' => array(
        'coGroups_gID' => 1,
        'coChannels_cID'=>6,
        'coName'=>'mysomesip1',
    ),
    'coOut7-2' => array(
        'coGroups_gID' => 2,
        'coChannels_cID'=>7,
        'coName'=>'mysomesip2'
    ),
    'coOut7-1' => array(
        'coGroups_gID' => 1,
        'coChannels_cID'=>7,
        'coName'=>'mysomesip2',
    ),
    'coOut8-1' => array(
        'coGroups_gID' => 1,
        'coChannels_cID'=>8,
        'coName'=>'myLife1',
    ),
    'coOut8-2' => array(
        'coGroups_gID' => 2,
        'coChannels_cID'=>8,
        'coName'=>'myLife2',
    ),
    'coOut9-2' => array(
        'coGroups_gID' => 2,
        'coChannels_cID'=>9,
        'coName'=>'myMts2',
    ),
    'coOut14-2' => array(
        'coGroups_gID' => 2,
        'coChannels_cID'=>14,
        'coName'=>'mychannel4',
    ),
);
?>