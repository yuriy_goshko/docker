<?php

class ParserTest extends DbTestCase {

    public $fixtures = array(
        'Groups' => 'Groups',
        'Channels' => 'Channels',
        'ChannelOwners' => 'ChannelOwners',
        'ecl' => 'models\events\CompletedCalls', //Clear eCompletedCalls table
        'cdr' => 'CallInfo', //Clear CallInfo table
    );
    static $_subjectTableName = 'cel';
    static $_jobsTableName = 'eCompletedCalls';
    protected static $_startCommand;
    protected static $_stopCommand;
    protected static $_tnsServer = null;

    public static function guidDataProvider() {
        return require_once 'data_providers' . DIRECTORY_SEPARATOR . 'Parser' . DIRECTORY_SEPARATOR . 'guid.php';
    }

    public static function autoCleanDataProvider() {
        return require_once 'data_providers' . DIRECTORY_SEPARATOR . 'Parser' . DIRECTORY_SEPARATOR . 'autoclean.php';
    }

    public static function fatDataProvider() {
        return require_once 'data_providers' . DIRECTORY_SEPARATOR . 'Parser' . DIRECTORY_SEPARATOR . 'fat.php';
    }

    public static function setUpBeforeClass() {
        self::$_startCommand = "php -f " . Yii::app()->basePath . "/yiic parser start > /dev/null &";
        self::$_stopCommand = "php -f " . Yii::app()->basePath . "/yiic parser stop > /dev/null &";
        self::startCommand();
    }

    public static function tearDownAfterClass() {
        self::stopCommand();
    }

    public static function clearTables() {
        Yii::app()->db->createCommand()->truncateTable(self::$_jobsTableName);
        Yii::app()->db->createCommand()->truncateTable(self::$_subjectTableName);
    }

    public static function startTcpServer() {
        if (is_resource(self::$_tnsServer))
            socket_close(self::$_tnsServer);
        set_time_limit(0);
        $address = '127.0.0.1';
        $port = 77;
        self::$_tnsServer = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_bind(self::$_tnsServer, $address, $port);  //0 for localhost
        socket_listen(self::$_tnsServer);
        socket_set_nonblock(self::$_tnsServer);
    }

    public static function startCommand() {
        exec(self::$_startCommand);
    }

    public static function stopCommand() {
        exec(self::$_stopCommand);
    }

    public function setUp() {
        self::clearTables();
        $this->assertEqualsSync(0, array($this, 'getCurrentWorkersCount'));
        $this->getFixtureManager()->basePath = Yii::getPathOfAlias('application.tests.func.fixtures.parser');
        parent::setUp();
    }

    public function tearDown() {
        self::clearTables();
        $this->assertEqualsSync(0, array($this, 'getCurrentWorkersCount'));
    }

    /**
     * Test maximum simultaneous workers.
     * 
     * @param array $data
     * @dataProvider fatDataProvider
     * @test
     */
    public function maxSimultaneousForks($data) {
        self::stopCommand();
        $insertedRowsCount = $this->insertRows(self::$_subjectTableName, $data);
        $this->assertEquals(count($data), $insertedRowsCount);
        self::startCommand();
        $this->assertGreaterThanSync(0, array($this, 'getCurrentWorkersCount'), array(), 2000);
        while (($workers = $this->getCurrentWorkersCount()) > 0) {
            sleep(1);
            $this->assertLessThan(\parser\CompletedCallObserver::MAX_FORKS + 1, $workers);
        }
        $this->assertEqualsSync(0, array($this, 'getCurrentWorkersCount'), array(), 2000);
    }

    /**
     * Test \parser\listeners\TNSNotifier class.
     * 
     * @param array $data
     * @param array $expectedResponse
     * @dataProvider guidDataProvider
     * @test
     */
    public function tnsNotify($data, $expectedResponse) {
        self::startTcpServer();
        $insertedRowsCount = $this->insertRows(self::$_subjectTableName, $data);
        $this->assertEquals(count($data), $insertedRowsCount);
        $tnsInc = $this->getTnsIncomings(count($expectedResponse));
        $this->assertCount(count($expectedResponse), $tnsInc);
        foreach ($expectedResponse as $resp) {
            $this->assertContains($resp, $tnsInc);
        }
        socket_close(self::$_tnsServer);
    }

    /**
     * Test \parser\listeners\AutoCleaner class.
     * 
     * @param array $data
     * @param array $expectedResponse
     * @param int $lid
     * @param int $rid
     * @dataProvider autoCleanDataProvider
     * @test
     */
    public function autoClean($data, $expectedResponse, $lid, $rid) {
        $insertedRowsCount = $this->insertRows(self::$_subjectTableName, $data);
        $this->assertEquals(count($data), $insertedRowsCount);
        $this->assertEqualsSync($expectedResponse, function($table)use($lid, $rid) {
            return Yii::app()->db->createCommand()
                            ->select('count(*) as existRows')
                            ->from($table)
                            ->where('lid=:lid AND rid=:rid', array(':lid' => $lid, ':rid' => $rid))
                            ->queryRow();
        }, array(self::$_jobsTableName));
    }

    /**
     * Reads from TCP socket.
     * 
     * @param int $count
     * @return array
     */
    protected function getTnsIncomings($count = 2) {
        $resp = array();
        $i = 0;
        while ($i <= $count + 1) {
            $client = @socket_accept(self::$_tnsServer);
            if ($client == false) {
                sleep(1);
                $i++;
                continue;
            }
            $input = socket_read($client, 1024000);
            $resp[] = $input;
            socket_close($client);
        }
        return $resp;
    }

    /**
     * Insert package of rows.
     * 
     * @param type $tableName
     * @param array $data
     * @return int
     */
    protected function insertRows($tableName, array $data) {
        $numOfSuccess = 0;
        foreach ($data as $row) {
            $res = Yii::app()->db->createCommand()->insert($tableName, $row);
            if ($res == true)
                $numOfSuccess++;
        }
        return $numOfSuccess;
    }

    /**
     * Get actual count of workers.
     * 
     * @return int
     */
    public function getCurrentWorkersCount() {
        $output = array();
        $id = trim(file_get_contents(Yii::getPathOfAlias('locks') . DIRECTORY_SEPARATOR . 'parser.lock'));
        if ($id == false)
            return 0;
        exec('pgrep -P ' . $id, $output);
        return count($output);
    }

}
