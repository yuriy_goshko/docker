<?php

// change the following paths if necessary
$yiit = dirname(__FILE__) . '/../../../framework/yiit.php';
$config = dirname(__FILE__) . '/../../config/funcTest.php';

require_once($yiit);
//require_once(dirname(__FILE__).'/WebTestCase.php');

Yii::createConsoleApplication($config);
Yii::$enableIncludePath = false;
Yii::getLogger()->autoFlush = 1;
Yii::getLogger()->autoDump = true;

include_once Yii::getPathOfAlias('application.components') . DIRECTORY_SEPARATOR . 'bootstrap.php';

require_once('Phake.php');
require_once('Phake/ClassGenerator/FileLoader.php');
Phake::setMockLoader(new Phake_ClassGenerator_FileLoader('/tmp'));
