<?php
namespace cdbl\helpers;

/**
 * Simple autoloader class.
 *
 * @package cdbl
 * @category helpers
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class Autoloader {
    
    public static function load($className){
        $classFile = str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
        include_once $classFile;
    }
    
}

 spl_autoload_register(array('cdbl\helpers\Autoloader', 'load'));

?>
