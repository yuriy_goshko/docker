<?php

namespace cdbl\helpers;

/**
 * ReflactionModel is library class for providing information about
 * given "doc blocked" class file.
 * 
 * @package cdbl
 * @category helpers
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
abstract class ReflactionModel {
    
    /**
     *  Create \cdbl\models\UpgradeMetaModel;
     * 
     * @param \SplFileInfo $source
     * @return \cdbl\models\UpgradeMetaModel
     */
    static function createFromFile(\SplFileInfo $source) {
        include_once $source;
        $source = $source->getBasename('.php');
        preg_match('/\d+_\d+_\d+$/', $source, $m);
        $version = preg_replace('/\_/', '.', $m[0]);
        $metaModel = new \cdbl\models\UpgradeMetaModel($version);
        $ref = new \ReflectionClass($source);
        $dbc = $ref->getDocComment();
        self::parseDockBlockTo($dbc, $metaModel);
        return $metaModel;
    }

    /**
     * Create a \cdbl\models\MetaModel from class or object.
     * 
     * @param mixed $class
     * @return \cdbl\models\MetaModel
     */
    static function createFromClass($class) {
        $metaModel = new \cdbl\models\MetaModel();
        $ref = new \ReflectionClass($class);
        $dbc = $ref->getDocComment();
        self::parseDockBlockTo($dbc, $metaModel);
        return $metaModel;
    }

    /**
     * Parse dock block to model.
     * 
     * @param string $dbc
     * @param \cdbl\helpers\cdbl\models\MetaModel $class
     * @return \cdbl\models\MetaModel
     */
    static function parseDockBlockTo($dbc, \cdbl\models\MetaModel $class) {
        $dbcResult = array_map(function($val) {
                    return trim(preg_replace('/^\\s\*/', '', $val));
                }, explode("\n", trim($dbc, "/* \n")));

        $prop = 'shortDesc';
        foreach ($dbcResult as $i => $val) {
            if (strlen($val) === 0) {
                $prop = 'desc';
                continue;
            }
            if (preg_match('/^\*/', $val) && isset($class->keys)) {
                $prop = 'keys';
                $val = trim(preg_replace('/^\*/', '', $val));
                $class->keys[] = $val;
                continue;
            }

            if (preg_match('/^\@(?P<directives>\w*)\\s(?P<value>.*)/', $val, $matches)) {
                $class->directives[$matches['directives']] = trim($matches['value']);
                continue;
            }

            $class->$prop .= $val . "\n";
        }
        $class->desc = trim($class->desc);
        $class->shortDesc = trim($class->shortDesc);
        return $class;
    }

}

?>
