<?php

namespace cdbl;

use \cdbl\exceptions, \cdbl\helpers;

/**
 * FileLogger is a single file logger.
 *
 * @package cdbl
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class FileLogger extends \cdbl\abstracts\Logger {

    /**
     * A SplFileInfo object.
     *
     * @var mixed
     */
    protected $sourceFile;
    
    /**
     * Initialize objects.
     */
    public function init() {

        if (!$this->sourceFile instanceof \SplFileInfo) {
            $this->sourceFile = new \SplFileInfo($this->sourceFile);
        }
        if (!$this->sourceFile->isFile())
            throw new exceptions\SourceFileException('File not exists or it is not a file. File: ' . $this->sourceFile);
    }

    /**
     * An implementation parent's log method for single file.
     */
    public function log() {
        $this->getMedia()->log($this->getView(), helpers\ReflactionModel::createFromFile($this->sourceFile));
    }

    /**
     * Set a SplFileInfo object. Can take a link to file.
     * 
     * @param mixed $file
     * @return \cdbl\FileLogger
     */
    public function setFile($file) {
        $this->sourceFile = $file;
        return $this;
    }

}

?>
