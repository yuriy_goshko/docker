<?php

namespace cdbl\media;

/**
 * Google drive media device.
 * 
 * @package cdbl
 * @category media
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class Gdoc implements \cdbl\interfaces\IMedia {

    /**
     * Google API library path.
     *
     * @var string
     */
    static $gapiPath = '';

    /**
     *
     * @var \Google_DriveService
     */
    protected $gservice;

    /**
     *
     * @var \Google_DriveFile
     */
    protected $gfile;

    /**
     * 
     * An id of Google Drive file.
     *
     * @var string
     */
    protected $gid;


    /**
     * Initialize work with Google Drive.
     * 
     * @param string $gid
     * @param \Google_Client $client
     * @param string $fileName
     */
    public function __construct($gid = '', \Google_Client $client, $fileName='changelog') {
        $this->gid = $gid;
        include_once self::$gapiPath . 'contrib' . DIRECTORY_SEPARATOR . 'Google_DriveService.php';

        $this->gservice = new \Google_DriveService($client);
        $authUrl = $client->createAuthUrl();

        //Request authorization
        print "Please visit:\n$authUrl\n\n";
        print "Please enter the auth code:\n";
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for access token
        $accessToken = $client->authenticate($authCode);
        $client->setAccessToken($accessToken);
        if ($this->gid == false) {
            //create new file there (with self::$fileName);
            $this->gfile = new \Google_DriveFile();
            $this->gfile->setTitle($fileName);
            $this->gfile->setDescription('changelog for web statistic');
            $this->gfile->setMimeType('text/plain');
            $result = $this->gservice->files->insert($this->gfile, array(
                'data' => 'qwerty',
                'mimeType' => 'text/plain',
            ));

            $this->gid = $result->id;
        }
        $this->gfile = $this->gservice->files->get($this->gid); 
        
        if ($this->gid != false) {
            $this->gservice->files->update($this->gid, $this->gfile, array('data' => ' ', 'mimeType' => 'text/plain',));
        }
    }

    /**
     * Set up options for whloe class.
     * 
     * @param array $options
     */
    public static function setOptions($options = array()) {

        foreach ($options as $property => $value) {
            call_user_func(array(__CLASS__, 'set' . ucfirst($property)), $value);
        }
    }

    /**
     * Set base gapi path.
     * 
     * @param string $gpath
     */
    public static function setGapiPath($gpath) {
        self::$gapiPath = $gpath;
    }

    /**
     * Log to Google Drive file.
     * 
     * @param string $view
     * @param \cdbl\models\MetaModel $model
     */
    public function log($view, \cdbl\models\MetaModel $model) {
        ob_start();
        include $view;
        $content = ob_get_contents();
        ob_clean();

        $resultRequest = '';
        $downloadUrl = $this->gfile->getDownloadUrl();
        if ($downloadUrl) {
            $request = new \Google_HttpRequest($downloadUrl, 'GET', null, null);
            $httpRequest = \Google_Client::$io->authenticatedRequest($request);
            if ($httpRequest->getResponseHttpCode() == 200) {
                $resultRequest = $httpRequest->getResponseBody();
            }
        }
        $content = $resultRequest . $content;
        $this->gservice->files->update($this->gid, $this->gfile, array('data' => $content, 'mimeType' => 'text/plain',));
    }


    /**
     * Get Google Drive file ID.
     * 
     * @return string
     */
    public function getGid() {
        return $this->gid;
    }

}