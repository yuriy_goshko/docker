<?php
namespace cdbl\media;

/**
 * File media device.
 * 
 * @package cdbl
 * @category media
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class File implements \cdbl\interfaces\IMedia 
{
    /**
     * Path to output file.
     *
     * @var string
     */
    protected $toFile;
    
    /**
     * Set up path to file.
     * 
     * @param string $pathToFile
     */
    public function __construct($pathToFile) {
        $this->toFile = $pathToFile;
    }
    
    /**
     * Log to file.
     * 
     * @param string $view
     * @param \cdbl\models\MetaModel $model
     */
    public function log($view, \cdbl\models\MetaModel $model) {
        ob_start();
        include $view;
        $content = ob_get_contents();
        ob_clean();
        file_put_contents($this->toFile, $content, FILE_APPEND);
    }
}