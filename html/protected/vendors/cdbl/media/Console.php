<?php

namespace cdbl\media;

/**
 * Console media device.
 * 
 * @package cdbl
 * @category media
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class Console implements \cdbl\interfaces\IMedia {

    /**
     * Echo to console.
     * 
     * @param string $view
     * @param \cdbl\models\MetaModel $model
     */
    public function log($view, \cdbl\models\MetaModel $model) {
        include $view;
    }

}