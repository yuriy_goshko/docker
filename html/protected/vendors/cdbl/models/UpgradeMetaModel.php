<?php

namespace cdbl\models;

/**
 * UpgradeMetaModel is model for reflact information about upgrade class.
 * 
 * @package cdbl
 * @category models
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class UpgradeMetaModel extends MetaModel {

    /**
     * Key features.
     *
     * @var array
     */
    public $keys = array();

    /**
     * A version.
     *
     * @var string
     */
    public $version;

    /**
     * Set up version.
     * 
     * @param string $version
     */
    public function __construct($version) {
        $this->version = $version;
    }

}

?>
