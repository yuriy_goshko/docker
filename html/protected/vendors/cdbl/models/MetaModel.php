<?php

namespace cdbl\models;

/**
 * This class is designed for provide DocBlock meta information.
 * 
 * @package cdbl
 * @category models
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class MetaModel {

    /**
     * Short description.
     *
     * @var string
     */
    public $shortDesc;

    /**
     * Description.
     *
     * @var string
     */
    public $desc;

    /**
     * A DocBlock directives (i.e @autor, @version etc)
     *
     * @var array
     */
    public $directives = array();

}

?>
