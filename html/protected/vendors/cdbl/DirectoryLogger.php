<?php
namespace cdbl;

/**
 * DirectoryLogger is a multiple file logger from directory.
 * 
 * DirectoryLogger implements Iterator interface and agreates a lot of FileLoggers.
 *
 * @package cdbl
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class DirectoryLogger extends abstracts\Logger implements \Iterator {

    /**
     * Absolute directory path to files.
     *
     * @var string
     */
    protected $sourceDirectory;

    /**
     * An sorted array map by file name. Each value is FileLogger related to current
     * directory.
     *
     * @var array
     */
    private $fileLoggers = array();
    
    /**
     * Current position.
     *
     * @var integer
     */
    private $position = 0;
    
    
    /**
     * Initialize $fileLoggers property.
     * 
     * @return type
     */
    public function init() {
        $files = array();
        $iterator = new \DirectoryIterator($this->sourceDirectory);
        foreach ($iterator as $fileinfo) {
            if ($fileinfo->isFile()) 
                $files[] = $fileinfo->getPathname();
        }
        rsort($files);
        $media = $this->media;
        $view = $this->view;
        $this->fileLoggers = array_map(function($val) use($media, $view) {
                    return new FileLogger($media, $view, array('sourceFile'=>$val));
                }, $files);
    }

    /**
     * An implementation parent's log method for directory with files.
     */
    public function log() {
        foreach ($this as $fileLogger)
            $fileLogger->log();
    }

    public function setDirectory($dir) {
        $this->sourceDirectory = $dir;
        return $this;
    }
    
    /**
     * Get current value according to position.
     * 
     * @return FileLogger
     */
    public function current() {
        return $this->fileLoggers[$this->position];
    }
    
    /**
     * A current iterator position.
     * 
     * @return integer
     */
    public function key(){
        return $this->position;
    }
    
    /**
     * A next iterator position.
     */
    public function next() {
        ++$this->position;
    }
    
    /**
     * Rewind iterator.
     */
    public function rewind() {
        $this->position = 0;
    }
    
    /**
     * Iterator validation.
     * 
     * @return bool
     */
    public function valid() {
        return isset($this->fileLoggers[$this->position]);
    }

}

?>
