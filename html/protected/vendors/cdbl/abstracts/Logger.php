<?php
namespace cdbl\abstracts;

/**
 * Class DockBlock Logger.
 * 
 * cdbl\abstracts\Logger is a base abstract class for loggers.
 * 
 * @package cdbl
 * @category abstracts
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
abstract class Logger {

    /**
     *
     * @var iMedia
     */
    protected $media;

    /**
     *
     * @var string
     */
    protected $view;
    
    /**
     * 
     * @param \cdbl\interfaces\IMedia  $media
     * @param string $view
     * @param array $properties
     */
    public function __construct($media, $view, $properties = array()) {
        $this->media = $media;
        $this->view = $view;
        foreach ($properties as $property => $value)
            $this->$property = $value;
        $this->init(); //last method
    }

    abstract public function log();

    /**
     * An init method invoke after __construct method;
     * 
     * @return bool;
     */
    public function init() {
        return true;
    }

    public function setMedia($media) {
        $this->media = $media;
    }

    public function setView($view) {
        return $this->view = $view;
    }

    public function getMedia() {
        return $this->media;
    }

    public function getView() {
        return $this->view;
    }
    

}
