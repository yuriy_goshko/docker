<?php
namespace cdbl\interfaces;

/**
 * Interface for media devices. Each media device has to know how render model 
 * information according to given view.
 * 
 * @package cdbl
 * @category interfaces
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
interface IMedia {
    
    /**
     * Render model information according to given view.
     * 
     * @param string $view
     * @param \cdbl\models\MetaModel $model
     */
    public function log($view, \cdbl\models\MetaModel $model);
}

?>
