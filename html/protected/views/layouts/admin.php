<?php
/* @var $this Controller */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="<?= Yii::app()->language ?>"
	lang="<?= Yii::app()->language ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content=<?= Yii::app()->language ?> />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?
								// Register css files
								Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'css'))) . '/' . 'main.css');
								
								Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'css',
										'admin'))) . '/' . 'admin.css');
								
								// Register JS
								Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'js'))) . '/' . 'main.js', CClientScript::POS_BEGIN);
								
								Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'js'))) . '/' . 'tbTabsEvents.js', CClientScript::POS_BEGIN);
								
								Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'js',
										'admin'))) . '/' . 'admin.js', CClientScript::POS_BEGIN);
								?>
</head>
<body>

        <?php
								$this->widget('bootstrap.widgets.TbNavbar', array(
										'brand' => Yii::app()->user->getState('StatCaption'),
										'fluid' => true,
										'items' => array(
												array(
														'class' => 'bootstrap.widgets.TbMenu',
														'items' => array(
																
																array(
																		'label' => 'Управление агентами',
																		'visible' => (yii::app()->user->getState("rEditAgents") === true),
																		'url' => '#',
																		'items' => array(
																				array(
																						'label' => Yii::t('m', 'Agents'),
																						'url' => array('/admin/agents')),
																				array(
																						'label' => Yii::t('m', 'Relations agents with departments'),
																						'url' => '/admin/depagents'))),
																array(
																		'label' => Yii::t('m', 'Пользователи'),
																		'visible' => (yii::app()->user->getState("rEditUsers") === true),
																		'url' => '#',
																		'items' => array(
																				array('label' => Yii::t('m', 'Пользователи'),'url' => '/admin/users'),
																				array(
																						'label' => Yii::t('m', 'Привязка к отделам'),
																						'url' => '/admin/depusers'),
																				array(
																						'label' => Yii::t('m', 'Привилегии'),
																						'url' => '/admin/usersrights'))),
																
																array(
																		'label' => Yii::t('m', 'Management departments'),
																		'visible' => (yii::app()->user->getState("rEditDepartments") === true),
																		'url' => '#',
																		'items' => array(
																				array('label' => Yii::t('m', 'Departments'),'url' => '/admin/dep'),
																				array(
																						'label' => Yii::t('m', 'Relations queues with departments'),
																						'url' => '/admin/depqueues'))),
																array(
																		'label' => Yii::t('m', 'Management queues'),
																		'visible' => (yii::app()->user->getState("rEditQueues") === true),
																		'items' => array(
																				array(
																						'label' => Yii::t('m', 'Queues'),
																						'url' => array('/admin/queues')),
																				array(
																						'label' => Yii::t('m', 'Relations agents with queues'),
																						'url' => array('/admin/queueagents')))),
																
																array(
																		'label' => Yii::t('m', 'Log out') . '(' . Yii::app()->user->name . ')',
																		'url' => array('/site/logout'),
																		'visible' => ! Yii::app()->user->isGuest))))));
								?>
        <div class="container main" id="page">
		<div class="header-buffer"></div>
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
													$this->widget('bootstrap.widgets.TbBreadcrumbs', array('links' => $this->breadcrumbs));
													?>
		<!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>
            <div class="footer-buffer"></div>
	</div>
	<!-- page -->
	<div id="footer">
            Copyright &copy; <?php echo date('Y'); ?>. Version <b><?= Yii::app()->params->product['version'] ?></b>
            <?php if (Yii::app()->params->product['testbuildNo'] != false): ?>
            <b> - <?= Yii::app()->params->product['testbuildNo'] ?></b>
            <?php endif; ?><br />
            <?= Yii::t('m', 'All Rights Reserved') ?>.<br />
	</div>
	<!-- footer -->
</body>
</html>
