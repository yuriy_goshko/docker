<?php
/* @var $this Controller */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= Yii::app()->language ?>" lang="<?= Yii::app()->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content=<?= Yii::app()->language ?> />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="icon" type="image/png" href="/images/favicon.ico" />
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
        <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
        <?
        //Register css files
        Yii::app()->clientScript->registerCssFile(
                Yii::app()->assetManager->publish(
                        implode('/', array(Yii::app()->basePath, '..', 'css'))
                ) . '/' . 'main.css'
        );

        Yii::app()->clientScript->registerCssFile(
                Yii::app()->assetManager->publish(
                        implode('/', array(Yii::app()->basePath, '..', 'css'))
                ) . '/' . 'front.css'
        );

        if (isset($this->finalCss))
            Yii::app()->clientScript->registerCssFile(
                    $this->finalCss
            );

        //Register JS
        Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                        implode('/', array(Yii::app()->basePath, '..', 'js'))
                ) . '/' . 'main.js', CClientScript::POS_BEGIN);


        Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                        implode('/', array(Yii::app()->basePath, '..', 'js'))
                ) . '/' . 'front.js', CClientScript::POS_BEGIN);

        Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                        implode('/', array(Yii::app()->basePath, '..', 'js'))
                ) . '/' . 'table2CSV.js', CClientScript::POS_END);

        Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                        implode('/', array(YiiBase::getPathOfAlias('webroot'), 'js'))
                ) . '/' . 'jquery.jsForm.js', CClientScript::POS_END);

        if (Yii::app()->locale->id != 'en_us')
            Yii::app()->clientScript->registerScriptFile(
                    Yii::app()->assetManager->publish(
                            implode('/', array(
                        Yii::app()->basePath, '..', 'js',
                        'jui', 'datetimepicker-i18n', 'jquery-ui-timepicker-' . Yii::app()->language . '.js'
                            ))
                    ), CClientScript::POS_END);
        ?>

    </head>

    <body>
        <? if (!Yii::app()->user->isGuest) $this->renderPartial('//layouts/front/menu/_mainTbMenu') ?>
        <? echo $content ?>
    </body>
</html>
