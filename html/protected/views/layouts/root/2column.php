<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/root'); ?>
<div class="row">
    <div class="span2">
        <div id="sidebar">
        <?php
            $this->beginWidget('zii.widgets.CPortlet', array(
                'title'=>Yii::t('m','Control block'),
            ));
            $this->widget('bootstrap.widgets.TbMenu', array(
                'items'=>array(
                     array('label'=>Yii::t('m','Manage pages'), 'url'=>array('/'.$this->module->id)),
                     array('label'=>Yii::t('m','Add a link to a resource'), 'url'=>array(Yii::app()->getUrlManager()->createUrl($this->module->id.'/addeditr')), 'visible'=>  StaticPages::model()->menus()->findAll()),
                     array('label'=>Yii::t('m','Add a new page'), 'url'=>array(Yii::app()->getUrlManager()->createUrl($this->module->id.'/addedit')), 'visible'=>  StaticPages::model()->menus()->findAll()),
                     array('label'=>Yii::t('m','Add a new menu'), 'url'=>array(Yii::app()->getUrlManager()->createUrl($this->module->id.'/addeditm'))),
                ),
                'htmlOptions'=>array('class'=>'operations'),
            ));
            $this->endWidget();
        ?>
        </div><!-- sidebar -->
    </div>
    <div class="span10">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
</div>
<?php $this->endContent(); ?>