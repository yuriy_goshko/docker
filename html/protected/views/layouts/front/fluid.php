<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/front'); ?>
<div class="container-fluid main" id="page">
    <div class="header-buffer"></div>
    <?php if (isset($this->breadcrumbs)): ?>
        <?php
        $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        ));
        ?><!-- breadcrumbs -->
    <?php endif ?>

    <?php echo $content; ?>

    <div class="footer-buffer"></div>
</div><!-- page -->
<div id="footer">
    Copyright &copy; <?php echo date('Y'); ?>
    <?php if (Yii::app()->params->product['testbuildNo'] != false): ?>
	- <?= Yii::app()->params->product['testbuildNo'] ?>
    <?php endif; ?><br/>    
    <?= Yii::t('m', 'All Rights Reserved') ?>.<br/>
</div><!-- footer -->
<?php $this->endContent(); ?>