<?php
/* @var $this Controller */
?>
<?php $this->beginContent('//layouts/front'); ?>
<div class="container main" id="page">
    <div class="header-buffer"></div>

    <?php echo $content; ?>

    <div class="footer-buffer"></div>

</div><!-- page -->
<div id="footer">
    Copyright &copy; <?php echo date('Y'); ?>
    <?php if (Yii::app()->params->product['testbuildNo'] != false): ?>
	- <?= Yii::app()->params->product['testbuildNo'] ?>
    <?php endif; ?><br/>        
    <?= Yii::t('m', 'All Rights Reserved') ?>.<br/>
</div><!-- footer -->
<?php $this->endContent(); ?>

<?php
Yii::app()->clientScript->registerCssFile(
        Yii::app()->assetManager->publish(
                implode('/', array(Yii::app()->basePath, '..', 'css'))
        ) . '/' . 'fixed.css'
);
?>