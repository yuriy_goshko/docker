<?php
$this->widget('bootstrap.widgets.TbNavbar', array(
		'brand' => Yii::app()->user->getState('StatCaption'),
		'fluid' => true,
		'htmlOptions' => array('id' => 'topbar'),
		'items' => array(
				array('class' => 'bootstrap.widgets.TbMenu','items' => StaticPages::getMainTbMenu()),
				array(
						'class' => 'bootstrap.widgets.TbMenu',
						'htmlOptions' => array('class' => 'pull-right'),
						'items' => array(
								array(
										'label' => Yii::t('m', 'Log in'),
										'linkOptions' => array('class' => 'login'),
										'url' => '/login',
										'visible' => Yii::app()->user->isGuest),
								array(
										'linkOptions' => array(
												'class' => 'logout',
												'data-toggle' => "tooltip",
												'title' => Yii::t('m', 'Log out') . ', ' . Yii::app()->user->name,
												'data-placement' => 'bottom'),
										'url' => array('/site/logout'),
										'visible' => ! Yii::app()->user->isGuest),
								array(
										'linkOptions' => array(
												'class' => 'admin',
												'data-toggle' => "tooltip",
												'title' => Yii::t('m', 'Настройки') ,
												'data-placement' => 'bottom'),
										'url' => array('/admin'),
										'visible' => (! Yii::app()->user->isGuest) && Yii::app()->user->isWithAdminRights),
								array(
										'url' => '#',
										'visible' => ! Yii::app()->user->isGuest,
										'linkOptions' => array('class' => 'sett'),
										'items' => array(
												//array('label' => Yii::t('m', 'Account'),'url' => '/settings','linkOptions' => array('class' => 'visited')),
												array('label' => 'Сменить пароль','url' => '/settings/security'))))))));
?>