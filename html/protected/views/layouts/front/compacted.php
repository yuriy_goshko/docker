<?php
/* @var $this Controller */
?>

<?php $this->beginContent('//layouts/front/fixed'); ?>
    <?php echo $content; ?>
<?php $this->endContent(); ?>
<?php
Yii::app()->clientScript->registerCssFile(
        Yii::app()->assetManager->publish(
                implode('/', array(Yii::app()->basePath, '..', 'css'))
        ) . '/' . 'compacted.css'
);
?>