<?php
/* @var $this adminController */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="<?= Yii::app()->language ?>"
	lang="<?= Yii::app()->language ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content=<?= Yii::app()->language ?> />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>

       
        <?
								// Register css files
								Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'css'))) . '/' . 'main.css');
								
								Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'css',
										'root'))) . '/' . 'root.css');
								
								// Register JS
								Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'js'))) . '/' . 'main.js', CClientScript::POS_BEGIN);
								
								Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'js'))) . '/' . 'tbTabsEvents.js', CClientScript::POS_BEGIN);
								
								Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
										Yii::app()->basePath,
										'..',
										'js',
										'root'))) . '/' . 'root.js', CClientScript::POS_BEGIN);
								?>
    </head>

<body>

        <?php
								$ww = $this->widget('bootstrap.widgets.TbNavbar', array(
										
										'items' => array(
												array(
														'class' => 'bootstrap.widgets.TbMenu',
														'items' => array(
																array('label' => Yii::t('m', 'Главное меню'),'url' => '/root'),
																array(
																		'label' => Yii::t('m', 'Управление'),
																		'url' => '#',
																		'items' => array(
																				array('label' => 'Агенты','url' => '/root/agents'),
																				array('label' => 'Пользователи','url' => '/root/users'),
																				array('label' => 'Привилегии','url' => '/root/usersrights'),
																				array('label' => Yii::t('m', 'Departments'),'url' => '/root/dep'),
																				array('label' => Yii::t('m', 'Queues'),'url' => '/root/queues'),
																				array('label' => 'Города','url' => '/root/citynames'))),
																array(
																		'label' => 'Связи',
																		'url' => '№',
																		'items' => array(
																				array(
																						'label' => 'Привязка PBX Номеров',
																						'url' => '/root/pbxnumbersrelations'),
																				array(
																						'label' => Yii::t('m', 'Пользователи к отделам'),
																						'url' => '/root/depusers'),
																				array(
																						'label' => Yii::t('m', 'Отделы к пользователям'),
																						'url' => '/root/userdeps'),
																				array(
																						'label' => Yii::t('m', 'Очереди к отделам'),
																						'url' => '/root/depqueues'),
																				array(
																						'label' => Yii::t('m', 'Агенты к отделам'),
																						'url' => '/root/depagents'),
																				array(
																						'label' => Yii::t('m', 'Агенты к очередям'),
																						'url' => '/root/queueagents'))),
																
																array(
																		'label' => Yii::t('m', 'Внешние каналы'),
																		'url' => '#',
																		'items' => array(
																				array('label' => 'PBX Номера','url' => '/root/pbxnumbers'),
																				array('label' => Yii::t('m', 'Providers'),'url' => '/root/providers'))),
																array(
																		'label' => Yii::t('m', 'Statistics'),
																		'url' => '#',
																		'items' => array(
																				array(
																						'label' => Yii::t('m', 'Types of calls'),
																						'url' => '/root/statistic/directions'),
																				array(
																						'label' => Yii::t('m', 'Разделение по отделам'),
																						'url' => '/root/directionstodeps'),
																				array(
																						'label' => Yii::t('m', 'Retention rules numbers'),
																						'url' => '/root/statistic/rulesnumbers'),
																				array(
																						'label' => Yii::t('m', 'Warnings Log'),
																						'url' => '/root/statistic/warnings'))),
																
																array('label' => 'Настройки','url' => '/root/settings'),
																array(
																		'label' => Yii::t('m', 'Log out') . ' (' . Yii::app()->user->name . ')',
																		'url' => '/site/logout',
																		'visible' => ! Yii::app()->user->isGuest))))));
								?>

        <div class="container main" id="page">
		<div class="header-buffer"></div>
		<h3
			style="text-align: center; margin-top: 0px; margin-bottom: 20px; color: navy">
		            <?php
														function parsArray($array, $url) {
															foreach ( $array as $data ) {
																if (is_array($data)) {
																	if (array_key_exists('url', $data)) {
																		if (($data['url'] === $url)) {
																			echo $data['label'];
																			break;
																		}
																	}
																	parsArray($data, $url);
																}
															}
														}
														$array = $ww->items;
														$url = $_SERVER['REQUEST_URI'];
														$url = str_replace('/', ' ', $url);
														$url = rtrim($url);
														$url = str_replace(' ', '/', $url);
														echo parsArray($array, $url);
														// echo 'test';
														?>		
													</h3>
														
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
													$this->widget('bootstrap.widgets.TbBreadcrumbs', array('links' => $this->breadcrumbs));
													?>
		<!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>
            <div class="footer-buffer"></div>

	</div>
	<!-- page -->
	<div id="footer">
            Copyright &copy; <?php echo date('Y'); ?>. Version <b><?= Yii::app()->params->product['version'] ?></b>
            <?php if (Yii::app()->params->product['testbuildNo'] != false): ?>
            <b> - <?= Yii::app()->params->product['testbuildNo'] ?></b>
            <?php endif; ?><br />
            <?= Yii::t('m', 'All Rights Reserved') ?>.<br />
	</div>
	<!-- footer -->

</body>
</html>
