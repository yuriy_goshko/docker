<?php
/* @var $this PhoneBookController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model CActiveRecord */
/* @var $h1 string */
$user = Yii::app()->user;

Yii::app()->clientScript->registerCssFile(
		Yii::app()->assetManager->publish(implode('/', array(Yii::app()->basePath, '..', 'css'))
			) . '/' . 'grids.css');

$this->finalCss = Yii::app()->assetManager->publish(
                implode('/', array(Yii::app()->basePath, '..', 'css'))
        ) . '/' . 'summary_grid.css';
?>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => true, // use transitions?
    'closeText' => '&times;', // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'info', 'error'
    // success, info, warning, error
    ),
));
?>

<div class="row-fluid fhead">
    <div class="fhead-content">
        <div class="span10 <?= $h1 ?>"><h1><img src="/images/design/blue-pb.png"/><?= $h1 ?></h1></div>
    </div>
</div>
<div id="bottombar" class="row-fluid filter active">
    <div class="filter-content">
        <h3 class="toggled-title active"><span class="icon-chevron-up"></span><?= Yii::t('m', 'Add a number') ?></h3>
        <div class="form toggled-content">
            <div class="row-fluid line"></div>
            <?php echo $this->renderPartial('_form', array('model' => new PhoneBookForm)); ?>
        </div>
    </div>
</div>

<div class="row-fluid main-content">

    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' => $dataProvider,
        'ajaxUpdate' => false,
        'enableSorting' => true,
        'filter' => $model,
        'template' => '{items}<div class="gwfooter">{pager}{summary}</div>',
        'columns' => array(
            array(
                'name' => 'pbNumber',
                'cssClassExpression' => '"pbNumber"',
                'value' => '$data->pbNumber',
                'filter' => '<input name="' . CHtml::activeName(PhoneBook::model(), 'pbNumber') . '" type="text" value="' . $model->pbNumber . '"/>'
            ),
            array(
                'name' => 'pbOwner',
                'cssClassExpression' => '"pbOwner"',
                'value' => '$data->pbOwner',
                'filter' => '<input name="' . CHtml::activeName(PhoneBook::model(), 'pbOwner') . '" type="text" value="' . $model->pbOwner . '"/>'
            ),
            array(
                'name' => 'pbBlacked',
                'cssClassExpression' => '"pbBlacked-map"',
                'id' => '$data->pbID',
                'value' => function($data) {
                    return Yii::t('pb_data', $data->pbBlacked);
                },
                'filter' => CHtml::tag(
                        'label', array('class' => 'designed-select'), CHtml::dropDownList(
                                CHtml::activeName(PhoneBook::model(), 'pbBlacked'), $model->pbBlacked, mPop(array(
                            'yes' => Yii::t('pb_data', 'yes'),
                            'no' => Yii::t('pb_data', 'no'),
                                )), array('id' => 'pbBlacked_map')
                        )
                )
            ),
            array(
                'name' => 'pbReason',
                'cssClassExpression' => '"pbReason"',
                'value' => '$data->pbReason',
                'filter' => false
            ),
            array(
                'header' => Yii::t('m', 'Actions'),
                'class' => 'ButtonColumn',
                'template' => '{update} {delete}',
                'buttons' => array(
                    'update' => array(
                        'url' => '"/phonebook/create/id/$data->pbID"',
                        'visible' => '$data->pbID',
                    ),
                ),
            )
        )
    ));
    ?>

</div>

