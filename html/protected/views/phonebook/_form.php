<?php
/* @var $this PhoneBookController */
/* @var $model PhoneBookForm */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget('ActiveForm', array(
    'id' => 'phone-book-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'method' => 'post',
    'action' => '/phonebook/create'
        ));
?>

<div class="row-fluid">
    <div class="span3 controls">
        <div class="row-fluid">
            <?php
            echo $form->labelEx($model, 'pbNumber');
            echo $form->textField($model, 'pbNumber', array(
                'value' => $model->pbNumber,
                'class' => 'span7'), array(
                'size' => 13,
                'maxlength' => 13));
            echo $form->error($model, 'pbNumber');
            ?>
        </div>


        <div class="row-fluid">

            <?php echo $form->labelEx($model, 'pbOwner'); ?>
            <?php
            echo $form->textField($model, 'pbOwner', array(
                'value' => $model->pbOwner,
                'class' => 'span7'), array(
                'size' => 60,
                'maxlength' => 255));
            ?>
            <?php echo $form->error($model, 'pbOwner'); ?>
        </div>   


        <div class="row-fluid">
            <?php echo $form->labelEx($model, 'pbBlacked'); ?>
            <?php
            echo $form->wrappedDownList(array($model, 'pbBlacked', array(
                    'yes' => Yii::t('pb_data', 'yes'),
                    'no' => Yii::t('pb_data', 'no'),
                ), array('onChange' => "setVisibleReason()", 'options' => array('no' => array('selected' => true)))), array('class' => 'span7'));
            ?>
            <?php echo $form->error($model, 'pbBlacked'); ?>
        </div>
    </div>

    <div class="span6 hidden">
        <?php echo $form->labelEx($model, 'pbReason', array('style' => 'display:block')); ?>
        <?php
        echo $form->textArea($model, 'pbReason', array(
            'maxlength' => 80,
            'class' => 'span7',
            'style' => 'height:86px',));
        ?>
        <?php echo $form->error($model, 'pbReason'); ?>
    </div>

    <?php
    if ($model->pbID != false) {
        echo "<input type='hidden' name='id' value='" . $model->pbID . "' />";
    }
    ?>

</div>
<div class="row-fluid buttons">
    <div class="span3">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Добавить')); ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Сброс', 'htmlOptions' => array(
                'id' => 'reset'
        )));
        ?>
    </div>
</div>

<?php $this->endWidget(); ?>
