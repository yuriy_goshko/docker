<?
/* $model Users */
?>
<div class="form transformed">
        <?php
								$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
										'id' => 'settings-form',
										'enableAjaxValidation' => false,
										'enableClientValidation' => true,
										'clientOptions' => array('validateOnSubmit' => true),
										'method' => 'post',
										'action' => '/settings'));
								?>

        <? if (Yii::app()->user->hasFlash('error')): ?>
            <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
        <? endif; ?>

        <div class="row prefixed">
            <?php echo $form->textFieldRow($model, 'uIdentity'); ?>
        </div>

	<div class="row prefixed">
            <?php echo $form->textFieldRow($model, 'uName'); ?>
        </div>

	<div class="row buttons">
		<div class="pull-right">
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => Yii::t('gridview', 'Update'), 'htmlOptions'=>array('class'=>'submit'))); ?>
            </div>
	</div>

        <?php $this->endWidget(); ?>
</div>
