<?
/* $model Users */
?>
<div class="form transformed">
    <?php
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
						'id' => 'security-form',
						'enableAjaxValidation' => false,
						'enableClientValidation' => true,
						'clientOptions' => array('validateOnSubmit' => true),
						'method' => 'post',
						'action' => '/settings/security'));
				?>

    <? if (Yii::app()->user->hasFlash('error')): ?>
        <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
    <? endif; ?>


    <div class="row prefixed large">
        <?php
								echo $form->passwordFieldRow($model, 'uPassword', array(
										'name' => 'Users[upassword_attrib]',
										'id' => 'Users_uPassword'));
								?>
    </div>


	<div class="row buttons">
		<div class="pull-right">
<?php

$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'htmlOptions' => array('class' => 'submit'),
		'label' => Yii::t('gridview', 'Update')));
?>
        </div>
	</div>

<?php $this->endWidget(); ?>
</div>