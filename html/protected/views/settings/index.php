<?
/* @var $this SettingsController */
/* @var $model CActiveRecord */
?>

<h1><img src="/images/design/fixed/logo.png"><?=Yii::t('m','Setting up an account')?></h1>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => true, // use transitions?
    'closeText' => '&times;', // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
         // success, info, warning, error or danger
    ),
));
?>
<?php echo $this->renderPartial('_forms/index/admin/_form', array('model' => $model)); ?>