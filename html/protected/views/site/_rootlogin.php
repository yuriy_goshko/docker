<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Root';

?>

<h1 class="twice"><img src="/images/design/fixed/logo.png">Вход<p>Пожалуйста, введите ваши учетные данные:</p></h1>

<div class="form transformed">

    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'login-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>

    <div class="row">
        <?php
        echo $form->textField($model, 'username', array(
            'class' => 'person',
            'placeholder' => $model->getAttributeLabel('username')
        ));
        ?>
        <?php echo $form->error($model, 'username'); ?>
    </div>
    <div class="row">
        <?php
        echo $form->passwordField($model, 'password', array(
            'class' => 'credential',
            'placeholder' => $model->getAttributeLabel('password')
        ));
        ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="row buttons">
        <div class="pull-left">
            <?php echo $form->checkBoxRow($model, 'rememberMe'); ?>
        </div>
        <div class="pull-right">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'htmlOptions'=>array('class'=>'submit'),
                'buttonType' => 'submit',
                'label' => Yii::t('m', 'Log in'),
            ));
            ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
