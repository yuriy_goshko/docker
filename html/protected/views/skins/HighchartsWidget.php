<?php

return array(
    'default' => array(
        'buttonTools' => array(
            'btnExport' => array(
                'type' => 'dropdown',
                'items' => array(
                    array('label' => Yii::t('higcharts', 'Export to JPEG'), 'url' => '#jpeg'),
                    array('label' => Yii::t('higcharts', 'Export to PNG'), 'url' => '#png'),
                    array('label' => Yii::t('higcharts', 'Export to PDF'), 'url' => '#pdf'),
                    array('label' => Yii::t('higcharts', 'Export to SVG'), 'url' => '#svg'),
                ),
                'dropdownOptions' => array(
                    'class' => 'left'
                ),
                'wrapperOptions' => array(
                    'class' => 'tooltip-top dropdown',
                    'title' => Yii::t('higcharts', 'Export graph or chart.')
                )
            ),
            'btnPrint' => array(
                'htmlOptions' => array(
                    'class' => 'tooltip-top',
                    'title' => Yii::t('higcharts', 'Send a graph or chart to print.')
                )
            )
        )
    ),
    'filtered' => array(
        'buttonTools' => array(
            'btnExport' => array(
                'type' => 'dropdown',
                'items' => array(
                    array('label' => Yii::t('higcharts', 'Export to JPEG'), 'url' => '#jpeg'),
                    array('label' => Yii::t('higcharts', 'Export to PNG'), 'url' => '#png'),
                    array('label' => Yii::t('higcharts', 'Export to PDF'), 'url' => '#pdf'),
                    array('label' => Yii::t('higcharts', 'Export to SVG'), 'url' => '#svg'),
                ),
                'dropdownOptions' => array(
                    'class' => 'left'
                ),
                'wrapperOptions' => array(
                    'class' => 'tooltip-top dropdown',
                    'title' => Yii::t('higcharts', 'Export graph or chart.')
                )
            ),
            'btnPrint' => array(
                'htmlOptions' => array(
                    'class' => 'tooltip-top',
                    'title' => Yii::t('higcharts', 'Send a graph or chart to print.')
                )
            ),
            'btnInfo' => array(
                'htmlOptions' => array(
                    'class' => 'tooltip-top',
                    'title' => Yii::t('higcharts', 'You can filter the items displayed on the chart with the buttons under the header graphic.')
                )
            ),
        )
    ),
);