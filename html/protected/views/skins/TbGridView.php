<?php

return array(
    'default' => array(
        'pager' => array(
            'class' => 'LinkPager',
            'header' => '',
            'cssFile' => false,
            'maxButtonCount' => 9,
            'selectedPageCssClass' => 'active',
            'hiddenPageCssClass' => 'disabled',
            'firstPageCssClass' => 'first',
            'lastPageCssClass' => 'last',
            'previousPageCssClass' => 'prev',
            'nextPageCssClass' => 'next',
            'firstPageLabel' => '<span data-toggle="tooltip" data-original-title="' . Yii::t('pager', "Go to first page") . '">1</span>',
            'lastPageLabel' => function($obj) {
        return '<span data-toggle="tooltip" data-original-title="' . Yii::t('pager', "Go to last page") . '">' . $obj->getPageCount() . '</span>';
    },
            'prevPageLabel' => '<img data-toggle="tooltip" data-original-title="' . Yii::t('pager', "Previous page") . '" src="/images/design/pagination/prev.png"/>',
            'nextPageLabel' => '<img data-toggle="tooltip" data-original-title="' . Yii::t('pager', "Next page") . '" src="/images/design/pagination/next.png"/>',
        ),
    ),
);
