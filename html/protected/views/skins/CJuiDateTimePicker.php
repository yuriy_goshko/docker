<?php

return array(
    'default' => array(
        'language' => Yii::t('m', 'en-AU'),
        'options' => array(
            'timeFormat' => Yii::t('datepicker', 'h:mm:ss TT'),
            'dateFormat' => Yii::t('datepicker', 'M d, yy'),
            'ampm'=> Yii::t('datepicker', 'ampm'),
        )
    ),
);
?>
