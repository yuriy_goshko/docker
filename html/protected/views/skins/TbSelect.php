<?php

return array(
    'default' => array(
        'options' => array(
            'title' => Yii::t('m', 'All values'),
            'countSelectedText' => Yii::t('m', '{0} of {1} selected'),
        ),
        'htmlOptions' => array(
            'data-live-search'=>true,
            'data-selected-text-format' => 'count > 1',
            'data-header' =>
            '<a class="checkall" href="">' .
            Yii::t('m', 'Select All') . '</a> / <a data-id="CallInfoForm_cfDirection" class="uncheckall" href="">' .
            Yii::t('m', 'Deselect All') . '</a>',
        ),
    ),
);