<?
/* @var $this UssdController */
/* @var $model CActiveRecord Modems 
 * @var $result string
 * @var $message string
 */
?>
<h1><?=$h1?></h1>
<?php echo $this->renderPartial('forms/_form', array('model' => $model)); ?>
<div id="UContent" class="row-fluid">

    <div id="UMessage" class="span12"> 
        <?=$message?>
    </div>
    <div id="UResult" class="span12">
        <?=$result?>
    </div>
</div>