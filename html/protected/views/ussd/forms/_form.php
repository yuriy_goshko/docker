<?php
/* @var $this UssdController */
/* @var $model Modems */
/* @var $form CActiveForm */
?>
<div class="form">
    <?php
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
						'id' => 'ussd',
						'enableAjaxValidation' => false,
						'enableClientValidation' => true,
						'clientOptions' => array('validateOnSubmit' => true,'afterValidate' => 'js:afterValidate'),
						'method' => 'post',
						'action' => '/ussd'));
				?>    
    <? if (Yii::app()->user->hasFlash('error')): ?>
        <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
    <? endif; ?>
    <div class="row">
		<div class="span3">
            <?=$form->textFieldRow($model, 'ussd_attrib');?>
        </div>
		<div class="span3">
            <?=$form->labelEx($model, 'Name'); ?>
            <?=$form->dropDownList($model, 'Name', Chtml::listData($model->findAll(), 'Name', 'DisplayName'))?>
        </div>
	</div>
	<div class="row buttons">
		<div class="span3">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Отправить')); ?>
            <?php
												$this->widget('bootstrap.widgets.TbButton', array(
														'buttonType' => 'reset',
														'label' => 'Сброс',
														'htmlOptions' => array('id' => 'reset')));
												?>
        </div>
	</div>
    <?php $this->endWidget(); ?>
</div>