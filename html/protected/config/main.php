<?php

// Define there constants if need;
return array(
		'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
		'name' => 'main',
		// autoloading model and component classes
		'import' => array(
				'application.models.*',
				'application.models.dao.*',
				'application.models.forms.*',
				'application.messages.*',
				'application.components.*',
				'application.components.db.*',
				'application.components.validators.*',
				'application.components.asterisk.*',
				'application.components.behaviors.*',
				'application.components.widgets.*',
				'application.components.widgets.grid.*',
				'application.components.exceptions.*'),
		'modules' => array('api','callinfo','callslog','admin','root','memberstat','modemstat'),
		// application components
		'language' => 'ru',
		'preload' => array('bootstrap'),
		'components' => array(
				'gettext' => array('class' => 'ext.gettext.components.GetText'),
				// следующие параметры не обезательны
				// 'domain' => 'имя домена', // defualt messages
				// 'locale_dir' => 'путь до папки с переводами', // defualt Yii::app()->basePath.DIRECTORY_SEPARATOR.'messages';
				// 'locale' => array('ru' =>'ru_RU','en' => 'en_US',)
				
				'log' => array(
						'class' => 'CLogRouter',
						'routes' => array(array('class' => 'CFileLogRoute','levels' => 'trace, info, error, warning, vardump'))),
				'assetManager' => array(),
				// 'linkAssets' => true,
				'bootstrap' => array('class' => 'bootstrap.components.Bootstrap'),
				// 'clientScript' => array('class' => 'ext.nls.67.NLSClientScript','mergeCss' => false,),
				'user' => array(
						// enable cookie-based authentication
						'allowAutoLogin' => true,'class' => 'WebUser'),
				// uncomment the following to enable URLs in path-format
				'urlManager' => array(
						'urlFormat' => 'path',
						'showScriptName' => false,
						'caseSensitive' => false,
						'rules' => array(
								'login' => 'site/login',
                /*
                 * Static pages 
                 */
                'page/view/<p0>/<p1>/<p2>/<p3>' => 'site/view',
								'page/view/<p0>/<p1>/<p2>' => 'site/view',
								'page/view/<p0>/<p1>' => 'site/view',
								'page/view/<p0>' => 'site/view',
                /*
                 * Main rules for application and modules
                 */
                '<module:\w+>/<submodule:\w+>/<controller:\w+>/<action:\w+>/<id>' => '<module>/<submodule>/<controller>/<action>/<id>',
								'<module:\w+>/<submodule:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<submodule>/<controller>/<action>',
								'<module:\w+>/<controller:\w+>/<action:\w+>/<id>' => '<module>/<controller>/<action>/<id>',
								'<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
								'<controller:\w+>/<action:\w+>/<id>' => '<controller>/<action>/<id>',
								'<controller:\w+>/<action:\w+>/*' => '<controller>/<action>')),
				'db' => require (dirname(__FILE__) . '/dbparams.php'),
				'authManager' => array('class' => 'CPhpAuthManager','defaultRoles' => array('user','root')),
				'errorHandler' => array(
						// use 'site/error' action to display errors
						'errorAction' => 'site/error'),
				'widgetFactory' => array(
						'enableSkin' => true,
						'widgets' => array(
								'HighchartsWidget' => array(
										'callback' => 'js:function(event){highchartCallback(this)}',
										'options' => array(
												'title' => array('style' => array('font' => ' 22px OfficinaSans','color' => '#009ae9')),
												'plotOptions' => array('series' => array('pointWidth' => 20)),
												'chart' => array('height' => 430),
												'legend' => array('verticalAlign' => 'top','borderColor' => '#fff','itemMarginTop' => 20,'y' => 20),
												'exporting' => array('enabled' => false),
												'credits' => array('enabled' => false)))))),
		// application-level parameters that can be accessed
		// using Yii::app()->params['paramName']
		'onBeginRequest' => function ($event) {
			$route = Yii::app()->getRequest()->getPathInfo();
			$module = substr($route, 0, strpos($route, '/'));
			
			if (Yii::app()->hasModule($module)) {
				$module = Yii::app()->getModule($module);
				if (isset($module->urlRules)) {
					$urlManager = Yii::app()->getUrlManager();
					$urlManager->addRules($module->urlRules, false);
				}
			}
			return true;
		});
