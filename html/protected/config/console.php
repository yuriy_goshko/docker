<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.

return CMap::mergeArray(
                require(dirname(__FILE__) . '/globparams.php'), array(
            'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
            'name' => 'Console Application',
            // preloading 'log' component
            'preload' => array('log'),
            'import' => array(
                'system.cli.commands.*',
                'application.components.*',
                'application.components.db.UDbMigration',
                'application.components.behaviors.*',
                'application.components.exceptions.*',
                'application.components.asterisk.*',
                'application.views._cdbl.*', //set up views path for cdbl
                'application.models.*',
            ),
            // application components
            'components' => array(
                'db' => array_merge(require(dirname(__FILE__) . '/dbparams.php'),
                				array(
                				'class' => 'CDbConnection',
                				'username' => 'root')),
                'log' => array(
                    'class' => 'CLogRouter',
                    'routes' => array(
                        array(
                            'logFile' => 'console.log',
                            'class' => 'CFileLogRoute',
                        ),
                    ),
                ),
            ),
        ));
