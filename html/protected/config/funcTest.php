<?php

return CMap::mergeArray(
                require(dirname(__FILE__) . '/globparams.php'), require(dirname(__FILE__) . '/main.php'), array(
            'import' => array(
                'application.components.test.*',
            ),
            'aliases' => array(
                'dataTest' => dirname(__FILE__) . '/../data' . DIRECTORY_SEPARATOR . 'tests',
            ),
            'components' => array(
                'db' => require(dirname(__FILE__) . '/dbparams.php'),
                'fixture' => array(
                    'class' => 'system.test.CDbFixtureManager',
                    'basePath' => dirname(__FILE__) . '/../tests/func/fixtures'
                ),
            ),
                )
);
