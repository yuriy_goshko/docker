<?php
return array(
    'appenders' => array(
        'default' => array(
            'class' => 'LoggerAppenderConsole',
            'layout' => array(
                'class' => 'LoggerLayoutSimple',
            ),
        ),
    ),
    'rootLogger' => array(
        'appenders' => array('default'),
    ),
);

