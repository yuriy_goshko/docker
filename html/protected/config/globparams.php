<?php

//global configuration;
return array(		
	'params' => array(
			'product' => array(
					'version' => '2.1.0',
					'testbuildNo' => '',
			),
			'adminEmail' => 'support@cleverty.com.ua',
			'wikilink'=>'http://cleverty.com.ua/wiki/index.php?title=',
			'languages' => array('ru' => 'Русский'),			
	),
    'aliases' => array(
        'parser' => 'application.components.parser',
        'cdbl' => 'application.vendors.cdbl',
        'models' => 'application.models',
        'bootstrap' => 'application.extensions.bootstrap',
        'debager' => 'application.extensions/yii-debug-toolbar',
        'config' => dirname(__FILE__),
        'data' => dirname(__FILE__) . '/../data',
        'locks' => dirname(__FILE__) . '/../data' . DIRECTORY_SEPARATOR . 'locks',
    ),
);
