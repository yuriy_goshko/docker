<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
//Define development constats
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
ini_set('html_errors', 1);
ini_set('log_errors', 1);
ini_set('ignore_repeated_errors', 0);
ini_set('ignore_repeated_source', 0);
ini_set('report_memleaks', 1);
ini_set('track_errors', 1);
ini_set('docref_root', 0);
ini_set('docref_ext', 0);
ini_set('error_log', dirname(__FILE__) . '/runtime/PHP_errors.log');
ini_set('error_reporting', '-1');
ini_set('log_errors_max_len', 0);

return CMap::mergeArray(
                require(dirname(__FILE__) . '/globparams.php'), require(dirname(__FILE__) . '/main.php'), array(
            'name' => 'development',
            // preloading 'log' component
            'preload' => array('log'),
            'modules' => array(
                'gii' => array(//to develop
                    'generatorPaths' => array(
                        'bootstrap.gii',
                    ),
                    'class' => 'system.gii.GiiModule',
                    'password' => 'asf78',
                    // If removed, Gii defaults to localhost only. Edit carefully to taste.
                    'ipFilters' => array('127.0.0.1', '::1'),
                ),
            ),
            'components' => array(
                'db' => require(dirname(__FILE__) . '/dbparams.php'),
                'assetManager' => array(
                    'forceCopy' => true
                ),
                'log' => array(
                    'class' => 'CLogRouter',
                    'routes' => array(
                        array(
                            'class' => 'debager.YiiDebugToolbarRoute',
                        ),
                        array(
                            'class' => 'CProfileLogRoute',
                        )
                    ),
                )
            ),
                )
);
