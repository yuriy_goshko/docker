<?php
return array(
    'log4php.properties'=>realpath(__DIR__) . DIRECTORY_SEPARATOR .'l4php.php',
    'log4php.file' => 'l4php',
    'host' => 'localhost',
    'port' => 5038,
    'username' => 'admin',
    'secret' => 'eLmfSg',
    'connect_timeout' => 10,
    'read_timeout' => 2000,
    'scheme' => 'tcp://'
);