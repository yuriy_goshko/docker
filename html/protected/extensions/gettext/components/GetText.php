<?php
/**
 *http://ajaxblog.ru/yii/multilanguage-website-with-yii/
 *FIX - отмечено что исправлено
 *пока так, что-бы не устанавливать на серверах локали/расширения
 */
class GetText extends CApplicationComponent {
	/**
	 * @var GetText domain.
	 */
	public $domain = 'messages';
	
	/**
	 * @var Language in yii.
	 */
	public $language;
	
	/**
	 * @var Directory containing gettext messages.
	 */
	public $locale_dir;
	
	/**
	 * @var array locale (locale -a)
	 */
	public $locale = array('ru' => 'ru_RU','en' => 'en_US');
	
	/**
	 * Initialize php's gettext.
	 */
	public function init() {
		$this->setLocale();
	}
	
	/**
	 * Bind the gettext domain and make it the default
	 */
	public function bindDomain() {
		if (! bindtextdomain($this->domain, $this->locale_dir)) {
			throw new Exception("Found folder to translations {$this->locale_dir}");
		}
		bind_textdomain_codeset($this->domain, 'utf-8');
		textdomain($this->domain);
	}
	
	/**
	 * Get canonical locale to the format required for gettext
	 */
	public function getLocale($id) {
		$locale = isset($this->locale[$id]) ? $this->locale[$id] : $id;
		$locale = explode('_', $locale);
		if (isset($locale[1]))
			$locale[1] = strtoupper($locale[1]);
		return implode('_', $locale);
	}
	
	/**
	 * Set locale
	 */
	public function setLocale() {
		$this->language = $this->language ? $this->language : Yii::app()->language;
		$locale = $this->getLocale($this->language);
		
		$this->locale_dir = $this->locale_dir ? $this->locale_dir : Yii::app()->basePath . DIRECTORY_SEPARATOR . 'messages';
		
		if (! setlocale(LC_ALL, $locale . '.UTF-8', $locale . '.utf-8', $locale . '.UTF8', $locale . 'utf8')) {
			if (strtoupper(substr(PHP_OS, 0, 3)) != 'WIN')
				throw new Exception("Not installed in the system locale {$locale}");
		}
		
		header('Content-Language: ' . str_replace('_', '-', $this->language));
		// Fix, не исп.
		// $this->bindDomain();
	}
}