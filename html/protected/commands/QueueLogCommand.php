<?php

/**
 * queue_log generator.
 * 
 * @author Yurii Kuzich <qzichs@gmail.com>
 */
class QueueLogCommand extends CConsoleCommand {

    /**
     * Event field array.
     *
     * @var array 
     */
    protected $event = array(
        'ABANDON', 'ADDMEMBER', 'AGENTDUMP', 'AGENTLOGIN', 'AGENTCALLBACKLOGIN', 'AGENTLOGOFF', 'AGENTCALLBACKLOGOFF',
        'COMPLETEAGENT', 'COMPLETECALLER', 'CONFIGRELOAD', 'CONNECT', 'ENTERQUEUE', 'EXITEMPTY', 'EXITWITHKEY',
        'EXITWITHTIMEOUT'
    );
    protected $qname = array(
        'test0', 'test1', 'test2', 'test3'
    );
    protected $aname = array(
        'agent0', 'agent1', 'agent2', 'agent3', 'agent4',
    );

    /**
     * And command designed to generate a $count rows into `cdr` table.
     * 
     * @param int $count
     */
    public function actionGenerate($count = "0") {

        for ($i = 0; $i < $count; $i++) {
            $columns = array(
                'time' => $this->getTime(time()),
                'callid' => $this->getCallid(),
                'queuename' => $this->getQueuename(),
                'agent' => $this->getAgent(),
                'event' => $this->getEvent(),
                'data1' => $this->getData1(),
                'data2' => $this->getData2(),
                'data3' => $this->getData3(),
                'data4' => $this->getData4(),
                'data5' => $this->getData5(),
                'isparsed' => $this->getIsparsed(),
            );

            echo Yii::app()->db->createCommand()
                    ->insert("queue_log", $columns);
        }
    }

    protected function getTime($time) {
        return date('Y-m-d H:i:s', $time);
    }

    protected function getCallid() {
        return microtime(true) + rand(1, 5);
    }

    protected function getQueuename() {
        return $this->qname[rand(0, count($this->qname)-1)];
    }

    protected function getAgent() {
        return $this->qname[rand(0, count($this->qname)-1)];
    }
    
    protected function getEvent(){
        return $this->event[rand(0, count($this->event)-1)];
    }

    protected function getData1() {
        return false;
    }

    protected function getData2() {
        return false;
    }

    protected function getData3() {
        return false;
    }

    protected function getData4() {
        return false;
    }

    protected function getData5() {
        return false;
    }

    protected function getIsparsed() {
        return 0;
    }

}
