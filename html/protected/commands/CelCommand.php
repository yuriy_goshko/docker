<?php

declare(ticks = 1);

/**
 * Cel generator.
 * 
 * @author Yurii Kuzich <qzichs@gmail.com>
 */
class CelCommand extends CConsoleCommand {

    /**
     * Event field array.
     *
     * @var array 
     */
    protected $eventtype = array(
        'CHAN_START', 'CHAN_END', 'ANSWER', 'HANGUP', 'CONF_ENTER', 'CONF_EXIT', 'CONF_START',
        'CONF_END', 'APP_START', 'APP_END', 'PARK_START', 'PARK_END', 'BRIDGE_START', 'BRIDGE_END',
        'BRIDGE_UPDATE', '3WAY_START', '3WAY_END', 'BLINDTRANSFER', 'ATTENDEDTRANSFER',
        'TRANSFER', 'PICKUP', 'FORWARD', 'HOOKFLASH', 'LINKEDID_END', 'USER_DEFINED', 'GUID', 'CALL_TYPE'
    );

    /**
     * Random event types without 'CHAN_START', 'CHAN_END'.
     *
     * @var array
     */
    protected $randomeventtype = array(
        'ANSWER', 'HANGUP', 'CONF_ENTER', 'CONF_EXIT', 'CONF_START',
        'CONF_END', 'APP_START', 'APP_END', 'PARK_START', 'PARK_END', 'BRIDGE_START', 'BRIDGE_END',
        'BRIDGE_UPDATE', '3WAY_START', '3WAY_END', 'BLINDTRANSFER', 'ATTENDEDTRANSFER',
        'TRANSFER', 'PICKUP', 'FORWARD', 'HOOKFLASH', 'GUID', 'CALL_TYPE'
    );
    protected $context = array(
        'context0', 'context0', 'context0', 'context0'
    );

    /**
     * And command designed to generate a $count rows into `cel` table.
     * 
     * @param int $count
     */
    public function actionRandomGenerate($count = "0") {

        for ($i = 0; $i < $count; $i++) {
            $this->insertEventRow($this->generateRandomCallData());
        }
    }

    /**
     * And command designed to generate a $count rows into `cdr` table.
     * 
     * @param int $count
     */
    public function actionGenerateOther($count = "0") {

        for ($i = 0; $i < $count; $i++) {
            $this->insertEventRow($this->generateRandomCallData(array(
                        'eventtype' => $this->getRandomEventtype()
            )));
        }
    }

    /**
     * Generate completed calls.
     * 
     * @param int $count. Completed call quantity.
     */
    public function actionGenerateCall($count = 0, $chanCount = 1) {
        for ($i = 0; $i < $count; $i++) {
            usleep(100000);
            $callId = $this->getLinkedid() + $i;
            $rows = array();
            for ($d = 0; $d < $chanCount; $d++) {
                $rows[] = $this->generateRandomCallData(array(
                    'eventtype' => 'CHAN_START',
                    'uniqueid' => $callId,
                    'linkedid' => $callId,
                        )
                );

                for ($j = 0; $j < 4; $j++) {
                    $rows[] = $this->generateRandomCallData(
                            array(
                                'eventtype' => $this->getRandomEventtype(),
                                'linkedid' => $callId
                            )
                    );
                }

                $rows[] = $this->generateRandomCallData(array(
                    'eventtype' => 'CHAN_END',
                    'uniqueid' => $callId,
                    'linkedid' => $callId,
                ));

                $rows[] = $this->generateRandomCallData(array(
                    'eventtype' => 'HANGUP',
                    'uniqueid' => $callId,
                    'linkedid' => $callId,
                ));

                $rows[] = $this->generateRandomCallData(array(
                    'eventtype' => 'LINKEDID_END',
                    'uniqueid' => $callId,
                    'linkedid' => $callId,
                ));
            }

            foreach ($rows as $row) {
                $this->insertEventRow($row);
            }
        }
    }

    public function actionExport($count = "0", $fileName = 'exportPhpArray.php') {

        $res = Yii::app()->db->createCommand()->select("eventtype, eventtime, userdeftype,"
                        . " cid_name, cid_num, cid_ani, cid_rdnis, cid_dnid, exten, context, channame, "
                        . "appname, appdata, accountcode, peeraccount, uniqueid, linkedid, amaflags, "
                        . "userfield, peer, extra")
                ->from("cel")
                ->limit($count)
                ->queryAll();
        file_put_contents($fileName, "<?php return \n" . var_export($res, true).";");
    }

    /**
     * Generate event row data.
     * 
     * @param array $replData
     * @return array
     */
    protected function generateRandomCallData($replData = array()) {
        return array_merge(
                array(
            'eventtype' => $this->getEventtype(),
            'eventtime' => $this->getEventtime(time()),
            'userdeftype' => $this->getUserdeftype(),
            'cid_name' => $this->getCidName(),
            'cid_num' => $this->getCidNum(),
            'cid_ani' => $this->getCidAni(),
            'cid_rdnis' => $this->getCidRdnis(),
            'cid_dnid' => $this->getCidDnid(),
            'exten' => $this->getExten(),
            'context' => $this->getContext(),
            'channame' => $this->getChanname(),
            'appname' => $this->getAppname(),
            'appdata' => $this->getAppdata(),
            'accountcode' => $this->getAccountcode(),
            'peeraccount' => $this->getPeeraccount(),
            'uniqueid' => $this->getUniqueid(),
            'linkedid' => $this->getLinkedid(),
            'amaflags' => $this->getAmaflags(),
            'userfield' => $this->getUserfield(),
            'peer' => $this->getPeer(),
            'extra' => $this->getExtra(),
                ), $replData);
    }

    protected function insertEventRow($row = array()) {
        echo Yii::app()->db->createCommand()
                ->insert("cel", $row);
    }

    protected function getEventtype() {
        return $this->eventtype[rand(0, count($this->eventtype) - 1)];
    }

    protected function getRandomEventtype() {
        return $this->randomeventtype[rand(0, count($this->randomeventtype) - 1)];
    }

    protected function getEventtime($time) {
        return date('Y-m-d H:i:s', $time);
    }

    protected function getUserdeftype() {
        return false;
    }

    protected function getCidName() {
        return rand(1, 9) * 100;
    }

    protected function getCidNum() {
        return rand(1, 9) * 100;
    }

    protected function getCidAni() {
        return rand(1, 9) * 100;
    }

    protected function getCidRdnis() {
        return false;
    }

    protected function getCidDnid() {
        return rand(1, 9) * 100;
    }

    protected function getExten() {
        return rand(1, 9) * 100;
    }

    protected function getContext() {
        return $this->context[rand(0, count($this->context) - 1)];
    }

    protected function getChanname() {
        return "SIP/" . $this->getExten();
    }

    protected function getAppname() {
        return false;
    }

    protected function getAppdata() {
        return false;
    }

    protected function getAccountcode() {
        return false;
    }

    protected function getPeeraccount() {
        return false;
    }

    protected function getUniqueid() {
        return microtime(true);
    }

    protected function getLinkedid() {
        return microtime(true);
    }

    protected function getAmaflags() {
        return 3;
    }

    protected function getUserfield() {
        return false;
    }

    protected function getPeer() {
        return false;
    }

    protected function getExtra() {
        return false;
    }

}
