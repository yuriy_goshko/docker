<?php

/**
 * Console command responds for get misc information.
 * 
 * @author Yurii Kuzich <qzichs@gmail.com>
 */
class ShowCommand extends CConsoleCommand {

    public $defaultAction = 'fullVersion';

    /**
     *
     * @var array
     */
    protected $_version;

    public function beforeAction($action, $params) {
        $this->_version = Yii::app()->db->createCommand()
                ->select('vRelease, vDb, vApp')
                ->from('Version')
                ->queryRow();
        return parent::beforeAction($action, $params);
    }

    public function actionfullVersion() {
        echo "\n";
        echo implode('.', $this->_version) . "\n\n";
    }

    public function actionRelease() {
        echo "\n";
        echo $this->_version['vRelease'] . "\n\n";
    }

    public function actionDb() {
        echo "\n";
        echo $this->_version["vDb"] . "\n\n";
    }

    public function actionApp() {
        echo "\n";
        echo $this->_version['vApp'] . "\n\n";
    }

}