<?php

/**
 * AMI listener
 * 
 * @author Yurii Kuzich <qzichs@gmail.com>
 */
class ListenerCommand extends CConsoleCommand {


    public $defaultAction = 'start';

    /**
     * 
     * @var integer
     */
    public $pid;

    /**
     * Path to lock file.
     *
     * @var string
     */
    protected $lockFile;

    public function init() {
        Yii::$enableIncludePath = false;
        require_once 'PAMI/Autoloader/Autoloader.php'; // Include PAMI autoloader.
        require_once 'l4php/Logger.php';
        \PAMI\Autoloader\Autoloader::register(); // Call autoloader register for PAMI autoloader.
        AManager::setOptions(require Yii::getPathOfAlias('config') . '/' . 'pami.php');
        fclose(STDIN);
        fclose(STDERR);
        $this->lockFile = Yii::getPathOfAlias('locks') . DIRECTORY_SEPARATOR . 'listener.lock';
    }

    public function actionStart() {

        if (file_exists($this->lockFile)) {
            echo 'Process already running or was forced stoped ' . "\n";
            die();
        }
        pcntl_signal(SIGTERM, array($this, 'actionStop'));

        $pid = pcntl_fork();
        if ($pid == -1) {
            die('could not fork');
        } else if ($pid) {
            echo "\n\n";
            //pcntl_wait($status); //Protect against Zombie children
        } else {
            $this->pid = getmypid(); //set up pid
            file_put_contents($this->lockFile, $this->pid);
            AManager::getInstance()->catchAllEvents();
        }
    }

    public function actionMonitor() {
        AManager::getInstance()->catchAllEvents();
    }

    public function actionStop() {
        if (file_exists($this->lockFile)) {
            $this->pid = file_get_contents($this->lockFile);
            if ($this->pid) {
                unlink($this->lockFile);
                echo "Process successfull stoped" . "\n";
                if (posix_kill($this->pid, 9))
                    ;
            } else {
                echo "Wrong lock file!" . "\n";
            }
        } else {
            echo "Process is not running!" . "\n";
        }
    }

    public function actionRestart() {
        $this->actionStop();
        $this->actionStart();
    }

}