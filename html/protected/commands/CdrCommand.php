<?php

/**
 * Cdr generator.
 * 
 * @author Yurii Kuzich <qzichs@gmail.com>
 */
class CdrCommand extends CConsoleCommand {

    /**
     * Disposition field array.
     *
     * @var array 
     */
    protected $dispositions = array(
        'FAILED', 'BUSY', 'ANSWERED', 'UNKNOWN', 'NO ANSWER', 'uFAILED', 'uHANGUP'
    );
    
    

    /**
     * And command designed to generate a $count rows into `cdr` table.
     * 
     * @param int $count
     */
    public function actionGenerate($count = "0") {
        
        for ($i = 0; $i < $count; $i++) {
            $columns = array(
                'calldate' => $this->getCallDate(time()),
                'clid' => $this->getClid(),
                'src' => $this->getSrc(),
                'dst' => $this->getDst(),
                'src_num_in' => $this->getSrcNumIn(),
                'dst_num_in' => $this->getDstNumIn(),
                'src_num_out' => $this->getSrcNumOut(),
                'dst_num_out' => $this->getDstNumOut(),
                'dcontext' => $this->getDcontext(),
                'channel' => $this->getChannel(),
                'dstchannel' => $this->getDstchannel(),
                'lastapp' => $this->getLastapp(),
                'lastdata' => $this->getLastdata(),
                'duration' => $this->getDuration(),
                'billsec' => $this->getBillsec(),
                'disposition' => $this->getDisposition(),
                'amaflags' => $this->getAmaflags(),
                'accountcode' => $this->getAccountcode(),
                'userfield' => $this->getUserfield(),
                'uniqueid' => $this->getUniqueid(),
                'isparsed' => $this->getIsparsed(),
            );

            echo Yii::app()->db->createCommand()
                    ->insert("cdr", $columns);
        }
    }

    protected function getCallDate($time) {
        return date('Y-m-d H:i:s', $time);
    }

    protected function getClid() {
        return false;
    }

    protected function getSrc() {
        return rand(1, 9) * 100;
    }

    protected function getDst() {
        return rand(1, 9) * 100;
    }

    protected function getSrcNumIn() {
        return false;
    }

    protected function getDstNumIn() {
        return false;
    }

    protected function getSrcNumOut() {
        return false;
    }

    protected function getDstNumOut() {
        return false;
    }

    protected function getDcontext() {
        return false;
    }

    protected function getChannel() {
        return "SIP/" . $this->getSrc();
    }

    protected function getDstchannel() {
        return "SIP/" . $this->getDst();
    }

    protected function getLastapp() {
        return false;
    }

    protected function getLastdata() {
        return false;
    }

    protected function getDuration() {
        return $this->getBillsec() + rand(0, 30);
    }

    protected function getBillsec() {
        return rand(0, 120);
    }

    protected function getDisposition() {
        return $this->dispositions[rand(0, count($this->dispositions)- 1)];
    }

    protected function getAmaflags() {
        return false;
    }

    protected function getAccountcode() {
        return false;
    }

    protected function getUserfield() {
        return false;
    }

    protected function getIsparsed() {
        return 0;
    }

    protected function getUniqueid() {
        return microtime(true) + rand(1, 5);
    }

}
