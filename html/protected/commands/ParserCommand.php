<?php

declare(ticks = 1);

/**
 * An asteriskCel table parser.
 * Class processes new calls coming.
 * 
 * @author Yurii Kuzich <qzichs@gmail.com>
 */
class ParserCommand extends CConsoleCommand {

    /**
     * If maximum RAM usage exceed MAX_RAM_USAGE const than will restart.
     */
    const MAX_RAM_USAGE = 20; //mb

    public $defaultAction = 'start';

    /**
     * 
     * @var integer
     */
    public $pid;

    /**
     * Path to lock file.
     *
     * @var string
     */
    protected $lockFile;

    public function init() {
        Yii::getLogger()->autoDump = true;
        Yii :: getLogger()->autoFlush = 1;
        fclose(STDIN);
        fclose(STDERR);
        $this->lockFile = Yii::getPathOfAlias('locks') . DIRECTORY_SEPARATOR . 'parser.lock';
    }

    /**
     * Start an daemon
     */
    public function actionStart() {

        if (file_exists($this->lockFile)) {
            $procId = file_get_contents($this->lockFile);
            if ($procId != false && $this->isProcessExists($procId)) {
                echo 'Process already running or was forced stoped ' . "\n";
                die();
            }
        }
        pcntl_signal(SIGTERM, array($this, 'actionStop'));

        $pid = pcntl_fork();
        if ($pid == -1) {
            die('could not fork');
        } else if ($pid) {
            // pcntl_wait($status);
            exit();
            //echo pcntl_wait($status); //Protect against Zombie children
        } else {
            $this->pid = getmypid(); //set up pid
            file_put_contents($this->lockFile, $this->pid);

            $dataProvider = new parser\models\DataProvider();
            $parser = new parser\CompletedCallObserver($dataProvider);
            //mb astract factory use
            $parser->addListener(
                    new parser\listeners\TNSNotifier(), parser\models\Request::getInstance('models\events\CompletedCalls\TNSNotify'));
            $parser->addListener(
                    new parser\listeners\AutoCleaner(), parser\models\Request::getInstance('models\events\CompletedCalls\AutoClean'));
            //work loop

            while (true) {
                $parser->process();
                while ($parser->getWorkersCount() != 0) {
                    sleep(1);
                    continue;
                }
                sleep(1);
                if ($this->isNeedRestart()) {
                    file_put_contents($this->lockFile, false);
                    exec("php -f " . Yii::app()->basePath . "/yiic parser start > /dev/null &");
                    die();
                }
            }
            //security reason
            $this->actionStop();
        }
    }

    /**
     * Stop daemon
     */
    public function actionStop() {
        if (file_exists($this->lockFile)) {
            $this->pid = file_get_contents($this->lockFile);
            if ($this->pid) {
                file_put_contents($this->lockFile, false); //unlink($this->lockFile);
                echo "Process successfull stoped" . "\n";
                if (posix_kill($this->pid, 9))
                    ;
            } else {
                echo "Process is not running!" . "\n";
            }
        } else {
            echo "Wrong lock file!" . "\n";
        }
    }

    /**
     * Restart daemon
     */
    public function actionRestart() {
        $this->actionStop();
        $this->actionStart();
    }

    protected function isNeedRestart() {
        return self::MAX_RAM_USAGE < memory_get_usage(true) / 1024 / 1024;
    }

    /**
     * 
     * @param int $id
     * @return bool
     */
    protected function isProcessExists($id) {
        $output = array();
        exec('ps -p ' . $id, $output);
        unset($output[0]);
        return count($output) != false;
    }

}

function exception_error_handler($errno, $errstr, $errfile, $errline) {
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}
