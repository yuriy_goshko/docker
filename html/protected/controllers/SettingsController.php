<?php
class SettingsController extends Controller {
	public $layout = '//layouts/front/compacted';
	protected $models = array('admin' => array('Users','uIdentity','uPassword'));
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@')),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		$this->setPageTitle('Настройка аккаунта');
		$model = Users::model()->findByAttributes(array('uIdentity' => Yii::app()->user->name));
		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];
			if ($model->save()) {
				Yii::app()->user->setFlash('success', "Информация успешно обновлена!");
			}
		}
		$this->render('index', array('model' => $model));
	}
	public function actionSecurity() {
		$this->setPageTitle('Настройка безопасности');
		
		$agNum = yii::app()->user->getState(WebUser::$KEY_AGENT, 0);
		if ($agNum > 0) {
			$a = 'uPassword';
			$model = Agents::model()->findByAttributes(array('name' => Yii::app()->user->name));
			
			if (isset($_POST[get_class($model)])) {
				$model->attributes = $_POST[get_class($model)];
				if ($model->save()) {
					Yii::app()->user->setFlash('success', "Пароль успешно обновлен!");
				}
			}
			unset($model->$a);
			
			$this->render('security', array('page' => '_forms/security/agent/_form','model' => $model));
		} else {
			$a = 'uPassword';
			$model = Users::model()->findByAttributes(array('uIdentity' => Yii::app()->user->name));
			if (isset($_POST[get_class($model)])) {
				$model->attributes = $_POST[get_class($model)];
				if ($model->save()) {
					Yii::app()->user->setFlash('success', "Пароль успешно обновлен!");
				}
			}
			unset($model->$a);
			$this->render('security', array('page' => '_forms/security/admin/_form','model' => $model));
		}
	}
	protected function checkModel($model) {
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
	}
}