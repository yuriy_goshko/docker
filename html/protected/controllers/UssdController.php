<?php
use PAMI\Listener\IEventListener;
use PAMI\Message\Event;
use PAMI\Message\Action;
class UssdController extends Controller implements IEventListener {
	public $layout = '//layouts/front/fixed';
	protected $_amiClient;
	protected $_result;
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rViewReport")'),array('deny','users' => array('*')));
	}
	public function handle(Event\EventMessage $event) {
		if ($event instanceof Event\DongleNewUSSDEvent) {
			$result = '';
			foreach ( $event->getKeys() as $key => $val ) {
				if (preg_match('/MessageLine/', $key)) {
					$result .= $val . "<br>";
				}
			}
			$this->_amiClient->breakLoop();
			$this->_result = $result;
		}
	}
	public function actionIndex($id = false) {
		$title = 'Отправка USSD';
		$this->setPageTitle($title);
		$this->_amiClient = AManager::getInstance()->addHandler($this);
		$result = '';
		$message = '';
		$modems = $id != FALSE ? Modems::model()->findByAttributes(array('Name' => $id)) : Modems::model();
		$modems->scenario = 'sendUssd';
		if (Yii::app()->request->isPostRequest) {
			if (isset($_POST[get_class($modems)])) {
				$result = $_POST[get_class($modems)];
				$response = $this->_amiClient->send(new Action\DongleSendUSSDAction($result['Name'], $result['ussd_attrib']));
				$this->_amiClient->catchEvent();
				if (strlen($this->_result) != false)
					Yii::app()->user->setFlash('success', "Запрос успешно выполнен");
				else
					Yii::app()->user->setFlash('error', "Некорректный запрос или устройство недоступно. <a href='/modemstat'>Проверте состояние устройств</a>");
				$message = $this->renderPartial('messages/_msg', null, true);
				if (Yii::app()->request->isAjaxRequest) {
					$json = CJSON::encode(array('UMessage' => $message,'UResult' => $this->_result));
					echo $json;
					Yii::app()->end();
				}
			}
		}
		$this->render('index', array('model' => $modems,'result' => $this->_result,'message' => $message,'h1' => $title));
	}
}