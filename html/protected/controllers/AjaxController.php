<?php

/**
 * Ajax controller.
 *
 * @author Kuzich Yurii <qzichs@gmail.com>
 * @since 1.0.0
 */
class AjaxController extends CController {
	public function filters() {
		return array('ajaxOnly','accessControl');
	}
	public function accessRules() {
		return array(
				array('allow','users' => array('@')),
				array('allow','actions' => array('agent'),'users' => array('admin')),
				array('deny','users' => array('*')));
	}
	function actionAgent() {
		if (Yii::app()->getRequest()->getIsAjaxRequest()) {
			var_dump($_POST);
			// echo CActiveForm::validate(array($model));
			Yii::app()->end();
		}
	}
	function actionPb() {
	}
}