<?php

/**
 * 
 * @author Kuzich Yurii <qzichs@gmail.com>
 * @since 1.0.0
 */
class PhonebookController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations*/
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'create', 'delete', 'tst'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id = false) {
        try {
            $model = $this->loadModel($id);
        } catch (CHttpException $e) {
            $model = new PhoneBook();
        }
        
        if (isset($_POST['PhoneBookForm'])) {
            $model->attributes = $_POST['PhoneBookForm'];            
            if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                echo CActiveForm::validate(array($model));
                Yii::app()->end();
            }
            if ($model->validate() != false) {
                $model->save();
                if ($id == false) {
                    Yii::app()->user->setFlash('info', "Успешно добавлен!");
                } else {
                    Yii::app()->user->setFlash('info', "Успешно обновлен!");
                }
                if (Yii::app()->request->isAjaxRequest) {
                    echo true;
                    Yii::app()->end();
                }
            } else {
                Yii::app()->user->setFlash('error', "Ошибка сохранения!");
                if (Yii::app()->request->isAjaxRequest) {
                    echo false;
                    Yii::app()->end();
                }
            }
        }
        $this->redirect(array('index'));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id = false) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex() {
        $sp = StaticPages::model()->findByAttributes(array('spExtUrl' => '/phonebook'));
        $this->setPageTitle($sp["spTitle"]);

        $model = PhoneBook::model()->mine();

        if (isset($_GET['PhoneBook'])) {
            $model->attributes = $_GET['PhoneBook'];
        }
        $dataProvider = new CActiveDataProvider('PhoneBook', array('criteria' => $model->search()));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => $model,
            'h1' => $sp["spTitle"]
        ));
    }
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PhoneBook the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = PhoneBook::model()->mine()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param PhoneBook $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'phone-book-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
