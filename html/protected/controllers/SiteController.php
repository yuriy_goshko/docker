<?php

/**
 * Misc actions
 *
 * @author Kuzich Yurii <qzichs@gmail.com>
 * @since 1.0.0
 */
class SiteController extends Controller {
    
    
    public $layout = '//layouts/front/fixed';
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
       
        $this->redirect('callslog');
        // renders the view file 'protected/views/site/index.php'        
    }
    
    /**
     * 
     * @param string $p0
     * @param string $p1
     * @param string $p2
     * @throws CHttpException
     */
    public function actionView($p0 = false, $p1 = false, $p2 = false) 
    {
        $_GET = array_reverse($_GET);
        $id = key($_GET);
        $criteria = new CDbCriteria;
        $criteria->select = $id . '.*';
        $bfield = $criteria->alias = $id;
        foreach ($_GET as $field => $value) {
            $criteria->addCondition($field . '.`spUrl` = :' . $field);
            if ($criteria->alias != $field) {
                $criteria->join = $criteria->join . ' JOIN StaticPages as ' . $field . ' ON ' . $bfield . '.spParentID = ' . $field . '.`spID`';
            }
            $bfield = $field;
        }
        $criteria->params = $_GET;

        $row = StaticPages::model()->find($criteria);
        if($row == null) throw new CHttpException(404, 'The requested page does not exist.');
        $this->render('view', array('model' => $row));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $this->layout = '//layouts/front/compacted';
        $c = explode('/', Yii::app()->request->requestUri);
        $template = '_'.implode('', $c);
        $loginclass = implode('', array_map('ucfirst', $c)) . 'Identity';
        $modelclass = implode('', array_map('ucfirst', $c)) . 'Form';
        $model = new $modelclass;
        $model->loginclass = $loginclass;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST[$modelclass])) {
            $model->attributes = $_POST[$modelclass];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {

                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
        // display the login form
        $this->render($template, array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}