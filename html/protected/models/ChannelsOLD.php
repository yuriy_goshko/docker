<?php

/**
 * This is the model class for table "Channels".
 *
 * The followings are the available columns in table 'Channels':
 * @property string $cID
 * @property string $cName
 *
 * The followings are the available model relations:
 * @property CallInfo[] $callInfos
 * @property CallInfo[] $callInfos1
 * @property Modems[] $modems
 */
class Channels extends CActiveRecord
{

    const OUT_CHAN = '1';
    const IN_CHAN = '0';
    const SPLITTER_CHAN = '/';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Channels the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getTechnologyList()
    {
        return array(Modems::TECH_ID => Modems::CHAN_PREF,
            OuterSIP::TECH_ID => OuterSIP::CHAN_REF,
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Channels';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cName, cType', 'required'),
            array('cName', 'length', 'max' => 255),
            array('cName', 'unique'),
            array('cDescription', 'length', 'max' => 100),
            array('cDescription, cName, cType', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('cID, cName, cGroups_gID', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            //'callInfos' => array(self::HAS_MANY, 'CallInfo', 'cDstchannelChannels_cID'),
            //'callInfos1' => array(self::HAS_MANY, 'CallInfo', 'cChannelChannels_cID'),
            'sip' => array(self::HAS_MANY, 'SIP', 'sChannels_cID'), //?
            //'dongles' => array(self::HAS_MANY, 'Dongles', 'dChannels_cID'),
            //'group'=>array(self::BELONGS_TO, 'Groups', array('cGroups_gID'=>'gID')),
            'co' => array(self::HAS_MANY, 'ChannelOwners', array('coChannels_cID' => 'cID')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'cDescription' => 'Описание',
            'cName' => 'Название канала'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('cID', $this->cID, true);
        $criteria->compare('cName', $this->cName, true);
        $criteria->compare('cGroups_gID', $this->cGroups_gID);
        $criteria->compare('cID', $this->cID);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Retrieve pairs channel_id => agent_name as array
     * 
     * @return array
     */
    public function gPopIN()
    {

        $agents = $this->with('co')->findAll(new CDbCriteria(
                array('condition' => 't.cType = :t AND co.coGroups_gID = :g',
            'params' => array(
                't' => Channels::IN_CHAN,
                'g' => Yii::app()->user->getState('Groups_gID')
            ),
                )
        ));
        $tmp = array();
        if (count($agents) != false)
        {
            foreach ($agents as $k => $v) {
                if (count($v->co))
                {
                    $tmp[$v->cID] = $v->co[0]->coName != false ? $v->co[0]->coName : $v->cName;
                }
            }
        }
        return $tmp;
    }

    /**
     * Retrieve pairs channel_id => outer_channel_name as array
     * 
     * @return array
     */
    public function gPopOUT($id = false)
    {
        if ($id == false)
        {
            $criteria = new CDbCriteria(
                    array('condition' => 'cType = :t AND co.coGroups_gID = :g',
                'params' => array(
                    't' => Channels::OUT_CHAN,
                    'g' => Yii::app()->user->getState('Groups_gID')
                ),
                    )
            );
        } else {
            $criteria = new CDbCriteria(
                    array('condition' => 'cID = :id AND cType = :t AND co.coGroups_gID = :g',
                'params' => array(
                    't' => Channels::OUT_CHAN,
                    'g' => Yii::app()->user->getState('Groups_gID'),
                    'id'=>$id
                ),
                    )
            );
        }
        $outer = $this->with('co')->findAll($criteria);
        $tmp = array();
        if (count($outer) != false)
        {
            foreach ($outer as $k => $v) {
                if (count($v->co))
                {
                    $tmp[$v->cID] = $v->co[0]->coName != false ? $v->co[0]->coName : $v->cName;
                }
            }
        }
        return $tmp;
    }

}