<?php

/**
 * This is the model class for table "SIP" with channel relation cType 1.
 *
 */
class OuterSIP extends SIP {

    const CHAN_REF = 'SIP';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Agents the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {

        return array_merge(parent::rules(), array(
            array('name', 'length', 'min' => 4),
            array('name', 'match', 'pattern' => '/[a-zA-Z]*[0-9]*/'),
                )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'sChannels_cID' => 'A Channels C',
            'name' => 'Имя канала',
        );
    }

    public function defaultScope() {
        return array('condition' => 'channels.cType = :c',
            'params' => array('c' => Channels::OUT_CHAN),
            'with' => 'channels',
        );
    }

    public function scopes() {
        return array(
            'withMyChannels' => array('with' => 'channelowners',),
        );
    }
    protected function beforeSave() {
        $this->channels->attributes = array(
            'cType' => Channels::OUT_CHAN,
            'cName' => 'SIP/' . $this->name,
        );

        if (!$this->channels->save()) {
            $this->addErrors($this->channels->getErrors());
            return false;
        } else {
            $this->channels->refresh();
        }

        $this->sChannels_cID = $this->channels->cID;
        return true;
    }

    protected function beforeDelete() {
        $this->channels->delete();
    }

}