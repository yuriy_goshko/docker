<?php

/**
 * This is the model class for table "QueueLog".
 *
 * The followings are the available columns in table 'QueueLog':
 * @property integer $qlID
 * @property string $qlQueuename
 * @property string $qlCalldate
 * @property string $qlEvent
 * @property integer $qlCallInfo_cUniqueid
 * @property integer $qlAgent
 * @property integer $qlCalltime
 * @property integer $qlWaittime
 */
class QueueLog extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Queue the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'QueueLog';
    }

    public function attributeLabels() {
        return array(
            'qlQueuename' => mb_ucfirst(Yii::t('queuelog', 'queue name')),
            'qlCalldate' => mb_ucfirst(Yii::t('callinfo', 'date')),
            'qlEvent' => mb_ucfirst(Yii::t('queuelog', 'event')),
            'qlAgent' => mb_ucfirst(Yii::t('callinfo', 'agent')),
            'qlCalltime' => mb_ucfirst(Yii::t('callinfo', 'duration')),
            'qlWaittime' => mb_ucfirst(Yii::t('callinfo', 'billsec')),
        );
    }

    public static function getMaxDateWithOffset($exp, $type) {
        return Yii::app()->db->createCommand()
                        ->select('DATE_FORMAT(DATE_SUB(MAX(`qlCalldate`), INTERVAL ' . $exp . ' ' . $type . '), GET_FORMAT(DATETIME,"EUR")) ')
                        ->from('QueueLog')
                        ->queryScalar();
    }

}