<?php
class Queue extends CActiveRecord {
	const MOH_DIR = 'moh';
	const FULL_MOH_DIR = '/var/lib/asterisk/moh';
	const CLASS_DIR = 'queues';
	protected $ext = 'wav';
	// привязан к отделу
	public $wDep = null;
	// привязан к агенту
	public $wAg = null;
	// для привязки по умолчанию(при добалении)
	public $_DefDepartment = null;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'Queue';
	}
	public function rules() {
		return array(
				array('qName','required'),
				array('name','unique'),
				array('name','required'),
				array('wDep','safe'),
				array('wAg','safe'),
				array('membermacro','safe'),
				array('membermacro', 'default', 'value'=>'callid'),				
				array('_DefDepartment','safe'),
				array('ringinuse, timeoutrestart, reportholdtime','length','max' => 1),
				array('eventmemberstatus, eventwhencalled','length','max' => 4),
				array(
						'timeout, announce_frequency, announce_round_seconds, 
                retry, wrapuptime, maxlen, servicelevel, memberdelay, weight, 
                periodic_announce_frequency, ',
						'length',
						'max' => 11),
				array('monitor_type, periodic_announce','length','max' => 50),
				array(
						'name, musiconhold, monitor_format, queue_youarenext, queue_thereare, 
                queue_callswaiting, queue_holdtime, queue_minutes, queue_seconds, 
                queue_lessthan, queue_thankyou, queue_reporthold, announce_holdtime, 
                strategy, joinempty, leavewhenempty, qName, qFileName, announce, 
                context',
						'length',
						'max' => 128),
				array(
						'timeout, announce_frequency, announce_round_seconds, 
                retry, wrapuptime, maxlen, servicelevel, memberdelay,  weight, 
                periodic_announce_frequency, ringinuse, timeoutrestart, reportholdtime',
						'numerical',
						'integerOnly' => true),
				array('name','match','pattern' => '/^[a-zA-Z0-9_]+$/'),
				array('musiconhold','file','maxFiles' => 1,'maxSize' => '20000000','types' => array('mp3','wav','ogg'),'allowEmpty' => true),
				array('musiconhold, qFileName','unsafe'),
				array('name, musiconhold, qName','safe','on' => 'search'));
	}
	public function relations() {
		return array('queueagents' => array(self::MANY_MANY,'Channels','QueueMembers(queue_name, interface)'));
	}
	public function attributeLabels() {
		return array(
				'name' => 'Ключевое имя',
				'membermacro' => 'Макрос',				
				'musiconhold' => 'Музыка ожидания ( < 20 mb)',
				'_DefDepartment' => Yii::t('m', '_DefDepartment'),
				'qName' => Yii::t('m', 'Queue name'));
	}
	public function scopes() {
		return array('mine' => array());
	}
	public function defaultScope() {
		$DepIds = yii::app()->user->getState('depIds');
		if ($DepIds != '*')
			$depCond = '(exists(select 1 from DepartmentQueues dq where `name`=dq.dqQueues_qName and dq.dqDepartments_dID in (' . $DepIds . ')))';
		else
			$depCond = '(1=1) ';
		
		$wDep = '(select 1 from `DepartmentQueues` dq where dq.`dqQueues_qName`=`name` limit 1) as wDep';
		$wAg = '(select 1 from `QueueMembers` qm where qm.queue_name=t.`name` limit 1) as wAg';
		return array('select' => '*, ' . $wDep . ', ' . $wAg,'condition' => $depCond);
	}
	public function search() {
		$criteria = new CDbCriteria();
		
		$criteria->compare('name', $this->name, true);
		$criteria->compare('musiconhold', $this->musiconhold, true);
		$criteria->compare('qName', $this->qName, true);
		
		return new CActiveDataProvider($this, array('criteria' => $criteria));
	}
	protected function beforeSave() {
		parent::beforeSave();		
		if (! $this->isNewRecord && $this->qFileName != false) {
			$newpath = self::FULL_MOH_DIR . '/' . self::CLASS_DIR . '/' . $this->name;
			$musiconhold = MusicOnHold::model()->findByPk($this->oldPrimaryKey);
			rename($musiconhold->directory, $newpath);
			$this->musiconhold = $this->name;
			$musiconhold->name = $this->musiconhold;
			$musiconhold->directory = $newpath;
			$musiconhold->save();
		} else {
			if ($this->musiconhold instanceof CUploadedFile) {
				if (! is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . self::MOH_DIR . '/' . self::CLASS_DIR . '/' . $this->name))
					mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . self::MOH_DIR . '/' . self::CLASS_DIR . '/' . $this->name);
					// $this->qFileName = md5($this->name . $this->musiconhold) . $this->ext;
				$this->musiconhold = $this->name;
				if (! MusicOnHold::model()->findByPk($this->name)) {
					$musiconhold = new MusicOnHold();
					$musiconhold->name = $this->musiconhold;
					$musiconhold->directory = self::FULL_MOH_DIR . '/' . self::CLASS_DIR . '/' . $this->musiconhold;
					$musiconhold->save();
				}
			}
		}
		return $this->validate();
	}
	protected function afterSave() {
		parent::afterSave();
		if ($this->isNewRecord) {
			if ((yii::app()->user->getState("isRoot_")) && ($this->_DefDepartment != null)) {
				$sql = 'insert into `DepartmentQueues`(`dqQueues_qName`,`dqDepartments_dID`) values ("' . $this->name . '", ' . $this->_DefDepartment . ');';
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
			}
		}
	}
	protected function beforeDelete() {
		// QueueMembers::model()->deleteAllByAttributes(array('queue_name'=>$this->name));
		if (is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . self::MOH_DIR . '/' . self::CLASS_DIR . '/' . $this->name)) {
			MusicOnHold::model()->deleteByPk($this->name);
			delete($_SERVER['DOCUMENT_ROOT'] . '/' . self::MOH_DIR . '/' . self::CLASS_DIR . '/' . $this->name);
			rmdir($_SERVER['DOCUMENT_ROOT'] . '/' . self::MOH_DIR . '/' . self::CLASS_DIR . '/' . $this->name);
		}
		
		return true;
	}
	protected function getExt() {
		return $this->ext;
	}
}