<?php
class Modems extends CActiveRecord {
	//const CHAN_PREF = 'Dongle';
	public $ussd_attrib;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'PBXNumbers';
	}
	public function rules() {
		return array(array('Name','required'),array('ussd_attrib','required','on' => 'sendUssd'));
	}
	public function defaultScope() {
		return array('condition' => '(Type = "DONGLE")');
	}
	public function attributeLabels() {
		return array('mName' => 'Имя модема','mNumber' => 'Номер карточки','mProviders_pID' => 'Провайдер','ussd_attrib' => 'USSD запрос');
	}
}
?>