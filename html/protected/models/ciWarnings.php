<?php
Yii::import('application.modules.callslog.models.CallsLogSearchForm');
class ciWarnings extends CActiveRecord {
	public $MarkWarnings;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'CI_Warnings';
	}
	public function rules() {
		return array(
				array('Id','required'),
				array('Id','unique'),
				array('UniqueId, LinkedId','length','max' => 32),
				array('Type','length','max' => 100),
				array('Info','length','max' => 1000),
				array('Id, UniqueId, LinkedId, Type, Info, InsertDateTime, CallDateTime','safe','on' => 'search'));
	}
	public function attributeLabels() {
		return array();
	}
	public function RecalcWarningsByLinkedId($ALinkedId, $AForRecalc) {
		$ySql = Yii::app()->db->createCommand('call CI_RemoveLog(:L,:RF,"",@res)');
		$ySql->bindParam(":L", $ALinkedId, PDO::PARAM_STR);
		$ySql->bindParam(":RF", $AForRecalc, PDO::PARAM_INT);
		$ySql->execute();
	}
	public function MarkedWarnings($val) {
		$sql = 'INSERT INTO Settings (param, value) VALUES("MarkWarnings", :val)
				ON DUPLICATE KEY UPDATE param="MarkWarnings", value=:val;';
		$ySql = Yii::app()->db->createCommand($sql);
		$ySql->bindParam(":val", $val, PDO::PARAM_STR);
		$ySql->execute();
	}
	public function DeleteInfoWarnings($val) {
		$sql = 'delete from `CI_Warnings` where `Type` = "Info" and `LinkedId` = :val';
		$ySql = Yii::app()->db->createCommand($sql);
		$ySql->bindParam(":val", $val, PDO::PARAM_INT);
		$ySql->execute();
	}
	public static function GenerateLinkContent($Column, $data) {
		$text = $data[$Column];
		//$text = 'fsdf';
	
		$SearchTmp = new CallsLogSearchForm();
		$SearchTmp->sfLinkedId = array($text);	
		$content = CHtml::Tag('a', array(
				'href' => '/callslog?' . http_build_query(array(get_class($SearchTmp) => $SearchTmp)),
				'target' => '_blank',
				'style' => 'float:right'), CHtml::Tag('img', array('src' => '/images/16/tablefind.png')));
		return $text . ' ' . $content;
	}
	public function search() {
		$r = Yii::app()->db->createCommand('select value from Settings where param = "MarkWarnings"')->queryRow();
		$this->MarkWarnings = $r['value'];
		
		$criteria = new CDbCriteria();
		if ($this->Id != false)
			$criteria->addCondition('Id like "' . $this->Id . '"');
		if ($this->UniqueId != false)
			$criteria->addCondition('UniqueId like "' . $this->UniqueId . '"');
		if ($this->LinkedId != false)
			$criteria->addCondition('LinkedId like "' . $this->LinkedId . '"');
		
		if ($this->Type != "-1")
			$criteria->compare('Type', $this->Type, true);
		
		if ($this->Info != false)
			$criteria->addCondition('Info like "' . $this->Info . '"');
		$criteria->compare('InsertDateTime', $this->InsertDateTime, true);
		$criteria->compare('CallDateTime', $this->CallDateTime, true);
		
		return $criteria;
	}
}

?>