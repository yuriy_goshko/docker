<?php

class ciRulesForWritingNumbers extends CActiveRecord {
	
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Modems the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'CI_RulesForWritingNumbers';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
        	array('CharsLength, ActionType, OnlyRealNumbers, OrderKey', 'required'),        		
        	array('Id, CharsLength, OrderKey', 'numerical', 'integerOnly' => true),
        	array('ActionType, OldValue, NewValue', 'length', 'max' => 20),
        	array('OnlyRealNumbers', 'match', 'pattern' => '/[0,1]/'),
        	array('OnlyRealNumbers', 'safe'),
        	array('Id', 'unique'),
        	//array('*', 'safe', 'on'=>'search'),
        	// OnlyRealNumbers ///???
        );
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(            
        	'OrderKey' => 'Проядок выполнения',        		
            'CharsLength' => 'Кол-во символов (начальное)',
        	'OnlyRealNumbers' => 'Реальный номер (только из цифр)',        		
            'ActionType' => 'Действие',
            'OldValue' => 'Старое значение',
        	'NewValue' => 'Новое значение',        		
        );
    }
    
    public static function getActionTypesArray(){  	   	
    	return array(
    			'BeginningAdd' => 'Добавить в начало',
    			'Change' => 'Заменить',
    			'ReplaceFirst' => 'Поменять начало',
    	);    	
    }
    
    public static function getActionTypeNameByKey($name){
    	$array = self::getActionTypesArray();
    	while ($CurrentKey = key($array)) {
    		if ($CurrentKey == $name) {
    			return Current($array);
    		}
    		next($array);
    	}
    }
}

?>