<?php

/**
 * This is the model class for table "QueueMembers".
 *
 * The followings are the available columns in table 'QueueMembers':
 * @property string $queue_name
 * @property string $interface
 * @property integer $penalty
 */
class QueueMembers extends CActiveRecord {

    static $arrayOfAr;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return QueueMembers the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'QueueMembers';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('queue_name, interface', 'required'),
            array('penalty', 'numerical', 'integerOnly' => true),
            array('queue_name, interface', 'length', 'max' => 128),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('queue_name, interface, penalty', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'queue_name' => 'Queue Name',
            'interface' => 'Interface',
            'penalty' => 'Penalty',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('queue_name', $this->queue_name, true);
        $criteria->compare('interface', $this->interface, true);
        $criteria->compare('penalty', $this->penalty);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }


}