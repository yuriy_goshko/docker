<?php
class SIP extends CActiveRecord {
	// для связки депагентс
	public $checked = false;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'SIP';
	}
	public function rules() {
		return array(
				array('regseconds, ipaddr, port','unsafe'),
				array('name','required','except' => 'ajaxValidate'),
				array('name','unique'),
				array('checked','safe'),
				array(
						'lastms, busylevel, maxcallbitrate, session-expires, session-minse, 
                rtptimeout, rtpholdtimeout, timert1, timerb, qualifyfreq, rtpkeepalive, 
                call-limit',
						'numerical'),
				array(
						'lastms, busylevel, maxcallbitrate, session-expires, 
                session-minse, rtptimeout, rtpholdtimeout, timert1, timerb, qualifyfreq,
                rtpkeepalive, call-limit',
						'length',
						'max' => 11),
				array('name, defaultuser, regserver, useragent','length','max' => 20),
				array('fullcontact','length','max' => 35),
				array(
						'host, context, permit, deny, secret, md5secret, remotesecret,
                callgroup, pickupgroup, language, allow, disallow, insecure, 
                accountcode, setvar, callerid, amaflags, mailbox, t38pt_usertpsource, 
                regexten, fromdomain, fromuser, qualify, defaultip, outboundproxy, 
                callbackextension, contactpermit, contactdeny, auth, fullname, 
                trunkname, cid_number, mohinterpret, mohsuggest, parkinglot, vmexten',
						'length',
						'max' => 40),
				array('type','in','range' => array('friend','user','peer')),
				array('transport','in','range' => array('udp','tcp','udp,tcp','tcp,udp')),
				array('dtmfmode','in','range' => array('rfc2833','info','shortinfo','inband','auto')),
				array('directmedia','in','range' => array('yes','no','nonat','update')),
				array('nat','required'), //'yes'(depr),'no','never','route','force_rport','comedia'
				array(
						'trustrpid, promiscredir, useclientcode, callcounter, allowoverlap, 
                allowsubscribe, videosupport, rfc2833compensate, sendrpid, constantssrc,
                usereqphone, textsupport, faxdetect, buggymwi, hasvoicemail, subscribemwi, 
                autoframing, g726nonstandard, ignoresdpversion, allowtransfer, 
                dynamic',
						'in',
						'range' => array('yes','no')),
				array('progressinband','in','range' => array('yes','no','never')),
				array('session-timers','in','range' => array('accept','refuse','originate')),
				array('session-refresher','in','range' => array('uac','uas')),
				array(
						'callingpres',
						'in',
						'range' => array(
								'allowed_not_screened',
								'allowed_passed_screen',
								'allowed_failed_screen',
								'allowed',
								'prohib_not_screened',
								'prohib_passed_screen',
								'prohib_failed_screen',
								'prohib')),
				array('secret','length','min' => 6),
				array('secret','match','pattern' => '/^[a-zA-Z0-9]+$/'));
	}
}