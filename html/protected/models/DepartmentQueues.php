<?php
class DepartmentQueues extends CActiveRecord {
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'DepartmentQueues';
	}
	public function rules() {
		return array(
				array('dqDepartments_dID, dqQueues_qName','required'),
				array('dqDepartments_dID','numerical','integerOnly' => true),
				array('dqQueues_qName','length','max' => 128),
				array('dqDepartments_dID, dqQueues_qName','safe','on' => 'search'));
	}
	public function relations() {
		return array();
	}
	public function attributeLabels() {
		return array('dqDepartments_dID' => 'Dq Departments D','dqQueues_qName' => 'Dq Queues Q Name');
	}
	public function search() {
		$criteria = new CDbCriteria();
		$criteria->compare('dqDepartments_dID', $this->dqDepartments_dID);
		$criteria->compare('dqQueues_qName', $this->dqQueues_qName, true);
		return new CActiveDataProvider($this, array('criteria' => $criteria));
	}
	public static function getQueuesByDep($dep) {
		$depItems = Yii::app()->db->createCommand()->select('dqQueues_qName')->from('DepartmentQueues')->where('dqDepartments_dID=:id', array(
				':id' => $dep))->queryAll();
		
		$res = array();
		foreach ( $depItems as $chan )
			$res[] = $chan['dqQueues_qName'];
		return $res;
	}
	public static function getPopDepartments() {
		return Yii::app()->db->createCommand()->select('dep.dID, dep.dName')->from('Departments as dep')->leftJoin('DepartmentQueues as dq', 'dq.dqDepartments_dID = dep.dID')->andWhere('dep.dGroups_gID=:gid', array(
				':gid' => Yii::app()->user->getState('Groups_gID')))->andWhere('dq.dqDepartments_dID IS NOT NULL')->group('dep.dID')->queryAll();
	}
}