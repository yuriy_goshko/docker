<?php
class FiltersForm extends CFormModel {
	public $filters = array();
	public function __get($name) {
		if (! array_key_exists($name, $this->filters)) {
			$this->filters[$name] = '';
		}
		return $this->filters[$name];
	}
	public function __set($name, $value) {
		$this->filters[$name] = $value;
	}
	public function filter(array $data) {
		$newdata = array();
		$delRows = array();
		foreach ( $data as $rowIndex => $row ) {
			foreach ( $this->filters as $key => $searchValue ) {
				if (! is_null($searchValue) and $searchValue !== '') {
					$compareValue = null;
					if ($row instanceof CModel) {
						if (isset($row->$key) == false) {
							throw new CException("Property " . get_class($row) . "::{$key} does not exist!");
						}
						$compareValue = $row->$key;
					} elseif (is_array($row)) {
						if (! array_key_exists($key, $row)) {
							throw new CException("Key {$key} does not exist in array!");
						}
						$compareValue = $row[$key];
					} else {
						throw new CException("Data in CArrayDataProvider must be an array of arrays or an array of CModels!");
					}
					
					if (stripos($compareValue, $searchValue) === false) {
						// unset($data[$rowIndex]);
						$delRows[] = $rowIndex;
					}
				}
			}
		}
		// костыль, через unset ломается массив
		foreach ( $data as $rowIndex => $row ) {
			if (in_array($rowIndex, $delRows) === false)
				$newdata[] = $row;
		}
		return $newdata;
	}
}