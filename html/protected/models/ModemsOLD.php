<?php

class Modems extends CActiveRecord {

    const CHAN_PREF = 'Dongle';
    //const TECH_ID = 1;
    public $ussd_attrib;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Modems the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Modems';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('mName, mProviders_pID', 'required'),
            array('ussd_attrib', 'required', 'on'=>'sendUssd'),
            array('mName', 'unique'),
            array('mName', 'match', 'pattern'=>'/^[a-zA-Z0-9_]{3,}$/'),
            array('mNumber', 'numerical'),
            array('mNumber', 'length', 'max'=>13),
            array('mName', 'length', 'max' => 30),
            array('mProviders_pID, mChannels_cID', 'length', 'max' => 11),
            array('mChannels_cID', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'channels' => array(self::BELONGS_TO, 'Channels', array('mChannels_cID'=>'cID')),
            'providers' => array(self::BELONGS_TO, 'Providers', 'mProviders_pID'),
            'channelowners' => array(self::BELONGS_TO, 'ChannelOwners', array('mChannels_cID' => 'coChannels_cID')),
        );
    }

    public function scopes() {
        return array(
            'mine' => array(
                'condition' => 'channels.cType = :c ',
                'params' => array('c' => Channels::OUT_CHAN,
                ),
                'with' => array('channelowners', 'channels'),
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'mName' => 'Имя модема',
            'mNumber' => 'Номер карточки',
            'mProviders_pID'=>'Провайдер',
            'ussd_attrib'=>'USSD запрос'
        );
    }
    
    public function getAttachedGroups()
    {
        $data = ChannelOwners::model()->findAllByAttributes(array('coChannels_cID'=>$this->mChannels_cID));
        $res = array();
        foreach($data as $co) {
            $res[]=$co->coGroups_gID;
        }
        return implode(', ', $res);
    }
    
    
    public function getName_attrib()
    {
        if ($this->channelowners["coName"] !=false) {
               return $this->channelowners["coName"];
            } 
              return $this->mName ;
    }
    
    protected function beforeSave() {
        $this->channels->attributes = array(
            'cType' => Channels::OUT_CHAN,
            'cName' => self::CHAN_PREF . Channels::SPLITTER_CHAN . $this->mName,
        );

        if (!$this->channels->save()) {
            $this->addErrors($this->channels->getErrors());
            return false;
        } else {
            $this->channels->refresh();
        }

        $this->mChannels_cID = $this->channels->cID;
        return true;
    }
    
    protected function beforeDelete() {
        $this->channels->delete();
    }

}

?>