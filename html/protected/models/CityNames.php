<?php
class CityNames extends CActiveRecord {
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'CityNames';
	}
	public function rules() {
		return array(
				array('Name, DisplayName','required'),
				array('Name, DisplayName','unique'),
				array('Name, DisplayName','length','max' => 255),
				array('Name, DisplayName','safe'));
	}
	public function attributeLabels() {
		return array('Name' => 'Ключ. имя','DisplayName' => 'Название');
	}
}