<?php
class Agents extends SIP {
	private $isInsert = false;
	const DEF_CONTEXT = 'office_1';
	public $_status;
	// привязан к отделу
	public $wDep = null;
	// для привязки по умолчанию(при добалении)
	public $_DefDepartment = null;
	// для отображения названия Роли
	public $RoleName = null;
	protected $minname;
	protected $maxname;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(), array(
				array('secret','required','except' => 'ajaxValidate'),
				array('secret','length','min' => 6),
				array('secret','match','pattern' => '/^[a-zA-Z0-9]+$/'),
				array('name','numerical','max' => $this->getMaxName(),'min' => $this->getMinName()),
				array('wDep','safe'),
				array('_DefDepartment','safe'),
				array('userRole','safe'),		
				array('nat','safe'),
				array(
						'fullname',
						'compare',
						'allowEmpty' => false,
						'compareAttribute' => 'name',
						'operator' => '!=',
						'message' => 'Имя агента не может быть равным его номеру',
						'on' => 'insert, update')));
	}
	public function attributeLabels() {
		return array(
				'name' => 'Номер агента (' . $this->getMinName() . ' - ' . $this->getMaxName() . ') ',
				'fullname' => 'Имя',
				'secret' => 'Пароль',
				'ipaddr' => 'ip адрес',
				'userRole' => 'Роль',
				'nat' => 'Nat',
				'_DefDepartment' => Yii::t('m', '_DefDepartment'),
				'status' => 'Статус');
	}
	public function Scopes() {
		$arr = $this->defaultScope();
		$arr['condition'] = "2=1";		
		return array('login' => $arr);
	}
	public function defaultScope() {
		$DepIds = yii::app()->user->getState('depIds');
		if ($DepIds != '*')
			$depCond = '(exists(select 1 from DepartmentAgents where id=SIPId and DepartmentId in (' . $DepIds . '))) ';
		else
			$depCond = '(1=1) ';
		
		$wAddFields = '(select 1 from `DepartmentAgents` da where da.`SIPId`=t.`id` limit 1) wDep';
		$wAddFields .= ', (select u.`uName` from `Users` u where u.`IsRole` = 1 and u.`uIdentity` = t.`userRole` limit 1) RoleName';
		return array('select' => '*, ' . $wAddFields,'condition' => $depCond);
	}
	protected function beforeSave() {
		parent::beforeSave();
		if (trim($this->fullname) == false)
			$this->fullname = 'Агент ' . $this->name;
		$this->context = self::DEF_CONTEXT;
		$this->isInsert = ($this->id === null);
		if ($this->id === null) {
			$sql = 'select 1 from SIP where Name = :pName';
			$command = Yii::app()->db->createCommand($sql);
			$n = $this->name;
			$command->bindParam(":pName", $n, PDO::PARAM_STR);
			$r = $command->queryScalar();
			if ($r != null) {
				$this->addError('name', 'Номер агента "' . $n . '" уже занят.');
				return false;
			}
		}
		return true;
	}
	protected function afterSave() {
		parent::afterSave();
		if ($this->isInsert) {
			$DepIds = yii::app()->user->getState('depIds');
			if ($DepIds != '*') {
				$DepIdsArr = explode(',', $DepIds);
				foreach ( $DepIdsArr as $DepId ) {
					$command = Yii::app()->db->createCommand('insert into `DepartmentAgents`(`SIPId`,`DepartmentId`) values (' . $this->id . ', ' . $DepId . ');');
					$command->execute();
				}
			} else {
				if ($this->_DefDepartment != null) {
					$command = Yii::app()->db->createCommand('insert into `DepartmentAgents`(`SIPId`,`DepartmentId`) values (' . $this->id . ', ' . $this->_DefDepartment . ');');
					$command->execute();
				}
			}
		}
	}
	public function getMinName() {
		if ($this->minname != false) {
			return $this->minname;
		}
		return $this->minname = 200;
	}
	public function getMaxName() {
		if ($this->maxname != false) {
			return $this->maxname;
		}
		return $this->maxname = 999;
	}
	protected function getStatus() {
		$this->_status = 0;
		if ($this->port > 0) {
			$this->_status = 1;
		}
		return $this->_status;
	}
}