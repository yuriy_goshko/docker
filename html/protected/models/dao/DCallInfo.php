<?php

Yii::import('system.web.helpers.CHtml');

/**
 * Модель для работы с таблицей CallInfo.
 * 
 * 
 * @author qzich <qzichs@gmail.com>
 */
class DCallInfo extends DAOCharts {
    /*
     * Название таблицы с которой работаем
     */

    public $tableName = 'CallInfo';
    protected $noDigitData = array('name');

    public static function model($sql = '', $className = __CLASS__) {
        return parent::model($sql, $className);
    }

    /**
     * Метод добавляет условия к поиску по критериям указанных в свойствах
     * 
     * @return \DCallInfo
     */
    public function search() {
        $this->command->reset();
        if (isset($this->cfStartDate) && $this->cfStartDate != false)
            $this->command->andWhere('main.`cCalldate` >= :strdate', array('strdate' => $this->cfStartDate));
        if (isset($this->cfEndDate) && $this->cfEndDate != false)
            $this->command->andWhere('main.`cCalldate` <= :enddate', array('enddate' => $this->cfEndDate));

        if (isset($this->cfAgent) && $this->cfAgent != "-1")
            $this->command->andWhere('main.`cActiveAgent` IN (' . implode(', ',$this->cfAgent) . ')');

        if (isset($this->cfDirection) && $this->cfDirection != "-1")
            $this->command->andWhere('`cDirection` IN (' . implode(', ', Yii::app()->db->quoteValueSet($this->cfDirection)) . ')');

        if (isset($this->cfDisposition) && $this->cfDisposition != '-1')
            $this->command->andWhere('`cDisposition` IN (' . implode(', ', Yii::app()->db->quoteValueSet($this->cfDisposition)) . ')');

        if (isset($this->cfOutchan) && $this->cfOutchan != "-1")
            $this->command->andWhere('`cOutchanChannels_cID` IN (' . implode(', ', $this->cfOutchan) . ')');
        
        if (isset($this->cfDepartment) && $this->cfDepartment != "-1")
            $this->command->andWhere('name  IN (' . implode(', ', $this->cfDepartment) . ')');

        if (isset($this->cfDepartmentMb) && $this->cfDepartmentMb != "-1") {
            $values = array();
            $items = is_array($this->cfDepartmentMb) ? $this->cfDepartmentMb : array($this->cfDepartmentMb);
            foreach ($items as $depId) {
                if (count(DepartmentMembers::getAgentsChannels($depId)) != false) {
                    $values = array_unique(array_merge(DepartmentMembers::getAgentsChannels($depId), $values));
                }
            }
            if (count($values != false))
                $this->command->andWhere('cActiveAgent IN (' . implode(', ', $values) . ')');
        }
        return $this;
    }

    /**
     * 
     * @return \DCallInfo
     */
    public function fetchAllByDirection() {
        $this->command->select('
                                main.cDirection as name,
                                IFNULL(SUM(main.cDuration), 0) as duration,
                                IFNULL(SUM(main.cBillsec), 0) as billsec,

                                IFNULL(SUM( if(main.`cDisposition` = "uFAILED", 1, 0) ), 0) as ufailed,
                                IFNULL(SUM( if(main.`cDisposition` = "uHANGUP", 1, 0) ), 0) as uhangup,
                                IFNULL(SUM( if(main.`cDisposition` = "FAILED", 1, 0) ), 0) as failed,
                                IFNULL(SUM( if(main.`cDisposition` = "BUSY", 1, 0) ), 0) as busy, 
                                IFNULL(SUM( if(main.`cDisposition` = "ANSWERED", 1, 0) ), 0) as answered,
                                IFNULL(SUM( if(main.`cDisposition` = "NO ANSWER", 1, 0) ), 0) as noanswer,

                                IFNULL(SUM( if(main.`cDisposition` = "NO ANSWER", 1, 0) ) + 
                               SUM( if(main.`cDisposition` = "uFAILED", 1, 0) )+
                               SUM( if(main.`cDisposition` = "ANSWERED", 1, 0) ) +
                               SUM( if(main.`cDisposition` = "BUSY", 1, 0) )+
                               SUM( if(main.`cDisposition` = "FAILED", 1, 0) )+
                               SUM( if(main.`cDisposition` = "uHANGUP", 1, 0) )
                               , 0) as allcalls')
                ->from('CallInfo as main')
                ->andWhere('main.cGroups_gID = ' . Yii::app()->user->getState('Groups_gID'))
                ->group('main.cDirection');
        $this->result = $this->command->queryAll();
        $this->processDataByName($this->result);
        $this->processAllData();
        return $this;
    }

    /**
     * 
     * @return \DcallInfo
     */
    public function fetchAllByAgents() {
        $this->command->select('
                                main.cID,
                                main.cActiveAgent as name,
                                IFNULL(SUM(if(main.`cDirection` = "outbound", 1, 0)  ), 0) as call_0,
                                IFNULL(SUM(if(main.`cDirection` = "inbound", 1, 0)  ), 0) as call_1,
                                IFNULL(SUM(if(main.`cDirection` = "local", 1, 0)  ), 0) as call_2,
                                IFNULL(SUM(if(main.`cDirection` = "callback", 1, 0)  ), 0) as call_3,

                                IFNULL(SUM(if(main.`cDirection` = "outbound", 1, 0)  ) +
                                SUM(if(main.`cDirection` = "inbound", 1, 0)  )+
                                SUM(if(main.`cDirection` = "local", 1, 0)  )+
                                SUM(if(main.`cDirection` = "callback", 1, 0)  ), 0) as allcalls 
                                ')
                ->from($this->tableName . ' main')
                ->andWhere('main.cGroups_gID = ' . Yii::app()->user->getState('Groups_gID'))
                ->group('name');
        $this->result = $this->command->queryAll();
        $this->fiterBy('name', Channels::model()->gPopIN());
        $this->processDataByName($this->result);
        $this->processAllData();
        return $this;
    }

    /**
     * 
     * @return \DCallInfo
     */
    public function fetchAllByDepartments() {
        $from = Yii::app()->db->createCommand()
                ->select('main.*, dep.dID as name, dm.dpChannels_cID as agchanid')
                ->from($this->tableName . ' main')
                ->leftJoin('DepartmentMembers as dm', 'main.`cActiveAgent` = dm.`dpChannels_cID`')
                ->leftJoin('DepartmentQueues as dq', 'main.`cQueue` = dq.`dqQueues_qName`')
                ->leftJoin('Departments as dep', 'dm.`dpDepartments_dID` = dep.dID OR dq.dqDepartments_dID = dep.dID')
                ->andWhere('main.cGroups_gID = ' . Yii::app()->user->getState('Groups_gID'))
                ->group('main.cID, dep.dID');
        $this->command->select('
                                name,
                                IFNULL(SUM(if(main.`cDirection` = "outbound", 1, 0)  ), 0) as call_0,
                                IFNULL(SUM(if(main.`cDirection` = "inbound", 1, 0)  ), 0) as call_1,
                                IFNULL(SUM(if(main.`cDirection` = "local", 1, 0)  ), 0) as call_2,
                                IFNULL(SUM(if(main.`cDirection` = "callback", 1, 0)  ), 0) as call_3,

                                IFNULL(SUM(if(main.`cDirection` = "outbound", 1, 0)  ) +
                                SUM(if(main.`cDirection` = "inbound", 1, 0)  )+
                                SUM(if(main.`cDirection` = "local", 1, 0)  )+
                                SUM(if(main.`cDirection` = "callback", 1, 0)  ), 0) as allcalls  
                                ')
                ->from('(' . $from->getText() . ')' . 'as main')
                ->group('name');
        $this->result = $this->command->queryAll();
        $this->fiterBy('name', CHtml::listData(Departments::getPopDepartments(), 'dID', 'dName'));
        $this->processDataByName($this->result);
        $this->processAllData();
        return $this;
    }

    /**
     * @return \DCallInfo
     */
    public function fetchAllByOuterChannels() {

        $this->command->select('
                                `main`.`cOutchanChannels_cID` as name, 
                                IFNULL(SUM(if(main.`cDirection` = "outbound", 1, 0)  ), 0) as call_0,
                                IFNULL(SUM(if(main.`cDirection` = "inbound", 1, 0)  ), 0) as call_1,
                                IFNULL(SUM(if(main.`cDirection` = "callback", 1, 0)  ), 0) as call_3,

                                IFNULL(SUM(main.cDuration), 0) as duration,
                                 IFNULL(SUM(main.cBillsec), 0) as billsec, 
                                IFNULL(SUM( if(main.`cDisposition` = "uFAILED", 1, 0) ), 0) as ufailed, 
                                IFNULL(SUM( if(main.`cDisposition` = "uHANGUP", 1, 0) ), 0) as uhangup, 
                                IFNULL(SUM( if(main.`cDisposition` = "FAILED", 1, 0) ), 0) as failed, 
                                IFNULL(SUM( if(main.`cDisposition` = "BUSY", 1, 0) ), 0) as busy, 
                                IFNULL(SUM( if(main.`cDisposition` = "ANSWERED", 1, 0) ), 0) as answered, 
                                IFNULL(SUM( if(main.`cDisposition` = "NO ANSWER", 1, 0) ), 0) as noanswer, 

                                IFNULL(SUM( if(main.`cDisposition` = "NO ANSWER", 1, 0) ) +
                                SUM( if(main.`cDisposition` = "uFAILED", 1, 0) )+
                                SUM( if(main.`cDisposition` = "ANSWERED", 1, 0) )+
                                SUM( if(main.`cDisposition` = "BUSY", 1, 0) )+
                                SUM( if(main.`cDisposition` = "FAILED", 1, 0) )+
                                SUM( if(main.`cDisposition` = "uHANGUP", 1, 0) ) , 0) as allcalls 
                                ')
                ->from('CallInfo as main')
                ->andWhere('main.cGroups_gID = ' . Yii::app()->user->getState('Groups_gID'))
                ->andWhere('main.cOutchanChannels_cID IS NOT NULL')
                ->group('main.cOutchanChannels_cID');
        $this->result = $this->command->queryAll();
        $this->fiterBy('name', Channels::model()->gPopOUT());
        $this->processDataByName($this->result);
        $this->processAllData();
        return $this;
    }

    /**
     * 
     * @return array
     */
    public function getSeriesByNameDirection() {

        $arra = array();
        $dataAll = array();
        $dataGroup = array();
        if (count($this->namedData != false) && count($this->allData) != false) {
            foreach ($this->namedData as $name => $v) {
                $color = getRandomColor();
                if ($v['allcalls'] != 0)
                    $dataAll[] = array('name' => Yii::t('dir_data', $name),
                        'color' => $color,
                        'y' => getPercent($v['allcalls'], $this->allData['allcalls']));
                if ($v['ufailed'] != 0)
                    $dataGroup[] = array('name' => Yii::t('dir_data', $name) . '<br>' . mb_strtolower(Yii::t('dis_data', 'uFAILED'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['ufailed'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['answered'] != 0)
                    $dataGroup[] = array('name' => Yii::t('dir_data', $name) . '<br>' . mb_strtolower(Yii::t('dis_data', 'ANSWERED'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['answered'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['busy'] != 0)
                    $dataGroup[] = array('name' => Yii::t('dir_data', $name) . '<br>' . mb_strtolower(Yii::t('dis_data', 'BUSY'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['busy'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['failed'] != 0)
                    $dataGroup[] = array('name' => Yii::t('dir_data', $name) . '<br>' . mb_strtolower(Yii::t('dis_data', 'FAILED'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['failed'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['noanswer'] != 0)
                    $dataGroup[] = array('name' => Yii::t('dir_data', $name) . '<br>' . mb_strtolower(Yii::t('dis_data', 'NO ANSWER'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['noanswer'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['uhangup'] != 0)
                    $dataGroup[] = array('name' => Yii::t('dir_data', $name) . '<br>' . mb_strtolower(Yii::t('dis_data', 'uHANGUP'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['uhangup'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
            }
        }

        $arra[] = array('name' => Yii::t('m', 'All calls'), 'data' => $dataAll, 'size' => '80%', 'dataLabels' => array('distance' => 20, 'color' => 'black'));
        $arra[] = array('name' => ' ', 'data' => $dataGroup, 'innerSize' => '80%', 'dataLabels' => array(), 'allowPointSelect' => true, 'cursor' => 'pointer',);

        return $arra;
    }

    /**
     * 
     * @return array
     */
    public function getSeriesByNameChann($fields = array()) {
        if (count($fields) == false) {
            $fields = array(
                'ufailed',
                'answered',
                'busy',
                'failed',
                'noanswer',
                'uhangup',
                'call_0',
                'call_1',
                'call_2',
                'call_3'
            );
        }

        $arra = array();
        $dataAll = array();
        $dataGroup = array();
        $channels = Channels::model()->gPopOUT();
        if (count($this->namedData != false) && count($this->allData) != false) {
            foreach ($this->namedData as $name => $v) {
                $color = getRandomColor();
                if ($v['allcalls'] != 0)
                    $dataAll[] = array('name' => $channels[$name],
                        'color' => $color,
                        'y' => getPercent($v['allcalls'], $this->allData['allcalls']));
                if ($v['ufailed'] != 0 && in_array('ufailed', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dis_data', 'uFAILED'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['ufailed'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['answered'] != 0 && in_array('answered', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dis_data', 'ANSWERED'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['answered'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['busy'] != 0 && in_array('busy', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dis_data', 'BUSY'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['busy'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['failed'] != 0 && in_array('failed', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dis_data', 'FAILED'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['failed'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['noanswer'] != 0 && in_array('noanswer', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dis_data', 'NO ANSWER'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['noanswer'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['uhangup'] != 0 && in_array('uhangup', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dis_data', 'uHANGUP'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['uhangup'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['call_0'] != 0 && in_array('call_0', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'outbound'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_0'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['call_1'] != 0 && in_array('call_1', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'inbound'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_1'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if (in_array('call_2', $fields) && $v['call_2'] != 0)
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'local'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_2'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['call_3'] != 0 && in_array('call_3', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'callback'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_3'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
            }
        }

        $arra[] = array('name' => 'Все звонки', 'data' => $dataAll, 'size' => '80%', 'dataLabels' => array('distance' => 20, 'color' => 'black'));
        $arra[] = array('name' => ' ', 'data' => $dataGroup, 'innerSize' => '70%', 'dataLabels' => array(), 'allowPointSelect' => true, 'cursor' => 'pointer',);

        return $arra;
    }

    /**
     * @return array
     */
    public function getSeriesByNameAgent($fields = array()) {
        if (count($fields) == false) {
            $fields = array(
                'call_0',
                'call_1',
                'call_2',
                'call_3'
            );
        }

        $arra = array();
        $dataAll = array();
        $dataGroup = array();
        $channels = Channels::model()->gPopIN();
        if (count($this->namedData != false) && count($this->allData) != false) {
            foreach ($this->namedData as $name => $v) {
                $color = getRandomColor();
                if ($v['allcalls'] != 0)
                    $dataAll[] = array('name' => $channels[$name],
                        'color' => $color,
                        'y' => getPercent($v['allcalls'], $this->allData['allcalls']));

                if ($v['call_0'] != 0 && in_array('call_0', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'outbound'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_0'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['call_1'] != 0 && in_array('call_1', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'inbound'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_1'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['call_2'] != 0 && in_array('call_2', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'local'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_2'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['call_3'] != 0 && in_array('call_3', $fields))
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'callback'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_3'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
            }
        }

        $arra[] = array('name' => 'Все звонки', 'data' => $dataAll, 'size' => '80%', 'dataLabels' => array('distance' => 20, 'color' => 'black'));
        $arra[] = array('name' => ' ', 'data' => $dataGroup, 'innerSize' => '80%', 'dataLabels' => array(), 'allowPointSelect' => true, 'cursor' => 'pointer',);

        return $arra;
    }

    /**
     * @return array
     */
    public function getSeriesByNameDep($fields = array()) {
        if (count($fields) == false) {
            $fields = array(
                'call_0',
                'call_1',
                'call_2',
                'call_3'
            );
        }

        $arra = array();
        $dataAll = array();
        $dataGroup = array();
        $dep = Chtml::listData(Departments::getPopDepartments(), 'dID', 'dName');
        if (count($this->namedData != false) && count($this->allData) != false) {
            foreach ($this->namedData as $name => $v) {
                $color = getRandomColor();
                if ($v['allcalls'] != 0)
                    $dataAll[] = array('name' => $dep[$name],
                        'color' => $color,
                        'y' => getPercent($v['allcalls'], $this->allData['allcalls']));

                if ($v['call_0'] != 0 && in_array('call_0', $fields))
                    $dataGroup[] = array('name' => $dep[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'outbound'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_0'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['call_1'] != 0 && in_array('call_1', $fields))
                    $dataGroup[] = array('name' => $dep[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'inbound'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_1'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['call_2'] != 0 && in_array('call_2', $fields))
                    $dataGroup[] = array('name' => $dep[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'local'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_2'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
                if ($v['call_3'] != 0 && in_array('call_3', $fields))
                    $dataGroup[] = array('name' => $dep[$name] . '<br>' . mb_strtolower(Yii::t('dir_data', 'callback'), 'UTF-8'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['call_3'], $v['allcalls'], getPercent($v['allcalls'], $this->allData['allcalls'])));
            }
        }

        $arra[] = array('name' => 'Все звонки', 'data' => $dataAll, 'size' => '80%', 'dataLabels' => array('distance' => 20, 'color' => 'black'));
        $arra[] = array('name' => ' ', 'data' => $dataGroup, 'innerSize' => '80%', 'dataLabels' => array(), 'allowPointSelect' => true, 'cursor' => 'pointer',);

        return $arra;
    }

}