<?php

/**
 * Модель для работы с таблицей QueueLog.
 * 
 * 
 * @author qzich <qzichs@gmail.com>
 */
class DQueueLog extends DAOCharts {
    /*
     * Название таблицы с которой работаем
     */

    public $tableName = 'QueueLog';

    /*
     * перечень полей, которые не числовые
     */
    protected $noDigitData = array('fullname');

    public static function model($sql = '', $className = __CLASS__) {
        return parent::model($sql, $className);
    }

    public function fetchAllStatByHours($h = 24) {
        if ($h == 0) {
            return $this;
        }
        $t = 0;
        $arra = array();
        do {
            $this->command
                    ->select('IFNULL(SUM(main.qlCalltime), 0)  as fullDuration,
                              IFNULL(SUM( main.`qlWaittime` ), 0) AS waitTime,
                              IFNULL(SUM(main.qlCalltime) /SUM( if(main.`qlEvent` = "COMPLETEAGENT" 
                              OR  main.`qlEvent` = "COMPLETECALLER", 1, 0) ), 0) as avgDuration,
                              main.`qlQueuename` as queuename, 
                              IFNULL(SUM( if(main.`qlEvent` = "COMPLETEAGENT" OR  main.`qlEvent` = "COMPLETECALLER", 1, 0) ), 0) as recieved, 
                              IFNULL(SUM( if(main.`qlEvent` = "ABANDON" OR main.`qlEvent` = "EXITWITHTIMEOUT", 1, 0) ), 0) as failed,
                              IFNULL(SUM( if(main.`qlEvent` = "ABANDON" OR main.`qlEvent` = "EXITWITHTIMEOUT", 1, 0) ) + SUM( if(main.`qlEvent` = "COMPLETEAGENT" OR main.`qlEvent` = "COMPLETECALLER", 1, 0) ), 0) as allcall,
                              queue.qName as name')
                    ->from($this->tableName . ' main')
                    ->leftJoin('Queue as queue', 'queue.name = main.qlQueuename')
                    ->andWhere(array('AND', 'main.`qlQueuename` <> "NONE"', '(HOUR(main.`qlCalldate`) BETWEEN :sh AND :eh)'), array('sh' => $t, 'eh' => $t + $h - 1))
                    ->group('main.qlQueuename');
            /* ->andWhere('`qlCalldate` > "2012-02-13 13:43:50" AND `qlCalldate` < "2014-02-13 14:00:50"') */
            if (isset(Yii::app()->user->Groups_gID))
                $this->command->andWhere('queue.qGroups_gID =' . Yii::app()->user->getState('Groups_gID'));
            $up = $t + $h < 24 ? date('H:i', mktime($t + $h, 0)) : '24:00';
            $arra[date('H:i', mktime($t, 0)) . ' - ' . $up] = $this->command->queryAll();
        } while (24 > $t +=$h);

        $this->result = $arra;
        $this->processDataByName($arra); //обработка рез. по очередям
        $this->processAllData();        // обработка рез.
        return $this;
    }

    public function fetchAllStatByDays($h = 6) {
        if ($h == 0) {
            return $this;
        }
        $t = 0;
        $arra = array();
        do {
            $k = 'd' . $t . ' - ' . ($t + $h <= 6 ? 'd' . ($t + $h - 1) : 'd' . '6');
            if ($h == 1) {
                $k = 'd' . $t;
            }
            if ($t == 6) {
                $k = 'd6';
            }

            $this->command
                    ->select('IFNULL(SUM(main.qlCalltime), 0)  as fullDuration,
                            IFNULL(SUM( main.`qlWaittime` ), 0) AS waitTime,
                            IFNULL(SUM(main.qlCalltime) /SUM( if(main.`qlEvent` = "COMPLETEAGENT" 
                            OR  main.`qlEvent` = "COMPLETECALLER", 1, 0) ), 0) as avgDuration,
                            main.`qlQueuename` as queuename, 
                            IFNULL(SUM( if(main.`qlEvent` = "COMPLETEAGENT" OR  main.`qlEvent` = "COMPLETECALLER", 1, 0) ), 0) as recieved, 
                            IFNULL(SUM( if(main.`qlEvent` = "ABANDON" OR main.`qlEvent` = "EXITWITHTIMEOUT", 1, 0) ), 0) as failed,
                            IFNULL(SUM( if(main.`qlEvent` = "ABANDON" OR main.`qlEvent` = "EXITWITHTIMEOUT", 1, 0) ) + SUM( if(main.`qlEvent` = "COMPLETEAGENT" OR main.`qlEvent` = "COMPLETECALLER", 1, 0) ), 0) as allcall,
                            queue.qName as name')
                    ->from($this->tableName . ' main')
                    ->leftJoin('Queue as queue', 'queue.name = main.qlQueuename')
                    ->andWhere(array('AND', 'main.`qlQueuename` <> "NONE"', '(WEEKDAY(main.`qlCalldate`) BETWEEN :sd AND :ed)'), array('sd' => $t, 'ed' => $t == 6 ? 6 : $t + $h - 1))
                    ->group('main.qlQueuename');
            if (isset(Yii::app()->user->Groups_gID))
                $this->command->andWhere('queue.qGroups_gID =' . Yii::app()->user->getState('Groups_gID'));
            $arra[$k] = $this->command->queryAll();
        } while (6 >= $t +=$h);
        $this->result = $arra;
        $this->processDataByName($arra);
        $this->processAllData();
        return $this;
    }

    public function fetchAllByAgents() {

        $this->command->select('agents.sChannels_cID as name, agents.name as number, co.coName as fullname, channel.cName, 
                                IFNULL(SUM( if(main.`qlEvent` = "COMPLETEAGENT" OR  main.`qlEvent` = "COMPLETECALLER", 1, 0) ), 0) as recieved, 
                                IFNULL(SUM( if(main.`qlEvent` = "TRANSFER", 1, 0) ), 0) as transfer, 
                                IFNULL(SUM( if(main.`qlEvent` = "RINGNOANSWER", 1, 0) ), 0) as missed,
                                IFNULL(SUM( if(main.`qlEvent` = "COMPLETEAGENT" OR  main.`qlEvent` = "COMPLETECALLER", 1, 0) ) + SUM( if(main.`qlEvent` = "TRANSFER", 1, 0) ) + SUM( if(main.`qlEvent` = "RINGNOANSWER", 1, 0) ) , 0) as allcall
                                ')
                ->from($this->tableName . ' main')
                ->leftJoin('Channels as channel', 'channel.cID = main.`qlAgent`')
                ->leftJoin('ChannelOwners as co', 'co.coChannels_cID = main.`qlAgent`')
                ->leftJoin('SIP as agents', 'agents.sChannels_cID = channel.cID')
                ->andWhere('main.qlAgent <> ' . QueueLogForm::undefinedAgent)
                ->andWhere('channel.cType = :t ', array(':t' => Channels::IN_CHAN))
                ->group('main.qlAgent');
        if (isset(Yii::app()->user->Groups_gID))
            $this->command->andWhere('co.coGroups_gID =' . Yii::app()->user->getState('Groups_gID'));
        $this->result = $this->command->queryAll();
        $this->fiterBy('name', Channels::model()->gPopIN());
        $this->processDataByName($this->result);
        $this->processAllData();
        return $this;
    }

    /**
     * Метод добавляет условия к поиску по критериям указанных в свойствах
     * 
     * @return DQueueLog
     */
    public function search() {
        $this->command->reset();
        if (isset($this->startDate) && $this->startDate != false)
            $this->command->andWhere('main.`qlCalldate` >= :strdate', array('strdate' => $this->startDate));
        if (isset($this->endDate) && $this->endDate != false)
            $this->command->andWhere('main.`qlCalldate` <= :enddate', array('enddate' => $this->endDate));
        if (isset($this->qlQueuename) && $this->qlQueuename != "-1")
            $this->command->andWhere('main.`qlQueuename` IN (' . implode(', ', Yii::app()->db->quoteValueSet($this->qlQueuename)) . ')');

        if (isset($this->qlAgent) && $this->qlAgent != "-1")
            $this->command->andWhere('main.`qlAgent` IN (' . implode(', ', $this->qlAgent) . ')');

        if (isset($this->departmentQueue) && $this->departmentQueue != "-1") {
            $values = array();
            $items = is_array($this->departmentQueue) ? $this->departmentQueue : array($this->departmentQueue);
            foreach ($items as $depId) {
                if (count(DepartmentQueues::getQueuesByDep($depId)) != false) {
                    $values = array_unique(array_merge(DepartmentQueues::getQueuesByDep($depId), $values));
                }
            }
            if (count($values != false))
                $this->command->andWhere('main.`qlQueuename` IN ( ' . implode(', ', Yii::app()->db->quoteValueSet($values)) . ' )');
        }

        if (isset($this->departmentMb) && $this->departmentMb != "-1") {
            $values = array();
            $items = is_array($this->departmentMb) ? $this->departmentMb : array($this->departmentMb);
            foreach ($items as $depId) {
                if (count(DepartmentMembers::getAgentsChannels($depId)) != false) {
                    $values = array_unique(array_merge(DepartmentMembers::getAgentsChannels($depId), $values));
                }
            }
            if (count($values != false))
                $this->command->andWhere('main.`qlAgent` IN ( ' . implode(', ', $values) . ')');
        }

        return $this;
    }

    public function getCategories() {
        if (is_array($this->result)) {
            return array_keys($this->result);
        }
    }

    public function getCategoriesByDays() {
        $d = $this->getCategories();
        $arra = array();
        foreach ($d as $k => $v) {
            $arra[] = preg_replace_callback('/d(\d)+/i', "getDayName", $v);
        }
        return $arra;
    }

    public function getSeriesByField($ser = false) {
        $arra = array();
        if ($ser == false)
            return $this->result;
        if (is_array($this->result)) {
            foreach ($this->result as $k => $v) {
                foreach ($v as $field => $val) {
                    foreach ($val as $field => $value) {
                        if ($field === $ser[0]) {
                            $dat[$val['name']][$k] = intval($value);
                        }
                    }
                    if (isset($ser['options']) && is_array($ser['options']))
                        $arra[$val['name']] = array_merge($ser['options'], array('name' => $val['name'], 'data' => dissAsoc(array_merge(array_fill_keys($this->getCategories(), 0), $dat[$val['name']]))));
                    else {

                        $arra[$val['name']] = array('name' => $val['name'], 'data' => dissAsoc(array_merge(array_fill_keys($this->getCategories(), 0), $dat[$val['name']])));
                    }
                }
            }
        }

        return dissAsoc($arra);
    }

    public function getSeriesByCount() {
        $arra = array();
        $dataAll = array();
        $dataGroup = array();
        if (count($this->namedData != false) && count($this->allData) != false) {
            foreach ($this->namedData as $name => $v) {
                $color = getRandomColor();
                if ($v['recieved'] + $v['failed'] != 0)
                    $dataAll[] = array('name' => $name,
                        'color' => $color,
                        'y' => getPercent($v['recieved'] + $v['failed'], $this->allData['recieved'] + $this->allData['failed']));
                if ($v['recieved'] != 0)
                    $dataGroup[] = array('name' => $name . '<br>' . Yii::t('queuelog', 'recieved'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['recieved'], $v['recieved'] + $v['failed'], getPercent($v['recieved'] + $v['failed'], $this->allData['recieved'] + $this->allData['failed'])));
                if ($v['failed'] != 0)
                    $dataGroup[] = array('name' => $name . '<br>' . Yii::t('queuelog', 'failed'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['failed'], $v['recieved'] + $v['failed'], getPercent($v['recieved'] + $v['failed'], $this->allData['recieved'] + $this->allData['failed'])));
            }
        }

        $arra[] = array('name' => 'Все звонки', 'data' => $dataAll, 'size' => '80%', 'dataLabels' => array('distance' => 20, 'color' => 'black'));
        $arra[] = array('name' => ' ', 'data' => $dataGroup, 'innerSize' => '80%', 'dataLabels' => array(), 'allowPointSelect' => true, 'cursor' => 'pointer',);

        return $arra;
    }

    /**
     * 
     * @param array $series
     * @return array
     */
    public function getSeriesByAgents($series = array()) {
        if (count($series) == false) {
            $series = array(
                'recieved',
                'missed',
                'transfer'
            );
        }
        $arra = array();
        $dataAll = array();
        $dataGroup = array();
        $channels = Channels::model()->gPopIN();
        if (count($this->namedData != false) && count($this->allData) != false) {
            foreach ($this->namedData as $name => $v) {
                $color = getRandomColor();
                if ($v['allcall'] != 0)
                    $dataAll[] = array('name' => $channels[$name],
                        'color' => $color,
                        'y' => getPercent($v['allcall'], $this->allData['allcall']));

                if (in_array('recieved', $series) && $v['recieved'] != 0)
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . Yii::t('queuelog', 'recieved'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['recieved'], $v['allcall'], getPercent($v['allcall'], $this->allData['allcall'])));
                if (in_array('missed', $series) && $v['missed'] != 0)
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . Yii::t('queuelog', 'missed'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['missed'], $v['allcall'], getPercent($v['allcall'], $this->allData['allcall'])));
                if (in_array('transfer', $series) && $v['transfer'] != 0)
                    $dataGroup[] = array('name' => $channels[$name] . '<br>' . Yii::t('queuelog', 'transfer'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['transfer'], $v['allcall'], getPercent($v['allcall'], $this->allData['allcall'])));
            }
        }

        $arra[] = array('name' => 'Все звонки', 'data' => $dataAll, 'size' => '80%', 'dataLabels' => array('distance' => 20, 'color' => 'black'));
        $arra[] = array('name' => ' ', 'data' => $dataGroup, 'innerSize' => '80%', 'dataLabels' => array(), 'allowPointSelect' => true, 'cursor' => 'pointer',);
        return $arra;
    }

    public function getSeriesByTime() {
        $arra = array();
        $dataAll = array();
        $dataGroup = array();
        if (count($this->namedData != false) && count($this->allData) != false) {
            foreach ($this->namedData as $name => $v) {
                $color = getRandomColor();
                if ($v['fullDuration'] + $v['waitTime'] != 0)
                    $dataAll[] = array('name' => $name,
                        'y' => getPercent($v['fullDuration'] + $v['waitTime'], $this->allData['fullDuration'] + $this->allData['waitTime']),
                        'color' => $color);
                if ($v['fullDuration'] != 0)
                    $dataGroup[] = array('name' => $name . '<br>' . Yii::t('queuelog', 'talk time'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['fullDuration'], $v['fullDuration'] + $v['waitTime'], getPercent($v['fullDuration'] + $v['waitTime'], $this->allData['fullDuration'] + $this->allData['waitTime'])));
                if ($v['waitTime'] != 0)
                    $dataGroup[] = array('name' => $name . '<br>' . Yii::t('queuelog', 'waittime'),
                        'color' => getLightlyColor($color),
                        'y' => getPercent($v['waitTime'], $v['fullDuration'] + $v['waitTime'], getPercent($v['fullDuration'] + $v['waitTime'], $this->allData['fullDuration'] + $this->allData['waitTime'])));
            }
        }

        $arra[] = array('name' => 'Все звонки', 'data' => $dataAll, 'size' => '80%', 'dataLabels' => array('distance' => 20, 'color' => 'black'));
        $arra[] = array('name' => ' ', 'data' => $dataGroup, 'innerSize' => '80%', 'dataLabels' => array(), 'allowPointSelect' => true, 'cursor' => 'pointer',);

        return $arra;
    }

}