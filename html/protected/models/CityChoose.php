<?php
class CityChoose extends CActiveRecord {
	public $CityName = null;
	public $number_ed = null;
	public $city_ed = null;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'CityChoose';
	}
	public function rules() {
		return array(
				array('number','required'),
				array('city','length','max' => 255),
				array('number','length','max' => 25),
				array('city, number, CityName','safe'),
				array('city, number','safe','on' => 'search'),
				array('city_ed, number_ed','safe'));
	}
	public function scopes() {
		return array(
				'CityNames' => array(
						'select' => array('c.*, COALESCE(cn.DisplayName, c.city) CityName '),
						'alias' => 'c',
						'join' => ('left join CityNames cn on cn.Name = c.city ')));
	}
	public function attributeLabels() {
		return array('city' => 'Город','number' => 'Номер','CityName' => 'Город','InsertDateTime' => 'Добавлен');
	}
	public function search() {
		if (($this->number) != false) {
			$this->dbCriteria->addCondition('number like "%' . $this->number . '%"');
		}
		if (($this->CityName != false) && ($this->CityName != '&All'))
			if ($this->CityName === '&Null')
				$this->dbCriteria->addCondition('c.city = ""');
			else
				$this->dbCriteria->addCondition('c.city = "' . $this->CityName . '"');
	}
	public function CitysDropDownFilter() {
		$ListData = array('&All' => Yii::t('m', 'All values'),'&Null' => 'Без города');
		$ListData = $ListData + Chtml::listData(DataModule::getCitysList(), 'Name', 'DisplayName');
		return CHtml::tag('label', array('class' => 'designed-select'), CHtml::dropDownList(CHtml::activeName(self::model(), 'CityName'), $this->CityName, $ListData));
	}
	public function CitysDropDownList() {
		$ListData = array('' => 'Без города');
		$ListData = $ListData + Chtml::listData(DataModule::getCitysList(), 'Name', 'DisplayName');
		return CHtml::tag('label', array('class' => 'designed-select'), CHtml::dropDownList('frmCitychoose[city_ed]', $this->city_ed, $ListData));
	}
	public function GetDefaultGridColumns($isExprtCSV = false) {
		if ($isExprtCSV)
			return array(
					array('name' => 'number'),
					array('name' => 'CityName'),
					array('name' => 'InsertDateTime','value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data["InsertDateTime"])'));
		
		$btnTemplate = '{update}';
		if (yii::app()->user->getState("isRoot_"))
			$btnTemplate .= ' {delete}';
		return array(
				array(
						'name' => 'number',
						'cssClassExpression' => '"number"',
						'filter' => '<input name="' . CHtml::activeName($this, 'number') . '"  type="text" value="' . $this->number . '"/>'),
				array('name' => 'CityName','cssClassExpression' => '"CityName"','filter' => $this->CitysDropDownFilter()),
				array(
						'name' => 'InsertDateTime',
						'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data["InsertDateTime"])',
						'filter' => false),
				array(
						'header' => 'Действия',
						'class' => 'CButtonColumn',
						'template' => $btnTemplate,
						'buttons' => array(
								'update' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-edit addpbn')),
								'delete' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-remove-circle')))),
				array(
						'name' => "number_ed",
						'value' => '$data["number"]',
						'cssClassExpression' => '"number_ed"',
						'filter' => false,
						'filterHtmlOptions' => array("style" => "display:none"),
						'headerHtmlOptions' => array("style" => "display:none"),
						'htmlOptions' => array("style" => "display:none")),
				array(
						'name' => "city_ed",
						'value' => '$data["city"]',
						'cssClassExpression' => '"city_ed"',
						'filter' => false,
						'filterHtmlOptions' => array("style" => "display:none"),
						'headerHtmlOptions' => array("style" => "display:none"),
						'htmlOptions' => array("style" => "display:none")));
	}
}