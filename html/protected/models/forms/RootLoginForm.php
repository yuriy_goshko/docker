<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class RootLoginForm extends LoginForm
{
        
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>Yii::t('m','Remember me next time'),
                        'username'=>Yii::t('m','Root login'),
                        'password'=>Yii::t('m','Password'),
		);
	}

}
