<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class CallInfoForm extends CFormModel {

    public $cfSrc;
    public $cfDst;
    public $cfStartDate;
    public $cfEndDate = false;
    public $cfDirection = '-1';
    public $cfAgent = '-1';
    public $cfOutchan = '-1';
    public $pbID;
    public $cfQueue = '-1';
    public $cfPagesize = 10;
    public $cfDepartmentMb = '-1';
    public $cfDepartment = '-1';
    public $cfDisposition = '-1';

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            /* // username and password are required
              array('username, password', 'required'),
              // rememberMe needs to be a boolean
              array('rememberMe', 'boolean'),
              // password needs to be authenticated
              array('password', 'authenticate'), */
            array('cfPagesize', 'required'),
            array('cfPagesize', 'numerical', 'min' => 1, 'max' => 100),
            array('cfSrc, cfDst, cfOutchanMult, cfDepartment, aDepartmentMb, cfDisposition 
                agDepartmentMb, cfStartDate, cfEndDate, 
                cfDirection, cfAgent, cfOutchan, cfQueue, cfPagesize, cfDepartmentMb', 'safe')
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'cfSrc' => mb_ucfirst(Yii::t('callinfo', 'number')) . ':',
            'cfDst' => mb_ucfirst(Yii::t('callinfo', 'destination number')) . ':',
            'cfStartDate' => mb_ucfirst(Yii::t('callinfo', 'start date')) . ':',
            'cfEndDate' => mb_ucfirst(Yii::t('callinfo', 'end date')) . ':',
            'cfDirection' => mb_ucfirst(Yii::t('callinfo', 'call direction')) . ':',
            'cfOutchan' => mb_ucfirst(Yii::t('callinfo', 'external channel')) . ':',
            'cfAgent' => mb_ucfirst(Yii::t('callinfo', 'agent')) . ':',
            'cfQueue' => mb_ucfirst(Yii::t('callinfo', 'queue')) . ':',
            'cfPagesize' => mb_ucfirst(Yii::t('callinfo', 'page size')) . ':',
            'cfDepartment' => mb_ucfirst(Yii::t('callinfo', 'departments')) . ':',
            'cfDepartmentMb' => mb_ucfirst(Yii::t('callinfo', 'department members')) . ':',
            'cfDisposition' => mb_ucfirst(Yii::t('callinfo', 'status')) . ':',
        );
    }

    /**
     * Initialize model
     */
    public function init() {
       $this->cfStartDate = date('Y-m-1');
    }

}
