<?php

/**
 * QueueLogForm class.
 * QueueLogForm used to create/populate forms for actions with QueueLog table working
 * 
 * @property type $startDate Description
 * @property type $endDate Description
 * @property type $qlAgent Description
 * @property type $departmentQueue Description
 * @property type $departmentMb Description
 * @property type $period Description
 */
class QueueLogForm extends CFormModel {
    
    
    const undefinedAgent = -1;

    public $period = 1;
    public $startDate;
    public $endDate = false;
    public $qlAgent = '-1';
    public $departmentQueue = '-1';
    public $departmentMb = '-1';
    public $qlQueuename = '-1';

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('startDate, departmentQueue, departmentMb, endDate, period, qlQueuename, qlAgent', 'safe'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'qlAgent' => mb_ucfirst(Yii::t('callinfo', 'agent')).':',
            'qlQueuename' => mb_ucfirst(Yii::t('queuelog', 'queue name')).':',
            'startDate' => mb_ucfirst(Yii::t('callinfo', 'start date')).':',
            'endDate' => mb_ucfirst(Yii::t('callinfo', 'end date')).':',
            'period' => mb_ucfirst(Yii::t('queuelog', 'period')).':',
            'periodD' => mb_ucfirst(Yii::t('queuelog', 'period days a week')).':',
            'departmentQueue' => mb_ucfirst(Yii::t('queuelog', 'department queues')).':',
            'departmentMb' => mb_ucfirst(Yii::t('callinfo', 'department members')).':',
        );
    }
    /**
     * Generate hours period
     * 
     * @return array
     */
    public static function getPeriodListByHours() {
        $i = 1;
        do {
            $a[$i] = $i;
            $i++;
        } while ($i <= 24);
        return $a;
    }
    
    /**
     * Generate days period
     * 
     * @return array
     */
    public static function getPeriodListByDays() {
        $i = 1;
        do {
            $a[$i] = $i;
            $i++;
        } while ($i <= 7);
        return $a;
    }

    /**
     * Initialize model
     */
    public function init() {
        $this->startDate = date('Y-m-1');
    }

}
