<?php

/**
 * This is the model class for table "PhoneBook".
 *
 * The followings are the available columns in table 'PhoneBook':
 * @property string $pbID
 * @property string $pbNumber
 * @property string $pbOwner
 * @property string $pbBlacked
 * @property string $pbReason
 */
class PhoneBookForm extends CFormModel {

    public $pbNumber;
    public $pbOwner;
    public $pbBlacked;
    public $pbReason;
    public $pbID;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('pbNumber, pbOwner', 'required'),
            array('pbOwner', 'length', 'max' => 40),
            array('pbBlacked', 'length', 'max' => 3),
            array('pbNumber', 'length', 'max' => 13),
            array('pbNumber', 'numerical'),
            array('pbReason', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('pbID, pbNumber, pbOwner, pbBlacked, pbReason', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'pbNumber' => Yii::t('m', 'Telephone number') . ':',
            'pbOwner' => Yii::t('m', 'Name') . ':',
            'pbBlacked' => Yii::t('m', 'Blacklist') . ':',
            'pbReason' => Yii::t('m', 'The reason for blocking') . ':'
        );
    }

}