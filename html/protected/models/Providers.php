<?php
class Providers extends CActiveRecord {
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'Providers';
	}
	public function rules() {
		return array(array('pName','required'),array('pName','unique'));
	}
	public function attributeLabels() {
		return array('pName' => 'Имя провайдера');
	}
}