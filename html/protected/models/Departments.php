<?php
class Departments extends CActiveRecord {
	// для связки депюзерс
	public $checked = false;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'Departments';
	}
	public function rules() {
		return array(
				array('dName','required'),
				array('dName','length','max' => 80),
				array('dKeyName','required'),
				array('dKeyName','length','max' => 80),
				array('dColor','required'),
				array('dColor','length','max' => 40),
				array('checked','safe'),
				array('dID, dName, dColor','safe','on' => 'search'));
	}
	public function relations() {
		return array(
				'departmentMembers' => array(self::HAS_MANY,'DepartmentMembers','dpDepartments_dID'),
				'departmentQueues' => array(self::HAS_MANY,'DepartmentQueues','dqDepartments_dID'),
				'depagents' => array(self::MANY_MANY,'Channels','DepartmentMembers(dpDepartments_dID, dpChannels_cID)'),
				'depqueues' => array(self::MANY_MANY,'Queue','DepartmentQueues(dqDepartments_dID, dqQueues_qName)'));
	}
	public function attributeLabels() {
		return array('dID' => 'D','dKeyName' => 'Ключевое имя','dName' => Yii::t('m', 'Department name'),'dColor' => Yii::t('m', 'Department color'));
	}
	public function search() {
		$criteria = new CDbCriteria();
		$criteria->compare('dID', $this->dID);
		$criteria->compare('dName', $this->dName, true);
		return new CActiveDataProvider($this, array('criteria' => $criteria));
	}
	public function defaultScope() {
		$DepIds = yii::app()->user->getState('depIds');
		if ($DepIds != '*') {
			return array('condition' => '(dID in (' . $DepIds . '))');
		} else
			return array();
	}
	public function scopes() {
		return array('mine' => array());
	}
	public function beforeSave() {
		parent::beforeSave();
		return $this->validate();
	}
	public static function getPopDepartments() {
		return Yii::app()->db->createCommand()->select('dep.dID, dep.dName')->from('Departments as dep')->leftJoin('DepartmentMembers as dm', 'dm.dpDepartments_dID = dep.dID')->leftJoin('DepartmentQueues as dq', 'dq.dqDepartments_dID = dep.dID')->andWhere('dm.dpDepartments_dID IS NOT NULL OR dq.dqDepartments_dID IS NOT NULL')->group('dep.dID')->queryAll();
	}
}