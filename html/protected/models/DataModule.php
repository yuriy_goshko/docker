<?php
class DataModule extends CActiveRecord {
	private static $Settings;
	public static function GetSettingValue($Param) {
		if (! isset(self::$Settings)) {
			self::$Settings = DataModule::getSettnigsArray();
		}
		if (array_key_exists($Param, self::$Settings))
			return self::$Settings[$Param];
		else
			return '';
	}
	public static function getCitysList() {
		$sql = 'select Name, DisplayName from CityNames';
		$command = Yii::app()->db->createCommand($sql);
		return $command->queryAll();
	}
	public static function getSettnigsArray() {
		$r = array();
		$sql = 'select param, value from Settings';
		$command = Yii::app()->db->createCommand($sql);
		$qa = $command->queryAll();
		foreach ( $qa as $row ) {
			$r[$row['param']] = $row['value'];
		}
		return $r;
	}
	public static function getRolesList() {
		$sql = 'select u.`uIdentity`, u.`uName` from `Users` u where u.`IsRole` = 1 order by u.`uName`';
		$command = Yii::app()->db->createCommand($sql);
		
		return $command->queryAll();
	}
	public static function getDepartmentsList() {
		$depIds = yii::app()->user->getState('depIds');
		if ($depIds === '*') {
			$sql = 'select dID, dName from Departments order by dID';
			$command = Yii::app()->db->createCommand($sql);
		} else {
			$sql = 'select dID, dName from Departments where dID in (' . $depIds . ') order by dID';
			$command = Yii::app()->db->createCommand($sql);
		}
		return $command->queryAll();
	}
	public static function DepsCond($alias = 'c') {
		$depIds = yii::app()->user->getState('depIds');
		$depKeys = yii::app()->user->getState('depKeys');
		if ($depIds === '*')
			return '1=1';
		$sqlCond = 'exists (select 1 from CI_CallsInfo ci_ where [alias].Id = ci_.CallId and ci_.InfoType = "DEP_NAME" and ci_.InfoValue in ("[depKeys]"))';
		$sqlCond = str_replace('[alias]', $alias, $sqlCond);
		$sqlCond = str_replace('[depKeys]', $depKeys, $sqlCond);
		return $sqlCond;
	}
	public static function DepsCondByArr($deps, $alias = 'c') {
		if (! yii::app()->user->getState('UseOldDepsConds')) {
			$sqlCond = '(exists (select 1 from CI_CallsInfo ci_ 
						inner join `Departments` dp_ on dp_.`dKeyName` = ci_.`InfoValue` 
						where [alias].Id = ci_.CallId and ci_.InfoType = "DEP_NAME" and dp_.`dID` in ([DepartsIs])))';
			$sqlCond = str_replace('[alias]', $alias, $sqlCond);
			$sqlCond = str_replace('[DepartsIs]', implode(',', $deps), $sqlCond);
		} else {
			$sqlCond = '((exists (select 1 from `CI_CallsInfo` ci 
				  inner join `DepartmentQueues` dq on dq.`dqDepartments_dID` in ([DepartsIs]) and ci.`InfoValue` = dq.dqQueues_qName 
				   where ci.CallId = [alias].`Id` and ci.InfoType in ("ENTERQUEUE", "QUEUE_NAME"))) 
			      or
			 	  (exists (select 1 from SIP s 
				  inner join DepartmentAgents da on s.id = da.SIPId and da.DepartmentId in ([DepartsIs]) 
				  where (s.name = [alias].NumDst) or (s.name = [alias].NumSrc) or (s.name = c.TransferFrom) or (s.name = [alias].TransferTo)) 
				  or exists (select 1 from CI_CallsDetail cd where c.Id=cd.CallId 
				  and exists (select 1 from SIP s
				  inner join DepartmentAgents da on s.id = da.SIPId and da.DepartmentId in ([DepartsIs]) 
				  where (s.name = cd.NumDst) or (s.name = cd.NumSrc) or (s.name = cd.TransferFrom) or (s.name = cd.TransferTo)))))';
			$sqlCond = str_replace('[alias]', $alias, $sqlCond);
			$sqlCond = str_replace('[DepartsIs]', implode(',', $deps), $sqlCond);
		}
		return $sqlCond;
	}
	public static function getStatesList($Full = false) {
		if (! $Full)
			$sql = 'select Id, Name from CI_States where Id not in ("ANSWERED_PART","UNKNOWN") order by OrderKey';
		else
			$sql = 'select Id, Name from CI_States order by OrderKey';
		$command = Yii::app()->db->createCommand($sql);
		return $command->queryAll();
	}
	public static function getDirectionsList($Full = false) {
		if (! $Full)
			$sql = 'select Id, Name from CI_Directions where (Id <> "CallbackInit") and (Used = 1) order by OrderKey';
		else
			$sql = 'select Id, Name from CI_Directions order by OrderKey';
		$command = Yii::app()->db->createCommand($sql);
		return $command->queryAll();
	}
	public static function getAgentsList($DepIds) {
		$GenDepIds = yii::app()->user->getState('depIds');
		$sql = 'select (case when (fullname="") or (fullname is null) then name else CONCAT(name," (",fullname,")") end) FN from SIP';
		if (($DepIds != false) || ($GenDepIds != '*'))
			$sql .= ' inner join DepartmentAgents on SIPId=id ';
		if ($DepIds != false) {
			$DepIds = implode(',', $DepIds);
			$sql .= ' and DepartmentId in (' . $DepIds . ')';
		}
		if ($GenDepIds != '*')
			$sql .= ' and DepartmentId in (' . $GenDepIds . ')';
			// $sql .= ' left join Channels on cID=sChannels_cID where cType is null or cType=1';
		$sql .= ' Order By 1';
		$command = Yii::app()->db->createCommand($sql);
		return $command->queryAll();
	}
	public static function getPBXNumsList() {				
		$sql = 'select n.`Number`, (case when COALESCE(n.DisplayName,"")="" then n.`Number` else CONCAT(n.`Number`," (",n.DisplayName,")") end) DisplayNumber from `PBXNumbers` n ';
		$depIds = yii::app()->user->getState('depIds');
		if ($depIds != '*') {
			$sql .= '
				inner join `PBXNumbersRelations` nr on nr.`Number`=n.`Number`
        		and nr.`DepartmentId` in (' . $depIds . ')
				';
		}
		$sql .= ' order by n.`Number`';
		$command = Yii::app()->db->createCommand($sql);
		return $command->queryAll();
	}
	/*
	 public static function getOutChannelsList() {
	 $sql = 'select distinct c.KeyName, COALESCE(c.DisplayName, c.KeyName) FullName from PBXChannelllllllllls c ';
	 $depIds = yii::app()->user->getState('depIds');
	 if ($depIds != '*') {
	 $sql .= '
	 inner join `PBXNumbersRelations` nrChan on nrChan.ChannelId=c.`Id`
	 inner join `PBXNumbersRelations` nrNums on nrChan.`Number`=nrNums.`Number` 
	 and nrNums.`DepartmentId` in (' . $depIds . ')
	 ';
	 }		
	 $sql .= ' order by COALESCE(DisplayName, c.KeyName)';
	 $command = Yii::app()->db->createCommand($sql);
	 return $command->queryAll();
	 }*/
	public static function getQueuesList() {
		$sql = 'select name, qName from Queue';
		$depIds = yii::app()->user->getState('depIds');
		if ($depIds != '*')
			$sql .= ' inner join `DepartmentQueues` dq on dq.`dqQueues_qName` = Queue.name and dq.`dqDepartments_dID` in (' . $depIds . ')';
		$command = Yii::app()->db->createCommand($sql);
		return $command->queryAll();
	}
	public static function csvExport($dataProvider, $fname, $gridcolumns, $columnsheaders) {
		if (! yii::app()->user->getState('rCSVExport'))
			throw new CHttpException(403, Yii::t('yii', 'You are not authorized to perform this action.'));
		$fp = fopen('php://temp', 'w');
		$fname .= Yii::app()->dateFormatter->format("d.M.yy_H:m:s", time());
		$row = array();
		if (empty($columnsheaders)) {
			foreach ( $gridcolumns as $h ) {
				$row[] = $h['header'];
			}
		} else {
			foreach ( $gridcolumns as $h ) {
				$row[] = $columnsheaders[$h['name']];
			}
		}
		fputcsv($fp, $row);
		foreach ( (($dataProvider instanceof CArrayDataProvider) ? $dataProvider->rawData : $dataProvider->getData()) as $record ) {
			$row = array();
			foreach ( $gridcolumns as $c ) {
				if (array_key_exists('value', $c)) {
					try {
						$row[] = strip_tags(yii::app()->controller->evaluateExpression($c['value'], array('data' => $record)));
					} catch ( Exception $e ) {
						$row[] = 'ErrorValue: ' . $e->getMessage();
					}
				} else
					$row[] = $record[$c['name']];
			}
			fputcsv($fp, $row);
		}
		rewind($fp);
		Yii::app()->request->sendFile($fname . '.csv', stream_get_contents($fp), null, false);
		fclose($fp);
	}
	public static function adjustBrightness($hex, $steps) {
		// Steps should be between -255 and 255. Negative = darker, positive = lighter
		$steps = max(- 255, min(255, $steps));
		
		// Normalize into a six character long hex string
		$hex = str_replace('#', '', $hex);
		if (strlen($hex) == 3) {
			$hex = str_repeat(substr($hex, 0, 1), 2) . str_repeat(substr($hex, 1, 1), 2) . str_repeat(substr($hex, 2, 1), 2);
		}
		
		// Split into three parts: R, G and B
		$color_parts = str_split($hex, 2);
		$return = '#';
		
		foreach ( $color_parts as $color ) {
			$color = hexdec($color); // Convert to decimal
			$color = max(0, min(255, $color + $steps)); // Adjust color
			$return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
		}
		
		return $return;
	}
}





