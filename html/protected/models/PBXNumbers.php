<?php
class PBXNumbers extends CActiveRecord {
	public $ProviderName;
	// привязан к отделу
	public $wDep = null;
	// для привязки по умолчанию(при добалении)
	public $_DefDepartment = null;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'PBXNumbers';
	}
	public function rules() {
		return array(
				array('Number','required'),
				array('Number','unique'),
				array('Number','length','max' => 50),
				array('Note','length','max' => 100),
				array('Number, Note','safe'),
				array('Type, Name','required'),
				array('Type','length','max' => 20),
				array('Name','length','max' => 255),
				array('KeyName','length','max' => 275),
				array('DisplayName','length','max' => 255),
				array('DisplayName','default','setOnEmpty' => true,'value' => null),
				array('wDep','safe'),
				array('_DefDepartment','safe'),
				array('Type, KeyName, Name, DisplayName, ProviderId, ProviderName','safe'));
	}
	protected function beforeSave() {
		parent::beforeSave();
		$this->KeyName = $this->Type . '/' . $this->Name;
		return true;
	}
	protected function afterSave() {
		parent::afterSave();
		if ($this->isNewRecord) {
			if ((yii::app()->user->getState("isRoot_")) && ($this->_DefDepartment != null)) {
				$sql = 'insert into PBXNumbersRelations(Number, DepartmentId, DepartmentIncoming, DepartmentOutcoming) values (:pNumber, :pDepId, 1, null);
						insert into PBXNumbersRelations(Number, DepartmentId, DepartmentIncoming, DepartmentOutcoming) values (:pNumber, :pDepId, null, 1);';
				$command = Yii::app()->db->createCommand($sql);
				$n = $this->Number;
				$command->bindParam(":pNumber", $n, PDO::PARAM_STR);
				$command->bindParam(":pDepId", $this->_DefDepartment, PDO::PARAM_INT);
				$command->execute();
			}
		}
	}
	public function defaultScope() {
		$wDep = '(select 1 from `PBXNumbersRelations` nr where nr.`Number`=n.`Number` and nr.`DepartmentId` is not null limit 1) wDep';
		return array('select' => 'n.*, ' . $wDep,'alias' => 'n','order' => 'Type,Number');
	}
	public function scopes() {
		$defaultScope = $this->defaultScope();
		return array(
				'withproviders' => array(
						'select' => array($defaultScope['select'],'pr.pName ProviderName'),
						'alias' => 'n',
						'join' => ('left join Providers pr on n.ProviderId = pr.pID ')));
	}
	public function attributeLabels() {
		return array(
				'Number' => 'Номер',
				'Note' => 'Примечание(не исп.)',
				'Type' => 'Тип канала',
				'Name' => 'Название канала',
				'DisplayName' => 'Отображаемое название',
				'ProviderId' => 'Провайдер',
				'_DefDepartment' => Yii::t('m', '_DefDepartment'),
				'ProviderName' => 'Провайдер');
	}
}