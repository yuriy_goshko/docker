<?php

/**
 * This is the model class for table "StaticPages".
 *
 * The followings are the available columns in table 'StaticPages':
 * @property integer $spID
 * @property string $spTitle
 * @property string $spText
 * @property string $spKey
 * @property string $spDesc
 * @property integer $spParentID
 * @property integer $spOrder
 * @property string $spUpdateDate
 * @property string $spUrl
 * @property string $spExtUrl
 * @property string $spVisible
 * @property string $spType
 * @property string $spHtmlClass
 */
class StaticPages extends CActiveRecord {
	const menuParentID = - 1;
	public static $menuList = array(self::menuParentID => 'Новое меню');
	public static $mainTbMenu = array();
	public static $sortBypID = array();
	
	/**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return StaticPages the static model class
     */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	private static function CanView($pid) {
		$RightName = $pid->spRightName;
		$RightNameState = Yii::app()->user->getState($RightName);
		if (trim($RightName) === '')
			return true;
		else {
			if ($RightNameState)
				return true;
			else
				return false;
		}
	}
	public static function getMainTbMenu() {
		if (count(self::$mainTbMenu) == false) {
			$pid = self::model()->ordered()->find('spParentID = :pid', array('pid' => self::menuParentID));
			if ($pid != false && $pid->spVisible && self::CanView($pid)) {
				self::model()->sortByParent($pid);
				if (isset(self::$sortBypID[$pid->spID]) && count(self::$sortBypID[$pid->spID])) {
					foreach ( self::$sortBypID[$pid->spID] as $child ) {
						self::$mainTbMenu[] = self::model()->getMenuItemsTbMenu($child, array('p0' => $pid->spUrl));
					}
				}
			}
		}
		return self::$mainTbMenu;
	}
	public static function menuList($flag = false) {
		if ($flag == false) {
			unset(self::$menuList[self::menuParentID]);
			
			$rows = self::model()->ordered()->findAll('spParentID = :pid', array('pid' => self::menuParentID));
			foreach ( $rows as $row ) {
				self::$menuList[$row->spID] = $row->spTitle;
			}
			
			return self::$menuList;
		}
		
		return self::$menuList;
	}
	
	/**
     * @return string the associated database table name
     */
	public function tableName() {
		return 'StaticPages';
	}
	
	/**
     * @return array validation rules for model attributes.
     */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('spTitle','required'),
				array('spParentID, spOrder','numerical','integerOnly' => true),
				array('spTitle, spUrl, spType, spExtUrl, spHtmlClass','length','max' => 255),
				array('spText, spVisible, spKey, spDesc, spUpdateDate','safe'),
				array('spTitle','UUniqueValidator','compValue' => 'spParentID'),
				
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
						'spID, spTitle, spText, spKey, spDesc, spType, spParentID, spOrder, spVisible,  spUpdateDate, spUrl, spExtUrl, spHtmlClass',
						'safe',
						'on' => 'search'));
	}
	
	/**
     * @return array relational rules.
     */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}
	
	/**
     * @return array customized attribute labels (name=>label)
     */
	public function attributeLabels() {
		return array(
				'spID' => 'Sp',
				'spTitle' => 'Заголовок',
				'spText' => 'Текст',
				'spKey' => 'Ключевые слова',
				'spDesc' => 'Ключевое описание',
				'spParentID' => 'Родитель',
				'spOrder' => 'Sp Order',
				'spUpdateDate' => 'Sp Update Date',
				'spUrl' => 'Sp Url',
				'spExtUrl' => 'Внешняя ссылка',
				'spVisible' => 'Показать',
				'spHtmlClass' => 'CSS class');
	}
	
	/**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria = new CDbCriteria();
		
		$criteria->compare('spID', $this->spID);
		$criteria->compare('spTitle', $this->spTitle, true);
		$criteria->compare('spText', $this->spText, true);
		$criteria->compare('spKey', $this->spKey, true);
		$criteria->compare('spDesc', $this->spDesc, true);
		$criteria->compare('spParentID', $this->spParentID);
		$criteria->compare('spOrder', $this->spOrder);
		$criteria->compare('spUpdateDate', $this->spUpdateDate, true);
		$criteria->compare('spUrl', $this->spUrl, true);
		$criteria->compare('spExtUrl', $this->spExtUrl, true);
		$criteria->compare('spVisible', $this->spVisible, true);
		$criteria->compare('spVisible', $this->spType, true);
		$criteria->compare('spHtmlClass', $this->spHtmlClass, true);
		
		return new CActiveDataProvider($this, array('criteria' => $criteria));
	}
	public function scopes() {
		return array(
				'ordered' => array('order' => 'spOrder ASC'),
				'menus' => array('condition' => 'spParentID = :pid','params' => array('pid' => self::menuParentID)),
				'childs' => array('condition' => 'spParentID = :pid'));
	}
	public function beforeSave() {
		parent::beforeSave();
		if ($this->spUrl == false)
			$this->spUrl = RusToLat($this->spTitle);
		return true;
	}
	protected function getMenuItemsTbMenu($child, $urlParts, $i = 0) {
		$i ++;
		$urlParts['p' . $i] = $child["url"];
		
		if (isset(self::$sortBypID[$child["spID"]])) {
			$child['items'] = self::$sortBypID[$child["spID"]];
			foreach ( $child['items'] as $k => $item ) {
				$child['items'][$k] = $this->getMenuItemsTbMenu($item, $urlParts, $i);
			}
		}
		
		if ($child["extUrl"] == false) {
			$child['url'] = Yii::app()->request->hostInfo . Yii::app()->getUrlManager()->createUrl('site/view', $urlParts);
		} else {
			$child['url'] = $child["extUrl"];
		}
		
		return $child;
	}
	protected function sortByParent(CActiveRecord $pid) {
		if ($pid != false && $pid->spVisible && self::CanView($pid)) {
			$childs = self::model()->ordered()->childs()->findAll(array('params' => array('pid' => $pid->spID)));
			if (count($childs) != false) {
				foreach ( $childs as $p => $child ) {
					if ($child->spVisible && self::CanView($child)) {
						self::$sortBypID[$pid->spID][$p] = array(
								'label' => $child->spTitle,
								'url' => $child->spUrl,
								'spID' => $child->spID,
								'linkOptions' => array('class' => $child->spHtmlClass),
								'extUrl' => $child->spExtUrl);
						$this->sortByParent($child);
					}
				}
			}
		}
	}
}