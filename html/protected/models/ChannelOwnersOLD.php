<?php

/**
 * This is the model class for table "ChannelOwners".
 *
 * The followings are the available columns in table 'Agents':
 * @property int $coID
 * @property int $coGroups_gID
 * @property int $coChannels_cID
 * @property string $coName
 *
 * The followings are the available model relations:
 * @property Channels $aChannelsC
 */
class ChannelOwners extends CActiveRecord {
    

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ChannelOwners the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'ChannelOwners';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
             array('coGroups_gID, coChannels_cID, coName', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'channels' => array(self::BELONGS_TO, 'Channels', 'coChannels_cID'),
            'group'=>array(self::BELONGS_TO, 'Groups', 'coGroups_gID'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'coName'=>'Псевдоним'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {

    }

}