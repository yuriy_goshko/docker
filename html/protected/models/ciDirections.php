<?php
class ciDirections extends CActiveRecord {
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'CI_Directions';
	}
	public function rules() {
		return array(
				array('Id, Name, Used, DepsLinkByPBX, DepsLinkByAgents','required'),
				array('Id','unique'),
				array('Id','length','max' => 20),
				array('Id','match','pattern' => '/[a-zA-Z]/'),
				array('Used','match','pattern' => '/[0,1]/'),
				array('DepsLinkByPBX','match','pattern' => '/[0,1]/'),
				array('DepsLinkByAgents','match','pattern' => '/[0,1]/'),
				array('Name','length','max' => 100),
				array('OrderKey','numerical','integerOnly' => true));
	}
	public function attributeLabels() {
		return array('Id' => 'Ключевое имя','Name' => 'Отображаемое название','Used' => 'Используется','OrderKey' => 'Порядок отображения');
	}
}
?>