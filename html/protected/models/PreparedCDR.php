<?php

/**
 * This is the model class for table "PreparedCDR".
 *
 * The followings are the available columns in table 'PreparedCDR':
 * @property string $prID
 * @property string $prNameSrc
 * @property string $prNameDst
 * @property string $prNameExtChan
 */
class PreparedCDR extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PreparedCDR the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PreparedCDR';
	}
        
        public function primaryKey() {
            return 'prID';
        }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prID', 'length', 'max'=>11),
			array('prNameSrc, prNameDst, prNameExtChan', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('prID, prNameSrc, prNameDst, prNameExtChan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prID' => 'Pr',
			'prNameSrc' => 'Pr Name Src',
			'prNameDst' => 'Pr Name Dst',
			'prNameExtChan' => 'Pr Name Ext Chan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('prID',$this->prID,true);
		$criteria->compare('prNameSrc',$this->prNameSrc,true);
		$criteria->compare('prNameDst',$this->prNameDst,true);
		$criteria->compare('prNameExtChan',$this->prNameExtChan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}