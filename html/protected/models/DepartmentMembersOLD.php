<?php
class DepartmentMembers extends CActiveRecord {
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'DepartmentMembers';
	}
	public function rules() {
		return array(
				array('dpDepartments_dID, dpChannels_cID','required'),
				array('dpDepartments_dID','numerical','integerOnly' => true),
				array('dpChannels_cID','length','max' => 11),
				array('dpDepartments_dID, dpChannels_cID','safe','on' => 'search'));
	}
	public function relations() {
		return array();
	}
	public function attributeLabels() {
		return array('dpDepartments_dID' => 'Dp Departments D','dpChannels_cID' => 'Dp Channels C');
	}
	public function search() {
		$criteria = new CDbCriteria();
		
		$criteria->compare('dpDepartments_dID', $this->dpDepartments_dID);
		$criteria->compare('dpChannels_cID', $this->dpChannels_cID, true);
		
		return new CActiveDataProvider($this, array('criteria' => $criteria));
	}
	public static function getAgentsChannels($dep) {
		$depItems = Yii::app()->db->createCommand()->select('dpChannels_cID')->from('DepartmentMembers')->where('dpDepartments_dID=:id', array(
				':id' => $dep))->queryAll();
		$res = array();
		foreach ( $depItems as $chan )
			$res[] = $chan['dpChannels_cID'];
		return $res;
	}
	public static function getAgChanCompare($dep) {
		$data = self::getAgentsChannels($dep);
		return count($data) ? $data : 'NULL';
	}
	public static function getPopDepartments() {
		return Yii::app()->db->createCommand()->select('dep.dID, dep.dName')->from('DepartmentMembers as main')->join('Departments as dep', 'dep.dID = main.dpDepartments_dID')->where('dep.dGroups_gID=:gid', array(
				':gid' => Yii::app()->user->getState('Groups_gID')))->group('dpDepartments_dID')->queryAll();
	}
}