<?php
class Users extends CActiveRecord {
	// для связки депюзерс
	public $checked = false;
	// привязан к отделу
	public $wDep = null;
	// права на все права
	public $wAR = null;
	// для привязки по умолчанию(при добалении)
	public $_DefDepartment = null;
	public function GetGGGN() {
		// от автоподстановки хрома
		return $this->uName;
	}
	public function SetGGGN($value) {
		return $this->uName = $value;
	}
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'Users';
	}
	public function rules() {
		return array(
				array('uIdentity, uPassword','required'),
				array('uName','length','max' => 255),
				array('uIdentity','length','max' => 80),
				array('uIdentity','unique'),
				array('uIdentity','match','pattern' => '/[a-zA-Z]+[0-9]*/'),
				array('uPassword','length','max' => 64),
				array('uPassword','length','min' => 8),
				array('uSalt','length','max' => 6),
				array('IsRole','length','max' => 1),
				array('uIdentity, uName, upassword_attrib, GGGN','safe'),
				array('uSalt, uPassword','unsafe'),
				array('checked','safe'),
				array('wDep','safe'),
				array('wAR','safe'),
				array('_DefDepartment','safe'),
				array('uID, uName, uIdentity, IsRole','safe','on' => 'search'));
	}
	public function attributeLabels() {
		return array(
				'uID' => 'UID',
				'uName' => 'Имя',
				'GGGN' => 'Имя',
				'uIdentity' => 'Логин',
				'uPassword' => 'Пароль',
				'IsRole' => 'Роль(набор привилегий для агента)',
				'_DefDepartment' => Yii::t('m', '_DefDepartment'),
				'uSalt' => 'USalt');
	}
	public function defaultScope() {
		$DepIds = yii::app()->user->getState('depIds');
		if (($DepIds != '*') && ($DepIds != ''))
			$depCond = '(exists(select 1 from DepartmentUsers where uID=UserId and DepartmentId in (' . $DepIds . '))) ';
		else
			$depCond = '(1=1) ';
		
		if ($DepIds != '*') {
			$depCond .= ' and (IsRole=0)';
		}
		$wDep = '(select 1 from `DepartmentUsers` where `UserId` = `uID` limit 1) wDep';
		$wAR = '(select 1 from `UsersRights` ur where ur.`Name`= "All" and ur.`UserId`=`uID` limit 1) wAR';
		return array('select' => '*, ' . $wDep . ', ' . $wAR,'condition' => $depCond,'order' => 'IsRole, uName');
	}
	protected function afterSave() {
		parent::afterSave();
		if ($this->isNewRecord) {
			$command = Yii::app()->db->createCommand('insert into `UsersRights`(`UserId`,`Name`) values (' . $this->uID . ', "ViewStat");');
			$command->execute();
			$DepIds = yii::app()->user->getState('depIds');
			if ($this->IsRole == 0)
				if ($DepIds != '*') {
					$DepIdsArr = explode(',', $DepIds);
					foreach ( $DepIdsArr as $DepId ) {
						$command = Yii::app()->db->createCommand('insert into `DepartmentUsers`(`UserId`,`DepartmentId`) values (' . $this->uID . ', ' . $DepId . ');');
						$command->execute();
					}
				} else {
					if ($this->_DefDepartment != null) {
						$command = Yii::app()->db->createCommand('insert into `DepartmentUsers`(`UserId`,`DepartmentId`) values (' . $this->uID . ', ' . $this->_DefDepartment . ');');
						$command->execute();
					}
				}
		}
	}
	public function setUpassword_attrib($val) {
		$this->uSalt = generateSalt();
		$this->uPassword = md5($this->uSalt) . md5($val . md5($this->uSalt));
	}
}