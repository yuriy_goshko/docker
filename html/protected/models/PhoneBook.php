<?php

/**
 * This is the model class for table "PhoneBook".
 *
 * The followings are the available columns in table 'PhoneBook':
 * @property string $pbID
 * @property string $pbNumber
 * @property string $pbOwner
 * @property string $pbBlacked
 * @property string $pbReason
 */
class PhoneBook extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PhoneBook the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'PhoneBook';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {

// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('pbNumber, pbOwner', 'required'),
            array('pbOwner', 'length', 'max' => 40),
            array('pbBlacked', 'length', 'max' => 3),
            array('pbNumber', 'length', 'max' => 13),
            array('pbNumber', 'numerical'),
            array('pbReason', 'safe'),
        	array('pbNumber', 'unique'),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('pbID, pbNumber, pbOwner, pbBlacked, pbReason', 'safe', 'on' => 'search'),
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'pbNumber' => Yii::t('m', 'Telephone number'),
            'pbOwner' => Yii::t('m', 'Name'),
            'pbBlacked' => Yii::t('m', 'Blacklist'),
            'pbReason' => Yii::t('m', 'The reason for blocking')
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
// Warning: Please modify the following code to remove attributes that
// should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('pbID', $this->pbID, true);
        $criteria->compare('pbNumber', $this->pbNumber, true);
        $criteria->compare('pbOwner', $this->pbOwner, true);
        if ($this->pbBlacked != "-1")
            $criteria->compare('pbBlacked', $this->pbBlacked, true);
        $criteria->compare('pbReason', $this->pbReason, true);

        return $criteria;
    }

    public function scopes() {
        return array(
            'mine' => array()
        );
    }

}