<?php

/**
 * This is the model class for table "CallInfo".
 *
 * The followings are the available columns in table 'CallInfo':
 * @property string $cID
 * @property integer $cGroups_gID
 * @property integer $cQueue
 * @property integer $cActiveAgent
 * @property string $cChannelChannels_cID
 * @property string $cDstchannelChannels_cID
 * @property string $cOutchanChannels_cID
 * @property string $cSrc
 * @property string $cDst
 * @property string $cDcontext
 * @property integer $cDuration
 * @property integer $cBillsec
 * @property string $cDisposition
 * @property string $cUniqueid
 * @property string $cCalldate
 * @property string $cDirection
 *
 * The followings are the available model relations:
 * @property Channels $cDstchannelChannelsC
 * @property Users $cUsersU
 * @property Channels $cChannelChannelsC
 */
class CallInfo extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallInfo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getMaxDateWithOffset($exp, $type) {
        return Yii::app()->db->createCommand()
                        ->select('DATE_SUB(MAX(`cCalldate`), INTERVAL ' . $exp . ' ' . $type . ') ')
                        ->from('CallInfo')
                        ->queryScalar();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'CallInfo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cDirection', 'required'),
            array('cGroups_gID, cDuration, cBillsec', 'numerical', 'integerOnly' => true),
            array('cChannelChannels_cID, cDstchannelChannels_cID', 'length', 'max' => 11),
            array('cSrc, cDst, cDcontext', 'length', 'max' => 80),
            array('cDisposition', 'length', 'max' => 45),
            array('cUniqueid', 'length', 'max' => 32),
            array('cCalldate', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('cID, cGroups_gID, cOutchanChannels_cID, cChannelChannels_cID, cDstchannelChannels_cID, cSrc, cDst, cDcontext, cDuration, cBillsec, cDisposition, cUniqueid, cCalldate, cDirection', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'prepared' => array(self::BELONGS_TO, 'PreparedCDR', array('cID' => 'prID')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'cSrc' => mb_ucfirst(Yii::t('callinfo', 'outbound number')),
            'cDst' => mb_ucfirst(Yii::t('callinfo', 'destination number')),
            'cDuration' => mb_ucfirst(Yii::t('callinfo', 'duration')),
            'cBillsec' => mb_ucfirst(Yii::t('callinfo', 'billsec')),
            'cDisposition' => mb_ucfirst(Yii::t('callinfo', 'status')),
            'cCalldate' => mb_ucfirst(Yii::t('callinfo', 'date')),
            'cDirection' => mb_ucfirst(Yii::t('callinfo', 'call direction')),
            'cOutchanChannels_cID' => mb_ucfirst(Yii::t('callinfo', 'external channel')),
            'cQueue' => mb_ucfirst(Yii::t('callinfo', 'queue')),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($form = false) {

        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        if ($form->cfAgent != '-1') {
            $this->dbCriteria->compare('cDstchannelChannels_cID', $form->cfAgent);
            $this->dbCriteria->compare('cChannelChannels_cID', $form->cfAgent, false, 'OR');
        }
        if ($form->cfOutchan != '-1') {
            $this->dbCriteria->compare('cOutchanChannels_cID', $form->cfOutchan);
        }
        if ($form->cfQueue != '-1') {
            $this->dbCriteria->compare('cQueue', $form->cfQueue);
        }
        if ($form->cfSrc != false)
            $this->dbCriteria->addCondition(implode(' OR ', array('cSrc LIKE ' . Yii::app()->db->quoteValue('%' . $form->cfSrc . '%'), 'cDst LIKE ' . Yii::app()->db->quoteValue('%' . $form->cfSrc . '%'))));

        if ($form->cfStartDate != false)
            $this->dbCriteria->compare('cCalldate', '>=' . $form->cfStartDate);
        if ($form->cfEndDate != false)
            $this->dbCriteria->compare('cCalldate', '<=' . $form->cfEndDate);

        if ($form->cfDirection != '-1')
            $this->dbCriteria->compare('cDirection', $form->cfDirection);

        if ($form->cfDisposition != '-1')
            $this->dbCriteria->compare('cDisposition', $form->cfDisposition);

        if ($form->cfDepartmentMb != '-1') {
            $values = array();
            $items = is_array($form->cfDepartmentMb) ? $form->cfDepartmentMb : array($form->cfDepartmentMb);
            foreach ($items as $depId) {
                if (count(DepartmentMembers::getAgentsChannels($depId)) != false) {
                    $values = array_unique(array_merge(DepartmentMembers::getAgentsChannels($depId), $values));
                }
            }
            if (count($values != false))
                $this->dbCriteria->addCondition(
                        'cActiveAgent IN (' . implode(', ', $values) . ')'
                );
        }
    }

}
