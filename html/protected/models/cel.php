<?php

/**
 * asteriskCel AR model.
 * 
 * @property id
 * @property eventtype
 * @property eventtime
 * @property userdeftype
 * @property cid_name
 * @property cid_num
 * @property cid_ani
 * @property cid_rdnis
 * @property cid_dnid
 * @property exten
 * @property context
 * @property channame
 * @property appname
 * @property appdata
 * @property accountcode
 * @property peeraccount
 * @property uniqueid
 * @property linkedid
 * @property amaflags
 * @property userfield
 * @property peer
 * @property extra
 * @property causeCode
 * @property causeChanname
 * @property callStatus
 * @property linkedLid
 * @property linkedRid
 */
class cel extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Version the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cel';
    }

    public function scopes() {
        return array(
            'status' => array(
                'order' => 'id DESC',
                'condition' => "eventtype = 'HANGUP'",
            ),
            'guid' => array(
                'condition' => 'eventtype = :eventType',
                'params' => array(':eventType' => 'GUID')
            )
        );
    }

    public function call($lid, $rid) {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'linkedLid = :lid AND linkedRid = :rid',
            'params' => array(':lid' => $lid, ':rid' => $rid)
        ));
        return $this;
    }

    /**
     * Protected to write. Table is read only.
     * 
     * @return boolean
     */
    protected function beforeSave() {
        return false;
    }

}
