<?php
class Depagents_Controller extends RenderfileController {
	private $tmpAgents = array();
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rEditAgents")'),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/depagents.js', CClientScript::POS_BEGIN);
		$model = Departments::model();
		$rows = $model->findAll();
		if ($rows == null)
			Yii::app()->user->setFlash('info', "Еще нет записей");
		$AgentsDefaultScope = Agents::model()->defaultScope();
		
		$dataProvider = new CActiveDataProvider(Agents::model(), array(
				'criteria' => array(
						'select' => $AgentsDefaultScope['select'] . ' , da.DepartmentId as checked',
						'join' => 'LEFT JOIN `DepartmentAgents` `da` ON (da.SIPId = t.id) AND `da`.DepartmentId = :depId'),
				'sort' => array(
						'attributes' => array('*','checked' => array('asc' => 'checked asc, t.`name`','desc' => 'checked desc, t.`name`')),
						'defaultOrder' => array('checked' => true)),
				'pagination' => false));
		$page = dirname(__FILE__) . '/depagents_page.php';
		$this->render($page, array('model' => $rows,'dataProvider' => $dataProvider));
	}
	private function CheckAgentsNoDeps($AfterUpdate, $DepId) {
		if ($AfterUpdate) {
			if (empty($this->tmpAgents))
				return true;
			$sql = 'select s.`name`, count(da.`SIPId`) from `SIP` s
			left join `DepartmentAgents` da on da.`SIPId`=s.`id`
			where s.`id` in (' . implode($this->tmpAgents, ',') . ')
			group by 1 having count(da.`SIPId`) = 0';
			$command = Yii::app()->db->createCommand($sql);
			$this->tmpAgents = $command->queryColumn();
			if (! empty($this->tmpAgents))
				throw new Exception('Нельзя оставлять агентов без отделов: ' . chr(10) . implode(', ', $this->tmpAgents));
		} else {
			$sql = 'select SIPId from DepartmentAgents where DepartmentId = :depId order by SIPId';
			$command = Yii::app()->db->createCommand($sql);
			$command->bindParam(":depId", $DepId, PDO::PARAM_INT);
			$this->tmpAgents = $command->queryColumn();
		}
	}
	public function actionAddedit($id = false) {
		if (Yii::app()->request->isAjaxRequest) {
			$ids = Yii::app()->request->getParam('ids');
			$cat = Yii::app()->request->getParam('cat');
			
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if ($cat === null)
					throw new CHttpException(404, 'cat is null.');
				
				$this->CheckAgentsNoDeps(False, $cat);
				$sql = 'delete from DepartmentAgents where DepartmentId = :depId';
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(":depId", $cat, PDO::PARAM_INT);
				$command->execute();
				
				if (is_array($ids)) {
					foreach ( $ids as $v ) {
						$sql = 'insert into DepartmentAgents(DepartmentId, SIPId) values (:pDepartmentId, :pSIPId);';
						$command = Yii::app()->db->createCommand($sql);
						$command->bindParam(":pDepartmentId", $cat, PDO::PARAM_INT);
						$command->bindParam(":pSIPId", $v, PDO::PARAM_INT);
						$command->execute();
					}
				}
				$this->CheckAgentsNoDeps(True, $cat);
				$transaction->commit();
			} catch ( Exception $e ) {
				$transaction->rollback();
				echo json_encode(array($e->getMessage(),'error'));
				return;
			}
			echo json_encode(array(Yii::t('m', 'The operation completed successfully'),'info'));
		} else {
			$this->redirect($this->redirectUrl);
		}
	}
	protected function getArrayFromAR($model = false, $dataProvider, $id) {
		$res = array();
		$tId = Yii::app()->request->getParam('tabid');
		$page = dirname(__FILE__) . '/depagents_patialtab.php';
		if (count($model) != false) {
			foreach ( $model as $k => $v ) {
				$dataProvider->criteria->params = array('depId' => $v->dID);
				$dataProvider->getData(true);
				$dataProvider->getKeys(true); // необходимо пересчитать ключи для редактирования!!!
				$res[$k] = array(
						'label' => $v->dName,
						'id' => $v->dKeyName,
						'content' => $this->renderPartial($page, array('dataProvider' => $dataProvider,'dep' => $v), true));
				if ((($tId === null) && ($k == 0)) || ($tId == $v->dKeyName)) {
					$res[$k]['active'] = true;
				}
			}
		}
		return $res;
	}
}