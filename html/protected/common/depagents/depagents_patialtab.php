<?
$this->widget('bootstrap.widgets.TbGridView', array(
		'dataProvider' => $dataProvider,
		'template' => '{items}',
		'enableHistory' => true,
		'enableSorting' => true,
		'selectableRows' => 0,
		'id' => 'gm_' . $dep->dID,
		'rowCssClassExpression' => '$data["wDep"] != 1 ? "c_nodep" : ""',
		'afterAjaxUpdate' => 'js:init',
		'columns' => array(
				'name',
				array(
						'header' => Agents::model()->getAttributeLabel("fullname"),
						'name' => "fullname",
						'cssClassExpression' => '"fullname"'),
				array(
						'class' => 'CCheckBoxColumn',
						'selectableRows' => 2,
						'name' => 'Чек',
						'headerTemplate' => '<div data-toggle = "tooltip" data-placement="right" data-title="' . Yii::t('m', 'Select all members') . '">{item}</div>',
						'checkBoxHtmlOptions' => array(
								'data-toggle' => 'tooltip',
								'data-placement' => 'right',
								'data-title' => Yii::t('m', 'Select an item')),
						'id' => 'gm' . $dep->dID . '_' . 'membertd',
						'checked' => function ($data, $row, $column) use($dep) {
							return $data->checked;
						}))));
?>

