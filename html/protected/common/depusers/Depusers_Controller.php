<?php
class Depusers_Controller extends RenderfileController {
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rEditUsers")'),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/depusers.js', CClientScript::POS_BEGIN);
		$model = Departments::model();
		$rows = $model->findAll();
		if ($rows == null)
			Yii::app()->user->setFlash('info', "Еще нет записей");
		$UserDefaultScope = Users::model()->defaultScope();
		
		$dataProvider = new CActiveDataProvider('Users', array(
				'criteria' => array(
						'select' => $UserDefaultScope['select'] . ', du.DepartmentId as checked',
						'join' => 'LEFT JOIN `DepartmentUsers` `du` ON (du.UserId = t.uID) AND `du`.DepartmentId = :depId'),
				'sort' => array(
						'attributes' => array('*','checked' => array('asc' => 'checked asc, uIdentity','desc' => 'checked desc, uIdentity')),
						'defaultOrder' => array('checked' => true)),
				'pagination' => false));
		$page = dirname(__FILE__) . '/depusers_page.php';
		$this->render($page, array('model' => $rows,'dataProvider' => $dataProvider));
	}
	public function actionAddedit($id = false) {
		if (Yii::app()->request->isAjaxRequest) {
			$ids = Yii::app()->request->getParam('ids');
			$cat = Yii::app()->request->getParam('cat');
			
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if ($cat === null)
					throw new CHttpException(404, 'cat is null.');
				
				$sql = 'delete from DepartmentUsers where  DepartmentId = :depId';
				$DepIds = yii::app()->user->getState('depIds');
				if ($DepIds != '*')
					$sql .= ' and DepartmentId in (' . $DepIds . ')';
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(":depId", $cat, PDO::PARAM_INT);
				$command->execute();
				
				if (is_array($ids)) {
					foreach ( $ids as $v ) {
						$sql = 'insert into DepartmentUsers(DepartmentId, UserId) values (:pDepartmentId, :pUserId);';
						$command = Yii::app()->db->createCommand($sql);
						$command->bindParam(":pDepartmentId", $cat, PDO::PARAM_INT);
						$command->bindParam(":pUserId", $v, PDO::PARAM_INT);
						$command->execute();
					}
				}
				$transaction->commit();
			} catch ( Exception $e ) {
				$transaction->rollback();
				echo json_encode(array($e->getMessage(),'error'));
				return;
			}
			echo json_encode(array(Yii::t('m', 'The operation completed successfully'),'info'));
		} else {
			$this->redirect($this->redirectUrl);
		}
	}
	protected function getArrayFromAR($model = false, $dataProvider, $id) {
		$res = array();
		$tId = Yii::app()->request->getParam('tabid');
		$page = dirname(__FILE__) . '/depusers_patialtab.php';
		if (count($model) != false) {
			foreach ( $model as $k => $v ) {
				$dataProvider->criteria->params = array('depId' => $v->dID);
				$dataProvider->getData(true);
				$dataProvider->getKeys(true); // необходимо пересчитать ключи для редактирования!!!
				$res[$k] = array(
						'label' => $v->dName,
						'id' => $v->dKeyName,
						'content' => $this->renderPartial($page, array('dataProvider' => $dataProvider,'dep' => $v), true));
				if ((($tId === null) && ($k == 0)) || ($tId == $v->dKeyName))
					$res[$k]['active'] = true;
			}
		}
		return $res;
	}
}