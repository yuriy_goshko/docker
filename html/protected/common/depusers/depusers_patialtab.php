<?
$this->widget('bootstrap.widgets.TbGridView', array(
		'dataProvider' => $dataProvider,
		'template' => '{items}',
		'enableHistory' => true,
		'enableSorting' => true,
		'selectableRows' => 0,
		'id' => 'gm_' . $dep->dID,
		'afterAjaxUpdate' => 'js:init',
		'columns' => array(
				array(
						'header' => Users::model()->getAttributeLabel("uIdentity"),
						'name' => "uIdentity",
					//	'cssClassExpression' => '($data["wDep"] != 1 ? "c_nodep" : "")' . '.($data["wAR"] == 1 ? " c_superuser" : "")'
					),
				array(
						'header' => Users::model()->getAttributeLabel("uName"),
						'name' => "uName",
						'cssClassExpression' => '($data["wDep"] != 1 ? "c_nodep" : "")' . '.($data["wAR"] == 1 ? " c_superuser" : "")'),
				array(
						'class' => 'CCheckBoxColumn',
						'selectableRows' => 2,
						'name' => 'Чек',
						'headerTemplate' => '<div data-toggle = "tooltip" data-placement="right" data-title="' . Yii::t('m', 'Select all members') . '">{item}</div>',
						'checkBoxHtmlOptions' => array(
								'data-toggle' => 'tooltip',
								'data-placement' => 'right',
								'data-title' => Yii::t('m', 'Select an item')),
						'id' => 'gm' . $dep->dID . '_' . 'membertd',
						'checked' => function ($data, $row, $column) use($dep) {
							return $data->checked;
						}))));
?>

