<?php
class DepartmentsController extends RenderfileController {
	/*public $redirectUrl;
	public function Init() {
		$this->redirectUrl = '/' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/';
	}*/
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(
				array('deny','users' => array('*'),'actions' => array('add','delete'),'expression' => '$user->getState("isRoot_") != true'),
				array('allow','users' => array('@'),'expression' => '$user->getState("rEditDepartments")'),
				array('deny','users' => array('*')));
	}
	public function actionIndex() {
		$model = Departments::model();
		$dataProvider = new CActiveDataProvider('Departments', array(
				'pagination' => array('pageSize' => wu::getRequestPagPageValue('depsPGS')),
				'sort' => array('defaultOrder' => 'dName')));
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/dep.js', CClientScript::POS_BEGIN);
		$dep_page = dirname(__FILE__) . '/dep_page.php';
		$this->render($dep_page, array('dataProvider' => $dataProvider,'model' => $model));		
	}
	public function AddEdit($id = false) {
		$model = $id != false ? $this->loadModel($id) : new Departments();
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_REQUEST[get_class($model)];
			if ($model->validate() && $model->save()) {
				$id == false ? Yii::app()->user->setFlash('success', "Успешно добавлен!") : Yii::app()->user->setFlash('success', "Успешно обновлен!");
			} else {
				Yii::app()->user->setFlash('error', $model);
			}
		}
		$this->redirect($this->redirectUrl);
	}
	public function actionAdd($id = false) {
		$this->AddEdit($id);
	}
	public function actionUpdate($id) {
		$this->AddEdit($id);
	}
	public function actionDelete($id) {
		$this->loadModel($id)->delete();
		if (! isset($_GET['ajax']))
			$this->redirect($this->redirectUrl);
	}
	public function loadModel($id) {
		$model = Departments::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
}