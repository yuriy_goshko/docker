
$(document).ready(function(){
    action = $('#dep').attr('action');
    $('.addpbn').click(function(e){
    	$('#edit').removeClass('hide');
        e.preventDefault();        
        data =  pGVfields('dep', 'Departments_', this);
        $('#dColor').val(data['dColor']);
        $('#dep button[type="submit"]').html(buttons.upd);
    }); 
    
     $('#reset').click(function(e){
    	 $('#edit').addClass('hide');
         $('#dep').attr('action', action);
         $('#dep button[type="submit"]').html(buttons.add);
     });
    
});

$('#dep').submit(function(e){
    e.preventDefault();
    $.post($(this).attr('action'), $(this).serializeArray(), function(){
        
    });
    
});