<?php
$isRoot_ = yii::app()->user->getState("isRoot_");
?>
<div class="form">
    <?php
				$this->widget('bootstrap.widgets.TbAlert', array(
						'block' => true,
						'fade' => true,
						'closeText' => '&times;',
						'alerts' => array('success' => array('block' => true,'fade' => true,'closeText' => '&times;'))));
				?>
    <div class="row">
        <?php
								$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
										'id' => 'dep',
										'enableClientValidation' => true,
										'clientOptions' => array('validateOnSubmit' => true),
										'method' => 'post',
										'action' => $this->redirectUrl . 'add'));
								?>

        <? if (Yii::app()->user->hasFlash('error')): ?>
            <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
        <? endif; ?>

        <div class="row">
        	<? if ($isRoot_): ?>
			 <div class="span3">
                 <?=$form->textFieldRow($model, 'dKeyName', array('value' => $model->dKeyName))?>                    
             </div>
            <? endif; ?>
			<div class="span3">
                <?=$form->textFieldRow($model, 'dName', array('value' => $model->dName))?>                    
            </div>
			<div class="span3">
			<?php echo $form->labelEx($model, 'dColor', array()); ?>                      
            <?php $form->widget('bootstrap.widgets.TbColorPicker', array('name' => 'Departments[dColor]', 'value' => $model->dColor));?>
			<?php echo $form->error($model, 'dColor'); ?>										
			</div>
		</div>

		<div class="row buttons">
			<div class="span3">
                <?php
																if ($isRoot_)
																	$EditOptions = array();
																else
																	$EditOptions = array('id' => 'edit','class' => 'hide');
																$this->widget('bootstrap.widgets.TbButton', array(
																		'buttonType' => 'submit',
																		'type' => 'primary',
																		'htmlOptions' => $EditOptions,
																		'label' => 'Добавить'));
																?>
                <?php
																$this->widget('bootstrap.widgets.TbButton', array(
																		'buttonType' => 'reset',
																		'label' => 'Сброс',
																		'htmlOptions' => array('id' => 'reset')));
																?>
            </div>
		</div>

    <?php $this->endWidget(); ?>
    </div>
</div>
<div class="row">
    <?php
				$btnTemplate = '{update}';
				if ($isRoot_)
					$btnTemplate .= ' {delete}';
				$gridId = 'deps-grid';
				$this->widget('bootstrap.widgets.TbGridView', array(
						'dataProvider' => $dataProvider,
						'enableHistory' => true,
						'enableSorting' => true,
						'ajaxUpdate' => false,
						'id' => $gridId,
						'columns' => array(
								array('name' => "dKeyName",'cssClassExpression' => '"dKeyName"','visible' => $isRoot_),
								array('name' => "dName",'cssClassExpression' => '"dName"'),
								array(
										'name' => "dColor",
										'cssClassExpression' => '"dColor"',
										'headerHtmlOptions' => array("style" => "display:none"),
										'htmlOptions' => array("style" => "display:none")),
								array(
										'name' => "dColor",
										'type' => 'raw',
										'value' => function ($data) {
											return CHtml::tag('color ', array('style' => 'color:' . $data->dColor), $data->dColor);
										}),
								array(
										'header' => wu::pagPageSizeList($gridId, 'depsPGS'),
										'class' => 'CButtonColumn',
										'template' => $btnTemplate,
										'buttons' => array(
												'update' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-edit addpbn')),
												'delete' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-remove-circle')))))));
				
				?>
</div>