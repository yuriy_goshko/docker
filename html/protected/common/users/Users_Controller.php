<?php
class Users_Controller extends RenderfileController {	
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(
				// array('deny','users' => array('*'),'actions' => array('add','delete'),'expression' => '$user->getState("isRoot_") != true'),
				array('allow','users' => array('@'),'expression' => '$user->getState("rEditUsers")'),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		$model = Users::model();
		$model->scenario = 'securityAttrib';
		// $sort->applyOrder($crit);
		$dataProvider = new CActiveDataProvider('Users', array(
				'pagination' => array('pageSize' => wu::getRequestPagPageValue('usersPGS')),
				'sort' => array('defaultOrder' => 'uName')));
		
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/users.js', CClientScript::POS_BEGIN);
		$page = dirname(__FILE__) . '/users_page.php';
		$this->render($page, array('dataProvider' => $dataProvider,'model' => $model));
	}
	public function AddEdit($id = false) {
		$model = $id != false ? $this->loadModel($id) : new Users();
		$model->scenario = 'securityAttrib';
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_REQUEST[get_class($model)];
			if ($model->save()) {
				$id == false ? Yii::app()->user->setFlash('success', "Успешно добавлен!") : Yii::app()->user->setFlash('success', "Успешно обновлен!");
			} else {
				Yii::app()->user->setFlash('error', $model);
			}
		}
		$this->redirect($this->redirectUrl);
	}
	public function actionAdd($id = false) {
		$this->AddEdit($id);
	}
	public function actionUpdate($id) {
		$this->AddEdit($id);
	}
	public function actionDelete($id) {
		$this->loadModel($id)->delete();
		
		if (! isset($_GET['ajax']))
			$this->redirect($this->redirectUrl);
	}
	public function loadModel($id) {
		$model = Users::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
}