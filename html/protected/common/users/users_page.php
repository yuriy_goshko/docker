<?php
$isRoot_ = yii::app()->user->getState("isRoot_");
?>

<?php

$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => true,
		'fade' => true,
		'closeText' => '&times;',
		'alerts' => array('success' => array('block' => true,'fade' => true,'closeText' => '&times;'))));
?>
<div class="row">
	<div class="form">
    <?php
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
						'id' => 'users',
						'enableClientValidation' => true,
						'clientOptions' => array('validateOnSubmit' => true),
						'htmlOptions' => array('autocomplete' => 'off'),
						'method' => 'post',
						'action' => $this->redirectUrl . 'add'));
				
				?>
    
    <? if (Yii::app()->user->hasFlash('error')): ?>
        <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
    <? endif; ?>

    <div class="row">

			<div class="span3">
            <?=$form->textFieldRow($model, 'uIdentity', array('value' => $model->uIdentity));?>
        </div>
			<div class="span3">
            <?=$form->textFieldRow($model, 'uName', array('value' => $model->uName));?>
        </div>
			<div class="span3">			
            <?=$form->passwordFieldRow($model, 'uPassword', array('name' => 'Users[upassword_attrib]','id' => 'Users_uPassword'))?>
        </div>
        	<?php wu::getDepartmensCB($form, $model); ?>
        	
        	<? if ($isRoot_):  ?>
        	<div class="span3">        	
        	 <?=$form->dropDownListRow($model,'IsRole',array('0'=>'Нет','1'=>'Да'));?>        	          	         	 
        	</div>
        	    <? endif; ?>
        	        
		</div>

		<div class="row buttons">
			<div class="span3">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Добавить')); ?>
            <?php
												$this->widget('bootstrap.widgets.TbButton', array(
														'buttonType' => 'reset',
														'label' => 'Сброс',
														'htmlOptions' => array('id' => 'reset')));
												?>
        </div>
		</div>

    <?php $this->endWidget(); ?>
</div>
</div>
<div class="row">
    <?php
				$gridId = 'users-grid';
				$this->widget('bootstrap.widgets.TbGridView', array(
						'dataProvider' => $dataProvider,
						'ajaxUpdate' => false,
						'id' => $gridId,
						'rowCssClassExpression' => '($data["wDep"] != 1 ? "c_nodep" : "")' . '.($data["wAR"] == 1 ? " c_superuser" : "")',
						'columns' => array(
								array(
										'header' => Users::model()->getAttributeLabel("uIdentity"),
										'name' => "uIdentity",
										'cssClassExpression' => '"uIdentity"',
										'value' => '$data->uIdentity'),
								array(
										'header' => Users::model()->getAttributeLabel("uName"),
										'name' => "uName",
										'cssClassExpression' => '"uName"',
										'value' => '$data->uName'),
								array(
										'name' => "IsRole",
										'cssClassExpression' => '"IsRole"',
										'htmlOptions' => array('style' => 'display:none'),
										'headerHtmlOptions' => array('style' => 'display:none')),
								array(
										// 'header' => 'Действия',
										'header' => wu::pagPageSizeList($gridId, 'usersPGS'),
										'class' => 'CButtonColumn',
										'template' => '{update} {delete}',
										'buttons' => array(
												'update' => array('label' => '',
														// 'visible' => '$data["IsRole"] == 0',
														'imageUrl' => '','options' => array('class' => 'icon-edit addpbn')),
												'delete' => array('label' => '',
														// 'visible' => '$data["IsRole"] == 0',
														'imageUrl' => '','options' => array('class' => 'icon-remove-circle')))))));
				?>
</div>