<?php
class Depqueues_Controller extends RenderfileController {
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rEditDepartments")'),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/depqueues.js', CClientScript::POS_BEGIN);
		$model = Departments::model();
		$rows = $model->findAll();
		if ($rows == null)
			Yii::app()->user->setFlash('info', "Еще нет записей");
		$dataProvider = new CActiveDataProvider('Queue', array(
				'criteria' => array(
						'select' => 't.*, belongToDep.dqDepartments_dID as checked',
						'join' => 'LEFT OUTER JOIN `DepartmentQueues` `belongToDep` 
                            ON (belongToDep.dqQueues_qName = t.name) 
                            AND `belongToDep`.dqDepartments_dID = :depId'),
				'sort' => array(
						'attributes' => array('*','checked' => array('asc' => 'checked asc, t.name','desc' => 'checked desc, t.qName')),
						'defaultOrder' => array('checked' => true)),
				'pagination' => false));
		$page = dirname(__FILE__) . '/depqueues_page.php';
		$this->render($page, array('model' => $rows,'dataProvider' => $dataProvider));
	}
	/*	public function actionAddedit($id = false) {
	 if (Yii::app()->request->isAjaxRequest) {
	 $dis = Yii::app()->request->getParam('ids');
	 $cat = Yii::app()->request->getParam('cat');
	 $depmemb = DepartmentMembers::model();
	 $depmemb->deleteAll('dpDepartments_dID=:qname', array('qname' => $cat));
	 
	 if (is_array($dis)) {
	 foreach ( $dis as $v ) {
	 $depmemb = new DepartmentMembers();
	 $depmemb->attributes = array('dpDepartments_dID' => $cat,'dpChannels_cID' => SIP::model()->findByPk($v)->channels["cID"]);
	 $depmemb->save();
	 }
	 }
	 echo Yii::t('m', 'The operation completed successfully');
	 } else {
	 $this->redirect($this->redirectUrl);
	 }
	 }*/
	public function actionAddeditQueue($id = false) {
		if (Yii::app()->request->isAjaxRequest) {
			
			$dis = Yii::app()->request->getParam('ids');
			$cat = Yii::app()->request->getParam('cat');
			$depmemb = DepartmentQueues::model();
			$depmemb->deleteAll('dqDepartments_dID=:qname', array('qname' => $cat));
			
			if (is_array($dis)) {
				foreach ( $dis as $v ) {
					yii::log($v);
					$depmemb = new DepartmentQueues();
					$depmemb->attributes = array('dqDepartments_dID' => $cat,'dqQueues_qName' => Queue::model()->findByPk($v)->name);
					$depmemb->save();
				}
			}
			echo Yii::t('m', 'The operation completed successfully');
		} else {
			$this->redirect($this->redirectUrl);
		}
	}
	protected function getArrayFromAR($model = false, $dataProvider, $id) {
		$res = array();
		$tId = Yii::app()->request->getParam('tabid');
		$page = dirname(__FILE__) . '/depqueues_patialtab.php';
		if (count($model) != false) {
			foreach ( $model as $k => $v ) {
				$dataProvider->criteria->params = array('depId' => $v->dID);
				$dataProvider->getData(true);
				$dataProvider->getKeys(true); // необходимо пересчитать ключи для редактирования!!!
				$res[$k] = array(
						'label' => $v->dName,
						'id' => $v->dKeyName,
						'content' => $this->renderPartial($page, array('dataProvider' => $dataProvider,'dep' => $v), true));
				if ((($tId === null) && ($k == 0)) || ($tId == $v->dKeyName))
					$res[$k]['active'] = true;
			}
		}
		return $res;
	}
}