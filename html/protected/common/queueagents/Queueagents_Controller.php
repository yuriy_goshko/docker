<?php
class Queueagents_Controller extends RenderfileController {
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rEditQueues")'),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/queueagents.js', CClientScript::POS_BEGIN);
		$model = Queue::model();
		$rows = $model->findAll();
		if ($rows == null)
			Yii::app()->user->setFlash('info', "Еще нет записей");
		$page = dirname(__FILE__) . '/queueagents_page.php';
		$this->render($page, array('model' => $rows));
	}
	public function actionAddedit($id = false) {
		if (Yii::app()->request->isAjaxRequest) {
			$ids = Yii::app()->request->getParam('ids');
			$cat = Yii::app()->request->getParam('cat');
			
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if ($cat === null)
					throw new CHttpException(404, 'cat is null.');
				
				$sql = 'delete from QueueMembers where queue_name = :pQueueName';
				
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(":pQueueName", $cat, PDO::PARAM_STR);
				$command->execute();
				
				if (is_array($ids)) {
					foreach ( $ids as $v ) {
						$sql = 'insert into QueueMembers(interface, queue_name) values (:pInterface, :pQueueName);';
						$command = Yii::app()->db->createCommand($sql);
						$command->bindParam(":pInterface", $v, PDO::PARAM_STR);
						$command->bindParam(":pQueueName", $cat, PDO::PARAM_STR);
						$command->execute();
					}
				}
				$transaction->commit();
			} catch ( Exception $e ) {
				$transaction->rollback();
				echo json_encode(array($e->getMessage(),'error'));
				return;
			}
			echo json_encode(array(Yii::t('m', 'The operation completed successfully'),'info'));
		} else {
			$this->redirect($this->redirectUrl);
		}
	}
	protected function getArrayFromAR($model = false) {
		$tId = Yii::app()->request->getParam('tabid');
		$page = dirname(__FILE__) . '/queueagents_patialtab.php';
		$res = array();
		if (count($model) != false) {
			foreach ( $model as $k => $v ) {
				$dataProvider = new CActiveDataProvider(Agents::model(), array(
						'pagination' => false,
						'criteria' => array(
								'select' => 't.sip_name, t.name, t.fullname, IF(qm.uniqueid is null,0,1) as checked',
								'join' => 'LEFT JOIN `QueueMembers` `qm` ON (qm.interface = t.sip_name) 
                          		 AND `qm`.queue_name = ' . Yii::app()->db->quoteValue($v->name)),
						'keyAttribute' => 'sip_name',
						'sort' => array(
								'attributes' => array('*','checked' => array('asc' => 'checked asc, t.`name`','desc' => 'checked desc, t.`name`')),
								'defaultOrder' => array('checked' => true))));
				
				$itemOptions = array();
				$css = "";
				if ($v['wAg'] != '1')
					$css .= ' c_nodep';
				if ($css != "")
					$itemOptions = array('class' => $css);
				
				$res[$k] = array(
						'label' => $v->qName,
						'id' => $v->name,
						'itemOptions' => $itemOptions,
						'content' => $this->renderPartial($page, array('dataProvider' => $dataProvider,'queue' => $v), true));
				if ((($tId === null) && ($k == 0)) || ($tId == $v->name))
					$res[$k]['active'] = true;
			}
		}
		return $res;
	}
}