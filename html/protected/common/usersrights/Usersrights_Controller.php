<?php
class Usersrights_Controller extends RenderfileController {
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rEditUsers")'),array('deny','users' => array('*')));
	}
	function CurrentUserRightsCondition() {
		$AllRightsList = WebUser::$RightsList;
		$RightsList = array();
		foreach ( $AllRightsList as $rName ) {
			if (yii::app()->user->getState($rName) === true)
				$RightsList[] = substr($rName, 1);
		}
		if ($this->isRoot)
			$RightsList[] = 'All';
		if (count($RightsList) === 0)
			$cond = '(1=0)';
		else
			$cond = '(Name in ("' . implode($RightsList, '","') . '"))';
		return $cond;
	}
	public function actionIndex() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/usersrights.js', CClientScript::POS_BEGIN);
		
		$UserDefaultScope = Users::model()->defaultScope();
		
		$sql = 'select ' . $UserDefaultScope['select'] . 
		// 'select u.`uID`, u.`uIdentity`, u.`uName`,
		// (select 1 from `UsersRights` ur where ur.`Name`="All" and ur.`UserId`=u.`uID` limit 1) su
		' 
				from `Users` u 
				where u.`uIdentity` <> :pCurrUser 
				order by u.`uIdentity`';
		$command = Yii::app()->db->createCommand($sql);
		$un = Yii::app()->user->name;
		$command->bindParam(":pCurrUser", $un, PDO::PARAM_STR);
		$Users = $command->queryAll();
		if ($Users == null)
			Yii::app()->user->setFlash('info', "Еще нет пользователей");
		$dataProvider = new CArrayDataProvider($Users, array('keyField' => 'uID','pagination' => false));
		$page = dirname(__FILE__) . '/usersrights_page.php';
		$this->render($page, array('dataProvider' => $Users));
	}
	public function actionAddedit($id = false) {
		if (Yii::app()->request->isAjaxRequest) {
			$ids = Yii::app()->request->getParam('ids');
			$cat = Yii::app()->request->getParam('cat');
			
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if ($cat === null)
					throw new CHttpException(404, 'cat is null.');
				$sql = 'delete from UsersRights where UserId = :pUserId and ' . $this->CurrentUserRightsCondition();
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(":pUserId", $cat, PDO::PARAM_INT);
				$command->execute();
				
				if (is_array($ids)) {
					foreach ( $ids as $v ) {
						$sql = 'insert into UsersRights(UserId, Name) values (:pUserId, :pName);';
						$command = Yii::app()->db->createCommand($sql);
						$command->bindParam(":pUserId", $cat, PDO::PARAM_INT);
						$command->bindParam(":pName", $v, PDO::PARAM_INT);
						$command->execute();
					}
				}
				$transaction->commit();
			} catch ( Exception $e ) {
				$transaction->rollback();
				echo json_encode(array($e->getMessage(),'error'));
				return;
			}
			echo json_encode(array(Yii::t('m', 'The operation completed successfully'),'info'));
		} else {
			$this->redirect($this->redirectUrl);
		}
	}
	private function isVisibleRight($rName, $isRole) {
		//которые сам имеет (rViewStatOnlySelf - не исп,вероятно в буд.)
		$hr = (yii::app()->user->getState($rName) || ($isRole && ($rName = 'rViewStatOnlySelf') && yii::app()->user->getState('rViewStat')));
		$hr = ($hr || ($rName === 'rAll'));
		
		//актуальные(агент/юзер)
		if ($isRole)
			$b = in_array($rName, WebUser::$RightsAgentsFilter);
		else
			$b = ($rName != 'rViewStatOnlySelf');
		
		return ($b && $hr);
	}
	protected function getArrayFromAR($model = false) {
		$res = array();
		$tId = Yii::app()->request->getParam('tabid');
		$page = dirname(__FILE__) . '/usersrights_patialtab.php';
		if (count($model) != false) {
			foreach ( $model as $k => $v ) {
				$sql = 'select Name from UsersRights where UserId = ' . $v['uID'];
				$command = Yii::app()->db->createCommand($sql);
				$UserRights = $command->queryColumn();
				
				$data = array();
				$UserRightsList = WebUser::$RightsList;
				if ($this->isRoot)
					array_unshift($UserRightsList, 'rAll');
				foreach ( $UserRightsList as $rName ) {
					if ($this->isVisibleRight($rName, $v['IsRole'] == '1')) {
						$rName = substr($rName, 1);
						$data[] = array('rName' => $rName,'rDisplayName' => Yii::t('usersrights', $rName),'rChecked' => in_array($rName, $UserRights));
					}
				}
				$dataProvider = new CArrayDataProvider($data, array('keyField' => 'rName','pagination' => false));
				$label = ($v['uName'] === '') ? $v['uIdentity'] : $v['uIdentity'] . ' (' . $v['uName'] . ')';
				$itemOptions = array();
				$css = "";
				if ($v['wAR'] == '1')
					$css .= 'c_superuser';
				if ($v['wDep'] != '1')
					$css .= ' c_nodep';
				if ($css != "")
					$itemOptions = array('class' => $css);
				
				$res[$k] = array(
						'label' => $label,
						'id' => $v['uIdentity'],
						'itemOptions' => $itemOptions,
						'content' => $this->renderPartial($page, array('dataProvider' => $dataProvider,'urRec' => $v), true));
				if ((($tId === null) && ($k == 0)) || ($tId == $v['uIdentity']))
					$res[$k]['active'] = true;
			}
		}
		return $res;
	}
}