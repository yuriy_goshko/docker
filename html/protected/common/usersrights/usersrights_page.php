<?php
$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => true,
		'fade' => true,
		'closeText' => '&times;',
		'alerts' => array('info' => array('block' => true,'fade' => true,'closeText' => '&times;'))));

$this->widget('bootstrap.widgets.TbTabs', array(
		'type' => 'tabs',
		'id' => 'usersrights',
		'placement' => 'left',
		'events' => array('shown' => 'js:tbTabClick'),
		'tabs' => $this->getArrayFromAR($dataProvider)));
?>
