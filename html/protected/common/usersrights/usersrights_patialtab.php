<?
$isNotRoleOrCantEditAll = ($urRec['IsRole'] == '0') || (yii::app()->user->getState('isRoot_'));

$this->widget('bootstrap.widgets.TbGridView', array(
		'dataProvider' => $dataProvider,
		'template' => '{items}',
		'enableHistory' => true,
		'enableSorting' => true,
		'selectableRows' => 0,
		'id' => 'gm_' . $urRec['uID'],
		'afterAjaxUpdate' => 'js:init',
		'columns' => array(
				// array('header' => 'Название','name' => "rName",'cssClassExpression' => '"rName"'),
				array('header' => 'Название','name' => "rDisplayName",'cssClassExpression' => '"rDisplayName"'),
				array(
						'class' => 'CCheckBoxColumn',
						'selectableRows' => $isNotRoleOrCantEditAll ? 2 : 0,
						'name' => 'Чек',
						'headerTemplate' => '<div data-toggle = "tooltip" data-placement="right" data-title="' . Yii::t('m', 'Select all members') . '">{item}</div>',
						'checkBoxHtmlOptions' => array(
								'disabled' => $isNotRoleOrCantEditAll ? '' : 'disabled',
								'data-toggle' => 'tooltip',
								'data-placement' => 'right',
								'data-title' => Yii::t('m', 'Select an item')),
						'id' => 'gm' . $urRec['uID'] . '_' . 'membertd',
						'checked' => function ($data) {
							return $data['rChecked'];
						}))));

?>
