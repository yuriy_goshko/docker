<?php
class Agents_Controller extends RenderfileController {
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rEditAgents")'),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/agents.js', CClientScript::POS_BEGIN);
		$model = Agents::model();
		$dataProvider = new CActiveDataProvider('Agents', array(
				'pagination' => array('pageSize' => wu::getRequestPagPageValue('agentsPGS')),
				'sort' => array('defaultOrder' => 't.name')));
		
		$page = dirname(__FILE__) . '/agents_page.php';
		$this->render($page, array('dataProvider' => $dataProvider,'model' => $model));
	}
	public function actionAddedit($id = false) {
		$model = $id != false ? $this->loadModel($id) : new Agents();
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_REQUEST[get_class($model)];
			if ($model->validate() && $model->save())
				$id == false ? Yii::app()->user->setFlash('success', "Успешно добавлен!") : Yii::app()->user->setFlash('success', "Успешно обновлен!");
			else
				Yii::app()->user->setFlash('error', $model);
		}
		$this->redirect($this->redirectUrl);
	}
	public function actionDelete($id) {
		$this->loadModel($id)->delete();
		if (! isset($_GET['ajax']))
			$this->redirect($this->redirectUrl);
		// $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function loadModel($id) {
		$model = Agents::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
}