<?php
$user = Yii::app()->user;
?>

<div class="form">
<?php
$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => true, // display a larger alert block?
		'fade' => true, // use transitions?
		'closeText' => '&times;', // close link text - if set to false, no close link is displayed
		'alerts' => array( // configurations per alert type
'success' => array('block' => true,'fade' => true,'closeText' => '&times;'))));
?>

<div class="row">
   <?php
			$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
					'id' => 'agents',
					'enableClientValidation' => true,
					'clientOptions' => array('validateOnSubmit' => true),
					'method' => 'post',
					'action' => $this->redirectUrl . 'addedit'));
			?>
    
   <? if (Yii::app()->user->hasFlash('error')): ?>
   <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
   <? endif; ?>

    <div class="row">
			<div class="span3">
            <?=$form->textFieldRow($model, 'name', array('value' => $model->name));?>
        </div>
			<div class="span3">
            <?=$form->textFieldRow($model, 'fullname', array('value' => $model->fullname));?>
        </div>
			<div class="span3">
            <?=$form->textFieldRow($model, 'secret')?>
        </div>
        <?php wu::getRolesCB($form, $model); ?>
        	<?php wu::getDepartmensCB($form, $model); ?>       		
        	<div class="span3">        	
        	 <?=$form->dropDownListRow($model,'nat',array('no'=>'Нет','force_rport,comedia'=>'Да'));?>     	        	         	         	          	         	 
        </div>
		</div>


		<div class="row buttons">
			<div class="span3">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Добавить')); ?>            
            <?php
												$this->widget('bootstrap.widgets.TbButton', array(
														'buttonType' => 'reset',
														'label' => 'Сброс',
														'htmlOptions' => array('id' => 'reset')));
												?>
        </div>
		</div>
    <?php $this->endWidget(); ?>
</div>
</div>
<div class="row">
    <?php
				$ShowRolesField = wu::isUseAgentsRoles();
				$gridId = 'agents-grid';
				$this->widget('bootstrap.widgets.TbGridView', array(
						'dataProvider' => $dataProvider,
						'enableHistory' => true,
						'enableSorting' => true,
						'ajaxUpdate' => false,
						'id' => $gridId,
						'rowCssClassExpression' => '$data["wDep"] != 1 ? "c_nodep" : ""',
						'columns' => array(
								array(
										'header' => Agents::model()->getAttributeLabel("name"),
										'name' => "name",
										'cssClassExpression' => '"name"',
										'value' => '"$data->name"'),
								array(
										'header' => Agents::model()->getAttributeLabel("fullname"),
										'name' => "fullname",
										'cssClassExpression' => '"fullname"'),
								array(
										'name' => Agents::model()->getAttributeLabel("secret"),
										'cssClassExpression' => '"secret"',
										'value' => '"$data->secret"'),
								array(
										'name' => Agents::model()->getAttributeLabel("userRole"),
										'visible' => $ShowRolesField,
										'value' => '"$data->RoleName"'),
								array(
										'name' => Agents::model()->getAttributeLabel("userRole"),
										'cssClassExpression' => '"userRole"',
										'value' => '"$data->userRole"',
										'htmlOptions' => array('style' => 'display:none'),
										'headerHtmlOptions' => array('style' => 'display:none')),
								array(
										'header' => Agents::model()->getAttributeLabel("nat"),
										'name' => 'natChecked',
										'cssClassExpression' => '"natChecked"',
										'value' => '($data->nat=="force_rport,comedia")?"Да":"Нет"'),
								array(
										'header' => Agents::model()->getAttributeLabel("nat"),
										'name' => 'nat',
										'cssClassExpression' => '"nat"',
										'htmlOptions' => array('style' => 'display:none'),
										'headerHtmlOptions' => array('style' => 'display:none'),
										'value' => '"$data->nat"'),
								
								array(
										'header' => wu::pagPageSizeList($gridId, 'agentsPGS'),
										'class' => 'CButtonColumn',
										'template' => '{editpbn} {delete}',
										'buttons' => array(
												'editpbn' => array(
														'label' => '',
														'url' => function ($data) {
															return Yii::app()->urlManager->createUrl('/admin/agents/addedit/', array(
																	'id' => $data->id));
														},
														'options' => array('class' => 'icon-edit addpbn'),
														'visible' => '$data->id'),
												'delete' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-remove-circle')))))));
				?>
</div>