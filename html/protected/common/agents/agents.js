$(document).ready(function() {
	action = $('#agents').attr('action');
	$('.addpbn').click(function(e) {
		e.preventDefault();
		pGVfields('agents', 'Agents_', this);
		$('#agents button[type="submit"]').html(buttons.upd);
	});

	$('#reset').click(function(e) {
		$('#agents').attr('action', action);
		$('#agents button[type="submit"]').html(buttons.add);
	});
});

$('#agents').submit(function(e) {
	e.preventDefault();
	$.post($(this).attr('action'), $(this).serializeArray(), function() {
	});
});