<?php
Yii::import('application.extensions.stanislavkrsv-jouele-6fec871.Jouele');
$isRoot_ = yii::app()->user->getState("isRoot_");
?>
<div class="row">
<?php
$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => true,
		'fade' => true,
		'closeText' => '&times;',
		'alerts' => array('success' => array('block' => true,'fade' => true,'closeText' => '&times;'))));
?>
<div class="form">
    <?php
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
						'id' => 'queues',
						'enableClientValidation' => true,
						'clientOptions' => array('validateOnSubmit' => true),
						'method' => 'post',
						'action' => $this->redirectUrl . 'add',
						'htmlOptions' => array('enctype' => 'multipart/form-data')));
				?>
    
    <? if (Yii::app()->user->hasFlash('error')): ?>
        <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
    <? endif; ?>

    <div class="row">
    	<? if ($isRoot_): ?>
		<div class="span3">
            <?=$form->textFieldRow($model, 'name', array('value' => $model->name));?>
        </div>
        <? endif; ?>
		<div class="span3">
            <?=$form->textFieldRow($model, 'qName', array('value' => $model->qName));?>
        </div>
                <? if ($isRoot_): ?>
        <?php wu::getDepartmensCB($form, $model); ?>
        <? endif; ?>
		
		<div class="span3">
            <?=$form->fileFieldRow($model, 'musiconhold', array('value' => $model->musiconhold));?>
        </div>    
        
         <? if ($isRoot_): ?>
		<div class="span3" >
            <?=$form->textFieldRow($model, 'membermacro', array('value' => $model->membermacro));?>
        </div>        	
        	
         <? endif; ?>
        
		</div>


		<div class="row buttons">
			<div class="span3">
                <?php
																if ($isRoot_)
																	$EditOptions = array();
																else
																	$EditOptions = array('id' => 'edit','class' => 'hide');
																$this->widget('bootstrap.widgets.TbButton', array(
																		'buttonType' => 'submit',
																		'type' => 'primary',
																		'htmlOptions' => $EditOptions,
																		'label' => 'Добавить'));
																?>
                <?php
																$this->widget('bootstrap.widgets.TbButton', array(
																		'buttonType' => 'reset',
																		'label' => 'Сброс',
																		'htmlOptions' => array('id' => 'reset')));
																?>
            </div>
		</div>

    <?php $this->endWidget(); ?>
</div>
</div>
<div class="row">
    <?php
				$btnTemplate = '{update}';
				if ($isRoot_)
					$btnTemplate .= ' {delete}';
				$gridId = 'queues-grid';
				$this->widget('bootstrap.widgets.TbGridView', array(
						'dataProvider' => $dataProvider,
						'enableHistory' => true,
						'enableSorting' => true,
						'ajaxUpdate' => false,
						'id' => $gridId,
						'rowCssClassExpression' => '$data["wDep"] != 1 ? "c_nodep" : ""',
						'columns' => array(
								array(
										'header' => Queue::model()->getAttributeLabel("name"),
										'name' => "name",
										'cssClassExpression' => '"name"',
										'value' => '$data->name',
										'visible' => $isRoot_),
								array(
										'header' => Queue::model()->getAttributeLabel("qName"),
										'name' => "qName",
										'cssClassExpression' => '"qName"',
										'value' => '$data->qName'),
								array(
										'header' => Queue::model()->getAttributeLabel("membermacro"),
										'name' => "membermacro",
										'cssClassExpression' => '"membermacro"',
										'value' => '$data->membermacro'),
								array(
										'name' => Queue::model()->getAttributeLabel("musiconhold"),
										'cssClassExpression' => '"musiconhold"',
										'value' => function ($data, $row, $column) {
											if ($data->musiconhold === null) {
												return '';
											}
											$controller = $column->grid->owner;
											$controller->widget('Jouele', array(
													'file' => '/' . Queue::MOH_DIR . '/' . Queue::CLASS_DIR . '/' . $data->musiconhold . '/' . $data->qFileName,
													'htmlOptions' => array('class' => 'jouele-skin-silver')));
										}),
								array(
										'header' => wu::pagPageSizeList($gridId, 'queuesPGS'),
										'class' => 'CButtonColumn',
										'deleteConfirmation' => Yii::t('m', 'Are you sure you want to delete this queue?'),
										'template' => $btnTemplate,
										'buttons' => array(
												'update' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-edit addpbn')),
												'delete' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-remove-circle')))))));
				?>
</div>
