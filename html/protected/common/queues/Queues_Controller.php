<?php
class Queues_Controller extends RenderfileController {
	/*	public $redirectUrl;
	 public function Init() {
	 $this->redirectUrl = '/' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/';
	 }*/
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(
				array('deny','users' => array('*'),'actions' => array('add','delete'),'expression' => '$user->getState("isRoot_") != true'),
				array('allow','users' => array('@'),'expression' => '$user->getState("rEditQueues")'),
				array('deny','users' => array('*')));
	}
	public function actionIndex() {
		$model = Queue::model();
		$dataProvider = new CActiveDataProvider('Queue', array(
				'pagination' => array('pageSize' => wu::getRequestPagPageValue('queuesPGS')),
				'sort' => array('defaultOrder' => 't.qName')));
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/queues.js', CClientScript::POS_BEGIN);
		$page = dirname(__FILE__) . '/queues.php';
		$this->render($page, array('dataProvider' => $dataProvider,'model' => $model));
	}
	public function AddEdit($id = false) {
		$model = $id != false ? $this->loadModel($id) : new Queue();
		$mohQueuesPath = $_SERVER['DOCUMENT_ROOT'] . '/' . Queue::MOH_DIR . '/' . Queue::CLASS_DIR;
		if (Yii::app()->request->isPostRequest) {
			if (! isset($_REQUEST[get_class($model)])) {
				$queueValidators = $model->getValidators('musiconhold');
				$fileValidator = $queueValidators[1];
				$model->addError('musicclass', Yii::t('m', 'The file is too large. Its size cannot exceed "{limit}" bytes.', array(
						'{limit}' => $fileValidator->maxSize)));
				Yii::app()->user->setFlash('error', $model);
				$this->redirect($this->redirectUrl);
			}
			$model->attributes = $_REQUEST[get_class($model)];
			$file = CUploadedFile::getInstance($model, 'musiconhold');
			if ($file != false) {
				$cmd = 'avconv -i ' . $file->getTempName() . ' -ar 8000 -ab 16k -ac 1  ' . $file->getTempName() . '.wav';
				exec($cmd . " 2>&1", $out, $ret);
				if ($ret) {
					// echo "There was a problem!\n";
					print_r($out);
				} else {
					// echo "Everything went better than expected!\n";
					rename($file->getTempName() . '.wav', $file->getTempName());
				}
				// $model->ext = substr($file->name, strpos($file->name, '.'));
				$model->musiconhold = $file;
				if (! file_exists($mohQueuesPath))
					mkdir($mohQueuesPath);
			}
			if ($model->save()) {
				if ($file != false) {
					$model->qFileName = md5(microtime()) . '.' . $model->ext;
					delete($_SERVER['DOCUMENT_ROOT'] . '/' . Queue::MOH_DIR . '/' . Queue::CLASS_DIR . '/' . $model->name);
					$file->saveAs($mohQueuesPath . '/' . $model->name . '/' . $model->qFileName);
					$model->save();
				}
				
				if ($id == false) {
					Yii::app()->user->setFlash('success', "Успешно добавлен!");
				} else {
					Yii::app()->user->setFlash('success', "Успешно обновлен!");
				}
			} else {
				Yii::app()->user->setFlash('error', $model);
			}
		}
		$this->redirect($this->redirectUrl);
	}
	public function actionAdd($id = false) {
		$this->AddEdit($id);
	}
	public function actionUpdate($id) {
		$this->AddEdit($id);
	}
	public function actionDelete($id) {
		$this->loadModel($id)->delete();
		if (! isset($_GET['ajax']))
			$this->redirect($this->redirectUrl);
	}
	public function loadModel($id) {
		$model = Queue::model()->findByAttributes(array('name' => $id));
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
}