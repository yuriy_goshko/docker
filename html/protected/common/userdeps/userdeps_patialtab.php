<?
$this->widget('bootstrap.widgets.TbGridView', array(
		'dataProvider' => $dataProvider,
		'template' => '{items}',
		'enableHistory' => true,
		'enableSorting' => true,
		'selectableRows' => 0,
		'id' => 'gm_' . $usr->uID,
		'afterAjaxUpdate' => 'js:init',
		'columns' => array(
				array('name' => "dKeyName",'cssClassExpression' => '"dKeyName"'),
				array('name' => "dName",'cssClassExpression' => '"dName"'),
				array(
						'class' => 'CCheckBoxColumn',
						'selectableRows' => 2,
						'name' => 'Чек',
						'headerTemplate' => '<div data-toggle = "tooltip" data-placement="right" data-title="' . Yii::t('m', 'Select all members') . '">{item}</div>',
						'checkBoxHtmlOptions' => array(
								'data-toggle' => 'tooltip',
								'data-placement' => 'right',
								'data-title' => Yii::t('m', 'Select an item')),
						'id' => 'gm' . $usr->uID . '_' . 'membertd',
						'checked' => function ($data, $row, $column) use($usr) {
							return $data->checked;
						}))));
?>

