<?php
class Userdeps_Controller extends RenderfileController {
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rEditUsers")'),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__)) . '/userdeps.js', CClientScript::POS_BEGIN);
		$model = Users::model();
		$rows = $model->findAll();
		if ($rows == null)
			Yii::app()->user->setFlash('info', "Еще нет записей");
		
		$dataProvider = new CActiveDataProvider('Departments', array(
				'criteria' => array(
						'select' => 't.*, du.DepartmentId as checked',
						'join' => 'LEFT JOIN `DepartmentUsers` `du` ON (du.DepartmentId = t.dID) AND du.UserId = :UserId'),
				'sort' => array(
						'attributes' => array('*','checked' => array('asc' => 'checked asc, dName','desc' => 'checked desc, dName')),
						'defaultOrder' => array('checked' => true)),
				'pagination' => false));
		
		$page = dirname(__FILE__) . '/userdeps_page.php';
		$this->render($page, array('model' => $rows,'dataProvider' => $dataProvider));
	}
	public function actionAddedit($id = false) {
		if (Yii::app()->request->isAjaxRequest) {
			$ids = Yii::app()->request->getParam('ids');
			$cat = Yii::app()->request->getParam('cat');
			yii::log($ids);
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if ($cat === null)
					throw new CHttpException(404, 'cat is null.');
				
				//$sql = 'delete from DepartmentUsers where  DepartmentId = :depId';
				$sql = 'delete from DepartmentUsers where UserId = :usrId';
				$DepIds = yii::app()->user->getState('depIds');
				if ($DepIds != '*')
					$sql .= ' and DepartmentId in (' . $DepIds . ')';
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(":usrId", $cat, PDO::PARAM_INT);
				$command->execute();
				
				if (is_array($ids)) {
					foreach ( $ids as $v ) {
						$sql = 'insert into DepartmentUsers(DepartmentId, UserId) values (:pDepartmentId, :pUserId);';
						$command = Yii::app()->db->createCommand($sql);
						$command->bindParam(":pUserId", $cat, PDO::PARAM_INT);
						$command->bindParam(":pDepartmentId", $v, PDO::PARAM_INT);
						$command->execute();
					}
				}
				$transaction->commit();
			} catch ( Exception $e ) {
				$transaction->rollback();
				echo json_encode(array($e->getMessage(),'error'));
				return;
			}
			echo json_encode(array(Yii::t('m', 'The operation completed successfully'),'info'));
		} else {
			$this->redirect($this->redirectUrl);
		}
	}
	protected function getArrayFromAR($model = false, $dataProvider, $id) {
		$res = array();
		$tId = Yii::app()->request->getParam('tabid');
		$page = dirname(__FILE__) . '/userdeps_patialtab.php';		
		if (count($model) != false) {
			foreach ( $model as $k => $v ) {
				$dataProvider->criteria->params = array('UserId' => $v->uID);
				$dataProvider->getData(true);
				$dataProvider->getKeys(true); // необходимо пересчитать ключи для редактирования!!!
				
				$itemOptions = array();
				$css = "";
				if ($v['wAR'] == '1')
					$css .= 'c_superuser';
				if ($v['wDep'] != '1')
					$css .= ' c_nodep';
				if ($css != "")
					$itemOptions = array('class' => $css);
				
				$res[$k] = array(
						'label' => $v->uName,
						'id' => $v->uIdentity,
						'itemOptions' => $itemOptions,
						'content' => $this->renderPartial($page, array('dataProvider' => $dataProvider,'usr' => $v), true));
				if ((($tId === null) && ($k == 0)) || ($tId == $v->uIdentity)) {
					$res[$k]['active'] = true;
				}
			}
		}
		
		return $res;
	}
}