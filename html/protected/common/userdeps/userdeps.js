
$(document).ready(function() {
    
    init();
    
    $(document).on('click', "input:regex(id, ^gm\\d+_membertd_\\d+)", function(e) {
        setRowSelected(e.target);
        var id = $(e.target).attr('id').substring(0, $(e.target).attr('id').lastIndexOf('_'));
        updMembers(id);

    });

    $(document).on('click', 'input:regex(id, ^gm\\d+_membertd_all)', function(e) {
        $(e.target).closest('table').find('tbody').find('input:regex(id, ^gm\\d+_membertd_\\d+)').each(function(key, val) {
            $(val).attr('checked', e.target.checked);
            setRowSelected(val);
        });
        var id = $(e.target).attr('id').substring(0, $(e.target).attr('id').lastIndexOf('_'));
        updMembers(id);
    });


});
function updMembers(id) {
    var gridId = $('#userdeps .tab-content .active .grid-view').attr('id');
    var dep = gridId.substring(gridId.indexOf('_') + 1);
    var ids = $('#' + gridId).yiiGridView('getChecked', id);
    $('#userdeps .grid-view').addClass("grid-view-loading");
    $.post('/admin/userdeps/addedit', {'ids': ids, 'cat': dep}, function(data) {
    	var newarr = $.parseJSON(data);    	
    	$.notify(newarr[0], newarr[1]);
        $('#userdeps .grid-view').removeClass("grid-view-loading");
    });
}

function init() {
    $("input:regex(id, ^gm\\d+_membertd_\\d+)").not('#membertd_all').each(function(key, val) {
        setRowSelected(val);
    });
}
