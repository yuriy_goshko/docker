
/*==============================================================*/
/* Table: Groups                                                */
/*==============================================================*/
create table Groups
(
   gID                  int(11) not null auto_increment,
   gName                varchar(255),
   primary key (gID),
   unique key AK_Key_2 (gName)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

DELIMITER $
DROP PROCEDURE IF EXISTS drop_index_if_exists ;
CREATE PROCEDURE drop_index_if_exists(in theTable varchar(128), in theIndexName varchar(128) )
BEGIN
 IF((SELECT COUNT(*) AS index_exists FROM information_schema.statistics WHERE TABLE_SCHEMA = DATABASE() and table_name =
theTable AND index_name = theIndexName) > 0) THEN
   SET @s = CONCAT('DROP INDEX ' , theIndexName , ' ON ' , theTable);
   PREPARE stmt FROM @s;
   EXECUTE stmt;
 END IF;
END$

DELIMITER ;
/*==============================================================*/
/* Comments                                                     */
/* block                                                        */
/*==============================================================*/

#oneline sql command
drop table if exists CallInfo;

DELIMITER $

drop table if exists CallInfo;
