    /*==============================================================*/
/* Comments                                                     */
/* block                                                        */
/*==============================================================*/


#oneline sql command
drop table if exists CallInfo; #line comment (must be ignored)

/*==============================================================*/
/* Table: Groups                                                */
/*==============================================================*/
create table Groups
(
   gID                  int(11) not null auto_increment,
   gName                varchar(255),
   primary key (gID),
   unique key AK_Key_2 (gName)/* Внутренний звонок */
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;