-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)

alter table CallInfo add constraint FK_Reference_12 foreign key (cChannelChannels_cID)
      references Channels (cID) on delete cascade on update cascade$

DELIMITER ;
#Triggers for Cdr table

#Create, or modify if exists trigger on "Cdr". It process raw data fields after insertion row at "Cdr"
#and insert prepared information for web.
#@property string @trname
#@property string @ontable
#@property string @totable
#@property string @userfield

DROP TRIGGER IF EXISTS `cdr.data-flow`;



CREATE TRIGGER `cdr.data-flow` AFTER INSERT ON `cdr`
FOR EACH ROW BEGIN

 CALL getChannelID(NEW.channel, @cChannelChannels_cID)$
 CALL getChannelID(NEW.dstchannel, @cDstchannelChannels_cID)$
 CALL getParamsByDirection(
    NEW.channel, 
    NEW.dstchannel, 
    NEW.src, 
    NEW.dst, 
    NEW.src_num_out,
    NEW.dst_num_out,
    NEW.src_num_in,
    NEW.dst_num_in,
    @cChannelChannels_cID, 
    @cDstchannelChannels_cID, 
    @cDirection, 
    @cOutChan, 
    @src, 
    @dst)$

END ;