    /*==============================================================*/
/* Comments                                                     */
/* block                                                        */
/*==============================================================*/


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;

#oneline sql command
drop table if exists CallInfo;

/*!40101 SET NAMES utf8 */;
create table Groups
(
   gID                  int(11) not null auto_increment,
   gName                varchar(255),
   primary key (gID),
   unique key AK_Key_2 (gName)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;