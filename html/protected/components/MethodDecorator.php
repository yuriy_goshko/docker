<?php
class MethodDecorator{
    
    public $decObj;
    
    public function __construct($className) {
        $this->decObj = new $className;
    }
    
    public function __call($name, $arguments) {
            //$this->decObj->command->reset();
            return $this->decObj->$name(implode(',', $arguments));
    }
    
}