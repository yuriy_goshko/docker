<?php
class RenderfileController extends Controller {
	public $redirectUrl;
	public $isRoot = false;
	public function Init() {
		$this->redirectUrl = '/' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/';
		$this->isRoot = yii::app()->user->getState('isRoot_');
	}
	public function getViewFile($viewName) {
		$ResViewFile = parent::getViewFile($viewName);
		if ($ResViewFile != null)
			return $ResViewFile;
		if (file_exists($viewName))
			return $viewName;
	}
}