<?php

abstract class DbTestCase extends CDbTestCase {

    protected $_extendetTestCase;

    public function __call($name, $arguments) {
        if (method_exists($this->_extendetTestCase, $name))
            return call_user_func_array(array($this->_extendetTestCase, $name), $arguments);
    }

    public function setUp() {
        parent::setUp();
        $this->_extendetTestCase = new ExtendedTestCase();
    }

}
