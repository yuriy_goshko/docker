<?php

class ExtendedTestCase extends CTestCase {

    public function getMethod($method, $object) {
        $reflection = new ReflectionClass($object);
        $method = $reflection->getMethod($method);
        $method->setAccessible(true);

        return $method;
    }

    public function getProperty($property, $object) {
        $reflection = new ReflectionClass($object);
        $property = $reflection->getProperty($property);
        $property->setAccessible(true);

        return $property->getValue($object);
    }

    public function setProperty($property, $value, $object) {
        $reflection = new ReflectionClass($object);
        $property = $reflection->getProperty($property);
        $property->setAccessible(true);

        return $property->setValue($object, $value);
    }

    /**
     * 
     * @param type $func
     * @param type $argPar
     * @param type $waitMaxTime
     * @return type
     * @throws PHPUnit_Framework_ExpectationFailedException
     */
    public function assertSync($func, $argPar, $waitMaxTime) {
        $time = 0;
        $errors = array();
        $this->assertGreaterThan(0, $waitMaxTime);
        while (true) {
            if ($waitMaxTime <= $time)
                break;
            $arg = $this->createParams($argPar);
            try {
                call_user_func_array($func, $arg);
                return;
            } catch (PHPUnit_Framework_ExpectationFailedException $e) {
                $errors[] = $e->getMessage();
                sleep(1);
                $time++;
                continue;
            } catch (Exception $e) {
                throw $e;
            }
            break;
        }
        //implode("\n", $errors);
        call_user_func_array($func, $arg);
    }

    /**
     * Create params from callable if exists.
     * 
     * @param array $argPar
     * @return array
     * @throws Exception
     */
    public function createParams(array $argPar) {
        foreach ($argPar[0] as $paramPosition => &$paramValue) {
            $params = $argPar[1][$paramPosition];
            if ($params === false)
                continue;

            if (!is_callable($paramValue)) {
                throw new Exception('Passed params for not a callable value');
            }
            $paramValue = call_user_func_array($paramValue, $params);
        }
        return $argPar[0];
    }

    /**
     * 
     * @param type $expVar
     * @param type $act
     * @param type $params
     * @param type $waitMaxTime
     */
    public function assertCountSync($expVar, $act, $params = array(), $waitMaxTime = 5) {
        $arg = array(
            array(
                $expVar,
                $act
            ),
            array(
                false,
                !is_callable($act) ? false : $params
            )
        );
        $this->assertSync(array($this, 'assertCount'), $arg, $waitMaxTime);
    }

    /**
     * Calls @method assertEquals within @param $waitMaxTime seconds;
     * 
     * @param mixed $expVar
     * @param mixed $act
     * @param array $params
     * @param int $waitMaxTime
     */
    public function assertEqualsSync($expVar, $act, $params = array(), $waitMaxTime = 5) {
        $arg = array(
            array(
                $expVar,
                $act
            ),
            array(
                false,
                !is_callable($act) ? false : $params
            )
        );
        $this->assertSync(array($this, 'assertEquals'), $arg, $waitMaxTime);
    }

    public function assertLessThanSync($act, $expVar, $params = array(), $waitMaxTime = 5) {
        $arg = array(
            array(
                $expVar,
                $act
            ),
            array(
                false,
                !is_callable($act) ? false : $params,
            )
        );
        $this->assertSync(array($this, 'assertLessThan'), $arg, $waitMaxTime);
    }

    public function assertGreaterThanSync($expVar, $act, $params = array(), $waitMaxTime = 5) {
        $arg = array(
            array(
                $expVar,
                $act
            ),
            array(
                false,
                !is_callable($act) ? false : $params,
            )
        );
        $this->assertSync(array($this, 'assertGreaterThan'), $arg, $waitMaxTime);
    }

    public function assertTrueSync($act, $params = array(), $waitMaxTime = 5) {
        $arg = array(
            array(
                $act
            ),
            array(
                !is_callable($act) ? false : $params
            )
        );
        $this->assertSync(array($this, 'assertTrue'), $arg, $waitMaxTime);
    }

}
