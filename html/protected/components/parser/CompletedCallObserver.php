<?php

declare(ticks = 1);

namespace parser;

/**
 * This class is designed for monitoring and analyzing the subject table (eCompletedCall). 
 * Implements the Observer design pattern. 
 * Lets tie the request to the model of data base with specific handler. 
 * One of analyzing the model can have multiple handlers.
 * By default does not has attached listeners and request model.
 * 
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class CompletedCallObserver {

    /**
     * A count of maximum simultaneous working handlers.
     */
    const MAX_FORKS = 3;

    /**
     * A master process type id
     */
    const PROCESS_TYPE_MASTER = 1;

    /**
     * A worker process type id
     */
    const PROCESS_TYPE_WORKER = 2;

    /**
     * A parent id.
     */
    protected $_pid;

    /**
     * A array map of event handlers. 
     * 
     * @var array
     */
    protected $_listeners = array();

    /**
     * An request models data provider.
     * 
     * @var models\DataProvider
     */
    protected $_dataProvider;

    /**
     * A forked workers list.
     *
     * @var array
     */
    protected $_workers = array();

    /**
     * A current process type;
     * 
     * @var integer 
     */
    protected $_procType;

    /**
     * Construct observer.
     * 
     * @param models\DataProvider $dataProvider
     */
    public function __construct(models\DataProvider $dataProvider) {
        set_error_handler("exception_error_handler");
        $this->_dataProvider = $dataProvider;
        $this->_procType = self::PROCESS_TYPE_MASTER;
        $this->_pid = posix_getpid(); // get main proccess pid.
        $obj = $this;
        pcntl_signal(SIGCHLD, function($signo) use($obj) {
            $method = new \ReflectionMethod($obj, "onHaltFork");
            $method->setAccessible(true);
            $method->invoke($obj, $signo);
        });
        pcntl_setpriority(20);
    }

    /**
     * Notifies all handlers by coming new rows.
     */
    public function process() {
        $data = $this->read();
        if (count($data) != false) {
            foreach ($data as $class => $row) {
                if (!isset($this->_listeners[$class]))
                    continue;
                foreach ($row as $item)
                    $this->notify($this->_listeners[$class], $item);
            }
        }
    }

    /**
     * Attach handler with request model.
     * 
     * @param \parser\abstracts\Handler $listener
     * @param \parser\models\Request $model
     */
    public function addListener(abstracts\Handler $listener, models\Request $model) {
        $this->_dataProvider->addChild($model);
        $this->_listeners[$model->getModelName()][] = $listener;
    }

    /**
     * 
     * @return int
     */
    public function getWorkersCount() {
        return count($this->_workers);
    }

    /**
     * Read new rows from request model.
     * 
     * @return type
     */
    protected function read() {
        $data = array();
        $data = $this->_dataProvider->getData();
        return $data;
    }

    /**
     * Notifies appropriate to model request handlers
     * 
     * @param array $listeners
     * @param \CActiveRecord $data
     */
    protected function notify($listeners, \CActiveRecord $data) {

        $data->setInProcess();
        foreach ($listeners as $listener) {
            while (!$this->isNeedMoreFork()) { //sleep if dont need more forks.
                sleep(1);
                continue;
            }
            \Yii::getLogger()->flush(true);
            $pid = pcntl_fork();
            $this->dbReconnect();
            if ($pid == 0) {
                $this->setProcessType(self::PROCESS_TYPE_WORKER);
                try {
                    $listener->init($data);
                    try {
                        $listener->notify($data);
                    } catch (\Exception $e) {
                        $listener->error($e);
                        \Yii::app()->end();
                    }
                    $listener->success($data);
                } catch (\Exception $e) {
                    \Yii::log($e, 'Error');
                }
                \Yii::app()->end(); //destroy worker after good job
            } else {
                if ($pid == -1)
                    die('could not fork');
                $this->setProcessType(self::PROCESS_TYPE_MASTER);
                $this->onAfterCreateFork($pid);
            }
        }
    }

    /**
     * Create new db connection. Need each time after fork.
     */
    protected function dbReconnect() {
        \Yii::app()->db->setActive(false);
        \Yii::app()->db->setActive(true);
    }

    /**
     * Method decides if need more forks.
     * 
     * @return bool
     */
    protected function isNeedMoreFork() {
        return self::MAX_FORKS > count($this->_workers);
    }

    /**
     * A callback method.It evokes on SIGCHLD signal (when children die).
     * Aim to release zombies.
     * 
     * @return void
     */
    protected function onHaltFork() {
        if (!$this->isMasterProcess())
            return;
        while (($pid = pcntl_waitpid(-1, $status, WNOHANG)) > 0) {
            $k = array_search($pid, $this->_workers);
            if ($k !== false) {
                unset($this->_workers[$k]);
            }
        }
    }

    /**
     * A method evokes after child worker is created.
     * Adds child process $pid to workers pull.
     * 
     * @param type $pid
     */
    protected function onAfterCreateFork($pid) {
        $this->_workers[] = $pid;
    }

    /**
     * Set current process type (master, worker).
     * 
     * @param type int
     */
    protected function setProcessType($type) {
        $this->_procType = $type;
    }

    /**
     * Find out if current process is a master process.
     * 
     * @return bool
     */
    protected function isMasterProcess() {
        return $this->_procType == self::PROCESS_TYPE_MASTER;
    }

}

?>
