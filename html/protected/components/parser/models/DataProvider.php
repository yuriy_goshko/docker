<?php

namespace parser\models;

/**
 * This class is aimed for aggregate request models and generate a data from them.
 *
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class DataProvider {

    /**
     * A list of Request models.
     *
     * @var array
     */
    protected $_childs = array();

    /**
     * Add new child to list.
     * 
     * @param Request $child
     * @return \parser\models\DataProvider
     */
    public function addChild(Request $child) {
        if (!isset($this->_childs[$child->getModelName()]))
            $this->_childs[$child->getModelName()] = $child;
        return $this;
    }

    /**
     * Get result of requests.
     * 
     * @return type
     */
    public function getData() {
        return $this->readAll();
    }

    /**
     * 
     * @return array
     */
    protected function readAll() {
        $data = array();
        foreach ($this->_childs as $child) {
            $data[$child->getModelName()] = $child->getAllRows();
        }
        return $data;
    }

}

?>
