<?php

namespace parser\models;

use parser\exceptions;

/**
 * Description of Request
 *
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class Request {

    /**
     * List of instanted models.
     *
     * @var array
     */
    static protected $_insts = array();

    /**
     * Current model class name.
     * 
     * @var string
     */
    protected $_name;

    /**
     * Current model instance.
     *
     * @var \CActiveRecord 
     */
    protected $_model;

    /**
     * Create once and get request model by name.
     * 
     * @param string $modelName
     * @return parser\models\Request
     */
    public static function getInstance($modelName) {
        if (!isset(self::$_insts[$modelName])) {
            $obj = new self($modelName);
        } else {
            $obj = self::$_insts[$modelName];
        }
        return $obj;
    }

    /**
     * Constructs request models.
     * 
     * @param string $modelName
     * @throws exceptions\ClassNotExistsException
     * @throws exceptions\ModelTypeException
     */
    protected function __construct($modelName) {
        if (!class_exists($modelName))
            throw new exceptions\ClassNotExistsException("Class not exists by passed name: '" . $modelName . "'");
        $model = new $modelName(null);
        if (!$model instanceof \CActiveRecord)
            throw new exceptions\ModelTypeException("Passed class name not instance of \CActiveRecord");
        $this->_model = $model;
        $this->_name = $modelName;

        self::$_insts[$modelName] = $this;
    }

    public function setProcessed() {
        return $this->_model->setInProcess();
    }

    /**
     * Get result rows by request model.
     * 
     * @return array
     */
    public function getAllRows() {
        return $this->_model->findAll();
    }

    /**
     * Get current model class name.
     * 
     * @return string
     */
    public function getModelName() {
        return $this->_name;
    }

}

?>
