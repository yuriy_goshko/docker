<?php

namespace parser\abstracts;

/**
 * A main interface for all handlers. Aimed for work with \models\events\CompletedCalls models.
 *
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
abstract class Handler {

    /**
     * Initialize handler object.
     * 
     * @param \models\events\CompletedCalls $data
     */
    public function init(\models\events\CompletedCalls $data) {
        
    }

    /**
     * Notifies a concrete handler.
     */
    abstract public function notify(\models\events\CompletedCalls $data);

    /**
     * Invoke if @method notify computed with errors.
     * Default is doing nothing.
     * 
     * @param \Exception $e
     */
    public function error(\Exception $e) {
        
    }

    /**
     * Invoke if @method notify computed without errors.
     * Default is doing nothing.
     * 
     * @param \models\events\CompletedCalls $data
     */
    public function success(\models\events\CompletedCalls $data) {
        
    }

}

?>
