<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {
    
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/front/fluid';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    
    /**
     * Path to custom css file wich append last.
     *
     * @var string
     */
    public $finalCss;

    protected function beforeAction($a = false) {
        
        
        //Let's js scripts include automatically depending current routing
        $routedJs = array(
            Yii::app()->basePath,
            '..',
            'js',
            $this->module? $this->module->id : '',
            $this->id,
        );
        $routedJspath = implode(DIRECTORY_SEPARATOR, $routedJs);
        if(is_dir($routedJspath))
            Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(implode(DIRECTORY_SEPARATOR, $routedJs)) .
                    DIRECTORY_SEPARATOR.$this->action->id . '.js', CClientScript::POS_BEGIN);
        return parent::beforeAction($a);
    }

}
