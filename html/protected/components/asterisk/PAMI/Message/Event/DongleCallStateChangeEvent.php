<?php
/**
 * Event triggered when a dongle call sate change.
 *
 * PHP Version 5
 *
 * @category   Pami
 * @package    Message
 * @subpackage Event
 * @author     Qzich <qzichs@gmail.com>
 *
 */
namespace PAMI\Message\Event;

use PAMI\Message\Event\EventMessage;

/**
 * Event triggered when a call sate change
 *
 * PHP Version 5
 *
 * @category   Pami
 * @package    Message
 * @subpackage Event
 * @author     Qzich <qzichs@gmail.com>
 */
class DongleCallStateChangeEvent extends EventMessage
{
    /**
     * Returns key: 'CallIdx'.
     *
     * @return string
     */
    public function getCallIdx()
    {
        return $this->getKey('CallIdx');
    }
    
    /**
     * Return ket: 'NewState'
     * 
     * @return string
     */
    public function getNewState()
    {
        return $this->getKey('NewState');
    }

    /**
     * Returns key: 'Device'.
     *
     * @return string
     */
    public function getDevice()
    {
        return $this->getKey('Device');
    }

    /**
     * Returns key: 'Privilege'.
     *
     * @return string
     */
    public function getPrivilege()
    {
        return $this->getKey('Privilege');
    }
}