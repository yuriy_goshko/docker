<?php

/**
 * Event triggered when device port fail.
 *
 * PHP Version 5
 *
 * @category   Pami
 * @package    Message
 * @subpackage Event
 * @author     Qzich <qzichs@gmail.com>
 *
 */

namespace PAMI\Message\Event;

use PAMI\Message\Event\EventMessage;

/**
 * Event triggered when device port fail
 *
 * PHP Version 5
 *
 * @category   Pami
 * @package    Message
 * @subpackage Event
 * @author     Qzich <qzichs@gmail.com>
 */
class DonglePortFailEvent extends EventMessage {

    /**
     * Reutnrns ket: 'ErrorCode'
     * 
     */
    public function getErrorCode()
    {
        return $this->getKey('ErrorCode');
    }

    /**
     * Returns key: 'Message'.
     *
     * @return string
     */
    public function getMessage() {
        return $this->getKey('Message');
    }

    /**
     * Returns key: 'Device'.
     *
     * @return string
     */
    public function getDevice() {
        return $this->getKey('Device');
    }

    /**
     * Returns key: 'Privilege'.
     *
     * @return string
     */
    public function getPrivilege() {
        return $this->getKey('Privilege');
    }

}