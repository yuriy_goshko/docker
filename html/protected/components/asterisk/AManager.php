<?php
require_once 'PAMI/Autoloader/Autoloader.php'; // Include PAMI autoloader.
require_once realpath(__DIR__) . DIRECTORY_SEPARATOR . '../l4php/Logger.php';
\PAMI\Autoloader\Autoloader::register(); // Call autoloader register for PAMI autoloader.

use PAMI\Client\Impl\ClientImpl;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Action\ListCommandsAction;
use PAMI\Message\Action\ListCategoriesAction;
use PAMI\Message\Action\CoreShowChannelsAction;
use PAMI\Message\Action\CoreSettingsAction;
use PAMI\Message\Action\CoreStatusAction;
use PAMI\Message\Action\StatusAction;
use PAMI\Message\Action\ReloadAction;
use PAMI\Message\Action\CommandAction;
use PAMI\Message\Action\HangupAction;
use PAMI\Message\Action\LogoffAction;
use PAMI\Message\Action\AbsoluteTimeoutAction;
use PAMI\Message\Action\OriginateAction;
use PAMI\Message\Action\BridgeAction;
use PAMI\Message\Action\CreateConfigAction;
use PAMI\Message\Action\GetConfigAction;
use PAMI\Message\Action\GetConfigJSONAction;
use PAMI\Message\Action\AttendedTransferAction;
use PAMI\Message\Action\RedirectAction;
use PAMI\Message\Action\DAHDIShowChannelsAction;
use PAMI\Message\Action\DAHDIHangupAction;
use PAMI\Message\Action\DAHDIRestartAction;
use PAMI\Message\Action\DAHDIDialOffHookAction;
use PAMI\Message\Action\DAHDIDNDOnAction;
use PAMI\Message\Action\DAHDIDNDOffAction;
use PAMI\Message\Action\AgentsAction;
use PAMI\Message\Action\AgentLogoffAction;
use PAMI\Message\Action\MailboxStatusAction;
use PAMI\Message\Action\MailboxCountAction;
use PAMI\Message\Action\VoicemailUsersListAction;
use PAMI\Message\Action\PlayDTMFAction;
use PAMI\Message\Action\DBGetAction;
use PAMI\Message\Action\DBPutAction;
use PAMI\Message\Action\DBDelAction;
use PAMI\Message\Action\DBDelTreeAction;
use PAMI\Message\Action\GetVarAction;
use PAMI\Message\Action\SetVarAction;
use PAMI\Message\Action\PingAction;
use PAMI\Message\Action\ParkedCallsAction;
use PAMI\Message\Action\SIPQualifyPeerAction;
use PAMI\Message\Action\SIPShowPeerAction;
use PAMI\Message\Action\SIPPeersAction;
use PAMI\Message\Action\SIPShowRegistryAction;
use PAMI\Message\Action\SIPNotifyAction;
use PAMI\Message\Action\QueuesAction;
use PAMI\Message\Action\QueueStatusAction;
use PAMI\Message\Action\QueueSummaryAction;
use PAMI\Message\Action\QueuePauseAction;
use PAMI\Message\Action\QueueRemoveAction;
use PAMI\Message\Action\QueueUnpauseAction;
use PAMI\Message\Action\QueueLogAction;
use PAMI\Message\Action\QueuePenaltyAction;
use PAMI\Message\Action\QueueReloadAction;
use PAMI\Message\Action\QueueResetAction;
use PAMI\Message\Action\QueueRuleAction;
use PAMI\Message\Action\MonitorAction;
use PAMI\Message\Action\PauseMonitorAction;
use PAMI\Message\Action\UnpauseMonitorAction;
use PAMI\Message\Action\StopMonitorAction;
use PAMI\Message\Action\ExtensionStateAction;
use PAMI\Message\Action\JabberSendAction;
use PAMI\Message\Action\LocalOptimizeAwayAction;
use PAMI\Message\Action\ModuleCheckAction;
use PAMI\Message\Action\ModuleLoadAction;
use PAMI\Message\Action\ModuleUnloadAction;
use PAMI\Message\Action\ModuleReloadAction;
use PAMI\Message\Action\ShowDialPlanAction;
use PAMI\Message\Action\ParkAction;
use PAMI\Message\Action\MeetmeListAction;
use PAMI\Message\Action\MeetmeMuteAction;
use PAMI\Message\Action\MeetmeUnmuteAction;
use PAMI\Message\Action\EventsAction;
use PAMI\Message\Action\VGMSMSTxAction;
use PAMI\Message\Action\DongleSendSMSAction;
use PAMI\Message\Action\DongleShowDevicesAction;
use PAMI\Message\Action\DongleReloadAction;
use PAMI\Message\Action\DongleStartAction;
use PAMI\Message\Action\DongleRestartAction;
use PAMI\Message\Action\DongleStopAction;
use PAMI\Message\Action\DongleResetAction;
use PAMI\Message\Action\DongleSendUSSDAction;
use PAMI\Message\Action\DongleSendPDUAction;

/*
 * Listener application for Asterisk Manager
 */

class AManager extends ClientImpl {

    const LIST_PREFIX = "Handle";

    /**
     *
     * @var array
     */
    public static $options;

    /**
     *
     * @var ClientImpl
     */
    public static $instance;

    /**
     * Time of catching events
     *
     * @var int
     */
    protected $_time = 0;
    
    /**
     * Maximum count of second to catch event;
     *
     * @var int
     */
    protected $_timeLimit = 10;

    /**
     * Set global options for all objects inherited this class
     * 
     * @param type array
     */

    /**
     *
     * @var array
     */
    private $handlers = array();

    public static function setOptions($opt) {
        self::$options = $opt;
    }

    public static function getInstance() {
        if (!self::$instance instanceof self)
            self::$instance = new self;
        return self::$instance;
    }

    /**
     * Constructor with register all attached children handlers.
     * 
     * @param array $options
     */
    public function __construct($options = array()) {
        if (isset(self::$options) && count($options) == false)
            $options = self::$options;
        parent::__construct($options);
        $this->open();
    }

    /**
     * Method catch response events.
     * 
     * If all events was catched wait for 5 sec and logout
     */
    public function catchAllEvents() {
        $this->updateListeners();
        $time = time();
        while (true) {
            usleep(2000000);
            $this->_time++;
            $this->process();
        }
        $this->close();
    }
    
    public function catchEvent()
    {
        $this->updateListeners();
        while ($this->_time < $this->_timeLimit) {
            usleep(1000000);
            $this->_time++;
            $this->process();
        }
        $this->close();
    }
    
    /**
     * Function for breaking catch events loop;
     */
    public function breakLoop()
    {
        $this->_timeLimit = 0;
    }
    
    /**
     * 
     * @param array() $handlers
     */
    public function addHandlers($handlers = array()) 
    {
            foreach ($handlers as $handler) {
                //$this->registerEventListener(new $handler);
                $this->addHandler($handler);
            }
            return $this;
        
    }
    
    /**
     * 
     * @param mixed $handler
     */
    public function addHandler($handler) 
    {   
        if(!in_array($handler, $this->handlers)) {
           if($handler instanceOf IEventListener || is_string ($handler) && class_exists($handler))
               $this->handlers[] = $handler;
        }
        return $this;
        
    }
    
    /**
     * 
     * @return array()
     */
    public function getHandlers(){
        return $this->handlers;
    }
    
    /**
     * Update listeners
     */
    protected function updateListeners()
    {
        if (isset($this->handlers) && is_array($this->handlers)) {
            foreach ($this->handlers as $handler) {
                if(!is_object($handler)){
                    $handler = new $handler;
                }
                $this->registerEventListener($handler);
            }
        }
    }

}