<?

/**
 * UDbMigration is class to make upgrading database with auto updating application 
 * version.
 * 
 * This class is designed to use with yiic upgrade command.
 */
abstract class UDbMigration extends CDbMigration {

    /**
     * A application version to update
     *
     * @var mixed
     */
    private $version;

    /**
     * Early init 
     */
    public function __construct() {
        $this->attachBehavior('SqlFileBahavior', array(
            'class' => 'SqlFileBehavior',
        ));
    }
    
    public function up() {
        $isMigrated = parent::up();
        if ($isMigrated !== false) 
               $this->appVersionUpdate();//update Version table;
         return $isMigrated;
            
    }
    
    protected function appVersionUpdate(){
        $o = new ReflectionClass($this);
        $this->version = preg_replace('/^m\d+_\d+_/i', '', $o->getName());
        preg_match('/^(?P<release>\d+)_(?P<db>\d+)_(?P<app>\d+)$/i', $this->version, $verA);
            Yii::app()->db->createCommand()->update('Version', array(
                'vRelease' => $verA['release'], 
                'vDb' => $verA['db'],
                'vApp' => $verA['app']));
    }

}