<?php

/**
 *  Modified for add useful methods/properties
 *  @author Qzich <qzich@gmail.com>
 */

class UDBConnection extends CDbConnection
{    
    /**
     * Quote a set of values. Useful for mass quoing
     * 
     * @param array $value
     * @return array
     */
    public function quoteValueSet($values) {
        return array_map(function($data) {
                    return Yii::app()->db->quoteValue($data);
                }, $values);
    }
    
    /**
     * Quote a set of columns names. Useful for mass quoing
     * 
     * @param array $value
     * @return array
     */
    public function quoteColumnSet($values) {
        return array_map(function($data) {
                    return Yii::app()->db->quoteColumnName($data); //fix for php version less or equal 5.3. For older can use $this in closure.
                }, $values);
    }
    
    
}