<?php

namespace string\format;

/**
 * Templater is a OOP implementation PHP's function \strtr.
 *
 * @package string
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class Templater {

    /**
     * A tamplate to compute.
     *
     * @var string
     */
    protected $pattern;
    
    /**
     * Construct templater object.
     * 
     * @param string $pattern
     * @param array $params
     */
    public function __construct($pattern, $params = array()) {

        $this->pattern = $pattern;
        $this->pattern = $this->getString($params);
    }
    
    /**
     * Return calculated string.
     * 
     * @return string
     */
    public function getString($params = array()){
        return \strtr($this->pattern, $params);
    }

    public function __toString() {
        return $this->getString();
    }
    
    

}