<?php
class LoginIdentity extends CUserIdentity {
	private $_id;
	private function allowIp($ipFilters, $ip) {
		// взято из YiiDebugToolbarRoute
		foreach ( $ipFilters as $filter ) {
			$filter = trim($filter);
			// normal or incomplete IPv4
			if (preg_match('/^[\d\.]*\*?$/', $filter)) {
				$filter = rtrim($filter, '*');
				if (strncmp($ip, $filter, strlen($filter)) === 0) {
					return true;
				}
			} else if (preg_match('/^([\d\.]+)\/(\d+)$/', $filter, $match)) {
				// CIDR
				if (self::matchIpMask($ip, $match[1], $match[2])) {
					return true;
				}
			} else if ($ip === $filter) {
				// IPv6
				return true;
			}
		}
		return false;
	}
	public static function isAgentUserName($name) {
		function isInteger($input) {
			return (ctype_digit(strval($input)));
		}
		return (isInteger($name) && ($name >= 200) && ($name < 1000));
	}
	public function authenticate() {
		$this->_id = null;
		$this->procUser();
		
		if (($this->errorCode == self::ERROR_USERNAME_INVALID) && ($this->isAgentUserName($this->username)))
			$this->procAgent();
		
		if ($this->errorCode == self::ERROR_NONE)
			$this->updatestates();
		
		return ! $this->errorCode;
	}
	private function updateStates() {
		$command = Yii::app()->db->createCommand('select `value` from `Settings` where `param` = "StatCaption"');
		$this->setState('StatCaption', $command->queryScalar());
	}
	private function procAgent() {
		$command = Yii::app()->db->createCommand('select `value` from `Settings` where `param` = "AgentIpFilters"');
		$ipFiltersStr = $command->queryScalar();
		if ($ipFiltersStr == "") {
			$this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
			return;
		}
		$ipFilters = explode(",", $ipFiltersStr);
		$ip = Yii::app()->request->userHostAddress;
		
		if (! $this->allowIp($ipFilters, $ip)) {
			$this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
			return;
		} 
		
		$sql = 'select t.`id`, u.`uID` from SIP t
 				inner join Users u on u.`uIdentity` = t.`userRole` and (u.`IsRole` = 1)
 				where (t.`name` = :pName) and (t.`secret` = :pSecret)';
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":pName", $this->username, PDO::PARAM_STR);
		$command->bindParam(":pSecret", $this->password, PDO::PARAM_STR);
		
		$qr = $command->queryRow();
		$uId = $qr['uID'];
		$aId = $qr['id'];
		if (($uId != "") && ($uId > 0)) {
			$this->_id = - $uId;	
			$this->errorCode = self::ERROR_NONE;
		} else {
			$this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
		}
	}
	private function procUser() {
		$user = Users::model();
		$user = $user->findByAttributes(array('uIdentity' => $this->username));
		if ($user == false) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} elseif ($user != false && $user->uPassword !== md5($user->uSalt) . md5($this->password . md5($user->uSalt))) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			$this->_id = $user->uID;
			$this->errorCode = self::ERROR_NONE;
		}
	}
	public function getId() {
		return $this->_id;
	}
}