<?php
class WebUser extends CWebUser {
	public static $KEY_AGENT = "___agentId";
	public function GetisWithAdminRights() {
		return ($this->getState("rEditAgents") || $this->getState("rEditUsers") || $this->getState("rEditQueues") || $this->getState("rEditDepartments"));
	}
	public static $RightsList = array(
			'rViewStat',
			'rViewReport',
			'rPhoneBook',
			'rListenAudio',
			'rListenAudioTransfer',
			'rCSVExport',
			'rEditAgents',
			'rEditDepartments',
			'rEditQueues',
			'rEditUsers');
	public static $RightsAgentsFilter = array('rViewStat','rPhoneBook','rListenAudio','rListenAudioTransfer','rCSVExport');
	private function ClearAllRights() {
		$this->setState('rHash', Null);
		$this->setState('depKeys', Null);
		$this->setState('depIds', Null);
		$this->setState(WebUser::$KEY_AGENT, Null);
		foreach ( self::$RightsList as $rName )
			$this->setState($rName, Null);
	}
	private function UpdateDepartmentsKeys() {
		$AId = $this->getId();
		if ($AId < 0) {
			$AId = abs($AId);
			$this->setState(WebUser::$KEY_AGENT, $this->getName());
			
			$sql = 'select distinct d.`dID`, d.`dKeyName`
			from `SIP` s
			inner join `DepartmentAgents` da on s.`id` = da.`SIPId`
			inner join `Departments` d on d.`dID`=`da`.`DepartmentId`
			where s.`name` = ' . $this->getName();
		} else
			$sql = 'select distinct d.`dID`, d.`dKeyName` 
				from `DepartmentUsers` du
				inner join `Departments` d on d.`dID`=`du`.`DepartmentId`
				where du.`UserId` = ' . $AId;
		$command = Yii::app()->db->createCommand($sql);
		$dbRows = $command->queryAll();
		$depKeys = array();
		$Ids = array();
		foreach ( $dbRows as $Row ) {
			$depKeys[] = $Row['dKeyName'];
			$Ids[] = $Row['dID'];
		}
		$depKeys = implode('","', $depKeys);
		$Ids = implode(',', $Ids);
		if (trim($Ids) === '') {
			$Ids = '0';
			$depKeys = '0';
		}
		$this->setState('depKeys', $depKeys);
		$this->setState('depIds', $Ids);
	}
	private function UpdateRights() {
		$AId = $this->getId();
		
		$AId = abs($AId);
		
		if ($AId != 'root') {
			$sql = 'select rRightsStamp from Users where uID = ' . $AId;
			$command = Yii::app()->db->createCommand($sql);
			$dbHashStamp = $command->queryScalar();
		} else
			$dbHashStamp = 'root';
		
		if (($AId === Null) || ($AId === False) || ($dbHashStamp === False)) {
			$this->ClearAllRights();
			return;
		}
		
		$rHash = $this->getState('rHash');
		if ((trim($rHash) === '') || (md5($dbHashStamp) != $rHash)) {
			$this->ClearAllRights();
			$rRows = array();
			if ($AId != 'root') {
				$sql = 'select Name from UsersRights Where UserId = ' . $AId;
				$command = Yii::app()->db->createCommand($sql);
				$dbRows = $command->queryAll();
				foreach ( $dbRows as $Name )
					$rRows[] = $Name['Name'];
			} else
				$rRows[] = 'All';
			
			if (in_array('All', $rRows)) {
				foreach ( self::$RightsList as $rName )
					$this->setState($rName, True);
				$this->setState('depIds', '*');
			} else {
				foreach ( $rRows as $rName )
					if (in_array('r' . $rName, self::$RightsList))
						$this->setState('r' . $rName, true);
				$this->UpdateDepartmentsKeys();
			}
			$this->setState('rHash', md5($dbHashStamp));
		}
		// yii::app()->user->getState('')
	}
	public function init() {
		parent::init();
		if ($this->isGuest) {
			$this->ClearAllRights();
		} else
			$this->UpdateRights();
		$this->setState('UseOldDepsConds', null);
	}
}
