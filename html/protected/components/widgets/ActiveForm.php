<?

Yii::import('bootstrap.widgets.TbActiveForm');

/**
 * ActiveForm is a class for creating custom form elements.
 * @method strin gwrapedDownList
 */
class ActiveForm extends TbActiveForm {

    /**
     * 
     * @param array $values A params for TbActiveForm::dropDownList method.
     * @param array $wrapperHtmlOptins Label html options.
     * @return string $html Generated html content.
     */
    public function wrappedDownList($values, $wrapperHtmlOptins = array()) {
        $html = '';
        $defaultWrapperOptions = array(
            'class' => 'designed-select'
        );
        array_walk($defaultWrapperOptions, function($value, $index) use(&$wrapperHtmlOptins) {
                    if (!isset($wrapperHtmlOptins[$index]))
                        $wrapperHtmlOptins[$index] = $value;
                });
        if (!preg_match('/'.$defaultWrapperOptions['class'].'/', $wrapperHtmlOptins['class'])) {
            $wrapperHtmlOptins['class'] = $wrapperHtmlOptins['class'] . ' ' . $defaultWrapperOptions['class'];
        }

        $method = new ReflectionMethod($this, 'dropDownList');
        $html = Chtml::label($method->invokeArgs($this, $values), false, $wrapperHtmlOptins);
        return $html;
    }

}