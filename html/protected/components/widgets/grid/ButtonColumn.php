<?php

/**
 * ButtonColumn is class for customize grid-view buttons.
 * There are available buttons: add, update, delete, view.
 */
class ButtonColumn extends CButtonColumn {

    /**
     * Initialize buttons.
     */
    public function init() {
        $this->buttons = CMap::mergeArray(array(
                    'delete' => array(
                        'options' => array(
                            'class' => 'delete',
                            'title' => Yii::t('gridview', 'Delete')
                        ),
                        'imageUrl' => '/images/design/grid/del.png',
                    ),
                    'add' => array(
                        'options' => array(
                            'class' => 'add',
                            'title' => Yii::t('gridview', 'Add')
                        ),
                        'imageUrl' => '/images/design/grid/add.png',
                    ),
                    'update' => array(
                        'options' => array(
                            'class' => 'update',
                            'title' => Yii::t('gridview', 'Update')
                        ),
                        'imageUrl' => '/images/design/grid/edit.png',
                    )), $this->buttons);
        parent::init();
    }

}