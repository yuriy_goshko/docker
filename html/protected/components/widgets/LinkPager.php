<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LinkPager
 *
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class LinkPager extends CLinkPager {
    //put your code here

    /**
     * Find callable properties;
     */
    public function init() {
        parent::init();
        if (is_callable($this->nextPageLabel))
            $this->nextPageLabel = $this->nextPageLabel->__invoke($this);
        if (is_callable($this->prevPageLabel))
            $this->prevPageLabel = $this->prevPageLabel->__invoke($this);
        if (is_callable($this->firstPageLabel))
            $this->firstPageLabel = $this->firstPageLabel->__invoke($this);
        if (is_callable($this->lastPageLabel))
            $this->lastPageLabel = $this->lastPageLabel->__invoke($this);
        if (is_callable($this->header))
            $this->header = $this->heade->__invoke($this);
    }

}

?>
