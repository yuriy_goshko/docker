<?php

if (!function_exists('mb_ucfirst')) {

    function mb_ucfirst($string, $encoding='utf8') {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

}

if (!function_exists('mPop')) {

    function mPop($ar) {
        $m = array(
            '-1' => Yii::t('m', 'All values'),
        );

        if (is_array($ar)) {
            $m = $m + $ar;
        }
        return $m;
    }

}

function RusToLat($str) {
    $str = trim($str);
    $str = strip_tags($str);
    $str = str_replace(' ', '-', $str);
    $str = str_replace('.', '', $str);
    $str = str_replace(',', '', $str);
    $str = str_replace('«', '', $str);
    $str = str_replace('»', '', $str);
    $str = str_replace('“', '', $str);
    $str = str_replace('”', '', $str);
    $str = str_replace("
", "", $str);
    $str = str_replace("–", "-", $str);
    $str = str_replace("\r", "_", $str);
    $str = str_replace(":", "_", $str);
    $str = str_replace("(", "_", $str);
    $str = str_replace(")", "_", $str);
    $str = str_replace("%", "", $str);
    $str = str_replace("!", "", $str);
    $str = str_replace("№", "", $str);
    $str = str_replace(";", "", $str);
    $str = str_replace("@", "", $str);
    $str = str_replace("#", "", $str);
    $str = str_replace("^", "", $str);
    $str = str_replace("*", "", $str);
    $str = str_replace("
", "_", $str);
    $str = str_replace("—", "-", $str);
    $str = str_replace("«", "", $str);
    $str = str_replace("»", "", $str);
    $str = str_replace("§", "", $str);
    $str = str_replace("\n", "_", $str);
    $str = str_replace("\r\n", "", $str);
    $str = str_replace(",", "", $str);
    $str = str_replace("+", "", $str);
    $str = str_replace('*', '', $str);
    $str = str_replace('/', '', $str);
    $str = str_replace('"', '', $str);
    $str = str_replace("'", '', $str);
    $str = str_replace('?', '', $str);
    $str = str_replace('<', '', $str);
    $str = str_replace('>', '', $str);
    $str = (string) $str;
    $str = str_replace('й', 'j', $str);
    $str = str_replace('ц', 'ts', $str);
    $str = str_replace('у', 'u', $str);
    $str = str_replace('к', 'k', $str);
    $str = str_replace('е', 'e', $str);
    $str = str_replace('ё', 'e', $str);
    $str = str_replace('Ё', 'e', $str);
    $str = str_replace('н', 'n', $str);
    $str = str_replace('г', 'g', $str);
    $str = str_replace('ш', 'sh', $str);
    $str = str_replace('щ', 'sch', $str);
    $str = str_replace('з', 'z', $str);
    $str = str_replace('х', 'h', $str);
    $str = str_replace('ъ', '', $str);
    $str = str_replace('ф', 'f', $str);
    $str = str_replace('ы', 'y', $str);
    $str = str_replace('в', 'v', $str);
    $str = str_replace('а', 'a', $str);
    $str = str_replace('п', 'p', $str);
    $str = str_replace('р', 'r', $str);
    $str = str_replace('о', 'o', $str);
    $str = str_replace('л', 'l', $str);
    $str = str_replace('д', 'd', $str);
    $str = str_replace('ж', 'zh', $str);
    $str = str_replace('э', 'e', $str);
    $str = str_replace('є', 'e', $str);
    $str = str_replace('Є', 'E', $str);
    $str = str_replace('ї', 'i', $str);
    $str = str_replace('Ї', 'I', $str);
    $str = str_replace('я', 'ja', $str);
    $str = str_replace('ч', 'ch', $str);
    $str = str_replace('с', 's', $str);
    $str = str_replace('м', 'm', $str);
    $str = str_replace('и', 'i', $str);
    $str = str_replace('т', 't', $str);
    $str = str_replace('ь', '', $str);
    $str = str_replace('б', 'b', $str);
    $str = str_replace('ю', 'ju', $str);
    $str = str_replace('Й', 'J', $str);
    $str = str_replace('Ц', 'TS', $str);
    $str = str_replace('У', 'U', $str);
    $str = str_replace('К', 'K', $str);
    $str = str_replace('Е', 'E', $str);
    $str = str_replace('Н', 'N', $str);
    $str = str_replace('Г', 'G', $str);
    $str = str_replace('Ш', 'SH', $str);
    $str = str_replace('Щ', 'SCH', $str);
    $str = str_replace('З', 'Z', $str);
    $str = str_replace('Х', 'h', $str);
    $str = str_replace('Ъ', '', $str);
    $str = str_replace('Ф', 'F', $str);
    $str = str_replace('Ы', 'Y', $str);
    $str = str_replace('В', 'V', $str);
    $str = str_replace('А', 'A', $str);
    $str = str_replace('П', 'P', $str);
    $str = str_replace('Р', 'R', $str);
    $str = str_replace('О', 'O', $str);
    $str = str_replace('Л', 'L', $str);
    $str = str_replace('Д', 'D', $str);
    $str = str_replace('Ж', 'ZH', $str);
    $str = str_replace('Э', 'E', $str);
    $str = str_replace('Я', 'JA', $str);
    $str = str_replace('Ч', 'CH', $str);
    $str = str_replace('С', 'S', $str);
    $str = str_replace('М', 'M', $str);
    $str = str_replace('И', 'I', $str);
    $str = str_replace('Т', 'T', $str);
    $str = str_replace('Ь', '', $str);
    $str = str_replace('Б', 'B', $str);
    $str = str_replace('Ю', 'JU', $str);
    $str = str_replace('і', 'i', $str);
    $str = str_replace('І', 'I', $str);
    $str = str_replace('Ї', 'I', $str);
    $str = str_replace('ї', 'ї', $str);
    $str = preg_replace('/\s+$/m', '', $str);
    $str = str_replace('
                 ', '', $str);
    return $str;
}

function format_time($t, $f = ':') { // t = seconds, f = separator 
    return sprintf("%02d%s%02d%s%02d", floor($t / 3600), $f, ($t / 60) % 60, $f, $t % 60);
}

function getDayName($n) {
    if (is_array($n)) {
        $n = isset($n[1]) ? $n[1] : $n;
    }
    $days = array(
        0 => 'Понедельник',
        1 => 'Вторник',
        2 => 'Среда',
        3 => 'Четверг',
        4 => 'Пятница',
        5 => 'Суббота',
        6 => 'Воскресенье'
    );
    return $days[$n];
}

function dissAsoc($a) {
    if (is_array($a)) {
        foreach ($a as $ar) {
            $arr[] = $ar;
        }
    }
    return $arr;
}

function getPercent($c = 0, $t = 0, $all = 100) {
    if ($t === 0) {
        return false;
    }
    return round(($c / $t) * $all, 2);
}

function getLightlyColor($color = false) {
    $ton = 50;
    $c = '';
    $r = '';
    if ($color == false)
        return '#000';
    $color = substr($color, 1);
    if (strlen($color) != false) {
        for ($i = 1; $i <= strlen($color); $i++) {
            $c .= $color[$i - 1];
            if (!is_float($i / 2)) {
                $r .= dechex(hexdec($c) + $ton);
                $c = '';
            }
        }
    }
    return '#' . $r;
}

function getRandomColor() {
    $color = '';
    for ($i = 0; $i < 3; ++$i) {
        $color .= dechex(getColor() - $i * 6);
    }
    return '#' . $color;
}

function getColor() {
    return rand(100, 205);
}

function generateSalt($max = 6) {
    $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
    $i = 0;
    $salt = "";
    while ($i < $max) {
        $salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
        $i++;
    }
    return $salt;
}

function sec2hms($sec, $padHours = false) {

    // start with a blank string
    $hms = "";

    // do the hours first: there are 3600 seconds in an hour, so if we divide
    // the total number of seconds by 3600 and throw away the remainder, we're
    // left with the number of hours in those seconds
    $hours = intval(intval($sec) / 3600);

    // add hours to $hms (with a leading 0 if asked for)
    $hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT) . ":" : $hours . ":";

    // dividing the total seconds by 60 will give us the number of minutes
    // in total, but we're interested in *minutes past the hour* and to get
    // this, we have to divide by 60 again and then use the remainder
    $minutes = intval(($sec / 60) % 60);

    // add minutes to $hms (with a leading 0 if needed)
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT) . ":";

    // seconds past the minute are found by dividing the total number of seconds
    // by 60 and using the remainder
    $seconds = intval($sec % 60);

    // add seconds to $hms (with a leading 0 if needed)
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

    // done!
    return $hms;
}

function delete($path) {
    if (!file_exists($path)) {
        throw new RecursiveDirectoryException('Directory doesn\'t exist.');
    }

    $directoryIterator = new DirectoryIterator($path);

    foreach ($directoryIterator as $fileInfo) {
        $filePath = $fileInfo->getPathname();

        if (!$fileInfo->isDot()) {
            if ($fileInfo->isFile()) {
                unlink($filePath);
            } else if ($fileInfo->isDir()) {
                if ($this->emptyDirectory($filePath)) {
                    rmdir($filePath);
                } else {
                    $this->delete($filePath);
                    rmdir($filePath);
                }
            }
        }
    }
}
