<?php

/**
 * Класс для работы с таблицами в стиле Data access object.
 * 
 * Инкапсулирует CDbCommand в $command, и является самодостаточным в случае
 * работы с любой таблицей. Для конкретных таблиц следует расширять данный класс
 * с указанием свойства $tableName.
 * 
 * @author qzich <qzichs@gmail.com>
 */
class DAO
{

    /**
     * @var string the sql expression 
     */
    static $sql;

    /**
     * @var string the model name.
     */
    static $name = __CLASS__;

    /**
     * @var mixed the result of job inherited object or CDbCommand object.
     */
    public $result = array();

    /**
     * @var CDbCommand
     */
    public $command;

    /**
     * @var string attribute value compare into __set method to call
     */
    protected $setAllName = 'attributes';

    /**
     * @var array the attribute wich contains undefined variables of class, 
     * setted by __set method.
     */
    protected $setted;

    /**
     * @var DAO inherited object.
     */
    protected $child;

    /**
     * @var MethodDecorator object.
     */
    protected $inh;

    /**
     * 
     * @param string $sql the sql expression. Default is empty.
     * @param string $className the class name wich calls this static method
     * @return DAO model instance.
     */
    public static function model($sql = '', $className = __CLASS__)
    {
        self::$name = $className;
        if ($sql != false)
            self::$sql = $sql;
        return new self();
    }

    /**
     *   
     * @return CDbCommand
     */
    public static function command()
    {
        $con = Yii::app()->db;
        return self::$sql ? $con->createCommand(self::$sql) : $con->createCommand();
    }

    /**
     * Constructor for DAO models. Initialize CDbCommand.
     */
    public function __construct()
    {
        $this->command = $this->command();
    }

    /**
     * Call methods
     * 
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     * @throws CDbException
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        if (!isset($this->child))
        {
            $this->inh = new MethodDecorator(self::$name);
            $this->child = $this->inh->decObj;
        }
        try {
            if (isset($this->child->tableName))
            {
                $this->result = $this->command->from($this->child->tableName)->$name(implode(',', $arguments));
            } else
            {
                $this->result = $this->command->$name(implode(',', $arguments));
            }
        } catch (CDbException $e) {
            throw $e;
        } catch (Exception $e) {
            $this->setAttributes(); //set childs attributes before call method;
            if (method_exists($this->child, $name))
            {
                $this->result = $this->inh->$name(implode(',', $arguments));
            } else
            {
                throw new Exception('Попытка вызвать несуществующий метод: ' . get_class($this->child) . '::' . $name);
            }
        };
        return $this->result;
    }

    /**
     * 
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        if ($name == $this->setAllName)
        {
            $this->setted = $value;
            if (is_array($value))
            {
                foreach ($value as $k => $v) {
                    $this->$k = $v;
                }
            }
        } else
        {
            $this->$name = $value;
        }
    }

    /**
     * 
     * @param string $sql
     * @return DAO
     */
    public function setSql($sql = '')
    {
        if ($sql != '')
        {
            self::$sql = $sql;
        }
        return new self($sql);
    }

    /**
     * Set properties for inherited object
     */
    protected function setAttributes()
    {
        if ($this->child != false && $this->setted != false)
        {
            $this->child->attributes = $this->setted;
        }
    }

    /**
     * Check if query result is empty
     * 
     * @return boolean
     */
    public function isEmpty()
    {
        if (is_array($this->result))
        {
            foreach ($this->result as $i => $a) {
                if (count($a) != false)
                {
                    return false;
                }
            }
            return true;
        }
        return true;
    }

    public function fiterBy($by, array $filter)
    {
        $arra=array();
        foreach ($this->result as $data) {
            foreach ($filter as $key => $val) {
                if($key == $data[$by]) $arra[] = $data;
            }
        }
        $this->result = $arra;
    }

}