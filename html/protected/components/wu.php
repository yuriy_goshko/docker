<?php
class wu {
	public static function pagPageSizeList($gridId, $defPGSName = 'defPageSize') {
		$pageSize = wu::getPagPageValue($defPGSName);
		return CHtml::dropDownList('pagPageSize', $pageSize, array(10 => 10,15 => 15,20 => 20,50 => 50,100 => 100), array(
				'onchange' => "$.fn.yiiGridView.update('" . $gridId . "',{ data:{pageSize: $(this).val() }})"));
	}
	public static function getPagPageValue($defPGSName = 'defPageSize') {
		$pageSize = Yii::app()->user->getState($defPGSName);
		if ($pageSize == null)
			$pageSize = 10;
		return $pageSize;
	}
	public static function getRequestPagPageValue($defPGSName = 'defPageSize') {
		if (isset($_GET['pageSize'])) {
			Yii::app()->user->setState($defPGSName, (int) $_GET['pageSize']);
			unset($_GET['pageSize']);
		}
		return wu::getPagPageValue($defPGSName);
	}
	public static function getDepartmensCB($form, $model) {
		if (yii::app()->user->getState('depIds') != '*')
			return;
		$sql = 'select d.`dID` from `Departments` d where d.`dKeyName`=(select value from Settings where param = "DefDepKeyName")';
		$command = Yii::app()->db->createCommand($sql);
		$defDepId = $command->queryScalar();
		$field = '_DefDepartment';
		$data = Chtml::listData(DataModule::getDepartmentsList(), 'dID', 'dName');
		$data = array(null => '-') + $data;
		echo CHtml::openTag('div', array('class' => 'span3'));
		echo $form->labelEx($model, $field);
		echo $form->dropDownList($model, $field, $data, array(
				'id' => '_AddDefDepartment',
				'options' => array($defDepId => array('selected' => 'selected'))));
		echo $form->error($model, $field);
		echo CHtml::closeTag('div');
	}
	public static function isUseAgentsRoles() {
		$sql = 'select value from Settings where param="AgentCanAccess"';
		return ('1' == (Yii::app()->db->createCommand($sql)->queryScalar()));
	}
	public static function getRolesCB($form, $model) {
		if (! wu::isUseAgentsRoles())
			return "";
		$sql = 'select value from Settings where param="AgentRoleDefault"';
		$defRole = Yii::app()->db->createCommand($sql)->queryScalar();
		
		$field = 'userRole';
		$data = Chtml::listData(DataModule::getRolesList(), 'uIdentity', 'uName');
		$data = array(null => '-') + $data;
		
		echo CHtml::openTag('div', array('class' => 'span3'));
		echo $form->labelEx($model, $field);
		echo $form->dropDownList($model, $field, $data, array('options' => array($defRole => array('selected' => 'selected'))));
		echo $form->error($model, $field);
		echo CHtml::closeTag('div');
	}
}