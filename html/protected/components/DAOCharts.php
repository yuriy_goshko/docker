<?php

/**
 * Class for handy work with highcharts extension
 */
class DAOCharts extends DAO
{
    
     /*
     * @var array результат работы запросов по очередям
     */

    public $namedData = array();
    
    /*
     * @var array сумирующий результат работы запросов
     */
    public $allData = array();
    
    /**
     * 
     * перечень полей, которые не числовые
     *
     * @var array
     */
    protected $noDigitData = array();
    
    /**
     * Process rows to sum all attributes by name
     * 
     * @param array $data
     * @return array
     */
    protected function processDataByName($data, $ndata=array())
    {
        $this->namedData = $ndata;
        if (is_array($data))
        {
            foreach ($data as $a) {


                $this->processDataByName($a, $this->namedData);

                if (is_array($a) && isset($a['name']))
                {
                    foreach ($a as $k => $v) {
                        if ($k != 'name')
                            if (!isset($this->namedData[$a['name']][$k]))
                                $this->namedData[$a['name']][$k] = $v;
                            else
                            {
                                if (is_numeric($v) && !in_array($k, $this->noDigitData))
                                    $this->namedData[$a['name']][$k] += $v;
                            }
                    }
                }
            }
        } else
        {
            return $data;
        }
    }
    
    /**
     * Process all data to get summary attributes;
     */
    protected function processAllData()
    {
        $this->allData = array();
        if (!empty($this->namedData))
        {
            foreach ($this->namedData as $k => $v) {
                if (is_array($v))
                {
                    foreach ($v as $f => $val) {
                        if (isset($this->allData[$f]))
                        {
                            if (is_numeric($val))
                                $this->allData[$f] += $val;
                        } else
                        {
                            if (is_numeric($val) && !in_array($f, $this->noDigitData))
                                $this->allData[$f] = $val;
                        }
                    }
                }
            }
        }
    }

    /**
     * 
     * Method for get series
     * 
     * @param array $categories
     * @param array $fields
     * @return array
     */
    public function getSeries(array $categories, array $fields)
    {
        $res = array();
        $data = array();
        foreach ($categories as $id => $cat) {
            if (isset($this->namedData[$id]))
                $data[] = array_intersect_key($this->namedData[$id], $fields);
        }

        foreach ($fields as $fldname => $label) {

            $res[] = array('name' => $label, 'data' => $this->getSeriesByName($fldname, $data));
        }

        return $res;
    }

    protected function getSeriesByName($name, array $data)
    {
        $res = array();
        foreach ($data as $item) {
            $res[] = intval($item[$name]);
        }
        return $res;
    }

}

?>