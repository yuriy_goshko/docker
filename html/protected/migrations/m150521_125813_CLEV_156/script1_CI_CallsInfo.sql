DELIMITER ;
DROP TABLE IF EXISTS `CI_CallsInfo`;
--
CREATE TABLE `CI_CallsInfo` (
  `Id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `CallId` INTEGER(11) DEFAULT NULL,
  `LinkedId` VARCHAR(32) NOT NULL,
  `InfoDateTime` DATETIME NOT NULL,
  `InfoType` VARCHAR(50) NOT NULL,
  `InfoValue` VARCHAR(50) DEFAULT NULL,
  `InsertDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `CI_CallsInfo_CallId_idx` (`CallId`) USING BTREE,
  KEY `CI_CallsInfo_LinkedId_idx` (`LinkedId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
DROP TRIGGER IF EXISTS `queue_log_after_ins_callinfo`;
DELIMITER $
CREATE TRIGGER `queue_log_after_ins_callinfo` AFTER INSERT ON `queue_log`
  FOR EACH ROW
BEGIN
  IF (NEW.event = 'ENTERQUEUE') THEN
	insert into `CI_CallsInfo`(`LinkedId`, `InfoDateTime`, `InfoType`, `InfoValue`)
    values (NEW.callid, NEW.time, NEW.event, NEW.queuename);
  END IF;
END$
--