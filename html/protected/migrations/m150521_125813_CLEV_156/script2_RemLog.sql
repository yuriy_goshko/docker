DROP PROCEDURE IF EXISTS `CI_RemoveLog`;
DELIMITER $
CREATE PROCEDURE `CI_RemoveLog`(
        IN `LinkedId` VARCHAR(32),
        IN `ForRecalc` TINYINT,
        IN `TablPref` VARCHAR(30),
        OUT `Res` VARCHAR(100)
    )
    NOT DETERMINISTIC
    MODIFIES SQL DATA
    SQL SECURITY DEFINER
    COMMENT ''
proc: BEGIN
  set Res = '';
  set @CallId = null;
  set @s='select cl.`CallId` into @CallId from `CI_LinkedIds` cl where `cl`.`LinkedId`=? limit 1;'; 
  set @s = REPLACE(@s,'CI_',Concat('CI_',TablPref));
  PREPARE sqlGetCallId FROM @s;
  set @p = LinkedId;
  EXECUTE sqlGetCallId USING @p;  
  DEALLOCATE PREPARE sqlGetCallId;
  if ISNULL(@CallId) then
    begin
     set Res = 'NotFind;';
     Leave proc;
    end;
  else
    set Res = CONCAT(@CallId, '; ');
  end if;
  set @s='delete from `CI_CallsDetail` where `CallId` =?;'; 
  set @s = REPLACE(@s,'CI_',Concat('CI_',TablPref));
  PREPARE sqlDelete FROM @s;
  EXECUTE sqlDelete USING @CallId;  
  DEALLOCATE PREPARE sqlDelete;
  set @s='delete from `CI_Calls` where `Id` =?;'; 
  set @s = REPLACE(@s,'CI_',Concat('CI_',TablPref));
  PREPARE sqlDelete FROM @s;
  EXECUTE sqlDelete USING @CallId;  
  DEALLOCATE PREPARE sqlDelete;
  set @s='delete from `CI_Conferences` where `CallId` =?;'; 
  set @s = REPLACE(@s,'CI_',Concat('CI_',TablPref));
  PREPARE sqlDelete FROM @s;
  EXECUTE sqlDelete USING @CallId;  
  DEALLOCATE PREPARE sqlDelete;
  set @s='delete from `CI_AudioFiles` where `CallId` =?;'; 
  set @s = REPLACE(@s,'CI_',Concat('CI_',TablPref));
  PREPARE sqlDelete FROM @s;
  EXECUTE sqlDelete USING @CallId;  
  DEALLOCATE PREPARE sqlDelete;
  set @s='delete from `CI_Warnings` where LinkedId in (select `cl`.`LinkedId` from `CI_LinkedIds` cl where cl.`CallId` = ?)'; 
  set @s = REPLACE(@s,'CI_',Concat('CI_',TablPref));
  PREPARE sqlDelete FROM @s;
  EXECUTE sqlDelete USING @CallId;  
  DEALLOCATE PREPARE sqlDelete;  
  set @s='delete from `CI_CallsInfo` where `InfoType` = "QueueName" and `CallId` =?;'; 
  set @s = REPLACE(@s,'CI_',Concat('CI_',TablPref));
  PREPARE sqlDelete FROM @s;
  EXECUTE sqlDelete USING @CallId;  
  DEALLOCATE PREPARE sqlDelete;
  set Res = CONCAT(Res, 'Clear; ');
  if (ForRecalc=1) then
    begin
	  set @s='update `CI_LinkedIds` set `Parsed`=0, `InsertDateTime`=CURRENT_TIMESTAMP where `CallId` =?;'; 
	  set @s = REPLACE(@s,'CI_',Concat('CI_',TablPref));
	  PREPARE sqlUpdateParsed FROM @s;
	  EXECUTE sqlUpdateParsed USING @CallId;  
	  DEALLOCATE PREPARE sqlUpdateParsed;   
      set Res = CONCAT(Res, 'Recalc; ');
    end;
  else
    begin 
	  set @s='delete from `CI_LinkedIds` where `CallId` =?;'; 
	  set @s = REPLACE(@s,'CI_',Concat('CI_',TablPref));
	  PREPARE sqlDelete FROM @s;
	  EXECUTE sqlDelete USING @CallId;  
	  DEALLOCATE PREPARE sqlDelete; 
      set Res = CONCAT(Res, 'Delete; ');
    end;
  end if;  
END$