<?php
class m160504_073819_CLEV_179 extends CDbMigration {
	public function safeUp() {
		$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior'));
		
		$table = Yii::app()->db->schema->getTable('PBXNumbers');
		if (! isset($table->columns['Type'])) {
			$this->execute('ALTER TABLE `PBXNumbers` ADD COLUMN `Type` varchar(20) NOT NULL;');
		}
		if (! isset($table->columns['Name'])) {
			$this->execute('ALTER TABLE `PBXNumbers` ADD COLUMN `Name` varchar(255) NOT NULL;');
		}
		if (! isset($table->columns['KeyName'])) {
			$this->execute('ALTER TABLE `PBXNumbers` ADD COLUMN `KeyName` varchar(275) NOT NULL;');
		}
		if (! isset($table->columns['DisplayName'])) {
			$this->execute('ALTER TABLE `PBXNumbers` ADD COLUMN `DisplayName` varchar(255) DEFAULT NULL;');
		}
		if (! isset($table->columns['ProviderId'])) {
			$this->execute('ALTER TABLE `PBXNumbers` ADD COLUMN `ProviderId` int(11) DEFAULT NULL;');
		}
		
		$table = Yii::app()->db->schema->getTable('PBXNumbersRelations');
		if (isset($table->columns['ChannelId'])) {
			if (Yii::app()->db->schema->getTable('PBXChannels') != null) {
				$this->execute('
					update `PBXNumbers` n
					inner join `PBXNumbersRelations` nr on nr.`Number`=n.`Number`
					inner join `PBXChannels` c on  nr.`ChannelId`=c.`Id`
					set n.`Type`=c.`Type`,
					n.`Name`=c.`Name`,
					n.`KeyName`=c.`KeyName`,
					n.`DisplayName`=c.`DisplayName`,
					n.`ProviderId`=c.`ProviderId`;');
			}
			if (array_key_exists('ChannelId', $table->foreignKeys))
				$this->dropForeignKey('PBXNumbersRelations_ChanelId_FK', 'PBXNumbersRelations');
			$this->execute('ALTER TABLE `PBXNumbersRelations` DROP COLUMN `ChannelId`;');
		}
		
		if (Yii::app()->db->schema->getTable('PBXChannels') != null)
			$this->execute('DROP TABLE IF EXISTS `PBXChannels`;');
		
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m160504_073819_CLEV_179/CI_RemoveLog.sql');
		
		$this->execute('update `StaticPages` sp set sp.`spTitle`="Статистика по PBX Номерам" where sp.`spUrl` = "reports-chanout";');
		
		$this->execute('ALTER TABLE `Settings` MODIFY COLUMN `param` VARCHAR(30) COLLATE utf8_general_ci NOT NULL;');
		$this->execute('ALTER TABLE `Settings` MODIFY COLUMN `value` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL;');
		
		$this->execute('select `spId` into @parId from `StaticPages` where `spUrl`="reports";
		delete from `StaticPages` where `spUrl`="reports-callback";
		INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spVisible`)
		VALUE ("Статистика по отзвонам",@parId,5,"reports-callback","/callslog/report/callback","addeditr","1");');
	}
	public function down() {
		echo "m160504_073819_CLEV_179 does not support migration down.\n";
		return false;
	}
}