<?php
class m150602_082527_CLEV_151 extends CDbMigration {
	public function safeUp() {
		$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior'));
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150602_082527_CLEV_151/script_contacts.sql');
		$table = Yii::app()->db->schema->getTable('Users');
		if (array_key_exists('uGroups_gID', $table->foreignKeys))
			$this->dropForeignKey('FK_Reference_9', 'Users');
		if (isset($table->columns['uGroups_gID']))
			$this->dropColumn('Users', 'uGroups_gID');
		if (array_key_exists('uRole', $table->foreignKeys))
			$this->dropForeignKey('FK_Reference_10', 'Users');
		if (isset($table->columns['uRole']))
			$this->dropColumn('Users', 'uRole');
		
		$table = Yii::app()->db->schema->getTable('Queue');
		if (array_key_exists('qGroups_gID', $table->foreignKeys))
			$this->dropForeignKey('FK_queue_groups', 'Queue');
		if (isset($table->columns['qGroups_gID']))
			$this->dropColumn('Queue', 'qGroups_gID');
		
		$table = Yii::app()->db->schema->getTable('ChannelOwners');
		if ($table != null) {
			if (array_key_exists('coGroups_gID', $table->foreignKeys))
				$this->dropForeignKey('FK_Reference_29', 'ChannelOwners');
			if (isset($table->columns['coGroups_gID']))
				$this->dropColumn('ChannelOwners', 'coGroups_gID');
		}
		
		$table = Yii::app()->db->schema->getTable('Departments');
		if (array_key_exists('dGroups_gID', $table->foreignKeys))
			$this->dropForeignKey('FK_dep_groups', 'Departments');
		if (isset($table->columns['dGroups_gID']))
			$this->dropColumn('Departments', 'dGroups_gID');
		if (! isset($table->columns['dKeyName'])) {
			$this->execute('ALTER TABLE `Departments` ADD COLUMN `dKeyName` VARCHAR(80) NOT NULL AFTER `dID`;');
			$this->execute('update `Departments` set dkeyName = dName');
		}
		
		$table = Yii::app()->db->schema->getTable('PhoneBook');
		if (array_key_exists('pbGroups_gID', $table->foreignKeys))
			$this->dropForeignKey('FK_Reference_14', 'PhoneBook');
		if (isset($table->columns['pbGroups_gID']))
			$this->dropColumn('PhoneBook', 'pbGroups_gID');
		
		if (Yii::app()->db->schema->getTable('Groups') != null) {
			$command = Yii::app()->db->createCommand('select gName from `Groups` limit 1');
			$StatCaption = $command->queryScalar();
			$this->execute('REPLACE INTO `Settings` SET `param` = "StatCaption", `value` = "' . $StatCaption . '"');
			$this->execute('DROP TABLE IF EXISTS `Groups`;');
		}
		$this->execute('DROP TABLE IF EXISTS `Roles`;');
		
		$table = Yii::app()->db->schema->getTable('Users');
		if (! isset($table->columns['rRightsStamp']))
			$this->execute('ALTER TABLE `Users` ADD COLUMN `rRightsStamp` VARCHAR(80) COLLATE utf8_general_ci NOT NULL DEFAULT "123";');
		
		$table = Yii::app()->db->schema->getTable('StaticPages');
		if (! isset($table->columns['spRightName']))
			$this->execute('ALTER TABLE `StaticPages` ADD COLUMN `spRightName` VARCHAR(80) DEFAULT "rViewReport";');
		
		$this->execute('update `StaticPages` set `spRightName`="" where `spParentID`=-1;');
		$this->execute('update `StaticPages` set `spRightName`="rViewStat" where `spExtUrl`="/callslog";');
		
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150602_082527_CLEV_151/script1.sql');
		// для any все права
		$command = Yii::app()->db->createCommand('select uID from Users where uIdentity = "any" and uPassword = "ffa1839b6545fa1b60cc8373ed20269a716a94bece4211981a4f04d695b87491"');
		if ($command->queryScalar() === "1")
			$this->execute('insert into `UsersRights` values(1,"All");');
		
		if (Yii::app()->db->schema->getTable('DepartmentMembers') != null) {
			$this->execute('DROP TABLE IF EXISTS `DepartmentAgents`;');
			$this->execute('CREATE TABLE `DepartmentAgents` (
			`DepartmentId` INTEGER(11) NOT NULL,
			`SIPId` INTEGER(11) NOT NULL,
			PRIMARY KEY (`DepartmentId`,`SIPId`) USING BTREE,
			KEY `DepartmentId` (`DepartmentId`) USING BTREE,
			KEY `SIPId` (`SIPId`) USING BTREE,
			CONSTRAINT `DepartmentAgents_DepId_FK` FOREIGN KEY (`DepartmentId`) REFERENCES `Departments` (`dID`) ON DELETE CASCADE ON UPDATE CASCADE,
			CONSTRAINT `DepartmentAgents_SIPId_FK` FOREIGN KEY (`SIPId`) REFERENCES `SIP` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB CHARACTER SET "utf8" COLLATE "utf8_general_ci";');
			
			$this->execute('insert into DepartmentAgents(`DepartmentId`, `SIPId`)
			select dm.`dpDepartments_dID`, s.`id`
			from `DepartmentMembers` dm
			inner join `SIP` s on s.`sChannels_cID`=dm.`dpChannels_cID`;');
		}
		
		$table = Yii::app()->db->schema->getTable('QueueMembers');
		if (array_key_exists('interface', $table->foreignKeys))
			try {
				$this->dropForeignKey('FK_Reference_16', 'QueueMembers');
			} catch ( Exception $e ) {
				echo 'FK_Reference_16 is Delete?: ' . $e->getMessage() . "\n";
			}
		
		$table = Yii::app()->db->schema->getTable('SIP');
		if (array_key_exists('sChannels_cID', $table->foreignKeys))
			$this->dropForeignKey('FK_Reference_23', 'SIP');
		if (isset($table->columns['sChannels_cID']))
			$this->dropColumn('SIP', 'sChannels_cID');
		if (! isset($table->columns['sip_name'])) {
			$this->execute('ALTER TABLE `SIP` ADD COLUMN `sip_name` VARCHAR(128) NOT NULL AFTER `name`;');
			$this->execute('ALTER TABLE `SIP` ADD INDEX  (`sip_name`);');
			$this->execute('update `SIP` s set s.`sip_name` = CONCAT("SIP/",s.`name`);');
			$this->execute('ALTER TABLE `QueueMembers` ADD INDEX (`interface`);');
			$this->execute('delete from `QueueMembers` where not exists (select 1 from `SIP` where `interface`=`sip_name`)');
			$this->execute('ALTER TABLE `QueueMembers` ADD CONSTRAINT `QueueMembers_sip_name_FK` FOREIGN KEY (`interface`) REFERENCES `SIP` (`sip_name`) ON DELETE CASCADE ON UPDATE CASCADE;');
		}
		
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150602_082527_CLEV_151/script2.sql');
		if (Yii::app()->db->schema->getTable('Channels') != null) {
			$this->execute('insert into `PBXChannels`(`Type`, `Name`, `KeyName`, `DisplayName`, `ProviderId`)
							select "SIP", SUBSTRING(c.`cName` FROM 5), c.`cName`, c.`cDescription`, null
							from `Channels` c
							where c.`cType`="1" and c.`cName` like "SIP%";');
			
			$this->execute('insert into `PBXChannels`(`Type`, `Name`, `KeyName`, `DisplayName`, `ProviderId`)
							select "Dongle", m.`mName`, concat("Dongle/", m.`mName`), m.`mNumber`, m.`mProviders_pID`
							from `Modems` m
							inner join `Channels` c on c.`cID`=m.`mChannels_cID`
							where c.`cType`="1" and c.`cName` like "Dongle%";');
			
			$this->execute('insert into `PBXNumbers`(Number)
							select m.`mNumber`
							from `Modems` m
							inner join `Channels` c on c.`cID`=m.`mChannels_cID`
							where c.`cType`="1" and c.`cName` like "Dongle%";');
			
			$this->execute('insert into PBXNumbersRelations(`Number`,`ChannelId`)
							select pn.`Number`, pc.`Id`
							from `PBXNumbers` pn
							inner join `PBXChannels` pc on pn.`Number`=pc.`Name`;');
		}
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150602_082527_CLEV_151/script3.sql');
		$this->execute('delete from SIP where secret is null');
		$this->execute('update `StaticPages` sp set sp.`spTitle`="Статистика по агентам" where sp.`spUrl`="reports-agents"');
		
		$this->execute('DROP TEMPORARY TABLE IF EXISTS `tmpIDs`;
						CREATE TEMPORARY TABLE `tmpIDs` (
							`ID` INTEGER(11) NOT NULL		
						) ENGINE=InnoDB CHARACTER SET "utf8" COLLATE "utf8_general_ci";
						insert into tmpIDs(ID) select u.`uID`from `Users` u ;
						insert ignore into `UsersRights`(UserId, `Name`) select `ID`, "ViewStat" from `tmpIDs`;
						insert ignore into `UsersRights`(UserId, `Name`) select `ID`, "ViewReport" from `tmpIDs`;
						insert ignore into `UsersRights`(UserId, `Name`) select `ID`, "ListenAudio" from `tmpIDs`;
						DROP TEMPORARY TABLE IF EXISTS `tmpIDs`;');
	}
	public function down() {
		echo "m150602_082527_CLEV_151 does not support migration down.\n";
		return false;
	}
}