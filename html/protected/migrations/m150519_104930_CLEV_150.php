<?php
class m150519_104930_CLEV_150 extends CDbMigration {
	public function safeUp() {
		$this->execute('
				select `spId` into @parId from `StaticPages` where `spUrl`="reports";
		
				delete from `StaticPages` where `spUrl`="reports-missedcalls";
				INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`,`spVisible`)
				VALUE ("Необработанные звонки",@parId,8,"reports-missedcalls","/callslog/report/missedcalls","addeditr",null,"1");
				
				delete from `StaticPages` where `spUrl`="reports-longtimecalls";
				INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`,`spVisible`)
				VALUE ("Длительные вызовы",@parId,9,"reports-longtimecalls","/callslog/report/longtimecalls","addeditr", null, "0");
		');
	}
	public function down() {
		echo "m150519_104930_CLEV_150 does not support migration down.\n";
		return false;
	}
}