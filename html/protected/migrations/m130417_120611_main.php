<?php

class m130417_120611_main extends CDbMigration {
	
    public function up() {
		$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior',));
        
        //$this->execute('create database if not exists asterisk');
        //create base tables
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/db.sql');
        //создание asterisk@localhost
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m141014_102343_CLEV27_138/script4_CI_ASTER_User.sql');     
    }

    public function down() {
        echo "m130417_120611_main does not support migration down.\n";
        return false;
    }

}