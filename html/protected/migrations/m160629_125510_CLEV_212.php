<?php
class m160629_125510_CLEV_212 extends CDbMigration {
	public function safeUp() {
		$table = Yii::app()->db->schema->getTable('Users');
		if (! isset($table->columns['IsRole']))
			$this->execute('ALTER TABLE `Users` ADD COLUMN `IsRole` TINYINT(1) NOT NULL DEFAULT 0;');
		
		$table = Yii::app()->db->schema->getTable('SIP');
		if (! isset($table->columns['userRole']))
			$this->execute('ALTER TABLE `SIP` ADD COLUMN `userRole` varchar(80) DEFAULT NULL;');
		
		$this->execute('update `StaticPages` sp
			 set sp.`spRightName` = "rPhoneBook"
			 where sp.`spExtUrl` = "/phonebook"');
		
		$this->execute('insert into `UsersRights`  (`Name`, `UserId`)
			select "PhoneBook", `UserId`
			from `UsersRights` ur1
			where `Name` = "ViewReport"
			and not exists
			(select 1 from `UsersRights` ur2
				 where ur2.`Name` = "PhoneBook"
					and ur1.`UserId` = ur2.`UserId`)');
	}
	public function down() {
		echo "m160629_125510_CLEV_212 does not support migration down.\n";
		return false;
	}
}