<?php

/**
 * Fixed a lot of bugs, make up. Added dropdown multiselects, realize reset button according to its issue.
 *  
 * Changed CallInfo.cDirection enum set values (0,1,2,3 => 'outbound','inbound','local', 'callback'). No default value.
 * Changed PhoneBook.pbBlacked enum set values (0,1 => 'no','yes'). Def. val is 'no'.
 * Changed QueueLog.qlCalldate from char(30) to datetime not null default '0000-00-00 00:00:00'.
 * CallInfo, CallInfoForm labels carried beyond this classes.
 * general model refactoring (CallInfo, QueueLog ).
 * moved enum field values from model to message files to achieve ability dynamically build data for list html elements.
 * Create PreparedCDR view for fater export CDR to CSV.
 * Fixed agents monitoring status (based on port)
 * 
 * * Done CLEV-94
 * * Done CLEV-105
 * * Done CLEV-57
 * 
 * * Fixed CLEV-62
 * * Fixed CLEV-82
 * * Fixed CLEV-54
 * * Fixed CLEV-65
 * * Fixed CLEV-71
 * * Fixed CLEV-52
 * * Fixed CLEV-53
 * * Fixed CLEV-56
 * * Fixed CLEV-70
 * * Fixed CLEV-66
 * * Fixed CLEV-64
 * * Fixed CLEV-95
 * * Fixed CLEV-80
 * * Fixed CLEV-78
 * * Fixed CLEV-67
 * * Fixed CLEV-68
 * 
 * * Minor corrections in the front-end design(tooltips, missed tag at /modemstat module)
 * * Display audio player inside each row's popover at /callinfo.
 * * Eport CDR to CSV became faster (~6 sec for 100k rows)
 * * fixed security weakness for callinfo
 * 
 */
class m131017_134303_1_2_4 extends CDbMigration {

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp() {
    	$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior',));
    	
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/procedures/getParamsByDirection.sql');
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/triggers/cdr/data-flow.sql');

        $this->alterColumn('CallInfo', 'cDirection', "varchar(128)");
        $this->update('CallInfo', array('cDirection' => 'outbound'), 'cDirection = "0"');
        $this->update('CallInfo', array('cDirection' => 'inbound'), 'cDirection = "1"');
        $this->update('CallInfo', array('cDirection' => 'local'), 'cDirection = "2"');
        $this->update('CallInfo', array('cDirection' => 'callback'), 'cDirection = "3"');
        $this->alterColumn('CallInfo', 'cDirection', "enum('outbound','inbound','local', 'callback') not null");

        $this->alterColumn('PhoneBook', 'pbBlacked', "varchar(128)");
        $this->update('PhoneBook', array('pbBlacked' => 'no'), 'pbBlacked = "0"');
        $this->update('PhoneBook', array('pbBlacked' => 'yes'), 'pbBlacked = "1"');
        $this->alterColumn('PhoneBook', 'pbBlacked', "enum('yes','no') not null default 'no'");

        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/views/PreparedCDR.sql');

        $this->alterColumn('QueueLog', 'qlCalldate', "datetime not null default '0000-00-00 00:00:00'");
    }

}