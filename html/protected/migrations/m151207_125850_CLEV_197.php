<?php
class m151207_125850_CLEV_197 extends CDbMigration {
	public function safeUp() {
		$this->execute('call drop_index_if_exists("PBXChannels","PBXChannels_KeyNameIdx");');
		$this->execute('ALTER TABLE `PBXChannels` ADD  INDEX `PBXChannels_KeyNameIdx` (`KeyName`);');
	}
	public function down() {
		echo "m151207_125850_CLEV_197 does not support migration down.\n";
		return false;
	}
}