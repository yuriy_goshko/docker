<?php
class m150108_104312_CLEV_27_138_147 extends CDbMigration {
	public function safeUp() {
		$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior'));
		
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150108_104312_CLEV_27_138_147/script1_functions.sql');
		$this->execute('DROP TABLE IF EXISTS `tbl_upgrades`');
		$this->execute('DROP TABLE IF EXISTS `Version`');
		
		$this->execute('
			update `StaticPages` sp
			set spTitle = CONCAT(spTitle,"(OLD)"), spVisible=0
			where (sp.`spUrl`= "Otchety" or sp.`spUrl` = "Detalnaja-statistika-zvonkov")
			and spTitle not like "%(OLD)"
					');
		
		$this->execute('
			delete from `StaticPages` where `spUrl`="rashirenaia-statistika-zvonkov" and `spExtUrl`="/callslog";
			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`)
			VALUE ("Детальная статистика звонков",126,0,"rashirenaia-statistika-zvonkov","/callslog","addeditr","cdr");');
		
		$this->execute('
			delete from `StaticPages` where `spUrl`="reports";
			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`)
			VALUE ("Отчеты",126,2,"reports","","addeditr","reports");');
		
		$this->execute('
			select `spId` into @parId from `StaticPages` where `spUrl`="reports";

			delete from `StaticPages` where `spUrl`="reports-general";
			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`)
			VALUE ("Общая статистика звонков",@parId,1,"reports-general","/callslog/report","addeditr",null);

			delete from `StaticPages` where `spUrl`="reports-departments";
			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`)
			VALUE ("Статистика по отделам",@parId,2,"reports-departments","/callslog/report/departments","addeditr",null);

			delete from `StaticPages` where `spUrl`="reports-agents";
			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`)
			VALUE ("Статистика по пользователям",@parId,2,"reports-agents","/callslog/report/agents","addeditr",null);

			delete from `StaticPages` where `spUrl`="reports-chanout";
			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`)
			VALUE ("Статистика по внешним каналам",@parId,2,"reports-chanout","/callslog/report/chanout","addeditr",null);
				');
		
		$table = Yii::app()->db->schema->getTable('Departments');
		if (! isset($table->columns['dColor'])) {
			$this->execute('
				ALTER TABLE `Departments` ADD COLUMN `dColor` VARCHAR(40) DEFAULT NULL;						
				update `Departments` set dColor = LOWER(CONCAT("#",HEX(rand()*1000&255),HEX(rand()*1000&255),HEX(rand()*1000&255)));						
				ALTER TABLE `Departments` MODIFY COLUMN `dColor` VARCHAR(40) COLLATE utf8_general_ci NOT NULL;
					');
		}
		
		$this->execute('
			delete from `CI_RulesForWritingNumbers` where ActionType="ReplaceFirst" and NewValue="" and OldValue="+";
			INSERT INTO `CI_RulesForWritingNumbers` (`CharsLength`, `ActionType`, `OldValue`, `NewValue`, `OnlyRealNumbers`, `OrderKey`) 
			VALUES (13, "ReplaceFirst", "+", "", 0, 0);
						');
		
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150108_104312_CLEV_27_138_147/script2_audiofiles.sql');
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150108_104312_CLEV_27_138_147/script3_RemLog.sql');
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150108_104312_CLEV_27_138_147/script4_AutoCalls.sql');
		
		$table = Yii::app()->db->schema->getTable('cel');
		if (isset($table->columns['callStatus']))
			$this->execute('ALTER TABLE `cel` DROP COLUMN `callStatus`;');
		if (isset($table->columns['linkedLid']))
			$this->execute('ALTER TABLE `cel` DROP COLUMN `linkedLid`;');
		if (isset($table->columns['linkedRid']))
			$this->execute('ALTER TABLE `cel` DROP COLUMN `linkedRid`;');
		if (isset($table->columns['causeCode']))
			$this->execute('ALTER TABLE `cel` DROP COLUMN `causeCode`;');
		if (isset($table->columns['causeChanname']))
			$this->execute('ALTER TABLE `cel` DROP COLUMN `causeChanname`;');

		$this->execute('							
				DELETE FROM Settings where param = "tnsNotifierActive";
				INSERT INTO Settings VALUES ("tnsNotifierActive", "0");
				');		
		
	}
	public function down() {
		echo "m150108_104312_CLEV_27_138_147 does not support migration down.\n";
		return false;
	}
}