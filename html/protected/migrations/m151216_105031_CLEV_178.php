<?php
class m151216_105031_CLEV_178 extends CDbMigration {
	public function safeUp() {
		//баг. фикс, дубликат в CLEV-179
		$this->execute('ALTER TABLE `Settings` MODIFY COLUMN `param` VARCHAR(30) COLLATE utf8_general_ci NOT NULL;');
		$this->execute('ALTER TABLE `Settings` MODIFY COLUMN `value` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL;');
		//end 
		$this->execute('REPLACE INTO `Settings` SET `param` = "CCSCommandsPort", `value` = "8888"');
		$this->execute('REPLACE INTO `Settings` SET `param` = "ContextConfOut", `value` = "ami_conference"');
		$this->execute('REPLACE INTO `Settings` SET `param` = "ContextConfOutAudio", `value` = "ami_conference_record"');		
	}
	public function down() {
		echo "m151216_105031_CLEV_178 does not support migration down.\n";
		return false;
	}
}