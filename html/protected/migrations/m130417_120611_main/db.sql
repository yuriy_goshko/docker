-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: asterisk
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.12.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CallInfo`
--

DROP TABLE IF EXISTS `CallInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CallInfo` (
  `cID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cSrc` varchar(80) NOT NULL DEFAULT '',
  `cDst` varchar(80) NOT NULL DEFAULT '',
  `cDcontext` varchar(80) NOT NULL DEFAULT '',
  `cDuration` int(11) NOT NULL DEFAULT '0',
  `cBillsec` int(11) NOT NULL DEFAULT '0',
  `cDisposition` enum('FAILED','BUSY','ANSWERED','UNKNOWN','NO ANSWER','uFAILED','uHANGUP') NOT NULL DEFAULT 'UNKNOWN',
  `cUniqueid` varchar(32) NOT NULL DEFAULT '',
  `cCalldate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cDirection` enum('0','1','2','3') NOT NULL DEFAULT '0',
  `cChannelChannels_cID` int(11) unsigned DEFAULT NULL,
  `cDstchannelChannels_cID` int(11) unsigned DEFAULT NULL,
  `cOutchanChannels_cID` int(11) unsigned DEFAULT NULL,
  `cQueue` varchar(128) DEFAULT NULL,
  `cGroups_gID` int(11) NOT NULL,
  `cActiveAgent` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`cID`),
  KEY `AK_Key_2` (`cUniqueid`),
  KEY `AK_Key_3` (`cCalldate`),
  KEY `FK_Reference_12` (`cChannelChannels_cID`),
  KEY `FK_Reference_13` (`cDstchannelChannels_cID`),
  KEY `FK_Reference_27` (`cOutchanChannels_cID`),
  KEY `FK_FK_callinfo_groups` (`cGroups_gID`),
  KEY `FK_AgentCallinfo_channels` (`cActiveAgent`),
  KEY `FK_callinfo_queue` (`cQueue`),
  CONSTRAINT `FK_callinfo_queue` FOREIGN KEY (`cQueue`) REFERENCES `Queue` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_AgentCallinfo_channels` FOREIGN KEY (`cActiveAgent`) REFERENCES `Channels` (`cID`),
  CONSTRAINT `FK_FK_callinfo_groups` FOREIGN KEY (`cGroups_gID`) REFERENCES `Groups` (`gID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Reference_12` FOREIGN KEY (`cChannelChannels_cID`) REFERENCES `Channels` (`cID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Reference_13` FOREIGN KEY (`cDstchannelChannels_cID`) REFERENCES `Channels` (`cID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Reference_27` FOREIGN KEY (`cOutchanChannels_cID`) REFERENCES `Channels` (`cID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CallInfo`
--

LOCK TABLES `CallInfo` WRITE;
/*!40000 ALTER TABLE `CallInfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `CallInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChannelOwners`
--

DROP TABLE IF EXISTS `ChannelOwners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChannelOwners` (
  `coGroups_gID` int(11) DEFAULT NULL,
  `coID` int(11) NOT NULL AUTO_INCREMENT,
  `coChannels_cID` int(11) unsigned NOT NULL,
  `coName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`coID`),
  UNIQUE KEY `AK_Key_2` (`coGroups_gID`,`coChannels_cID`),
  KEY `FK_Reference_28` (`coChannels_cID`),
  CONSTRAINT `FK_Reference_29` FOREIGN KEY (`coGroups_gID`) REFERENCES `Groups` (`gID`),
  CONSTRAINT `FK_Reference_28` FOREIGN KEY (`coChannels_cID`) REFERENCES `Channels` (`cID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChannelOwners`
--

LOCK TABLES `ChannelOwners` WRITE;
/*!40000 ALTER TABLE `ChannelOwners` DISABLE KEYS */;
/*!40000 ALTER TABLE `ChannelOwners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Channels`
--

DROP TABLE IF EXISTS `Channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Channels` (
  `cID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cName` varchar(255) NOT NULL,
  `cType` enum('0','1') DEFAULT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cName`),
  UNIQUE KEY `AK_Key_3` (`cID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Channels`
--

LOCK TABLES `Channels` WRITE;
/*!40000 ALTER TABLE `Channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `Channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DepartmentMembers`
--

DROP TABLE IF EXISTS `DepartmentMembers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DepartmentMembers` (
  `dpDepartments_dID` int(11) NOT NULL,
  `dpChannels_cID` int(11) unsigned NOT NULL,
  PRIMARY KEY (`dpChannels_cID`,`dpDepartments_dID`),
  KEY `FK_depmem_dep` (`dpDepartments_dID`),
  CONSTRAINT `FK_depmem_dep` FOREIGN KEY (`dpDepartments_dID`) REFERENCES `Departments` (`dID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_depmem_channels` FOREIGN KEY (`dpChannels_cID`) REFERENCES `Channels` (`cID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DepartmentMembers`
--

LOCK TABLES `DepartmentMembers` WRITE;
/*!40000 ALTER TABLE `DepartmentMembers` DISABLE KEYS */;
/*!40000 ALTER TABLE `DepartmentMembers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DepartmentQueues`
--

DROP TABLE IF EXISTS `DepartmentQueues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DepartmentQueues` (
  `dqDepartments_dID` int(11) NOT NULL,
  `dqQueues_qName` varchar(128) NOT NULL,
  PRIMARY KEY (`dqDepartments_dID`,`dqQueues_qName`),
  KEY `FK_depqueues_queue` (`dqQueues_qName`),
  CONSTRAINT `FK_dpqueues_dep` FOREIGN KEY (`dqDepartments_dID`) REFERENCES `Departments` (`dID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_depqueues_queue` FOREIGN KEY (`dqQueues_qName`) REFERENCES `Queue` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DepartmentQueues`
--

LOCK TABLES `DepartmentQueues` WRITE;
/*!40000 ALTER TABLE `DepartmentQueues` DISABLE KEYS */;
/*!40000 ALTER TABLE `DepartmentQueues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Departments`
--

DROP TABLE IF EXISTS `Departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Departments` (
  `dID` int(11) NOT NULL AUTO_INCREMENT,
  `dName` varchar(80) NOT NULL,
  `dGroups_gID` int(11) DEFAULT NULL,
  PRIMARY KEY (`dID`),
  UNIQUE KEY `AK_Key_2` (`dName`,`dGroups_gID`),
  KEY `FK_dep_groups` (`dGroups_gID`),
  CONSTRAINT `FK_dep_groups` FOREIGN KEY (`dGroups_gID`) REFERENCES `Groups` (`gID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Departments`
--

LOCK TABLES `Departments` WRITE;
/*!40000 ALTER TABLE `Departments` DISABLE KEYS */;
/*!40000 ALTER TABLE `Departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `gID` int(11) NOT NULL AUTO_INCREMENT,
  `gName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`gID`),
  UNIQUE KEY `AK_Key_2` (`gName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
INSERT INTO `Groups` VALUES (1,'☏ Cleverty');
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Modems`
--

DROP TABLE IF EXISTS `Modems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Modems` (
  `mProviders_pID` int(11) NOT NULL,
  `mID` int(11) NOT NULL AUTO_INCREMENT,
  `mChannels_cID` int(11) unsigned NOT NULL,
  `mName` varchar(30) DEFAULT NULL,
  `mNumber` char(13) DEFAULT NULL,
  PRIMARY KEY (`mID`),
  UNIQUE KEY `AK_Key_2` (`mName`),
  KEY `FK_Reference_21` (`mProviders_pID`),
  KEY `FK_Reference_22` (`mChannels_cID`),
  CONSTRAINT `FK_Reference_22` FOREIGN KEY (`mChannels_cID`) REFERENCES `Channels` (`cID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Reference_21` FOREIGN KEY (`mProviders_pID`) REFERENCES `Providers` (`pID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Modems`
--

LOCK TABLES `Modems` WRITE;
/*!40000 ALTER TABLE `Modems` DISABLE KEYS */;
/*!40000 ALTER TABLE `Modems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ModemsBalanceLog`
--

DROP TABLE IF EXISTS `ModemsBalanceLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModemsBalanceLog` (
  `mblID` int(11) NOT NULL AUTO_INCREMENT,
  `mName` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`mblID`),
  KEY `FK_Reference_24` (`mName`),
  CONSTRAINT `FK_Reference_24` FOREIGN KEY (`mName`) REFERENCES `Modems` (`mName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ModemsBalanceLog`
--

LOCK TABLES `ModemsBalanceLog` WRITE;
/*!40000 ALTER TABLE `ModemsBalanceLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `ModemsBalanceLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ModemsState`
--

DROP TABLE IF EXISTS `ModemsState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModemsState` (
  `msID` int(11) NOT NULL AUTO_INCREMENT,
  `msModems_mName` varchar(30) DEFAULT NULL,
  `msState` enum('free','disconnected','inuse') DEFAULT NULL,
  `msStateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`msID`),
  KEY `FK_Reference_25` (`msModems_mName`),
  CONSTRAINT `FK_Reference_25` FOREIGN KEY (`msModems_mName`) REFERENCES `Modems` (`mName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ModemsState`
--

LOCK TABLES `ModemsState` WRITE;
/*!40000 ALTER TABLE `ModemsState` DISABLE KEYS */;
/*!40000 ALTER TABLE `ModemsState` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ModemsStateLog`
--

DROP TABLE IF EXISTS `ModemsStateLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModemsStateLog` (
  `mslModems_mName` varchar(30) DEFAULT NULL,
  `mslState` enum('free','disconnected','inuse') DEFAULT NULL,
  `mslStateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mslID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`mslID`),
  KEY `FK_Reference_20` (`mslModems_mName`),
  CONSTRAINT `FK_Reference_20` FOREIGN KEY (`mslModems_mName`) REFERENCES `Modems` (`mName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ModemsStateLog`
--

LOCK TABLES `ModemsStateLog` WRITE;
/*!40000 ALTER TABLE `ModemsStateLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `ModemsStateLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MusicOnHold`
--

DROP TABLE IF EXISTS `MusicOnHold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MusicOnHold` (
  `name` varchar(80) NOT NULL,
  `directory` varchar(255) DEFAULT NULL,
  `application` varchar(255) DEFAULT NULL,
  `mode` varchar(80) DEFAULT 'files',
  `digit` char(1) DEFAULT NULL,
  `sort` char(10) DEFAULT NULL,
  `format` char(10) DEFAULT NULL,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MusicOnHold`
--

LOCK TABLES `MusicOnHold` WRITE;
/*!40000 ALTER TABLE `MusicOnHold` DISABLE KEYS */;
INSERT INTO `MusicOnHold` VALUES ('default','/var/lib/asterisk/moh/',NULL,'files','','','','2014-04-24 07:59:29');
/*!40000 ALTER TABLE `MusicOnHold` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PhoneBook`
--

DROP TABLE IF EXISTS `PhoneBook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PhoneBook` (
  `pbID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pbNumber` char(13) NOT NULL DEFAULT '',
  `pbOwner` varchar(40) NOT NULL DEFAULT '',
  `pbBlacked` enum('0','1') NOT NULL DEFAULT '0',
  `pbReason` text NOT NULL,
  `pbGroups_gID` int(11) NOT NULL,
  PRIMARY KEY (`pbID`),
  UNIQUE KEY `AK_Key_2` (`pbNumber`,`pbGroups_gID`),
  KEY `AK_Key_3` (`pbNumber`),
  KEY `FK_Reference_14` (`pbGroups_gID`),
  CONSTRAINT `FK_Reference_14` FOREIGN KEY (`pbGroups_gID`) REFERENCES `Groups` (`gID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PhoneBook`
--

LOCK TABLES `PhoneBook` WRITE;
/*!40000 ALTER TABLE `PhoneBook` DISABLE KEYS */;
/*!40000 ALTER TABLE `PhoneBook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Providers`
--

DROP TABLE IF EXISTS `Providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Providers` (
  `pID` int(11) NOT NULL AUTO_INCREMENT,
  `pName` varchar(30) NOT NULL,
  `pBalanceUssd` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`pID`),
  UNIQUE KEY `AK_Key_2` (`pName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Providers`
--

LOCK TABLES `Providers` WRITE;
/*!40000 ALTER TABLE `Providers` DISABLE KEYS */;
/*!40000 ALTER TABLE `Providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProvidersPatterns`
--

DROP TABLE IF EXISTS `ProvidersPatterns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProvidersPatterns` (
  `ppProviders_pID` int(11) DEFAULT NULL,
  `ppItem` varchar(255) DEFAULT NULL,
  KEY `FK_Reference_19` (`ppProviders_pID`),
  CONSTRAINT `FK_Reference_19` FOREIGN KEY (`ppProviders_pID`) REFERENCES `Providers` (`pID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProvidersPatterns`
--

LOCK TABLES `ProvidersPatterns` WRITE;
/*!40000 ALTER TABLE `ProvidersPatterns` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProvidersPatterns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Queue`
--

DROP TABLE IF EXISTS `Queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Queue` (
  `name` varchar(128) NOT NULL,
  `musiconhold` varchar(128) DEFAULT NULL,
  `announce` varchar(128) DEFAULT NULL,
  `context` varchar(128) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL,
  `monitor_type` varchar(50) NOT NULL DEFAULT 'MixMonitor',
  `monitor_format` varchar(128) DEFAULT NULL,
  `queue_youarenext` varchar(128) DEFAULT NULL,
  `queue_thereare` varchar(128) DEFAULT NULL,
  `queue_callswaiting` varchar(128) DEFAULT NULL,
  `queue_holdtime` varchar(128) DEFAULT NULL,
  `queue_minutes` varchar(128) DEFAULT NULL,
  `queue_seconds` varchar(128) DEFAULT NULL,
  `queue_lessthan` varchar(128) DEFAULT NULL,
  `queue_thankyou` varchar(128) DEFAULT NULL,
  `queue_reporthold` varchar(128) DEFAULT NULL,
  `announce_frequency` int(11) DEFAULT NULL,
  `announce_round_seconds` int(11) DEFAULT NULL,
  `announce_holdtime` varchar(128) DEFAULT NULL,
  `retry` int(11) DEFAULT NULL,
  `wrapuptime` int(11) DEFAULT NULL,
  `maxlen` int(11) DEFAULT NULL,
  `servicelevel` int(11) DEFAULT NULL,
  `strategy` varchar(128) DEFAULT NULL,
  `joinempty` varchar(128) DEFAULT NULL,
  `leavewhenempty` varchar(128) DEFAULT NULL,
  `eventmemberstatus` varchar(4) DEFAULT NULL,
  `eventwhencalled` varchar(4) DEFAULT NULL,
  `reportholdtime` tinyint(1) DEFAULT NULL,
  `memberdelay` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `timeoutrestart` tinyint(1) DEFAULT NULL,
  `periodic_announce` varchar(50) DEFAULT NULL,
  `periodic_announce_frequency` int(11) DEFAULT NULL,
  `ringinuse` tinyint(1) DEFAULT NULL,
  `qGroups_gID` int(11) DEFAULT NULL,
  `qName` varchar(128) NOT NULL,
  `qFileName` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `AK_Key_2` (`qGroups_gID`),
  CONSTRAINT `FK_queue_groups` FOREIGN KEY (`qGroups_gID`) REFERENCES `Groups` (`gID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Queue`
--

LOCK TABLES `Queue` WRITE;
/*!40000 ALTER TABLE `Queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `Queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QueueLog`
--

DROP TABLE IF EXISTS `QueueLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QueueLog` (
  `qlID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `qlQueuename` varchar(128) DEFAULT NULL,
  `qlCalldate` char(30) NOT NULL,
  `qlEvent` varchar(255) NOT NULL,
  `qlCallInfo_cUniqueid` varchar(32) NOT NULL DEFAULT '',
  `qlAgent` int(11) NOT NULL DEFAULT '-1',
  `qlCalltime` int(11) DEFAULT NULL,
  `qlWaittime` int(11) DEFAULT NULL,
  PRIMARY KEY (`qlID`),
  KEY `AK_Key_2` (`qlCallInfo_cUniqueid`),
  KEY `FK_Reference_18` (`qlQueuename`),
  CONSTRAINT `FK_Reference_18` FOREIGN KEY (`qlQueuename`) REFERENCES `Queue` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QueueLog`
--

LOCK TABLES `QueueLog` WRITE;
/*!40000 ALTER TABLE `QueueLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `QueueLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QueueMembers`
--

DROP TABLE IF EXISTS `QueueMembers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QueueMembers` (
  `interface` varchar(128) NOT NULL,
  `queue_name` varchar(128) NOT NULL,
  `penalty` int(11) DEFAULT NULL,
  `uniqueid` int(11) NOT NULL AUTO_INCREMENT,
  `membername` varchar(40) DEFAULT NULL,
  `paused` int(11) DEFAULT NULL,
  PRIMARY KEY (`interface`,`queue_name`),
  UNIQUE KEY `AK_Key_2` (`uniqueid`),
  KEY `FK_queumem_queue` (`queue_name`),
  CONSTRAINT `FK_queumem_queue` FOREIGN KEY (`queue_name`) REFERENCES `Queue` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Reference_16` FOREIGN KEY (`interface`) REFERENCES `Channels` (`cName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QueueMembers`
--

LOCK TABLES `QueueMembers` WRITE;
/*!40000 ALTER TABLE `QueueMembers` DISABLE KEYS */;
/*!40000 ALTER TABLE `QueueMembers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Roles`
--

DROP TABLE IF EXISTS `Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Roles` (
  `rID` int(11) NOT NULL AUTO_INCREMENT,
  `rName` varchar(255) NOT NULL,
  PRIMARY KEY (`rID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Roles`
--

LOCK TABLES `Roles` WRITE;
/*!40000 ALTER TABLE `Roles` DISABLE KEYS */;
INSERT INTO `Roles` VALUES (1,'admin'),(2,'agent');
/*!40000 ALTER TABLE `Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SIP`
--

DROP TABLE IF EXISTS `SIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIP` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `ipaddr` varchar(15) DEFAULT NULL,
  `port` int(5) DEFAULT NULL,
  `regseconds` int(11) DEFAULT NULL,
  `defaultuser` varchar(20) DEFAULT NULL,
  `fullcontact` varchar(35) DEFAULT NULL,
  `regserver` varchar(20) DEFAULT NULL,
  `useragent` varchar(20) DEFAULT NULL,
  `lastms` int(11) DEFAULT NULL,
  `host` varchar(40) DEFAULT 'dynamic',
  `type` enum('friend','user','peer') DEFAULT 'friend',
  `context` varchar(40) DEFAULT NULL,
  `permit` varchar(40) DEFAULT NULL,
  `deny` varchar(40) DEFAULT NULL,
  `secret` varchar(40) DEFAULT NULL,
  `md5secret` varchar(40) DEFAULT NULL,
  `remotesecret` varchar(40) DEFAULT NULL,
  `transport` enum('udp','tcp','udp,tcp','tcp,udp') DEFAULT NULL,
  `dtmfmode` enum('rfc2833','info','shortinfo','inband','auto') DEFAULT NULL,
  `directmedia` enum('yes','no','nonat','update') DEFAULT NULL,
  `nat` enum('yes','no','never','route') DEFAULT 'no',
  `callgroup` varchar(40) DEFAULT NULL,
  `pickupgroup` varchar(40) DEFAULT NULL,
  `language` varchar(40) DEFAULT NULL,
  `disallow` varchar(40) DEFAULT 'all',
  `allow` varchar(40) DEFAULT 'alaw',
  `insecure` varchar(40) DEFAULT NULL,
  `trustrpid` enum('yes','no') DEFAULT NULL,
  `progressinband` enum('yes','no','never') DEFAULT NULL,
  `promiscredir` enum('yes','no') DEFAULT NULL,
  `useclientcode` enum('yes','no') DEFAULT NULL,
  `accountcode` varchar(40) DEFAULT NULL,
  `setvar` varchar(40) DEFAULT NULL,
  `callerid` varchar(40) DEFAULT NULL,
  `amaflags` varchar(40) DEFAULT NULL,
  `callcounter` enum('yes','no') DEFAULT NULL,
  `busylevel` int(11) DEFAULT NULL,
  `allowoverlap` enum('yes','no') DEFAULT NULL,
  `allowsubscribe` enum('yes','no') DEFAULT NULL,
  `videosupport` enum('yes','no') DEFAULT NULL,
  `maxcallbitrate` int(11) DEFAULT NULL,
  `rfc2833compensate` enum('yes','no') DEFAULT NULL,
  `mailbox` varchar(40) DEFAULT NULL,
  `session-timers` enum('accept','refuse','originate') DEFAULT NULL,
  `session-expires` int(11) DEFAULT NULL,
  `session-minse` int(11) DEFAULT NULL,
  `session-refresher` enum('uac','uas') DEFAULT NULL,
  `t38pt_usertpsource` varchar(40) DEFAULT NULL,
  `regexten` varchar(40) DEFAULT NULL,
  `fromdomain` varchar(40) DEFAULT NULL,
  `fromuser` varchar(40) DEFAULT NULL,
  `qualify` varchar(40) DEFAULT 'no',
  `defaultip` varchar(40) DEFAULT NULL,
  `rtptimeout` int(11) DEFAULT NULL,
  `rtpholdtimeout` int(11) DEFAULT NULL,
  `sendrpid` enum('yes','no') DEFAULT NULL,
  `outboundproxy` varchar(40) DEFAULT NULL,
  `callbackextension` varchar(40) DEFAULT NULL,
  `timert1` int(11) DEFAULT NULL,
  `timerb` int(11) DEFAULT NULL,
  `qualifyfreq` int(11) DEFAULT NULL,
  `constantssrc` enum('yes','no') DEFAULT NULL,
  `contactpermit` varchar(40) DEFAULT NULL,
  `contactdeny` varchar(40) DEFAULT NULL,
  `usereqphone` enum('yes','no') DEFAULT NULL,
  `textsupport` enum('yes','no') DEFAULT NULL,
  `faxdetect` enum('yes','no') DEFAULT NULL,
  `buggymwi` enum('yes','no') DEFAULT NULL,
  `auth` varchar(40) DEFAULT NULL,
  `fullname` varchar(40) DEFAULT NULL,
  `trunkname` varchar(40) DEFAULT NULL,
  `cid_number` varchar(40) DEFAULT NULL,
  `callingpres` enum('allowed_not_screened','allowed_passed_screen','allowed_failed_screen','allowed','prohib_not_screened','prohib_passed_screen','prohib_failed_screen','prohib') DEFAULT NULL,
  `mohinterpret` varchar(40) DEFAULT NULL,
  `mohsuggest` varchar(40) DEFAULT NULL,
  `parkinglot` varchar(40) DEFAULT NULL,
  `hasvoicemail` enum('yes','no') DEFAULT NULL,
  `subscribemwi` enum('yes','no') DEFAULT NULL,
  `vmexten` varchar(40) DEFAULT NULL,
  `autoframing` enum('yes','no') DEFAULT NULL,
  `rtpkeepalive` int(11) DEFAULT NULL,
  `call-limit` int(11) DEFAULT NULL,
  `g726nonstandard` enum('yes','no') DEFAULT NULL,
  `ignoresdpversion` enum('yes','no') DEFAULT NULL,
  `allowtransfer` enum('yes','no') DEFAULT NULL,
  `dynamic` enum('yes','no') DEFAULT NULL,
  `sChannels_cID` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `ipaddr` (`ipaddr`,`port`),
  KEY `host` (`host`,`port`),
  KEY `FK_Reference_23` (`sChannels_cID`),
  CONSTRAINT `FK_Reference_23` FOREIGN KEY (`sChannels_cID`) REFERENCES `Channels` (`cID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SIP`
--

LOCK TABLES `SIP` WRITE;
/*!40000 ALTER TABLE `SIP` DISABLE KEYS */;
/*!40000 ALTER TABLE `SIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StaticPages`
--

DROP TABLE IF EXISTS `StaticPages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StaticPages` (
  `spID` int(11) NOT NULL AUTO_INCREMENT,
  `spTitle` varchar(255) NOT NULL,
  `spText` text,
  `spKey` text,
  `spDesc` text,
  `spParentID` int(11) NOT NULL DEFAULT '-1',
  `spOrder` int(11) DEFAULT NULL,
  `spUpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `spUrl` varchar(255) DEFAULT NULL,
  `spExtUrl` varchar(255) DEFAULT NULL,
  `spVisible` enum('0','1') NOT NULL DEFAULT '1',
  `spType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`spID`),
  UNIQUE KEY `AK_Key_2` (`spTitle`,`spParentID`)
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StaticPages`
--

LOCK TABLES `StaticPages` WRITE;
/*!40000 ALTER TABLE `StaticPages` DISABLE KEYS */;
INSERT INTO `StaticPages` VALUES (135,'Детальная статистика звонков',NULL,NULL,NULL,126,2,'2013-02-27 10:58:05','Detalnaja-statistika-zvonkov','/callinfo','1','addeditr'),(134,'Отчет по агентам в очередях',NULL,NULL,NULL,139,10,'2013-02-21 09:29:38','Otchet-po-agentam-v-ocheredjah','/callinfo/queuebyagents','1','addeditr'),(133,'Отчет по очередям(дни недели)',NULL,NULL,NULL,139,9,'2013-02-21 09:29:16','Otchet-po-ocheredjam_dni-nedeli_','/callinfo/queuebydays','1','addeditr'),(132,'Отчет по очередям(часы)',NULL,NULL,NULL,139,8,'2013-02-13 06:49:51','Otchet-po-ocheredjam_chasy_','/callinfo/queuebyhours','1','addeditr'),(128,'Телефонная книга',NULL,NULL,NULL,126,14,'2013-02-11 10:36:38','Telefonnaja-kniga','/phonebook','1','addeditr'),(146,'Отчет по пользователям',NULL,NULL,NULL,139,6,'2013-06-03 05:35:16','Otchet-po-polzovateljam','/callinfo/callreportbyagents','1','addeditr'),(126,'Главное меню',NULL,NULL,NULL,-1,1,'2013-02-11 10:33:31','Glavnoe-menju',NULL,'1','addeditm'),(137,'Отчет общей статистики звонков',NULL,NULL,NULL,139,4,'2013-05-28 09:04:49','Otchet-obschej-statistiki-zvonkov','/callinfo/callreport','1','addeditr'),(138,'Отчет по внешним каналам',NULL,NULL,NULL,139,5,'2013-05-29 04:39:35','Otchet-po-vneshnim-kanalam','/callinfo/callreportbyoc','1','addeditr'),(139,'Отчеты',NULL,NULL,NULL,126,3,'2013-05-29 04:41:13','Otchety','','1','addeditr'),(147,'Мониторинг агентов',NULL,NULL,NULL,149,12,'2013-06-04 15:22:28','Monitoring-agentov','/memberstat','1','addeditr'),(148,'Мониторинг модемов',NULL,NULL,NULL,149,13,'2013-06-04 15:22:47','Monitoring-modemov','/modemstat','1','addeditr'),(149,'Мониторинг',NULL,NULL,NULL,126,11,'2013-06-04 15:23:01','Monitoring','','1','addeditr'),(150,'Отчет по отделам',NULL,NULL,NULL,139,7,'2013-08-08 08:40:57','Otchet-po-otdelam','/callinfo/callreportbydep','1','addeditr');
/*!40000 ALTER TABLE `StaticPages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `uID` int(11) NOT NULL AUTO_INCREMENT,
  `uName` varchar(255) NOT NULL DEFAULT 'Hello, user',
  `uIdentity` varchar(80) NOT NULL DEFAULT 'default',
  `uPassword` varchar(64) NOT NULL,
  `uSalt` varchar(6) NOT NULL,
  `uGroups_gID` int(11) NOT NULL,
  `uRole` int(11) NOT NULL,
  PRIMARY KEY (`uID`),
  UNIQUE KEY `AK_Key_2` (`uIdentity`),
  KEY `AK_Key_3` (`uPassword`),
  KEY `FK_Reference_10` (`uRole`),
  KEY `FK_Reference_9` (`uGroups_gID`),
  CONSTRAINT `FK_Reference_9` FOREIGN KEY (`uGroups_gID`) REFERENCES `Groups` (`gID`),
  CONSTRAINT `FK_Reference_10` FOREIGN KEY (`uRole`) REFERENCES `Roles` (`rID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'Hello, user','any','ffa1839b6545fa1b60cc8373ed20269a716a94bece4211981a4f04d695b87491','ec7f7e',1,1);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Version`
--

DROP TABLE IF EXISTS `Version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Version` (
  `vVersion` tinyint(4) NOT NULL,
  `vSubVersion` tinyint(4) NOT NULL,
  `vLegacy` tinyint(4) NOT NULL,
  PRIMARY KEY (`vVersion`,`vSubVersion`,`vLegacy`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Version`
--

LOCK TABLES `Version` WRITE;
/*!40000 ALTER TABLE `Version` DISABLE KEYS */;
INSERT INTO `Version` VALUES (1,1,0);
/*!40000 ALTER TABLE `Version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdr`
--

DROP TABLE IF EXISTS `cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr` (
  `calldate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clid` varchar(80) NOT NULL DEFAULT '',
  `src` varchar(80) NOT NULL DEFAULT '',
  `dst` varchar(80) NOT NULL DEFAULT '',
  `src_num_in` varchar(80) DEFAULT NULL,
  `dst_num_in` varchar(80) DEFAULT NULL,
  `src_num_out` varchar(80) DEFAULT NULL,
  `dst_num_out` varchar(80) DEFAULT NULL,
  `dcontext` varchar(80) NOT NULL DEFAULT '',
  `channel` varchar(80) NOT NULL DEFAULT '',
  `dstchannel` varchar(80) NOT NULL DEFAULT '',
  `lastapp` varchar(80) NOT NULL DEFAULT '',
  `lastdata` varchar(80) NOT NULL DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT '0',
  `billsec` int(11) NOT NULL DEFAULT '0',
  `disposition` varchar(45) NOT NULL DEFAULT '',
  `amaflags` int(11) NOT NULL DEFAULT '0',
  `accountcode` varchar(20) NOT NULL DEFAULT '',
  `userfield` varchar(255) NOT NULL DEFAULT '',
  `uniqueid` varchar(32) NOT NULL DEFAULT '',
  KEY `calldate` (`calldate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Статистика звонков';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr`
--

LOCK TABLES `cdr` WRITE;
/*!40000 ALTER TABLE `cdr` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdr` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`asterisk`@`localhost`*/ /*!50003 TRIGGER `cdr.data-flow` AFTER INSERT ON `cdr`
FOR EACH ROW BEGIN
 CALL getChannelID(NEW.channel, @cChannelChannels_cID);
 CALL getChannelID(NEW.dstchannel, @cDstchannelChannels_cID);
 CALL getParamsByDirection(
    NEW.channel, 
    NEW.dstchannel, 
    NEW.src, 
    NEW.dst, 
    NEW.src_num_out,
    NEW.dst_num_out,
    NEW.src_num_in,
    NEW.dst_num_in,
    @cChannelChannels_cID, 
    @cDstchannelChannels_cID, 
    @cDirection, 
    @cOutChan, 
    @src, 
    @dst);
SELECT `qlEvent`, `qlCalltime`, `qlQueuename`, `qlCallInfo_cUniqueid` 
FROM `QueueLog`
WHERE `qlCallInfo_cUniqueid` = NEW.uniqueid 
ORDER BY `qlCalldate` 
DESC LIMIT 1
INTO @qlLastRowEvent,  @qlLastRowCalltime , @qlLastRowQueue, @qlLastRowUniqueid;
IF(SELECT ((SELECT @cDirection) = '2' OR (SELECT @cDirection = '0')))
THEN 
    SET @cActiveAgent = @cChannelChannels_cID;
ELSEIF(SELECT ((SELECT @cDirection) = '1' OR (SELECT @cDirection = '3')))
THEN
    SET @cActiveAgent = @cDstchannelChannels_cID;
ELSE SET @cActiveAgent = NULL;
END IF;
SET @cGroups_gID = NULL;
SELECT `coGroups_gID`  FROM `ChannelOwners` WHERE coChannels_cID = (SELECT @cActiveAgent) AND (SELECT @cActiveAgent) is NOT NULL LIMIT 1  INTO @cGroups_gID;
IF(SELECT(SELECT(@cGroups_gID)) IS NULL)
    THEN
    SELECT `qGroups_gID` FROM `Queue` WHERE name = (SELECT @qlLastRowQueue) LIMIT 1 INTO @cGroups_gID ;
END IF;
IF(SELECT(SELECT(@src)) IS NULL)
    THEN SET @src = NEW.src;
END IF;
IF(SELECT(SELECT(@dst)) IS NULL)
    THEN SET @dst = NEW.dst;
END IF;
IF((SELECT @cGroups_gID) IS NOT NULL)
THEN
IF(SELECT(NEW.userfield NOT LIKE '%callback%'                                                                            
    AND ( (SELECT(SELECT IF(COUNT(*), 1, 0) FROM `Channels` WHERE cType = '0' AND cID = (SELECT @cChannelChannels_cID))      
          OR (SELECT IF(COUNT(*), 1, 0) FROM `Channels` WHERE cType = '0' AND cID = (SELECT @cDstchannelChannels_cID)))
          OR (SELECT(SELECT IF(COUNT(*), 1, 0) FROM `QueueLog` WHERE qlCallInfo_cUniqueid =  NEW.uniqueid)))))
     THEN
INSERT INTO `CallInfo` Set 
                            cSrc = @src,
                            cDst = @dst,
                            cDcontext = NEW.dcontext,
                            cChannelChannels_cID = @cChannelChannels_cID,
                            cDstchannelChannels_cID = @cDstchannelChannels_cID,
                            cOutchanChannels_cID = @cOutChan,
                            cDuration = NEW.duration,
                            cBillsec = NEW.billsec,
                            cDisposition = NEW.disposition,
                            cUniqueid = NEW.uniqueid,
                            cCalldate = NEW.calldate,
                            cActiveAgent = @cActiveAgent,
                            cGroups_gID = @cGroups_gID,
                            cDirection = @cDirection;
END IF;
IF(SELECT(EXISTS(SELECT * FROM `QueueLog` as ql 
WHERE qlCallInfo_cUniqueid = NEW.uniqueid AND NEW.userfield LIKE '%callback%')))
THEN
    IF(SELECT IF(COUNT(*), 1, 0) FROM `QueueLog` WHERE qlCallInfo_cUniqueid =  NEW.uniqueid) THEN    
    SELECT sip.`name` INTO @dst FROM `Channels` chan LEFT JOIN `SIP` as sip ON sip.sChannels_cID = chan.cID  WHERE sip.sChannels_cID = @cDstchannelChannels_cID;
    INSERT INTO `CallInfo` Set 
                            cSrc = @src,
                            cDst = @dst,
                            cDcontext = NEW.dcontext,
                            cChannelChannels_cID = @cChannelChannels_cID,
                            cDstchannelChannels_cID = @cDstchannelChannels_cID,
                            cOutchanChannels_cID = @cOutChan,
                            cDuration = NEW.duration,
                            cBillsec = NEW.billsec,
                            cDisposition = NEW.disposition,
                            cUniqueid = NEW.uniqueid,
                            cCalldate = NEW.calldate,
                            cActiveAgent = @cActiveAgent,
                            cGroups_gID = @cGroups_gID,
                            cDirection = '3';    
    END IF;
END IF;
IF(SELECT(EXISTS(SELECT * FROM `QueueLog` WHERE qlCallInfo_cUniqueid = NEW.uniqueid ORDER BY `qlCalldate` DESC LIMIT 1)))
   THEN 
        IF(SELECT(@qlLastRowEvent = 'EXITWITHTIMEOUT' OR @qlLastRowEvent = 'ABANDON')) 
            THEN 
                IF(SELECT(@qlLastRowEvent = 'EXITWITHTIMEOUT'))                        
                    THEN 
                        IF(SELECT(EXISTS(SELECT * FROM `QueueLog` WHERE qlCallInfo_cUniqueid = NEW.uniqueid AND qlEvent = 'RINGNOANSWER')))
                            THEN SET @cDsiposition = 'NO ANSWER';                      
                            ELSE SET @cDsiposition = 'uFAILED';                        
                         END IF;
                    ELSE SET @cDsiposition = 'uHANGUP';                                
                END IF;
        ELSEIF(SELECT(@qlLastRowEvent = 'COMPLETEAGENT' OR @qlLastRowEvent = 'COMPLETECALLER' OR @qlLastRowEvent = 'TRANSFER'))
            THEN SET @cDsiposition= 'ANSWERED';                                        
        ELSE SET @cDsiposition='UNKNOWN';
        END IF;
    UPDATE `CallInfo` SET cBillsec = @qlLastRowCalltime, cDisposition=@cDsiposition, cQueue =  @qlLastRowQueue  WHERE cUniqueid = @qlLastRowUniqueid;
END IF;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `iaxfriends`
--

DROP TABLE IF EXISTS `iaxfriends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iaxfriends` (
  `name` varchar(40) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT 'friend',
  `username` varchar(40) DEFAULT NULL,
  `mailbox` varchar(40) DEFAULT NULL,
  `secret` varchar(40) DEFAULT NULL,
  `dbsecret` varchar(40) DEFAULT NULL,
  `context` varchar(40) DEFAULT NULL,
  `regcontext` varchar(40) DEFAULT NULL,
  `host` varchar(40) DEFAULT 'dynamic',
  `ipaddr` varchar(40) DEFAULT NULL,
  `port` int(5) DEFAULT NULL,
  `defaultip` varchar(20) DEFAULT NULL,
  `sourceaddress` varchar(20) DEFAULT NULL,
  `mask` varchar(20) DEFAULT NULL,
  `regexten` varchar(40) DEFAULT NULL,
  `regseconds` int(11) DEFAULT NULL,
  `accountcode` varchar(20) DEFAULT NULL,
  `mohinterpret` varchar(20) DEFAULT NULL,
  `mohsuggest` varchar(20) DEFAULT NULL,
  `inkeys` varchar(40) DEFAULT NULL,
  `outkey` varchar(40) DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `callerid` varchar(100) DEFAULT NULL,
  `cid_number` varchar(40) DEFAULT NULL,
  `sendani` varchar(10) DEFAULT NULL,
  `fullname` varchar(40) DEFAULT NULL,
  `trunk` varchar(3) DEFAULT NULL,
  `auth` varchar(20) DEFAULT NULL,
  `maxauthreq` varchar(5) DEFAULT NULL,
  `requirecalltoken` varchar(4) DEFAULT NULL,
  `encryption` varchar(20) DEFAULT NULL,
  `transfer` varchar(10) DEFAULT NULL,
  `jitterbuffer` varchar(3) DEFAULT NULL,
  `forcejitterbuffer` varchar(3) DEFAULT NULL,
  `disallow` varchar(40) DEFAULT NULL,
  `allow` varchar(40) DEFAULT NULL,
  `codecpriority` varchar(40) DEFAULT NULL,
  `qualify` varchar(10) DEFAULT NULL,
  `qualifysmoothing` varchar(10) DEFAULT NULL,
  `qualifyfreqok` varchar(10) DEFAULT NULL,
  `qualifyfreqnotok` varchar(10) DEFAULT NULL,
  `timezone` varchar(20) DEFAULT NULL,
  `adsi` varchar(10) DEFAULT NULL,
  `amaflags` varchar(20) DEFAULT NULL,
  `setvar` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `name` (`name`,`host`),
  KEY `name2` (`name`,`ipaddr`,`port`),
  KEY `ipaddr` (`ipaddr`,`port`),
  KEY `host` (`host`,`port`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iaxfriends`
--

LOCK TABLES `iaxfriends` WRITE;
/*!40000 ALTER TABLE `iaxfriends` DISABLE KEYS */;
/*!40000 ALTER TABLE `iaxfriends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetme`
--

DROP TABLE IF EXISTS `meetme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetme` (
  `bookid` int(11) NOT NULL AUTO_INCREMENT,
  `confno` char(80) NOT NULL DEFAULT '0',
  `starttime` datetime DEFAULT '1900-01-01 12:00:00',
  `endtime` datetime DEFAULT '2038-01-01 12:00:00',
  `pin` char(20) DEFAULT NULL,
  `adminpin` char(20) DEFAULT NULL,
  `opts` char(20) DEFAULT NULL,
  `adminopts` char(20) DEFAULT NULL,
  `recordingfilename` char(80) DEFAULT NULL,
  `recordingformat` char(10) DEFAULT NULL,
  `maxusers` int(11) DEFAULT NULL,
  `members` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bookid`),
  KEY `confno` (`confno`,`starttime`,`endtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetme`
--

LOCK TABLES `meetme` WRITE;
/*!40000 ALTER TABLE `meetme` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue_log`
--

DROP TABLE IF EXISTS `queue_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time` char(30) DEFAULT NULL,
  `callid` char(50) NOT NULL DEFAULT '',
  `queuename` char(50) DEFAULT NULL,
  `agent` char(50) DEFAULT NULL,
  `event` char(20) DEFAULT NULL,
  `data1` char(50) DEFAULT NULL,
  `data2` char(50) DEFAULT NULL,
  `data3` char(50) DEFAULT NULL,
  `data4` char(50) DEFAULT NULL,
  `data5` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bydate` (`time`),
  KEY `qname` (`queuename`,`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue_log`
--

LOCK TABLES `queue_log` WRITE;
/*!40000 ALTER TABLE `queue_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`asterisk`@`localhost`*/ /*!50003 TRIGGER `queue.data-flow` AFTER INSERT ON `queue_log`
FOR EACH ROW BEGIN
SET @qlAgent = IFNULL((SELECT `cID` FROM `Channels` WHERE cName = NEW.agent), -1);
IF (SELECT(NEW.event = 'CONNECT'))
    THEN SET @qlWaittime = new.data1;
ELSEIF (SELECT(NEW.event = 'EXITWITHTIMEOUT' OR NEW.event = 'ABANDON'))
    THEN  SET @qlWaittime = new.data3;
ELSE  SET @qlWaittime =NULL;
END IF;
IF (SELECT(NEW.event = 'COMPLETEAGENT' OR NEW.event = 'COMPLETECALLER'))
    THEN SET @qlCalltime = new.data2;
    DELETE FROM `QueueLog` WHERE qlCallInfo_cUniqueid = NEW.callid AND qlEvent = 'RINGNOANSWER'; 
ELSE SET @qlCalltime = NULL;
END IF;
SELECT IF(COUNT(*), 1, 0) FROM `QueueLog` WHERE `qlCallInfo_cUniqueid` = new.callid 
    AND qlEvent = 'RINGNOANSWER' AND qlAgent = @qlAgent AND new.event = 'RINGNOANSWER' INTO @isDuplicateRNA;
IF (SELECT (new.data1 = '0' AND new.event = 'RINGNOANSWER'))
    THEN SET @doNotDisturb = 1;
    ELSE SET @doNotDisturb = 0;
END IF;
IF (SELECT (new.event = 'RINGNOANSWER' AND (SELECT  IF(`qlEvent` = 'CONNECT',1 ,0) 
    FROM `QueueLog` WHERE `qlAgent` = @qlAgent ORDER BY `qlCalldate` DESC LIMIT 1)))
    THEN SET @iAmBusy = 1;
    ELSE SET @iAmBusy = 0;
END IF;
IF (SELECT( (NOT @isDuplicateRNA) AND (NOT @doNotDisturb) AND (NOT @iAmBusy)))
THEN
    IF(SELECT IF(COUNT(*), 1, 0) FROM `Queue` WHERE `name` = NEW.queuename ) THEN  
                            INSERT INTO `QueueLog` Set  
                            qlCallInfo_cUniqueid = NEW.callid, 
                            qlAgent = @qlAgent, 
                            qlCalldate = NEW.time,  
                            qlQueuename = NEW.queuename,
                            qlCalltime = @qlCalltime,
                            qlWaittime = @qlWaittime,
                            qlEvent = NEW.event;
    END IF;
    IF(SELECT(NEW.event = 'EXITWITHTIMEOUT' OR NEW.event = 'ABANDON'))
    THEN SET @cDsiposition = 'NO ANSWER';
        ELSEIF(SELECT(NEW.event = 'COMPLETEAGENT' OR NEW.event = 'COMPLETECALLER'))
    THEN SET @cDsiposition= 'ANSWERED';
        ELSE SET @cDsiposition='BUSY';
    END IF;
    IF(SELECT IF(COUNT(*), 1, 0) FROM `CallInfo` WHERE `cUniqueid` = NEW.callid ) THEN
        UPDATE `CallInfo` SET   
                        cDisposition = @cDsiposition, 
                        cBillsec  = @qlCalltime,
                        cQueue = NEW.queuename
        WHERE `cUniqueid` = NEW.callid;
    END IF;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `voicemail`
--

DROP TABLE IF EXISTS `voicemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voicemail` (
  `uniqueid` int(5) NOT NULL AUTO_INCREMENT,
  `context` char(80) NOT NULL DEFAULT 'default',
  `mailbox` char(80) NOT NULL,
  `password` char(80) NOT NULL,
  `fullname` char(80) DEFAULT NULL,
  `email` char(80) DEFAULT NULL,
  `pager` char(80) DEFAULT NULL,
  `attach` char(3) DEFAULT NULL,
  `attachfmt` char(10) DEFAULT NULL,
  `serveremail` char(80) DEFAULT NULL,
  `language` char(20) DEFAULT NULL,
  `tz` char(30) DEFAULT NULL,
  `deletevoicemail` char(3) DEFAULT NULL,
  `saycid` char(3) DEFAULT NULL,
  `sendvoicemail` char(3) DEFAULT NULL,
  `review` char(3) DEFAULT NULL,
  `tempgreetwarn` char(3) DEFAULT NULL,
  `operator` char(3) DEFAULT NULL,
  `envelope` char(3) DEFAULT NULL,
  `sayduration` char(3) DEFAULT NULL,
  `saydurationm` int(3) DEFAULT NULL,
  `forcename` char(3) DEFAULT NULL,
  `forcegreetings` char(3) DEFAULT NULL,
  `callback` char(80) DEFAULT NULL,
  `dialout` char(80) DEFAULT NULL,
  `exitcontext` char(80) DEFAULT NULL,
  `maxmsg` int(5) DEFAULT NULL,
  `volgain` decimal(5,2) DEFAULT NULL,
  `imapuser` varchar(80) DEFAULT NULL,
  `imappassword` varchar(80) DEFAULT NULL,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uniqueid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voicemail`
--

LOCK TABLES `voicemail` WRITE;
/*!40000 ALTER TABLE `voicemail` DISABLE KEYS */;
/*!40000 ALTER TABLE `voicemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voicemail_data`
--

DROP TABLE IF EXISTS `voicemail_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voicemail_data` (
  `filename` char(255) NOT NULL,
  `origmailbox` char(80) DEFAULT NULL,
  `context` char(80) DEFAULT NULL,
  `macrocontext` char(80) DEFAULT NULL,
  `exten` char(80) DEFAULT NULL,
  `priority` int(5) DEFAULT NULL,
  `callerchan` char(80) DEFAULT NULL,
  `callerid` char(80) DEFAULT NULL,
  `origdate` char(30) DEFAULT NULL,
  `origtime` int(11) DEFAULT NULL,
  `category` char(30) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voicemail_data`
--

LOCK TABLES `voicemail_data` WRITE;
/*!40000 ALTER TABLE `voicemail_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `voicemail_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voicemail_messages`
--

DROP TABLE IF EXISTS `voicemail_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voicemail_messages` (
  `dir` char(255) NOT NULL,
  `msgnum` int(4) NOT NULL,
  `context` char(80) DEFAULT NULL,
  `macrocontext` char(80) DEFAULT NULL,
  `callerid` char(80) DEFAULT NULL,
  `origtime` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `recording` blob,
  `flag` char(30) DEFAULT NULL,
  `category` char(30) DEFAULT NULL,
  `mailboxuser` char(30) DEFAULT NULL,
  `mailboxcontext` char(30) DEFAULT NULL,
  PRIMARY KEY (`dir`,`msgnum`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voicemail_messages`
--

LOCK TABLES `voicemail_messages` WRITE;
/*!40000 ALTER TABLE `voicemail_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `voicemail_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'asterisk'
--
/*!50003 DROP FUNCTION IF EXISTS `getCagent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`asterisk`@`localhost` FUNCTION `getCagent`(parID int(11)) RETURNS int(11)
BEGIN
        SELECT `cDstchannelChannels_cID`, `cChannelChannels_cID`, `cDirection`
        FROM `CallInfo`
        WHERE `cID` = parID
        INTO @cDstchannelChannels_cID,  @cChannelChannels_cID, @cDirection;
        IF(SELECT IFNULL(COUNT(*), 1) FROM `Channels` WHERE cID = @cChannelChannels_cID AND cType = '0')
            THEN SET @agent = (SELECT @cChannelChannels_cID);
            ELSEIF(SELECT IFNULL(COUNT(*), 1) FROM `Channels` WHERE cID = @cDstchannelChannels_cID AND cType = '0' AND @cDirection <> '2')
            THEN SET @agent = (SELECT @cDstchannelChannels_cID);
            ELSE SET @agent = NULL;
        END IF;
        RETURN @agent;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `drop_index_if_exists` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`asterisk`@`localhost` PROCEDURE `drop_index_if_exists`(in theTable varchar(128), in theIndexName varchar(128) )
BEGIN
 IF((SELECT COUNT(*) AS index_exists FROM information_schema.statistics WHERE TABLE_SCHEMA = DATABASE() and table_name =
theTable AND index_name = theIndexName) > 0) THEN
   SET @s = CONCAT('DROP INDEX ' , theIndexName , ' ON ' , theTable);
   PREPARE stmt FROM @s;
   EXECUTE stmt;
 END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `findAndDeleteByParams` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`asterisk`@`localhost` PROCEDURE `findAndDeleteByParams`(in findTable varchar(128), in findFiled1 varchar(128), in findFiled2 varchar(128), in findValue varchar(128), in selectField varchar(128), in deleteTable varchar(128), in deleteField varchar(128))
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE uid CHAR(255);
    DECLARE curs CURSOR FOR SELECT(CONCAT('SELECT', selectField, 'FROM' ,findTable, 'WHERE' ,findFiled1, '=', findValue, 'OR', findFiled2, '=', findValue));
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    OPEN curs;
        REPEAT
            FETCH curs INTO uid;
            IF (NOT done) THEN
                 SELECT CONCAT('DELETE FROM', deleteTable, 'WHERE', deleteField, '=', uid);
            END IF;
        UNTIL done END REPEAT;
    CLOSE curs;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getChannelID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`asterisk`@`localhost` PROCEDURE `getChannelID`(in channel varchar(80), out id int)
BEGIN
   IF(SELECT LOCATE('-', channel)) THEN
   SET @w = (SELECT SUBSTR(channel, 1, LOCATE('-', channel)-1));
   ELSE SET @w = (SELECT channel);
   END IF;
   SELECT `cID` INTO id FROM `Channels` WHERE cName = (SELECT @w);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getParamsByDirection` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`asterisk`@`localhost` PROCEDURE `getParamsByDirection`(
    in channel varchar(80), 
    in dstchannel varchar(80), 
    in isrc varchar(80), 
    in idst varchar(80), 
    in src_num_out varchar(80),
    in dst_num_out varchar(80),
    in src_num_in varchar(80),
    in dst_num_in varchar(80),
    in chanid int, 
    in dstchanid int, 
    out dir varchar(10), 
    out outchanid int, 
    out src varchar(80), 
    out dst varchar(80))
BEGIN
        IF ( SELECT (EXISTS (SELECT * FROM  `Channels` as c WHERE c.cName =  (SELECT SUBSTR(channel, 1, LOCATE('-', channel)-1)) AND c.cType = '0')) 
     AND ( SELECT (EXISTS ( SELECT * FROM `Channels` as c  WHERE c.cName =  (SELECT SUBSTR(dstchannel, 1, LOCATE('-', dstchannel)-1)) AND c.cType = '0')) ))
        THEN SET dir = '2'; SET outchanid = NULL; SET src = isrc; SET dst = idst;
    ELSEIF ( SELECT (EXISTS ( SELECT * FROM `Channels` as c  WHERE c.cName =  (SELECT SUBSTR(channel, 1, LOCATE('-', channel)-1)) AND c.cType = '0')))
        THEN SET dir = '0'; SET outchanid = dstchanid; SET src = src_num_out; SET dst = dst_num_out;  
    ELSE SET dir = '1'; SET outchanid = chanid; SET src = src_num_in; SET dst = dst_num_in; 
    END IF;
    SET src = (SELECT REPLACE(src, '-', ''));
    SET dst = (SELECT REPLACE(dst, '-', ''));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-24 11:02:09
