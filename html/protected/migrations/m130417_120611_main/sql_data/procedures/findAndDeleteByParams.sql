DELIMITER $
DROP PROCEDURE IF EXISTS findAndDeleteByParams $
CREATE PROCEDURE findAndDeleteByParams(in findTable varchar(128), in findFiled1 varchar(128), in findFiled2 varchar(128), in findValue varchar(128), in selectField varchar(128), in deleteTable varchar(128), in deleteField varchar(128))
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE uid CHAR(255);
    DECLARE curs CURSOR FOR SELECT(CONCAT('SELECT', selectField, 'FROM' ,findTable, 'WHERE' ,findFiled1, '=', findValue, 'OR', findFiled2, '=', findValue));
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    OPEN curs;
        REPEAT
            FETCH curs INTO uid;
            IF (NOT done) THEN
                 SELECT CONCAT('DELETE FROM', deleteTable, 'WHERE', deleteField, '=', uid);
            END IF;
        UNTIL done END REPEAT;
    CLOSE curs;
END$