DELIMITER $
DROP PROCEDURE IF EXISTS getParamsByDirection $
CREATE PROCEDURE getParamsByDirection(
    in channel varchar(80), 
    in dstchannel varchar(80), 
    in isrc varchar(80), 
    in idst varchar(80), 
    in src_num_out varchar(80),
    in dst_num_out varchar(80),
    in src_num_in varchar(80),
    in dst_num_in varchar(80),
    in chanid int, 
    in dstchanid int, 
    out dir varchar(10), 
    out outchanid int, 
    out src varchar(80), 
    out dst varchar(80))
BEGIN
        IF ( SELECT (EXISTS (SELECT * FROM  `Channels` as c WHERE c.cName =  (SELECT SUBSTR(channel, 1, LOCATE('-', channel)-1)) AND c.cType = '0')) 
     AND ( SELECT (EXISTS ( SELECT * FROM `Channels` as c  WHERE c.cName =  (SELECT SUBSTR(dstchannel, 1, LOCATE('-', dstchannel)-1)) AND c.cType = '0')) ))
        THEN SET dir = 'local'; SET outchanid = NULL; SET src = isrc; SET dst = idst;/* Внутренний звонок */
    ELSEIF ( SELECT (EXISTS ( SELECT * FROM `Channels` as c  WHERE c.cName =  (SELECT SUBSTR(channel, 1, LOCATE('-', channel)-1)) AND c.cType = '0')))
        THEN SET dir = 'outbound'; SET outchanid = dstchanid; SET src = src_num_out; SET dst = dst_num_out;  /* Исходящий звонок */
    ELSE SET dir = 'inbound'; SET outchanid = chanid; SET src = src_num_in; SET dst = dst_num_in; /* Входящий звонок */
    END IF;
    #start number preparation
    SET src = (SELECT REPLACE(src, '-', ''));
    SET dst = (SELECT REPLACE(dst, '-', ''));
    #end number preparation
END$