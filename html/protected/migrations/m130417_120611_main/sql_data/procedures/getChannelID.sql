DELIMITER $
DROP PROCEDURE IF EXISTS getChannelID $
CREATE PROCEDURE getChannelID(in channel varchar(80), out id int)
BEGIN
   IF(SELECT LOCATE('-', channel)) THEN
   SET @w = (SELECT SUBSTR(channel, 1, LOCATE('-', channel)-1));
   ELSE SET @w = (SELECT channel);
   END IF;
   SELECT `cID` INTO id FROM `Channels` WHERE cName = (SELECT @w);
END$