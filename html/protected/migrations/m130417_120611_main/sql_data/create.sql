/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     02.12.2013 13:27:24                          */
/*==============================================================*/


drop table if exists CallInfo;

drop table if exists ChannelOwners;

drop table if exists Channels;

drop table if exists DepartmentMembers;

drop table if exists DepartmentQueues;

drop table if exists Departments;

drop table if exists Groups;

drop table if exists Modems;

drop table if exists ModemsBalanceLog;

drop table if exists ModemsState;

drop table if exists ModemsStateLog;

drop table if exists PhoneBook;

drop table if exists Providers;

drop table if exists ProvidersPatterns;

drop table if exists Queue;

drop table if exists QueueLog;

drop table if exists QueueMembers;

drop table if exists Roles;

drop table if exists SIP;

drop table if exists Users;

/*==============================================================*/
/* Table: CallInfo                                              */
/*==============================================================*/
create table CallInfo
(
   cID                  int(11) unsigned not null auto_increment,
   cSrc                 varchar(80) not null default '',
   cDst                 varchar(80) not null default '',
   cDcontext            varchar(80) not null default '',
   cDuration            int(11) not null default 0,
   cBillsec             int(11) not null default 0,
   cDisposition         enum('FAILED','BUSY','ANSWERED','UNKNOWN', 'NO ANSWER', 'uFAILED', 'uHANGUP') not null default 'UNKNOWN',
   cUniqueid            varchar(32) not null default '',
   cUniqueid1 int(11) UNSIGNED NOT NULL,
   cUniqueid2 int(11) UNSIGNED NOT NULL,
   cCalldate            datetime not null default '0000-00-00 00:00:00',
   cDirection           enum('outbound','inbound','local', 'callback', 'autocall') not null,
   cChannelChannels_cID int(11) unsigned,
   cDstchannelChannels_cID int(11) unsigned,
   cOutchanChannels_cID int(11) unsigned,
   cQueue               varchar(128) default NULL,
   cGroups_gID          int(11) not null,
   cActiveAgent         int(11) unsigned,
   primary key (cID),
   key AK_Key_2 (cUniqueid),
   key AK_Key_3 (cCalldate),
   key `uniqueid_index` (`cUniqueid1`, `cUniqueid2`)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: ChannelOwners                                         */
/*==============================================================*/
create table ChannelOwners
(
   coGroups_gID         int(11),
   coID                 int(11) not null auto_increment,
   coChannels_cID       int(11) unsigned not null,
   coName               varchar(255),
   primary key (coID),
   unique key AK_Key_2 (coGroups_gID, coChannels_cID)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: Channels                                              */
/*==============================================================*/
create table Channels
(
   cID                  int(11) unsigned not null auto_increment,
   cName                varchar(255) not null,
   cType                enum('0', '1'),
   cDescription         varchar(100),
   primary key (cName),
   unique key AK_Key_3 (cID)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: DepartmentMembers                                     */
/*==============================================================*/
create table DepartmentMembers
(
   dpDepartments_dID    int not null,
   dpChannels_cID       int(11) unsigned not null,
   primary key (dpChannels_cID, dpDepartments_dID)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: DepartmentQueues                                      */
/*==============================================================*/
create table DepartmentQueues
(
   dqDepartments_dID    int not null,
   dqQueues_qName       varchar(128) not null,
   primary key (dqDepartments_dID, dqQueues_qName)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: Departments                                           */
/*==============================================================*/
create table Departments
(
   dID                  int not null auto_increment,
   dName                varchar(80) not null,
   dGroups_gID          int(11),
   primary key (dID),
   unique key AK_Key_2 (dName, dGroups_gID)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: Groups                                                */
/*==============================================================*/
create table Groups
(
   gID                  int(11) not null auto_increment,
   gName                varchar(255),
   primary key (gID),
   unique key AK_Key_2 (gName)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: Modems                                                */
/*==============================================================*/
create table Modems
(
   mProviders_pID       int(11) not null,
   mID                  int(11) not null auto_increment,
   mChannels_cID        int(11) unsigned not null,
   mName                varchar(30),
   mNumber              char(13),
   primary key (mID),
   unique key AK_Key_2 (mName)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: ModemsBalanceLog                                      */
/*==============================================================*/
create table ModemsBalanceLog
(
   mblID                int(11) not null auto_increment,
   mName                varchar(30),
   primary key (mblID)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: ModemsState                                           */
/*==============================================================*/
create table ModemsState
(
   msID                 int(11) not null auto_increment,
   msModems_mName       varchar(30),
   msState              enum('free', 'disconnected', 'inuse'),
   msStateTime          timestamp,
   primary key (msID)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: ModemsStateLog                                        */
/*==============================================================*/
create table ModemsStateLog
(
   mslModems_mName      varchar(30),
   mslState             enum('free', 'disconnected', 'inuse'),
   mslStateTime         timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   mslID                int(11) not null auto_increment,
   primary key (mslID)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: PhoneBook                                             */
/*==============================================================*/
create table PhoneBook
(
   pbID                 int(11) unsigned not null auto_increment,
   pbNumber             char(13) not null default '',
   pbOwner              varchar(40) not null default '',
   pbBlacked            enum('no', 'yes') not null default 'no',
   pbReason             text not null,
   pbGroups_gID         int(11) not null,
   pbCPhoneTimeStamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   primary key (pbID),
   unique key AK_Key_2 (pbNumber, pbGroups_gID),
   key AK_Key_3 (pbNumber),
   key PhoneBook_idx_CPhoneTimeStamp (`pbCPhoneTimeStamp`) COMMENT ""
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: Providers                                             */
/*==============================================================*/
create table Providers
(
   pID                  int(11) not null auto_increment,
   pName                varchar(30) not null,
   pBalanceUssd         varchar(10),
   primary key (pID),
   unique key AK_Key_2 (pName)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: ProvidersPatterns                                     */
/*==============================================================*/
create table ProvidersPatterns
(
   ppProviders_pID      int(11),
   ppItem               varchar(255)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: Queue                                                 */
/*==============================================================*/
create table Queue
(
   name                 varchar(128) not null,
   musiconhold          varchar(128) default NULL,
   announce             varchar(128) default NULL,
   context              varchar(128) default NULL,
   timeout              int(11) default NULL,
   monitor_type         varchar(50) not null default 'MixMonitor',
   monitor_format       varchar(128) default NULL,
   queue_youarenext     varchar(128) default NULL,
   queue_thereare       varchar(128) default NULL,
   queue_callswaiting   varchar(128) default NULL,
   queue_holdtime       varchar(128) default NULL,
   queue_minutes        varchar(128) default NULL,
   queue_seconds        varchar(128) default NULL,
   queue_lessthan       varchar(128) default NULL,
   queue_thankyou       varchar(128) default NULL,
   queue_reporthold     varchar(128) default NULL,
   announce_frequency   int(11) default NULL,
   announce_round_seconds int(11) default NULL,
   announce_holdtime    varchar(128) default NULL,
   retry                int(11) default NULL,
   wrapuptime           int(11) default NULL,
   maxlen               int(11) default NULL,
   servicelevel         int(11) default NULL,
   strategy             varchar(128) default NULL,
   joinempty            varchar(128) default NULL,
   leavewhenempty       varchar(128) default NULL,
   eventmemberstatus    varchar(4) default NULL,
   eventwhencalled      varchar(4) default NULL,
   reportholdtime       tinyint(1) default NULL,
   memberdelay          int(11) default NULL,
   weight               int(11) default NULL,
   timeoutrestart       tinyint(1) default NULL,
   periodic_announce    varchar(50) default NULL,
   periodic_announce_frequency int(11) default NULL,
   ringinuse            tinyint(1) default NULL,
   qGroups_gID          int(11),
   qName                varchar(128) not null,
   qFileName            varchar(128),
   primary key (name),
   key AK_Key_2 (qGroups_gID)
)
engine = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: QueueLog                                              */
/*==============================================================*/
create table QueueLog
(
   qlID                 int(11) unsigned not null auto_increment,
   qlQueuename          varchar(128),
   qlCalldate           datetime not null default '0000-00-00 00:00:00',
   qlEvent              varchar(255) not null,
   qlCallInfo_cUniqueid varchar(32) not null default '',
   qlAgent              int(11) not null default -1,
   qlCalltime           int(11) default NULL,
   qlWaittime           int(11) default NULL,
   primary key (qlID),
   key AK_Key_2 (qlCallInfo_cUniqueid),
   key AK_Key_3 (qlCalldate)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: QueueMembers                                          */
/*==============================================================*/
create table QueueMembers
(
   interface            varchar(128) not null,
   queue_name           varchar(128) not null,
   penalty              int(11),
   uniqueid             int(11) auto_increment,
   membername           varchar(40),
   paused               int(11),
   primary key (interface, queue_name),
   unique key AK_Key_2 (uniqueid)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: Roles                                                 */
/*==============================================================*/
create table Roles
(
   rID                  int(11) not null auto_increment,
   rName                varchar(255) not null,
   primary key (rID)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: SIP                                                   */
/*==============================================================*/
create table SIP
(
   id                   int(11) not null auto_increment,
   name                 varchar(20) not null,
   ipaddr               varchar(45) default NULL,
   port                 int(5) default NULL,
   regseconds           int(11) default NULL,
   defaultuser          varchar(20) default NULL,
   fullcontact          varchar(35) default NULL,
   regserver            varchar(20) default NULL,
   useragent            varchar(20) default NULL,
   lastms               int(11) default NULL,
   host                 varchar(40) default 'dynamic',
   type                 enum('friend','user','peer') default 'friend',
   context              varchar(40) default NULL,
   permit               varchar(40) default NULL,
   deny                 varchar(40) default NULL,
   secret               varchar(40) default NULL,
   md5secret            varchar(40) default NULL,
   remotesecret         varchar(40) default NULL,
   transport            enum('udp','tcp','udp,tcp','tcp,udp') default NULL,
   dtmfmode             enum('rfc2833','info','shortinfo','inband','auto') default NULL,
   directmedia          enum('yes','no','nonat','update') default NULL,
   nat                  enum('yes','no','never','route') default 'no',
   callgroup            varchar(40) default NULL,
   pickupgroup          varchar(40) default NULL,
   language             varchar(40) default NULL,
   disallow             varchar(40) default 'all',
   allow                varchar(40) default 'alaw',
   insecure             varchar(40) default NULL,
   trustrpid            enum('yes','no') default NULL,
   progressinband       enum('yes','no','never') default NULL,
   promiscredir         enum('yes','no') default NULL,
   useclientcode        enum('yes','no') default NULL,
   accountcode          varchar(40) default NULL,
   setvar               varchar(40) default NULL,
   callerid             varchar(40) default NULL,
   amaflags             varchar(40) default NULL,
   callcounter          enum('yes','no') default NULL,
   busylevel            int(11) default NULL,
   allowoverlap         enum('yes','no') default NULL,
   allowsubscribe       enum('yes','no') default NULL,
   videosupport         enum('yes','no') default NULL,
   maxcallbitrate       int(11) default NULL,
   rfc2833compensate    enum('yes','no') default NULL,
   mailbox              varchar(40) default NULL,
   `session-timers`     enum('accept','refuse','originate') default NULL,
   `session-expires`    int(11) default NULL,
   `session-minse`      int(11) default NULL,
   `session-refresher`  enum('uac','uas') default NULL,
   t38pt_usertpsource   varchar(40) default NULL,
   regexten             varchar(40) default NULL,
   fromdomain           varchar(40) default NULL,
   fromuser             varchar(40) default NULL,
   qualify              varchar(40) default 'no',
   defaultip            varchar(40) default NULL,
   rtptimeout           int(11) default NULL,
   rtpholdtimeout       int(11) default NULL,
   sendrpid             enum('yes','no') default NULL,
   outboundproxy        varchar(40) default NULL,
   callbackextension    varchar(40) default NULL,
   timert1              int(11) default NULL,
   timerb               int(11) default NULL,
   qualifyfreq          int(11) default NULL,
   constantssrc         enum('yes','no') default NULL,
   contactpermit        varchar(40) default NULL,
   contactdeny          varchar(40) default NULL,
   usereqphone          enum('yes','no') default NULL,
   textsupport          enum('yes','no') default NULL,
   faxdetect            enum('yes','no') default NULL,
   buggymwi             enum('yes','no') default NULL,
   auth                 varchar(40) default NULL,
   fullname             varchar(40) default NULL,
   trunkname            varchar(40) default NULL,
   cid_number           varchar(40) default NULL,
   callingpres          enum('allowed_not_screened','allowed_passed_screen','allowed_failed_screen','allowed','prohib_not_screened','prohib_passed_screen','prohib_failed_screen','prohib') default NULL,
   mohinterpret         varchar(40) default NULL,
   mohsuggest           varchar(40) default NULL,
   parkinglot           varchar(40) default NULL,
   hasvoicemail         enum('yes','no') default NULL,
   subscribemwi         enum('yes','no') default NULL,
   vmexten              varchar(40) default NULL,
   autoframing          enum('yes','no') default NULL,
   rtpkeepalive         int(11) default NULL,
   `call-limit`         int(11) default NULL,
   g726nonstandard      enum('yes','no') default NULL,
   ignoresdpversion     enum('yes','no') default NULL,
   allowtransfer        enum('yes','no') default NULL,
   dynamic              enum('yes','no') default NULL,
   sChannels_cID        int(11) unsigned,
   primary key (id),
   unique key name (name),
   key ipaddr (ipaddr, port),
   key host (host, port)
)
engine = InnoDB  AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: Users                                                 */
/*==============================================================*/
create table Users
(
   uID                  int(11) not null auto_increment,
   uName                varchar(255) not null default 'Hello, user',
   uIdentity            varchar(80) not null default 'default',
   uPassword            varchar(64) not null,
   uSalt                varchar(6) not null,
   uGroups_gID          int(11) not null,
   uRole                int(11) not null,
   primary key (uID),
   unique key AK_Key_2 (uIdentity),
   key AK_Key_3 (uPassword)
)
engine = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8;

alter table CallInfo add constraint FK_Reference_12 foreign key (cChannelChannels_cID)
      references Channels (cID) on delete cascade on update cascade;

alter table CallInfo add constraint FK_Reference_13 foreign key (cDstchannelChannels_cID)
      references Channels (cID) on delete cascade on update cascade;

alter table CallInfo add constraint FK_Reference_27 foreign key (cOutchanChannels_cID)
      references Channels (cID) on delete restrict on update restrict;

alter table CallInfo add constraint FK_FK_callinfo_groups foreign key (cGroups_gID)
      references Groups (gID) on delete cascade on update cascade;

alter table CallInfo add constraint FK_AgentCallinfo_channels foreign key (cActiveAgent)
      references Channels (cID) on delete restrict on update restrict;

alter table CallInfo add constraint FK_callinfo_queue foreign key (cQueue)
      references Queue (name) on delete cascade on update cascade;

alter table ChannelOwners add constraint FK_Reference_28 foreign key (coChannels_cID)
      references Channels (cID) on delete cascade on update cascade;

alter table ChannelOwners add constraint FK_Reference_29 foreign key (coGroups_gID)
      references Groups (gID) on delete restrict on update restrict;

alter table DepartmentMembers add constraint FK_depmem_channels foreign key (dpChannels_cID)
      references Channels (cID) on delete cascade on update cascade;

alter table DepartmentMembers add constraint FK_depmem_dep foreign key (dpDepartments_dID)
      references Departments (dID) on delete cascade on update cascade;

alter table DepartmentQueues add constraint FK_depqueues_queue foreign key (dqQueues_qName)
      references Queue (name) on delete cascade on update cascade;

alter table DepartmentQueues add constraint FK_dpqueues_dep foreign key (dqDepartments_dID)
      references Departments (dID) on delete cascade on update cascade;

alter table Departments add constraint FK_dep_groups foreign key (dGroups_gID)
      references Groups (gID) on delete restrict on update restrict;

alter table Modems add constraint FK_Reference_21 foreign key (mProviders_pID)
      references Providers (pID) on delete restrict on update restrict;

alter table Modems add constraint FK_Reference_22 foreign key (mChannels_cID)
      references Channels (cID) on delete cascade on update cascade;

alter table ModemsBalanceLog add constraint FK_Reference_24 foreign key (mName)
      references Modems (mName) on delete cascade on update cascade;

alter table ModemsState add constraint FK_Reference_25 foreign key (msModems_mName)
      references Modems (mName) on delete cascade on update cascade;

alter table ModemsStateLog add constraint FK_Reference_20 foreign key (mslModems_mName)
      references Modems (mName) on delete cascade on update cascade;

alter table PhoneBook add constraint FK_Reference_14 foreign key (pbGroups_gID)
      references Groups (gID) on delete restrict on update restrict;

alter table ProvidersPatterns add constraint FK_Reference_19 foreign key (ppProviders_pID)
      references Providers (pID) on delete cascade on update cascade;

alter table Queue add constraint FK_queue_groups foreign key (qGroups_gID)
      references Groups (gID) on delete restrict on update restrict;

alter table QueueLog add constraint FK_Reference_18 foreign key (qlQueuename)
      references Queue (name) on delete cascade on update cascade;

alter table QueueMembers add constraint FK_Reference_16 foreign key (interface)
      references Channels (cName) on delete cascade on update cascade;

alter table QueueMembers add constraint FK_queumem_queue foreign key (queue_name)
      references Queue (name) on delete cascade on update cascade;

alter table SIP add constraint FK_Reference_23 foreign key (sChannels_cID)
      references Channels (cID) on delete cascade on update cascade;

alter table Users add constraint FK_Reference_10 foreign key (uRole)
      references Roles (rID) on delete restrict on update restrict;

alter table Users add constraint FK_Reference_9 foreign key (uGroups_gID)
      references Groups (gID) on delete restrict on update restrict;

