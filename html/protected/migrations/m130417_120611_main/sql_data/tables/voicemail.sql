drop table if exists voicemail;

/*==============================================================*/
/* Table: voicemail                                             */
/*==============================================================*/
create table voicemail
(
   uniqueid             INT(5) not null auto_increment,
   context              CHAR(80) not null default 'default',
   mailbox              CHAR(80) not null,
   password             CHAR(80) not null,
   fullname             CHAR(80),
   email                CHAR(80),
   pager                CHAR(80),
   attach               CHAR(3),
   attachfmt            CHAR(10),
   serveremail          CHAR(80),
   language             CHAR(20),
   tz                   CHAR(30),
   deletevoicemail      CHAR(3),
   saycid               CHAR(3),
   sendvoicemail        CHAR(3),
   review               CHAR(3),
   tempgreetwarn        CHAR(3),
   operator             CHAR(3),
   envelope             CHAR(3),
   sayduration          CHAR(3),
   saydurationm         INT(3),
   forcename            CHAR(3),
   forcegreetings       CHAR(3),
   callback             CHAR(80),
   dialout              CHAR(80),
   exitcontext          CHAR(80),
   maxmsg               INT(5),
   volgain              DECIMAL(5,2),
   imapuser             VARCHAR(80),
   imappassword         VARCHAR(80),
   stamp                timestamp,
   primary key (uniqueid)
)
ENGINE=MyISAM  DEFAULT CHARSET=utf8;