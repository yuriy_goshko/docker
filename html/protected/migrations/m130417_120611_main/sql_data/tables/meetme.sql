drop table if exists meetme;

/*==============================================================*/
/* Table: meetme                                                */
/*==============================================================*/
create table meetme
(
   bookid               int(11) not null auto_increment,
   confno               char(80) not null default '0',
   starttime            datetime default '1900-01-01 12:00:00',
   endtime              datetime default '2038-01-01 12:00:00',
   pin                  char(20),
   adminpin             char(20),
   opts                 char(20),
   adminopts            char(20),
   recordingfilename    char(80),
   recordingformat      char(10),
   maxusers             int(11),
   members              integer not null default 0,
   primary key (bookid),
   key confno (confno, starttime, endtime)
)
ENGINE=MyISAM  DEFAULT CHARSET=utf8;