drop table if exists Version;

/*==============================================================*/
/* Table: Version                                               */
/*==============================================================*/
create table Version
(
   vRelease             tinyint not null,
   vDb                  tinyint not null,
   vApp                 tinyint not null,
   primary key (vRelease, vDb, vApp)
)
ENGINE=MyISAM  DEFAULT CHARSET=utf8;