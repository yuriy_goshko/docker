drop table if exists MusicOnHold;

/*==============================================================*/
/* Table: MusicOnHold                                           */
/*==============================================================*/
create table MusicOnHold
(
   name                 varchar(80) not null,
   directory            varchar(255),
   application          varchar(255),
   mode                 varchar(80) default "files",
   digit                char(1),
   sort                 char(10),
   format               char(10),
   stamp                timestamp,
   primary key (name)
)
ENGINE=MyISAM  DEFAULT CHARSET=utf8;


INSERT INTO `MusicOnHold` (
`name` ,
`directory` ,
`mode` ,
`digit` ,
`sort` ,
`format`
)
VALUES (
'default',  '/var/lib/asterisk/moh/',   'files',  '',  '',  ''
);
