drop table if exists cel;
/*==============================================================*/
/* Table: cel                                                   */
/*==============================================================*/
CREATE TABLE `cel` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `eventtype` enum('CHAN_START','CHAN_END','ANSWER','HANGUP', 'CONF_ENTER', 'CONF_EXIT', 'CONF_START',
                  'CONF_END', 'APP_START', 'APP_END', 'PARK_START', 'PARK_END', 'BRIDGE_START', 'BRIDGE_END',
                  'BRIDGE_UPDATE', '3WAY_START', '3WAY_END', 'BLINDTRANSFER', 'ATTENDEDTRANSFER',
                  'TRANSFER', 'PICKUP', 'FORWARD', 'HOOKFLASH', 'LINKEDID_END', 'CALL_TYPE', 'GUID'
                 ) not null,
 `eventtime` datetime NOT NULL,
 `userdeftype` varchar(255) NOT NULL,
 `cid_name` varchar(80) NOT NULL,
 `cid_num` varchar(80) NOT NULL,
 `cid_ani` varchar(80) NOT NULL,
 `cid_rdnis` varchar(80) NOT NULL,
 `cid_dnid` varchar(80) NOT NULL,
 `exten` varchar(80) NOT NULL,
 `context` varchar(80) NOT NULL,
 `channame` varchar(80) NOT NULL,
 `appname` varchar(80) NOT NULL,
 `appdata` varchar(80) NOT NULL,
 `accountcode` varchar(20) NOT NULL,
 `peeraccount` varchar(20) NOT NULL,
 `uniqueid` varchar(32) NOT NULL,
 `linkedid` varchar(32) NOT NULL,
 `amaflags` int(11) NOT NULL,
 `userfield` varchar(255) NOT NULL,
 `peer` varchar(80) NOT NULL,
 `extra` varchar(255) NOT NULL,
 `causeCode`  tinyint(1) unsigned default NULL,
 `causeChanname` varchar(30) default NULL,
 `callStatus` varchar(30) default NULL,
 `linkedLid` int(11) NOT NULL,
 `linkedRid` int(11) NOT NULL,
  KEY `linkedid_index` (`linkedLid`, `linkedRid`),
  PRIMARY KEY (`id`),
  KEY `eventtype_index` (`eventtype`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;