drop table if exists eCompletedCalls;

CREATE TABLE IF NOT EXISTS `eCompletedCalls` (
  `lid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `needNotifyTns` enum('no', 'yes', 'process') default 'no',
  PRIMARY KEY (`lid`, `rid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;