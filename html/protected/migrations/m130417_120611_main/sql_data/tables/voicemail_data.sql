drop table if exists voicemail_data;

/*==============================================================*/
/* Table: voicemail_data                                        */
/*==============================================================*/
create table voicemail_data
(
   filename             CHAR(255) not null,
   origmailbox          CHAR(80),
   context              CHAR(80),
   macrocontext         CHAR(80),
   exten                CHAR(80),
   priority             INT(5),
   callerchan           CHAR(80),
   callerid             CHAR(80),
   origdate             CHAR(30),
   origtime             INT(11),
   category             CHAR(30),
   duration             INT(11),
   primary key (filename)
)
ENGINE=MyISAM  DEFAULT CHARSET=utf8;