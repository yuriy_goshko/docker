drop table if exists queue_log;

/*==============================================================*/
/* Table: queue_log                                             */
/*==============================================================*/
create table queue_log
(
   id                   int(10) unsigned not null auto_increment,
   time                 char(30) default NULL,
   callid               char(50) not null default '',
   queuename            char(50),
   agent                char(50),
   event                char(20),
   data1                char(50),
   data2                char(50),
   data3                char(50),
   data4                char(50),
   data5                char(50),
   primary key (id),
   key bydate (time),
   key qname (queuename, time)
)
ENGINE=MyISAM  DEFAULT CHARSET=utf8  AUTO_INCREMENT=0;