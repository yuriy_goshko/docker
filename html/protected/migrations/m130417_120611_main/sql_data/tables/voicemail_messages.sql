drop table if exists voicemail_messages;

/*==============================================================*/
/* Table: voicemail_messages                                    */
/*==============================================================*/
create table voicemail_messages
(
   dir                  CHAR(255) not null,
   msgnum               INT(4) not null,
   context              CHAR(80),
   macrocontext         CHAR(80),
   callerid             CHAR(80),
   origtime             INT(11),
   duration             INT(11),
   recording            BLOB,
   flag                 CHAR(30),
   category             CHAR(30),
   mailboxuser          CHAR(30),
   mailboxcontext       CHAR(30),
   primary key (dir, msgnum)
)
ENGINE=MyISAM  DEFAULT CHARSET=utf8;