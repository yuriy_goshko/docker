CREATE OR REPLACE VIEW PreparedCDR AS SELECT `CallInfo`.cID as prID,

IF( `CallInfo`.`cDirection` IN ('inbound', 'callback'), 
        inPb.pbOwner,  
        IFNULL( 
             IF(LENGTH(outco.coName) = 0, NULL, outco.coName) , `outSip`.name
        )
) AS prNameSrc, 
IF(  
    `CallInfo`.`cDirection` =  'outbound',  
        `outPb`.pbOwner,   
        IFNULL( 
            IF(LENGTH(`inco`.coName) = 0, NULL, `inco`.coName) , `inSip`.name
        )
) AS prNameDst,

IF( LENGTH(`extco`.coName) = 0 OR `extco`.coName is NULL,
    `Channels`.`cName`,
    `extco`.`coName`) 
AS prNameExtChan 

FROM  `CallInfo`  

LEFT JOIN `Channels` ON `CallInfo`.`cOutchanChannels_cID`  = `Channels`.`cID`
LEFT JOIN  `PhoneBook`  `inPb` ON  `inPb`.`pbNumber` =  `CallInfo`.`cSrc` AND `inPb`.pbGroups_gID = `CallInfo`.cGroups_gID
LEFT JOIN  `PhoneBook`  `outPb` ON  `outPb`.`pbNumber` =  `CallInfo`.`cDst` AND `outPb`.pbGroups_gID = `CallInfo`.cGroups_gID
LEFT JOIN  `SIP`  `inSip` ON  `CallInfo`.`cDstchannelChannels_cID` =  `inSip`.`sChannels_cID` 
LEFT JOIN  `SIP`  `outSip` ON  `CallInfo`.`cChannelChannels_cID` =  `outSip`.`sChannels_cID` 
LEFT JOIN `ChannelOwners` as inco ON inco.coChannels_cID =  `inSip`.sChannels_cID AND inco.coGroups_gID = `CallInfo`.cGroups_gID
LEFT JOIN `ChannelOwners` as outco ON outco.coChannels_cID =  `outSip`.sChannels_cID AND outco.coGroups_gID = `CallInfo`.cGroups_gID
LEFT JOIN `ChannelOwners` as extco ON extco.coChannels_cID =  `Channels`.`cID`  AND extco.coGroups_gID = `CallInfo`.cGroups_gID;