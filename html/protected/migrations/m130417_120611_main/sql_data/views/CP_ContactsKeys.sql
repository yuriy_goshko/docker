CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER VIEW `CP_ContactsKeys` 
AS 
select pb.`pbID` ID 
from `PhoneBook` pb 
inner join ChannelOwners co ON co.coGroups_gID = pb.pbGroups_gID 
inner join SIP s ON s.sChannels_cID = co.coChannels_cID 
where pb.`pbBlacked`='no' 
and s.name = CP_SIP(); 