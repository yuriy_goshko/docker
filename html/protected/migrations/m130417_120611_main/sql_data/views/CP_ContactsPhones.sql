CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=CURRENT_USER VIEW `CP_ContactsPhones` 
AS 
select pb.`pbID` ContactID, pb.`pbCPhoneTimeStamp` ModifyTimeStamp, 
pb.pbNumber PhoneNumber, '' PhoneNote 
from `PhoneBook` pb 
inner join ChannelOwners co ON co.coGroups_gID = pb.pbGroups_gID 
inner join SIP s ON s.sChannels_cID = co.coChannels_cID 
where pb.`pbBlacked`='no' 
and s.name = CP_SIP(); 