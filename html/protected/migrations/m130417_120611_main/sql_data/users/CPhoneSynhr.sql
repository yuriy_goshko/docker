GRANT USAGE ON *.* TO 'CPhoneSynhr'@'%'; 
drop USER 'CPhoneSynhr'@'%'; 
create user 'CPhoneSynhr'@'%' identified by 'p@ss&$$d@F5'; 

use asterisk; 
GRANT SELECT ON `CP_Contacts` TO 'CPhoneSynhr'@'%'; 
GRANT SELECT ON `CP_ContactsKeys` TO 'CPhoneSynhr'@'%'; 
GRANT SELECT ON `CP_ContactsPhones` TO 'CPhoneSynhr'@'%'; 
GRANT EXECUTE ON FUNCTION `CP_SIP` TO 'CPhoneSynhr'@'%';