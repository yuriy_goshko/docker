DROP TRIGGER IF EXISTS `eCompletedCalls.bi`;
DELIMITER $
CREATE TRIGGER  `eCompletedCalls.bi` BEFORE INSERT ON `eCompletedCalls`
FOR EACH ROW BEGIN

IF((SELECT IF(COUNT(*) = 0, NULL, COUNT(*)) FROM `cel` WHERE eventtype = "GUID" AND linkedLid = new.lid AND linkedRid = new.rid) IS NOT NULL)
    THEN
        SET new.needNotifyTns = 'yes';
END IF;

END $

