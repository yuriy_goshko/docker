DELIMITER $
#Triggers for Cdr table

#Create, or modify if exists trigger on "Cdr". It process raw data fields after insertion row at "Cdr"
#and insert prepared information for web.
#@property string @trname
#@property string @ontable
#@property string @totable
#@property string @userfield

DROP TRIGGER IF EXISTS `cdr.data-flow`$



CREATE TRIGGER `cdr.data-flow` AFTER INSERT ON `cdr`
FOR EACH ROW BEGIN

 CALL getChannelID(NEW.channel, @cChannelChannels_cID);
 CALL getChannelID(NEW.dstchannel, @cDstchannelChannels_cID);
 CALL getParamsByDirection(
    NEW.channel, 
    NEW.dstchannel, 
    NEW.src, 
    NEW.dst, 
    NEW.src_num_out,
    NEW.dst_num_out,
    NEW.src_num_in,
    NEW.dst_num_in,
    @cChannelChannels_cID, 
    @cDstchannelChannels_cID, 
    @cDirection, 
    @cOutChan, 
    @src, 
    @dst);

SELECT `qlEvent`, `qlCalltime`, `qlQueuename`, `qlCallInfo_cUniqueid` 
FROM `QueueLog`
WHERE `qlCallInfo_cUniqueid` = NEW.uniqueid 
ORDER BY `qlCalldate` 
DESC LIMIT 1
INTO @qlLastRowEvent,  @qlLastRowCalltime , @qlLastRowQueue, @qlLastRowUniqueid;

IF(SELECT ((SELECT @cDirection) = 'local' OR (SELECT @cDirection = 'outbound')))
THEN 
    SET @cActiveAgent = @cChannelChannels_cID;
ELSEIF(SELECT ((SELECT @cDirection) = 'inbound' OR (SELECT @cDirection = 'callback')))
THEN
    SET @cActiveAgent = @cDstchannelChannels_cID;
ELSE SET @cActiveAgent = NULL;
END IF;

SET @cGroups_gID = NULL;
SELECT `coGroups_gID`  FROM `ChannelOwners` WHERE coChannels_cID = (SELECT @cActiveAgent) AND (SELECT @cActiveAgent) is NOT NULL LIMIT 1  INTO @cGroups_gID;
IF(SELECT(SELECT(@cGroups_gID)) IS NULL)
    THEN
    SELECT `qGroups_gID` FROM `Queue` WHERE name = (SELECT @qlLastRowQueue) LIMIT 1 INTO @cGroups_gID ;
END IF;

#Uncomment on production below!
IF(SELECT(SELECT(@src)) IS NULL)
    THEN SET @src = NEW.src;
END IF;

IF(SELECT(SELECT(@dst)) IS NULL)
    THEN SET @dst = NEW.dst;
END IF;


IF((SELECT @cGroups_gID) IS NOT NULL)
THEN
#Insert all except: callback
IF(SELECT(NEW.userfield NOT LIKE '%callback%'                                                                            #if is a regular call
    AND ( (SELECT(SELECT IF(COUNT(*), 1, 0) FROM `Channels` WHERE cType = '0' AND cID = (SELECT @cChannelChannels_cID))      #agent must figured at least in one of channels
          OR (SELECT IF(COUNT(*), 1, 0) FROM `Channels` WHERE cType = '0' AND cID = (SELECT @cDstchannelChannels_cID)))
          OR (SELECT(SELECT IF(COUNT(*), 1, 0) FROM `QueueLog` WHERE qlCallInfo_cUniqueid =  NEW.uniqueid)))))
                                                                                                               #of call related to queue
     THEN
INSERT INTO `CallInfo` Set 
                            cSrc = @src,
                            cDst = @dst,
                            cDcontext = NEW.dcontext,
                            cChannelChannels_cID = @cChannelChannels_cID,
                            cDstchannelChannels_cID = @cDstchannelChannels_cID,
                            cOutchanChannels_cID = @cOutChan,
                            cDuration = NEW.duration,
                            cBillsec = NEW.billsec,
                            cDisposition = NEW.disposition,
                            cUniqueid = NEW.uniqueid,
                            cCalldate = NEW.calldate,
                            cActiveAgent = @cActiveAgent,
                            cGroups_gID = @cGroups_gID,
                            cDirection = @cDirection;

END IF;

#insert callback
IF(SELECT(EXISTS(SELECT * FROM `QueueLog` as ql 
WHERE qlCallInfo_cUniqueid = NEW.uniqueid AND NEW.userfield LIKE '%callback%')))
THEN
    IF(SELECT IF(COUNT(*), 1, 0) FROM `QueueLog` WHERE qlCallInfo_cUniqueid =  NEW.uniqueid) THEN    #catch true callback
    
    #set dst from dstchannel
    SELECT sip.`name` INTO @dst FROM `Channels` chan LEFT JOIN `SIP` as sip ON sip.sChannels_cID = chan.cID  WHERE sip.sChannels_cID = @cDstchannelChannels_cID;
    #CALL findAndDeleteByParams('cdr', 'channel', 'dstchannel', NEW.channel, 'uniqueid', 'CallInfo', 'cUniqueid');
    #insert into CallInfo callback row
    INSERT INTO `CallInfo` Set 
                            cSrc = @src,
                            cDst = @dst,
                            cDcontext = NEW.dcontext,
                            cChannelChannels_cID = @cChannelChannels_cID,
                            cDstchannelChannels_cID = @cDstchannelChannels_cID,
                            cOutchanChannels_cID = @cOutChan,
                            cDuration = NEW.duration,
                            cBillsec = NEW.billsec,
                            cDisposition = NEW.disposition,
                            cUniqueid = NEW.uniqueid,
                            cCalldate = NEW.calldate,
                            cActiveAgent = @cActiveAgent,
                            cGroups_gID = @cGroups_gID,
                            cDirection = 'callback';    
    END IF;
END IF;



#Catch if its exists into QueueLog
IF(SELECT(EXISTS(SELECT * FROM `QueueLog` WHERE qlCallInfo_cUniqueid = NEW.uniqueid ORDER BY `qlCalldate` DESC LIMIT 1)))
   THEN 
        IF(SELECT(@qlLastRowEvent = 'EXITWITHTIMEOUT' OR @qlLastRowEvent = 'ABANDON')) #if its call was failed
            THEN 
                IF(SELECT(@qlLastRowEvent = 'EXITWITHTIMEOUT'))                        
                    THEN 
                        IF(SELECT(EXISTS(SELECT * FROM `QueueLog` WHERE qlCallInfo_cUniqueid = NEW.uniqueid AND qlEvent = 'RINGNOANSWER')))
                            THEN SET @cDsiposition = 'NO ANSWER';                      # if members was in queue, but no one answered on call
                            ELSE SET @cDsiposition = 'uFAILED';                        # if no one was in queue
                         END IF;
                    ELSE SET @cDsiposition = 'uHANGUP';                                # on ABANDON
                END IF;
        ELSEIF(SELECT(@qlLastRowEvent = 'COMPLETEAGENT' OR @qlLastRowEvent = 'COMPLETECALLER' OR @qlLastRowEvent = 'TRANSFER'))
            THEN SET @cDsiposition= 'ANSWERED';                                        # call was successesfully answered
        ELSE SET @cDsiposition='UNKNOWN';
        END IF;
    UPDATE `CallInfo` SET cBillsec = @qlLastRowCalltime, cDisposition=@cDsiposition, cQueue =  @qlLastRowQueue  WHERE cUniqueid = @qlLastRowUniqueid;
END IF;

END IF;

END$