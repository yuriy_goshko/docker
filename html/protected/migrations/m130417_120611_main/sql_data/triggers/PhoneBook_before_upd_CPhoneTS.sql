DELIMITER $
DROP TRIGGER IF EXISTS `PhoneBook_before_upd_CPhoneTS`$ 
CREATE TRIGGER `PhoneBook_before_upd_CPhoneTS` BEFORE UPDATE ON `PhoneBook` 
  FOR EACH ROW 
BEGIN   
  IF (NEW.pbOwner<>OLD.pbOwner) 
  or (NEW.pbNumber<>OLD.pbNumber) 
  or (NEW.pbBlacked<>OLD.pbBlacked) then 
   set NEW.pbCPhoneTimeStamp = CURRENT_TIMESTAMP; 
  END IF; 
END$