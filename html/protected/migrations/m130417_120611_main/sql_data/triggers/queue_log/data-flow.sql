DROP TRIGGER IF EXISTS `queue.data-flow`;
DELIMITER $
CREATE TRIGGER `queue.data-flow` AFTER INSERT ON `queue_log`
FOR EACH ROW BEGIN

SET @qlAgent = IFNULL((SELECT `cID` FROM `Channels` WHERE cName = NEW.agent), -1);
IF (SELECT(NEW.event = 'CONNECT'))
    THEN SET @qlWaittime = new.data1;
ELSEIF (SELECT(NEW.event = 'EXITWITHTIMEOUT' OR NEW.event = 'ABANDON'))
    THEN  SET @qlWaittime = new.data3;
ELSE  SET @qlWaittime =NULL;
END IF;


IF (SELECT(NEW.event = 'COMPLETEAGENT' OR NEW.event = 'COMPLETECALLER'))
    THEN SET @qlCalltime = new.data2;
    DELETE FROM `QueueLog` WHERE qlCallInfo_cUniqueid = NEW.callid AND qlEvent = 'RINGNOANSWER'; # RNA in this case can't consists RINGNOANSWER rows 
ELSE SET @qlCalltime = NULL;
END IF;

SELECT IF(COUNT(*), 1, 0) FROM `QueueLog` WHERE `qlCallInfo_cUniqueid` = new.callid 
    AND qlEvent = 'RINGNOANSWER' AND qlAgent = @qlAgent AND new.event = 'RINGNOANSWER' INTO @isDuplicateRNA;

IF (SELECT (new.data1 = '0' AND new.event = 'RINGNOANSWER'))
    THEN SET @doNotDisturb = 1;
    ELSE SET @doNotDisturb = 0;
END IF;

IF (SELECT (new.event = 'RINGNOANSWER' AND (SELECT  IF(`qlEvent` = 'CONNECT',1 ,0) 
    FROM `QueueLog` WHERE `qlAgent` = @qlAgent ORDER BY `qlCalldate` DESC LIMIT 1)))
    THEN SET @iAmBusy = 1;
    ELSE SET @iAmBusy = 0;
END IF;

IF (SELECT( (NOT @isDuplicateRNA) AND (NOT @doNotDisturb) AND (NOT @iAmBusy)))
THEN
    IF(SELECT IF(COUNT(*), 1, 0) FROM `Queue` WHERE `name` = NEW.queuename ) THEN  
                            INSERT INTO `QueueLog` Set  
                            qlCallInfo_cUniqueid = NEW.callid, 
                            qlAgent = @qlAgent, 
                            qlCalldate = NEW.time,  
                            qlQueuename = NEW.queuename,
                            qlCalltime = @qlCalltime,
                            qlWaittime = @qlWaittime,
                            qlEvent = NEW.event;
    END IF;

    IF(SELECT(NEW.event = 'EXITWITHTIMEOUT' OR NEW.event = 'ABANDON'))
    THEN SET @cDsiposition = 'NO ANSWER';
        ELSEIF(SELECT(NEW.event = 'COMPLETEAGENT' OR NEW.event = 'COMPLETECALLER'))
    THEN SET @cDsiposition= 'ANSWERED';
        ELSE SET @cDsiposition='BUSY';
    END IF;


    IF(SELECT IF(COUNT(*), 1, 0) FROM `CallInfo` WHERE `cUniqueid` = NEW.callid ) THEN
        UPDATE `CallInfo` SET   
                        cDisposition = @cDsiposition, 
                        cBillsec  = @qlCalltime,
                        cQueue = NEW.queuename
        WHERE `cUniqueid` = NEW.callid;
    END IF;
END IF;




END $

