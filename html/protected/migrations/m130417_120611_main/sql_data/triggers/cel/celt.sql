DROP TRIGGER IF EXISTS `cel.celt`;
DELIMITER $
CREATE TRIGGER `cel.celt` AFTER INSERT ON `cel`
FOR EACH ROW BEGIN
 IF(new.eventtype = "LINKEDID_END" )
    THEN INSERT INTO `eCompletedCalls`(`lid`, `rid`) VALUES (new.linkedLid, new.linkedRid);
END IF;
END $