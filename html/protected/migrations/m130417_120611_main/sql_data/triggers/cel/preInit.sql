DELIMITER $
DROP TRIGGER IF EXISTS `cel.preInit`$

CREATE TRIGGER `cel.preInit` BEFORE INSERT ON `cel`
FOR EACH ROW BEGIN
 SET @linkedIdSpliter = '.';
 SET @extraSpliter = ',';
 SET @extraEventType = "HANGUP";
 SET new.linkedLid = SUBSTR(new.linkedid, 1, LOCATE(@linkedIdSpliter, new.linkedid)-1);
 SET new.linkedRid = SUBSTR(new.linkedid, LOCATE(@linkedIdSpliter, new.linkedid)+1);
    IF(new.eventtype = @extraEventType)
        THEN
            SET new.callStatus = SUBSTRING_INDEX(new.extra, @extraSpliter, -1);
            SET new.causeCode = SUBSTRING_INDEX(new.extra, @extraSpliter, 1);
            SET new.causeChanname = SUBSTRING_INDEX(SUBSTRING_INDEX(new.extra, @extraSpliter, -2), @extraSpliter, 1);
    END IF;

END $