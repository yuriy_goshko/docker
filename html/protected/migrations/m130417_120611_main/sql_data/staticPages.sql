-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: asterisk
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `StaticPages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
drop table if exists StaticPages;

/*==============================================================*/
/* Table: StaticPages                                           */
/*==============================================================*/
create table StaticPages
(
   spID                 int(11) not null auto_increment,
   spTitle              varchar(255) not null,
   spText               text,
   spKey                text,
   spDesc               text,
   spParentID           int(11) not null default -1,
   spOrder              int(11) default NULL,
   spUpdateDate         timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   spUrl                varchar(255) default NULL,
   spExtUrl             varchar(255) default NULL,
   spVisible            enum('0','1') not null default '1',
   spType               varchar(255) default NULL,
   spHtmlClass          varchar(255) default NULL,
   primary key (spID),
   unique key AK_Key_2 (spTitle, spParentID)
)
ENGINE=MyISAM AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StaticPages`
--

LOCK TABLES `StaticPages` WRITE;
/*!40000 ALTER TABLE `StaticPages` DISABLE KEYS */;
INSERT INTO `StaticPages` (`spID`, `spTitle`, `spText`, `spKey`, `spDesc`, `spParentID`, `spOrder`, `spUpdateDate`, `spUrl`, `spExtUrl`, `spVisible`, `spType`, `spHtmlClass`) VALUES
(135, 'Детальная статистика звонков', NULL, NULL, NULL, 126, 2, '2013-02-27 10:58:05', 'Detalnaja-statistika-zvonkov', '/callinfo', '1', 'addeditr', 'cdr'),
(134, 'Отчет по агентам в очередях', NULL, NULL, NULL, 139, 10, '2013-02-21 09:29:38', 'Otchet-po-agentam-v-ocheredjah', '/callinfo/queuebyagents', '1', 'addeditr', ''),
(133, 'Отчет по очередям(дни недели)', NULL, NULL, NULL, 139, 9, '2013-02-21 09:29:16', 'Otchet-po-ocheredjam_dni-nedeli_', '/callinfo/queuebydays', '1', 'addeditr', ''),
(132, 'Отчет по очередям(часы)', NULL, NULL, NULL, 139, 8, '2013-02-13 06:49:51', 'Otchet-po-ocheredjam_chasy_', '/callinfo/queuebyhours', '1', 'addeditr', ''),
(128, 'Телефонная книга', NULL, NULL, NULL, 126, 14, '2013-02-11 10:36:38', 'Telefonnaja-kniga', '/phonebook', '1', 'addeditr', 'pb'),
(146, 'Отчет по пользователям', NULL, NULL, NULL, 139, 6, '2013-06-03 05:35:16', 'Otchet-po-polzovateljam', '/callinfo/callreportbyagents', '1', 'addeditr',''),
(126, 'Главное меню', NULL, NULL, NULL, -1, 1, '2013-02-11 10:33:31', 'Glavnoe-menju', NULL, '1', 'addeditm',''),
(137, 'Отчет общей статистики звонков', NULL, NULL, NULL, 139, 4, '2013-05-28 09:04:49', 'Otchet-obschej-statistiki-zvonkov', '/callinfo/callreport', '1', 'addeditr',''),
(138, 'Отчет по внешним каналам', NULL, NULL, NULL, 139, 5, '2013-05-29 04:39:35', 'Otchet-po-vneshnim-kanalam', '/callinfo/callreportbyoc', '1', 'addeditr',''),
(139, 'Отчеты', NULL, NULL, NULL, 126, 3, '2013-05-29 04:41:13', 'Otchety', '', '1', 'addeditr','reports'),
(147, 'Мониторинг агентов', NULL, NULL, NULL, 149, 12, '2013-06-04 15:22:28', 'Monitoring-agentov', '/memberstat', '1', 'addeditr',''),
(148, 'Мониторинг модемов', NULL, NULL, NULL, 149, 13, '2013-06-04 15:22:47', 'Monitoring-modemov', '/modemstat', '1', 'addeditr',''),
(149, 'Мониторинг', NULL, NULL, NULL, 126, 11, '2013-06-04 15:23:01', 'Monitoring', '', '1', 'addeditr','monitoring'),
(150, 'Отчет по отделам', NULL, NULL, NULL, 139, 7, '2013-08-08 08:40:57', 'Otchet-po-otdelam', '/callinfo/callreportbydep', '1', 'addeditr','');

/*!40000 ALTER TABLE `StaticPages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-27 13:16:19
