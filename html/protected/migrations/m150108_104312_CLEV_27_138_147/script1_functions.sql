DELIMITER ;
DROP FUNCTION IF EXISTS `isExistingNum`;
DELIMITER $
CREATE FUNCTION `isExistingNum`(
        `Num` VARCHAR(100),
        `Name` VARCHAR(100)
    )
    RETURNS VARCHAR(100) CHARACTER SET utf8
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT ''
BEGIN
 Declare _find VARCHAR(100) CHARACTER SET utf8;
 Set _find = null;
 if (Name<>'') then
  if (exists (select 1 from `SIP` s  where s.`name` = Num and  s.`fullname` = Name)) then
   Set _find='findsip';
  end if;
 end if;
 if (_find is null) then
   select pb.`pbOwner` 
   from `PhoneBook` pb 
   where pb.`pbNumber` = Num
   into _find;
 end if;
  
 RETURN _find; 
 END$  
DELIMITER ;
 
DROP FUNCTION IF EXISTS `FullName`;
DELIMITER $
 CREATE FUNCTION `FullName`(
        `Num` VARCHAR(100),
        `Name` VARCHAR(100)
    )
    RETURNS VARCHAR(200) CHARACTER SET utf8
    DETERMINISTIC
    NO SQL
    SQL SECURITY INVOKER
    COMMENT ''
BEGIN
  RETURN (case when Name="" then Num else CONCAT(Num," (",Name,")") end);
END$
DELIMITER ;

DROP FUNCTION IF EXISTS `isSipNum`;
DELIMITER %
CREATE FUNCTION `isSipNum`(
        `Num` VARCHAR(100)
    )
    RETURNS TINYINT(1)
    DETERMINISTIC
    NO SQL
    SQL SECURITY INVOKER
    COMMENT ''
BEGIN
  RETURN (Num REGEXP '^-?[0-9]+$') and (Num BETWEEN 200 and 999);
END%
DELIMITER ;
