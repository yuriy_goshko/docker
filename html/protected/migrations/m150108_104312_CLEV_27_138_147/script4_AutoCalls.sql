DELIMITER ;
DROP TABLE IF EXISTS `CI_AutoCalls`;
--
CREATE TABLE `CI_AutoCalls` (
  `Id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `UniqueId` VARCHAR(20) NOT NULL,
  `LinkedId` VARCHAR(32) NOT NULL,
  `Guid` VARCHAR(60) NOT NULL,
  `State` SMALLINT(6) DEFAULT NULL,
  `InsertDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Processed` VARCHAR(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Id`),
  KEY `CI_AutoCalls_Parsed` (`Processed`),
  KEY `CI_AutoCalls_InsertDateTime` (`InsertDateTime`),
  KEY `ProcIns` (`Processed`, `InsertDateTime`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
DROP TRIGGER IF EXISTS  `cel.preInit`;
--
DROP TABLE IF EXISTS `eCompletedCalls`;
--
DROP TRIGGER IF EXISTS  `cel.celt`;
--
DROP TRIGGER IF EXISTS  `cel_AI`;
--
DELIMITER @
CREATE TRIGGER `cel_AI` AFTER INSERT ON `cel` FOR EACH ROW
BEGIN
 IF ((new.eventtype = "LINKEDID_END")) THEN 
   INSERT INTO `CI_LinkedIds`(`LinkedId`) VALUES (new.linkedid);
 END IF;
 
 IF ((new.eventtype = "AC_GUID") and (new.extra<>'')) THEN 
  INSERT INTO `CI_AutoCalls` (`UniqueId`, `LinkedId`, `Guid` )
  VALUES (new.uniqueid, new.linkedid, new.extra);
 END IF;
END@
--
DELIMITER ;
--
call drop_index_if_exists('cel','linkedid_index');
