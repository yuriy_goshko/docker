<?php
class m150521_125813_CLEV_156 extends CDbMigration {
	public function safeUp() {
		$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior'));
		
		$this->execute('DROP TRIGGER IF EXISTS `cdr.data-flow`;');
		$this->execute('DROP TRIGGER IF EXISTS `queue.data-flow`;');
		$this->execute('DROP TABLE IF EXISTS `QueueLog`;');
		$this->execute('DROP TABLE IF EXISTS `CallInfo`;');
		$this->execute('DROP VIEW IF EXISTS PreparedCDR;');
		$this->execute('delete from `StaticPages` where `spUrl` = "Otchety";');
		$this->execute('delete from `StaticPages` where spExtUrl like "/callinfo%";');
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150521_125813_CLEV_156/script1_CI_CallsInfo.sql');
		
		$this->execute('
			set @dt1 = (select c.`eventtime` from `cel` c limit 1);
		
			delete from `CI_CallsInfo` where `InfoType`="ENTERQUEUE";
			insert into `CI_CallsInfo`(`LinkedId`, `InfoDateTime`, `InfoType`, `InfoValue`)
			select ql.callid, ql.`time`, ql.`event`, ql.`queuename`
			from `queue_log` ql
			where ql.`event`="ENTERQUEUE"
			and ql.`time` > @dt1;
		
			update `CI_CallsInfo` ci set ci.`CallId`=(select cl.`CallId` from `CI_LinkedIds` cl where cl.`LinkedId`=ci.`LinkedId`);
		');
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150521_125813_CLEV_156/script2_RemLog.sql');
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150521_125813_CLEV_156/script3_FullNameUtf.sql');
		//фикс 150
		$this->execute('
				select `spId` into @parId from `StaticPages` where `spUrl`="reports";
		
				delete from `StaticPages` where `spUrl`="reports-missedcalls";
				INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`,`spVisible`)
				VALUE ("Необработанные звонки",@parId,8,"reports-missedcalls","/callslog/report/missedcalls","addeditr",null,"1");
		
				delete from `StaticPages` where `spUrl`="reports-longtimecalls";
				INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`,`spVisible`)
				VALUE ("Длительные вызовы",@parId,9,"reports-longtimecalls","/callslog/report/longtimecalls","addeditr", null, "0");
		');
	}
	public function down() {
		echo "m150521_125813_CLEV_156 does not support migration down.\n";
		return false;
	}
}