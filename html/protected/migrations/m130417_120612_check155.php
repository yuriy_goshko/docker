<?php

class m130417_120612_check155 extends CDbMigration
{
	public function Up() {
		
		if ($this->getDbConnection()->schema->getTable('tbl_upgrades') != False) {
			$sql = 'insert into `tbl_migration` select * from `tbl_upgrades` tu where not exists(select 1 from `tbl_migration` tm where tm.`version`=tu.`version`)';		
			$InsCount = $this->getDbConnection()->createCommand($sql)->execute();
			if ($InsCount>0) {			
		  	echo "===================\n";
		  	echo "Migration forcibly stoped. This service need! (".$InsCount.") \n";
		  	echo "START AGAIN PLEASE! \n";
		  	echo "===================\n";
		  	return false; 
			}
		}
	}

	public function down()
	{
		echo "m130417_120612_check155 does not support migration down.\n";
		return false;
	}

}