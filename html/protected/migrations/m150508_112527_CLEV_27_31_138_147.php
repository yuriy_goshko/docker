<?php
class m150508_112527_CLEV_27_31_138_147 extends CDbMigration {
	public function safeUp() {
		$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior'));
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m150508_112527_CLEV_27_31_138_147/script1.sql');
		
		$this->execute('
		update `SIP` s
		inner join `ChannelOwners` co on co.coChannels_cID=s.`sChannels_cID`  and co.`coName` is not null
		inner join `Channels` c on c.`cID`=`co`.`coChannels_cID` and c.`cType` = "0"
		set s.`fullname` = co.`coName`
				');
		
		$this->execute('
			delete from `CI_RulesForWritingNumbers` where ActionType="Change" and NewValue="-" and OldValue="s";
			INSERT INTO `CI_RulesForWritingNumbers` (`CharsLength`, `ActionType`, `OldValue`, `NewValue`, `OnlyRealNumbers`, `OrderKey`) 
			VALUES (1, "Change", "s", "-", 0, 5);
				');
	}
	public function down() {
		echo "m150508_112527_CLEV_27_31_138_147 does not support migration down.\n";
		return false;
	}
}