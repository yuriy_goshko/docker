<?php

/**
 * Implemented shared contacts sync with any client database MySQL (unilaterally)
 * 
 *  To sync contacts from asterisk db was added new user "CPhoneSynhr". CPhone 
 *  client tries connect from this account.
 *  Configure file /etc/mysql/my.conf needs comment row "#bind-address = 127.0.0.1" 
 *  to allow connection from all but not localhost machines.
 * 
 * * Done CLEV-35
 */
class m140326_134747_1_4_4 extends CDbMigration {

    public function safeUp() {
    	$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior',));
    	
        //$this->executeSqlFile(Yii::app()->basePath . '/migrations/main/sql_data/views/PreparedCDR.sql');
        $this->execute('ALTER TABLE `PhoneBook` ADD COLUMN `pbCPhoneTimeStamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;');
        $this->execute('ALTER TABLE `PhoneBook` ADD INDEX `PhoneBook_idx_CPhoneTimeStamp` (`pbCPhoneTimeStamp`) COMMENT "";');
        $this->execute('ALTER TABLE `SIP` MODIFY `ipaddr` varchar(45) default NULL');
        /* triggers */
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/triggers/PhoneBook_before_upd_CPhoneTS.sql');
        $this->execute('UPDATE `PhoneBook` SET `pbCPhoneTimeStamp` = CURRENT_TIMESTAMP;');
        /* funcs */
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/funcs/CP_SIP.sql');
        /* views */
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/views/CP_Contacts.sql');
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/views/CP_ContactsKeys.sql');
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/views/CP_ContactsPhones.sql');
        /* users */
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/users/CPhoneSynhr.sql');
    }

}
