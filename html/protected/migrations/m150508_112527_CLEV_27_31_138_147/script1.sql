DELIMITER ;
DROP FUNCTION IF EXISTS `isExistingNum`;
DELIMITER $
CREATE FUNCTION `isExistingNum`(
        `Num` VARCHAR(100) CHARACTER SET utf8,
        `NumName` VARCHAR(100) CHARACTER SET utf8
    )
    RETURNS VARCHAR(100) CHARACTER SET utf8
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT ''
BEGIN
 Declare _find VARCHAR(100) CHARACTER SET utf8;
 Set _find = null;
 
 if (NumName<>'') then
  if (exists (select 1 from `SIP` s  where s.`name` = Num and  s.`fullname` = NumName)) then
   Set _find='findsip';
  end if;
 end if;
 if (_find is null) then
   select pb.`pbOwner` 
   from `PhoneBook` pb 
   where pb.`pbNumber` = Num
   into _find;
 end if;
 RETURN _find; 
END$
DELIMITER ;

