<?php

/**
 * Fixed outer channel calculations for Callinfo (v1.2.4 bug).
 * 
 * * Fixed CLEV-114
 */
class m140210_092025_1_3_4 extends CDbMigration {

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp() {
    	$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior',));
    	
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/views/PreparedCDR.sql');
    }

}
