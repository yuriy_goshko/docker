<?php

class m141014_102343_CLEV27_138 extends CDbMigration
{
	public function safeUp() {
		$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior',));	

		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m141014_102343_CLEV27_138/script1_CI_Tables.sql');
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m141014_102343_CLEV27_138/script2_CEL.sql');
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m141014_102343_CLEV27_138/script3_CI_Procedures.sql');
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m141014_102343_CLEV27_138/script4_CI_ASTER_User.sql');
		$this->executeSqlFile(Yii::app()->basePath . '/migrations/m141014_102343_CLEV27_138/script5_Settings.sql');
		
		//реализовать в след версии
		//$this->execute('DROP TABLE IF EXISTS `tbl_upgrades`');
		//$this->execute('DROP TABLE IF EXISTS `Version`');		
	}

	public function down()
	{
		echo "m141014_102343_CLEV27 does not support migration down.\n";
		return false;
	}

}