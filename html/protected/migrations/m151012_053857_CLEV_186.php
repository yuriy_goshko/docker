<?php
class m151012_053857_CLEV_186 extends CDbMigration {
	public function safeUp() {
		$table = Yii::app()->db->schema->getTable('CI_Directions');
		if (! isset($table->columns['DepsLinkByPBX'])) {
			$this->execute('ALTER TABLE `CI_Directions` ADD COLUMN `DepsLinkByPBX` TINYINT(1) NOT NULL DEFAULT 1;');
		}
		if (! isset($table->columns['DepsLinkByAgents'])) {
			$this->execute('ALTER TABLE `CI_Directions` ADD COLUMN `DepsLinkByAgents` TINYINT(1) NOT NULL DEFAULT 1;');
		}
		$this->execute('INSERT INTO Settings (param, value) VALUES("FindByCallbakInit", "1")
						ON DUPLICATE KEY UPDATE param="FindByCallbakInit", value="1";');
	}
	public function down() {
		echo "m151012_053857_CLEV_186 does not support migration down.\n";
		return false;
	}
}