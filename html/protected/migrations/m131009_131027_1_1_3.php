<?php

/**
 * Hot minor version. No DB changes.
 * 
 * * resolve CLEV-55 issue
 * 
 * @author Kuzich Yurii <qzichs@gmail.com>
 */
class m131009_131027_1_1_3 extends CDbMigration
{ 
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
            return true;
	}

}