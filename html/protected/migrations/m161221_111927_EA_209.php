<?php
class m161221_111927_EA_209 extends CDbMigration {
	public function safeUp() {
		$this->execute('
			select `spId` into @parId from `StaticPages` where `spUrl`="reports";
		
			delete from `StaticPages` where `spUrl`="reports-byhours";
			delete from `StaticPages` where `spUrl`="reports-bydays";
				
			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`,`spVisible`)
			VALUE ("Статистика по дням",@parId,15,"reports-bydays","/callslog/report/bydays","addeditr",null,"1");

			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`,`spVisible`)
			VALUE ("Статистика по часам",@parId,15,"reports-byhours","/callslog/report/byhours","addeditr",null,"1");');
	}
	public function down() {
		echo "m161221_111927_EA_209 does not support migration down.\n";
		return false;
	}
}