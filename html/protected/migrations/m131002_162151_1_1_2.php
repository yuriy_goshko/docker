<?php

/**
 * This version has new design. Fixed several bugs.
 * 
 * Mainly new design applied to front end reports, callInfo pages.
 * Fixed CLEV-60 to render title statically (early it got dynamically by current 
 * URL  controller/action from from StaticPages table).
 * 
 * * new design
 * * fixed CLEV-60
 * 
 * @author  Kuzich Yurii <qzichs@gmail.com>
 */
class m131002_162151_1_1_2 extends CDbMigration
{ 
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
			$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior',));
			
            $this->renameColumn('Version', 'vVersion', 'vRelease');
            $this->renameColumn('Version', 'vSubVersion', 'vDb');
            $this->renameColumn('Version', 'vLegacy', 'vApp');
            
            $this->addColumn('StaticPages', 'spHtmlClass', "varchar(255) default NULL");
            $this->update('StaticPages', array('spHtmlClass'=>'cdr'), 'spExtUrl = "/callinfo"');
            $this->update('StaticPages', array('spHtmlClass'=>'pb'), 'spExtUrl = "/phonebook"');
            $this->update('StaticPages', array('spHtmlClass'=>'reports'), 'spUrl = "Otchety"');
            $this->update('StaticPages', array('spHtmlClass'=>'monitoring'), 'spUrl = "Monitoring"');
	}
        
}