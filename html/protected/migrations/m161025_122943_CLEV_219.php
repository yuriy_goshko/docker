<?php
class m161025_122943_CLEV_219 extends CDbMigration {
	public function safeUp() {
		$this->execute('
				DROP TABLE IF EXISTS `CI_SipCalls`;
				
				CREATE TABLE `CI_SipCalls` (
				`Id` int(11) NOT NULL AUTO_INCREMENT,
				`CallId` int(11) DEFAULT NULL,
				`SipCallId` varchar(32) NOT NULL,
				`LinkedId` varchar(32) NOT NULL,
				`UniqueId` varchar(32) NOT NULL,
				`InsertDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`Id`),
				KEY `CI_SipCalls_SipId_idx` (`SipCallId`),
				KEY `CI_SipCalls_LinlId_idx` (`LinkedId`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
		
		$this->execute('ALTER TABLE `SIP` MODIFY COLUMN `nat` VARCHAR(30) NOT NULL DEFAULT "no";');
		$this->execute('update `SIP` s set s.`nat`="force_rport,comedia" where s.`nat`="yes";');
		
		$table = Yii::app()->db->schema->getTable('Queue');
		if (! isset($table->columns['membermacro']))
			$this->execute('ALTER TABLE `Queue` ADD COLUMN `membermacro` VARCHAR(128) DEFAULT "callid"');
		
		$sql = 'select count(*) from `Users` where `IsRole` = 1;';
		$RolesCount = $this->getDbConnection()->createCommand($sql)->queryScalar();		
		echo 'RolesCount: ' . $RolesCount;
		if ($RolesCount == 0)
			$this->execute('
			insert into `Users`(`uIdentity`, `uName`, `IsRole`, `uPassword`, `uSalt`)
			values ("rolea", "Роль для агентов", 1, "-", "-");
			insert into `UsersRights`(`UserId`, `Name`) values (last_insert_id(), "ListenAudio");			
			update `SIP` s set s.`userRole`="rolea" where (s.`userRole` is null) or (s.`userRole` = "");
			');
	}
	public function down() {
		echo "m161025_122943_CLEV_219 does not support migration down.\n";
		return false;
	}
}