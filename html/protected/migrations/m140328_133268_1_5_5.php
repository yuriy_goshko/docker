<?php

/**
 * Start work with cel table..
 * 
 * Asterisk configurations are undergit/etc/asterisk. 
 * Added cel.conf and cel_odbc.conf as example how to configure.
 * For determining TNS autocall use CELGenUserEvent(GUID, {123}) at your dialplan.
 * All custom events that needs to be for resolve CLEV-106:
 *   ________________
 *  |eventType |extra|
 *  |---------- -----|
 *  |GUID      |{123}|
 *  ------------------
 * 
 * Added parser daemon. It is designed for monitoring and analyzing the subject event
 * table (`eCompletedCall` wich based on `cel` table) and can easily extends 
 * to handle more solutions. Parser is designed to run each handler in separate flow 
 * (multiprocessing pcntl) to improve observer performance. If there is no jobs parser 
 * will restart.
 * For run daemon type into console (path www/protected):
 * 
 *           ./yiic parser start
 * 
 * In addition there are other params for command: stop, restart. (./yiic parser --help)
 * Configure host&port for TNS server at protected/console.php file.
 * An example of configuration params:
 * 
 *             'params' => array(
 *          'parser'=>array(
 *               'tnsNotifier'=>array(
 *                    'host'=>'192.172.2.1',
 *                     'port'=>'77'
 *                  )
 *               )
 *            )
 * Assumed that the parser will work together with statistic, and recommend run parser daemon at start system.
 * 
 * 
 * * https://estream.atlassian.net/secure/attachment/12603/IXP.1.1.txt
 * * done CLEV-106
 * 
 */
class m140328_133268_1_5_5 extends CDbMigration {

    public function safeUp() {
    	$this->attachBehavior('SqlFileBahavior', array('class' => 'SqlFileBehavior',));

        //create new tables
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/tables/cel.sql');
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/tables/eCompletedCalls.sql');
        //register triggers
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/triggers/cel/celt.sql');
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/triggers/cel/preInit.sql');
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/m130417_120611_main/sql_data/triggers/eCompletedCalls/beforeInsert.sql');
    }

}
