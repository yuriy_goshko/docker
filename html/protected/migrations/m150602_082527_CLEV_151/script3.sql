DROP FUNCTION IF EXISTS `getCagent`;
--
DROP PROCEDURE IF EXISTS `getChannelID`;
--
DROP PROCEDURE IF EXISTS `getParamsByDirection`;
--
DROP TABLE IF EXISTS `ChannelOwners`;
--
DROP TABLE IF EXISTS `ModemsBalanceLog`;
--
DROP TABLE IF EXISTS `ModemsStateLog`;
--
DROP TABLE IF EXISTS `ModemsState`;
--
DROP TABLE IF EXISTS `Modems`;
--
DROP TABLE IF EXISTS `DepartmentMembers`;
--
DROP TABLE IF EXISTS `Channels`;
--
DROP TRIGGER IF EXISTS `SIP_before_ins_tr1`;
--
DROP TRIGGER IF EXISTS `SIP_before_upd_tr1`;
--
DELIMITER $
CREATE TRIGGER `SIP_before_ins_tr1` BEFORE INSERT ON `SIP` FOR EACH ROW
BEGIN
  set NEW.sip_name = CONCAT('SIP/',new.`name`);
END$
--
CREATE TRIGGER `SIP_before_upd_tr1` BEFORE UPDATE ON `SIP` FOR EACH ROW
BEGIN
  set NEW.sip_name = CONCAT('SIP/',new.`name`);
END$
DELIMITER ;













