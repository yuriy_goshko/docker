DELIMITER ;
DROP TRIGGER IF EXISTS `UsersRights_after_ins_tr1`;
DROP TRIGGER IF EXISTS `UsersRights_after_del_tr1`;
DROP TABLE IF EXISTS `UsersRights`;
--
CREATE TABLE `UsersRights` (
  `UserId` INTEGER(11) NOT NULL,
  `Name` VARCHAR(100) COLLATE utf8_general_ci NOT NULL,
  KEY `UserId` (`UserId`) USING BTREE,
  CONSTRAINT `UsersRights_UserId_FK` FOREIGN KEY (`UserId`) REFERENCES `Users` (`uID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
--
DELIMITER $
CREATE TRIGGER `UsersRights_after_ins_tr1` AFTER INSERT ON `UsersRights` FOR EACH ROW
BEGIN
  update `Users` set `rRightsStamp` = md5(CURRENT_TIMESTAMP() + RAND()) where uID = new.UserId;
END$
--
CREATE TRIGGER `UsersRights_after_del_tr1` AFTER DELETE ON `UsersRights` FOR EACH ROW
BEGIN
  update `Users` set `rRightsStamp` = md5(CURRENT_TIMESTAMP() + RAND()) where uID = old.UserId;
END$
DELIMITER ;
DROP TABLE IF EXISTS `DepartmentUsers`;
--
CREATE TABLE `DepartmentUsers` (
  `DepartmentId` INTEGER(11) DEFAULT NULL,
  `UserId` INTEGER(11) DEFAULT NULL,
  KEY `DepartmentId` (`DepartmentId`) USING BTREE,
  KEY `UserId` (`UserId`) USING BTREE,
  CONSTRAINT `DepartmentUsers_DepId_FK` FOREIGN KEY (`DepartmentId`) REFERENCES `Departments` (`dID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `DepartmentUsers_UserId_FK` FOREIGN KEY (`UserId`) REFERENCES `Users` (`uID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';