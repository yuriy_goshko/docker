DROP VIEW IF EXISTS `CP_Contacts`;
--
DROP VIEW IF EXISTS `CP_ContactsPhones`;
--
DROP VIEW IF EXISTS `CP_ContactsKeys`;
--
DELIMITER $
--
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `CP_Contacts`
AS
select
  `pb`.`pbID` AS `ID`,
  `pb`.`pbCPhoneTimeStamp` AS `ModifyTimeStamp`,
  `pb`.`pbOwner` AS `DisplayName`,
  `pb`.`pbOwner` AS `FirstName`,
  ''  AS `LastName`,
  ''  AS `Email`
from
  `PhoneBook` `pb`
where
  `pb`.`pbBlacked` = 'no';$ 
--
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `CP_ContactsPhones`
AS
select
  `pb`.`pbID` AS `ContactID`,
  `pb`.`pbCPhoneTimeStamp` AS `ModifyTimeStamp`,
  `pb`.`pbNumber` AS `PhoneNumber`,
  ''  AS `PhoneNote`
from
  `PhoneBook` `pb`
where
  `pb`.`pbBlacked` = 'no';$
--  
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `CP_ContactsKeys`
AS
select
  `pb`.`pbID` AS `ID`
from
  `PhoneBook` `pb`
where
  `pb`.`pbBlacked` = 'no';$

  
--
DELIMITER ;
--
  