DROP TABLE IF EXISTS `PBXNumbersRelations`;
--
DROP TABLE IF EXISTS `PBXChannels`;
--
DROP TABLE IF EXISTS `PBXNumbers`;
--
CREATE TABLE `PBXNumbers` (
  `Number` VARCHAR(50) COLLATE utf8_general_ci NOT NULL,
  `Note` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`Number`) USING BTREE,
  UNIQUE KEY `Number` (`Number`) USING BTREE
) ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
--
CREATE TABLE `PBXChannels` (
  `Id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `Type` VARCHAR(20) COLLATE utf8_general_ci NOT NULL,
  `Name` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  `KeyName` VARCHAR(275) COLLATE utf8_general_ci NOT NULL,
  `DisplayName` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL,
  `ProviderId` INTEGER(11) DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE KEY `Id` (`Id`) USING BTREE,
  KEY `ProviderId` (`ProviderId`) USING BTREE,
  CONSTRAINT `PBXChannels_Provider_FK` FOREIGN KEY (`ProviderId`) REFERENCES `Providers` (`pID`) ON UPDATE CASCADE
) ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
--
CREATE TABLE `PBXNumbersRelations` (
  `Id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `Number` VARCHAR(50) COLLATE utf8_general_ci NOT NULL,
  `DepartmentId` INTEGER(11) DEFAULT NULL,
  `DepartmentIncoming` TINYINT(1) DEFAULT NULL,
  `DepartmentOutcoming` TINYINT(1) DEFAULT NULL,
  `ChannelId` INTEGER(11) DEFAULT NULL,
  `QueueName` VARCHAR(128) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE KEY `Id` (`Id`) USING BTREE,
  KEY `Number` (`Number`) USING BTREE,
  KEY `DepartmentId` (`DepartmentId`) USING BTREE,
  KEY `ChannelId` (`ChannelId`) USING BTREE,
  KEY `QueueName` (`QueueName`) USING BTREE,
  CONSTRAINT `PBXNumbersRelations_QueueName_FK` FOREIGN KEY (`QueueName`) REFERENCES `Queue` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PBXNumbersRelations_ChanelId_FK` FOREIGN KEY (`ChannelId`) REFERENCES `PBXChannels` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PBXNumbersRelations_DepId_FK` FOREIGN KEY (`DepartmentId`) REFERENCES `Departments` (`dID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PBXNumbersRelations_Number_FK` FOREIGN KEY (`Number`) REFERENCES `PBXNumbers` (`Number`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';