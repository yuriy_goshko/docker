<?php
class m150713_121856_CLEV_148 extends CDbMigration {
	public function safeUp() {
		$table = Yii::app()->db->schema->getTable('CityChoose');
		if ($table === null)
			$this->execute('	
				CREATE TABLE `CityChoose` (
				`id` INTEGER(11) NOT NULL AUTO_INCREMENT,
				`city` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
				`number` CHAR(13) COLLATE utf8_general_ci NOT NULL,
				PRIMARY KEY (`id`) USING BTREE
				) ENGINE=InnoDB AUTO_INCREMENT=1 CHARACTER SET "utf8" COLLATE "utf8_general_ci";');
		
		$this->execute('DROP TRIGGER IF EXISTS `CityChoose_before_ins1`;');
		$this->execute('DROP TRIGGER IF EXISTS `CityChoose_before_upd1`;');
		$this->execute('ALTER TABLE `CityChoose` MODIFY COLUMN `number` VARCHAR(25) COLLATE utf8_general_ci NOT NULL;');
		$this->execute('call drop_index_if_exists("CityChoose","CityChoose_number_idx");');
		$this->execute('ALTER TABLE `CityChoose` ADD UNIQUE INDEX `CityChoose_number_idx` (`number`);');
		$table = Yii::app()->db->schema->getTable('CityChoose');
		if (! isset($table->columns['rebmun'])) {
			$this->execute('ALTER TABLE `CityChoose` ADD COLUMN `rebmun` VARCHAR(25) COLLATE utf8_general_ci NOT NULL DEFAULT "";');
			$this->execute('call drop_index_if_exists("CityChoose","CityChoose_rebmun_idx");');
			$this->execute('ALTER TABLE `CityChoose` ADD INDEX `CityChoose_rebmun_idx` (`rebmun`);');
			$this->execute('update `CityChoose` set `rebmun` = REVERSE(`number`);');
		}
		if (! isset($table->columns['InsertDateTime'])) {
			$this->execute('ALTER TABLE `CityChoose` ADD COLUMN `InsertDateTime` DATETIME NULL;');
			$this->execute('call drop_index_if_exists("CityChoose","CityChoose_InsertDateTime_idx");');
			$this->execute('ALTER TABLE `CityChoose` ADD INDEX `CityChoose_InsertDateTime_idx` (`InsertDateTime`);');
		}
		$this->execute('CREATE TRIGGER `CityChoose_before_ins1` BEFORE INSERT ON `CityChoose` FOR EACH ROW
						BEGIN 
						  set NEW.rebmun = REVERSE(NEW.number);
						  set NEW.InsertDateTime = now();
						END;');
		$this->execute('CREATE TRIGGER `CityChoose_before_upd1` BEFORE UPDATE ON `CityChoose` FOR EACH ROW
						BEGIN 
						  set NEW.rebmun = REVERSE(NEW.number); 
						END;');
		$table = Yii::app()->db->schema->getTable('CityNames');
		if ($table === null) {
			$this->execute('
				CREATE TABLE `CityNames` (
				`Name` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
				`DisplayName` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
				PRIMARY KEY (`Name`) USING BTREE,
				UNIQUE KEY `Name` (`Name`) USING BTREE,
				UNIQUE KEY `DisplayName` (`DisplayName`) USING BTREE
				) ENGINE=InnoDB CHARACTER SET "utf8" COLLATE "utf8_general_ci";');
			$this->execute('insert into `CityNames`(`Name`,`DisplayName`) select distinct cc.`city`, cc.`city` from `CityChoose` cc');
		}
		
		$this->execute('
			select `spId` into @parId from `StaticPages` where `spUrl`="Glavnoe-menju";
		
			delete from `StaticPages` where `spUrl`="CityChoose";
			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`,`spVisible`)
			VALUE ("Выбор города",@parId,14,"CityChoose","/callslog/citychoose","addeditr","pb","1");'); // !!!
		
		$this->execute('
			select `spId` into @parId from `StaticPages` where `spUrl`="reports";
				
			delete from `StaticPages` where `spUrl`="reports-citychoose";
			INSERT INTO `StaticPages`(`spTitle`,`spParentID`,`spOrder`,`spUrl`,`spExtUrl`,`spType`,`spHtmlClass`,`spVisible`)
			VALUE ("Выбор городов",@parId,15,"reports-citychoose","/callslog/report/citychoose","addeditr",null,"1");'); // !!!
	}
	public function down() {
		echo "m150713_121856_CLEV_148 does not support migration down.\n";
		return false;
	}
}