GRANT USAGE ON *.* TO 'CCS_User'@'localhost'; 
drop USER 'CCS_User'@'localhost'; 
create user 'CCS_User'@'localhost' identified by 'secretciuser'; 
--
use asterisk; 
GRANT SELECT, UPDATE, INSERT, DELETE, EXECUTE ON asterisk.* TO 'CCS_User'@'localhost';
GRANT SELECT ON `mysql`.* TO 'CCS_User'@'localhost';
--
GRANT USAGE ON *.* TO 'asterisk'@'localhost'; 
drop USER 'asterisk'@'localhost'; 
create user 'asterisk'@'localhost' identified by 'gjhjctyjr26'; 
--
use asterisk; 
GRANT ALL ON asterisk.* TO 'asterisk'@'localhost';