DROP TABLE IF EXISTS `CI_LinkedIds`;
--
CREATE TABLE `CI_LinkedIds` (
  `Id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `LinkedId` VARCHAR(32) NOT NULL,
  `Parsed` tinyint(1) NOT NULL DEFAULT '0',
  `CallId` INTEGER(11),
  `InsertDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `CI_LinkedIds_LinkedId_idx` (`LinkedId`),
  KEY `CI_LinkedIds_CallId_idx` (`CallId`),
  KEY `CI_LinkedIds_NewParsed_idx` (`Parsed`,`InsertDateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
DROP TABLE IF EXISTS `CI_CallsDetail`;
--
CREATE TABLE `CI_CallsDetail` (
`Id` INTEGER(11) NOT NULL AUTO_INCREMENT,
`CallId` int(11) NOT NULL,
`NumSrc` VARCHAR(100) NOT NULL,
`NumDst` VARCHAR(100) NOT NULL,
`NumOut` VARCHAR(100) NOT NULL,
`NameSrc` VARCHAR(100) NOT NULL,
`NameDst` VARCHAR(100) NOT NULL,
`ChanNameOut` VARCHAR(100) NOT NULL,
`Duration` int(11) NOT NULL,
`BridgeDuration` int(11) NOT NULL,
`CallDateTime` datetime NOT NULL,
`CallEndDateTime` datetime NOT NULL,
`UniqueId` VARCHAR(32) NOT NULL,
`LinkedId` VARCHAR(32) NOT NULL,
`Context` VARCHAR(100) NOT NULL,
`TransferFrom` VARCHAR(100) NOT NULL,
`TransferTo` VARCHAR(100) NOT NULL,
`StateId` VARCHAR(20) NOT NULL,
`StateInfo` VARCHAR(100) NOT NULL,
`CanceledOutgoing` tinyint(1),
`DirectionId` VARCHAR(20) NOT NULL,
PRIMARY KEY (`Id`),
KEY `CI_CallsDetail_CallId_idx` (`CallId`),
KEY `CI_CallsDetail_LinkedId_idx` (`LinkedId`),
KEY `CI_CallsDetail_CallDateTime_idx` (`CallDateTime`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
DROP TABLE IF EXISTS `CI_Calls`;
--
CREATE TABLE `CI_Calls` (
`Id` INTEGER(11) NOT NULL AUTO_INCREMENT,
`NumSrc` VARCHAR(100) NOT NULL,
`NumDst` VARCHAR(100) NOT NULL,
`NumOut` VARCHAR(100) NOT NULL,
`NameSrc` VARCHAR(100) NOT NULL,
`NameDst` VARCHAR(100) NOT NULL,
`ChanNameOut` VARCHAR(100) NOT NULL,
`Duration` int(11) NOT NULL,
`BridgeDuration` int(11) NOT NULL,
`CallDateTime` datetime NOT NULL,
`CallEndDateTime` datetime NOT NULL,
`UniqueId` VARCHAR(32) NOT NULL,
`LinkedId` VARCHAR(32) NOT NULL,
`Context` VARCHAR(100) NOT NULL,
`TransferFrom` VARCHAR(100) NOT NULL,
`TransferTo` VARCHAR(100) NOT NULL,
`StateId` VARCHAR(20) NOT NULL,
`StateInfo` VARCHAR(100) NOT NULL,
`CanceledOutgoing` tinyint(1),
`DirectionId` VARCHAR(20) NOT NULL,
`InsertDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`Id`),
KEY `CI_Calls_LinkedId_idx` (`LinkedId`),
KEY `CI_Calls_CallDateTime_idx` (`CallDateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
DROP TABLE IF EXISTS `CI_Conferences`;
--
CREATE TABLE `CI_Conferences` (
`Id` INTEGER(11) NOT NULL AUTO_INCREMENT,
`CallId` int(11) NOT NULL,
`RoomNum` VARCHAR(100) NOT NULL,
`CelIdMin` INTEGER(11) NOT NULL,
`CelIdMax` INTEGER(11) NOT NULL,
`ConfOutGuid` VARCHAR(32),
`RecordUniqueId` VARCHAR(32) NOT NULL,
PRIMARY KEY (`Id`) COMMENT '',
UNIQUE KEY `CI_Conferences_CallId_idx` (`CallId`),
KEY `CI_Conferences_ConfOutGuid_idx` (`ConfOutGuid`),
KEY `CI_Conferences_CelIdMin_idx` (`CelIdMin`),
KEY `CI_Conferences_CelIdMax_idx` (`CelIdMax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
DROP TABLE IF EXISTS `CI_States`;
--
CREATE TABLE `CI_States` (
`Id` VARCHAR(20) NOT NULL,
`Name` VARCHAR(100) NOT NULL,
`OrderKey` INTEGER(11),
PRIMARY KEY (`Id`) COMMENT '') ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
insert into `CI_States` values ('ANSWERED','Отвечен',1);
insert into `CI_States` values ('ANSWERED_PART','Отвечен *',2); -- 'ANSWERED'
insert into `CI_States` values ('BUSY','Занято',3);
insert into `CI_States` values ('NO_ANSWER','Не отвечен',4);
insert into `CI_States` values ('FAILED','Неудавшийся',5);
insert into `CI_States` values ('UNKNOWN','Неизвестен',6);
--
DROP TABLE IF EXISTS `CI_Directions`;
--
CREATE TABLE `CI_Directions` (
`Id` VARCHAR(20) NOT NULL,
`Name` VARCHAR(100) NOT NULL,
`Used` tinyint(1) NOT NULL DEFAULT '1',
`OrderKey` INTEGER(11),
PRIMARY KEY (`Id`) COMMENT '') ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
insert into `CI_Directions` values ('Outbound','Исходящий',1,1);
insert into `CI_Directions` values ('Inbound','Входящий',1,2);
insert into `CI_Directions` values ('Local','Внутренний',1,3);
insert into `CI_Directions` values ('Conference','Конференция',1,4);
insert into `CI_Directions` values ('ConfOut','Внешняя конференция',0,5);
insert into `CI_Directions` values ('AutoCall','Автозвонок',0,6);
insert into `CI_Directions` values ('Callback','Отзвон',0,7);
insert into `CI_Directions` values ('CallbackInit','Запрос отзвона',0,8); -- 'Callback'
--
DROP TABLE IF EXISTS `CI_Warnings`;
--
CREATE TABLE `CI_Warnings` (
`Id` INTEGER(11) NOT NULL AUTO_INCREMENT,
`UniqueId` VARCHAR(32) NOT NULL,
`LinkedId` VARCHAR(32) NOT NULL,
`Type` VARCHAR(100) NOT NULL,
`Info` VARCHAR(1000) NOT NULL,
`Count` INTEGER(11) NOT NULL DEFAULT 1,
`InsertDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
`UpdateDateTime` TIMESTAMP,
`CallDateTime` TIMESTAMP NOT NULL,
PRIMARY KEY (`Id`),
KEY `CI_Warnings_LinkedId_idx` (`LinkedId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
DROP TABLE IF EXISTS `CI_RulesForWritingNumbers`;
--
CREATE TABLE `CI_RulesForWritingNumbers` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CharsLength` int(11) NOT NULL,
  `ActionType` varchar(20) NOT NULL,
  `OldValue` varchar(20) NOT NULL,
  `NewValue` varchar(20) NOT NULL,
  `OnlyRealNumbers` tinyint(1) NOT NULL,
  `OrderKey` int(11) NOT NULL,
  PRIMARY KEY (`Id`) COMMENT '') ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
INSERT INTO `CI_RulesForWritingNumbers`  VALUES (1, 9, 'BeginningAdd', '', '0', 1, 1);
INSERT INTO `CI_RulesForWritingNumbers`  VALUES (2, 10, 'BeginningAdd', '', '38', 1, 2);
INSERT INTO `CI_RulesForWritingNumbers`  VALUES (3, 0, 'Change', 'Restricted', 'anonymous', 0, 3);
INSERT INTO `CI_RulesForWritingNumbers`  VALUES (4, 7, 'BeginningAdd', '', '38044', 1, 4);
