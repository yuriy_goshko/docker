ALTER TABLE `cel` MODIFY COLUMN `eventtype` VARCHAR(255) COLLATE utf8_general_ci NOT NULL;
-- 
call drop_index_if_exists('cel','cel_eventtime_ind');
ALTER TABLE `cel` ADD  INDEX `cel_eventtime_ind` (`eventtime`) COMMENT '';
--
call drop_index_if_exists('cel','cel_linkedid_ind');
ALTER TABLE `cel` ADD  INDEX `cel_linkedid_ind` (`linkedid`) COMMENT '';
--
DROP TRIGGER IF EXISTS `cel.celt`;
DELIMITER $
CREATE TRIGGER `cel.celt` AFTER INSERT ON `cel`
  FOR EACH ROW
BEGIN
 IF(new.eventtype = "LINKEDID_END" ) THEN 
 Begin
   INSERT INTO `CI_LinkedIds`(`LinkedId`) VALUES (new.linkedid);
   INSERT INTO `eCompletedCalls`(`lid`, `rid`) VALUES (new.linkedLid, new.linkedRid);   
 end;   
 END IF;
END$
--
DELIMITER ;
DROP PROCEDURE IF EXISTS `drop_index_if_exists`;
DELIMITER $
CREATE PROCEDURE `drop_index_if_exists`(
        IN `theTable` VARCHAR(128),
        IN `theIndexName` VARCHAR(128)
    )
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
  IF ((SELECT COUNT(*) AS index_exists 
       FROM information_schema.statistics 
  	   WHERE TABLE_SCHEMA = DATABASE() and table_name = theTable AND index_name = theIndexName) > 0) THEN
       begin
    	 if theIndexName='PRIMARY' then	   
		   SET @s = CONCAT('ALTER TABLE  ' , theTable , ' DROP PRIMARY KEY ');
         else
           SET @s = CONCAT('DROP INDEX ' , theIndexName , ' ON ' , theTable);
         end if;
		 PREPARE stmt FROM @s;
		 EXECUTE stmt;
       end;       
 END IF;
END$
DELIMITER ;
--
call drop_index_if_exists('eCompletedCalls','eCompletedCalls_idx1');
ALTER TABLE `eCompletedCalls` ADD  INDEX `eCompletedCalls_idx1` USING BTREE (`lid`, `rid`) COMMENT '';
call drop_index_if_exists('eCompletedCalls','PRIMARY');
