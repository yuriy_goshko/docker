DROP TABLE IF EXISTS `Settings`;
CREATE TABLE `Settings` (
  `param` varchar(20) NOT NULL,
  `value` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`param`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `Settings`  VALUES ('tnsNotifierHost', '127.0.0.1');
INSERT INTO `Settings`  VALUES ('tnsNotifierPort', '8001');
