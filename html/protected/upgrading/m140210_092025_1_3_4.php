<?php

/**
 * Fixed outer channel calculations for Callinfo (v1.2.4 bug).
 * 
 * * Fixed CLEV-114
 */
class m140210_092025_1_3_4 extends UDbMigration {

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp() {
        $this->executeSqlFile(Yii::app()->basePath . '/migrations/main/sql_data/views/PreparedCDR.sql');
    }

}
