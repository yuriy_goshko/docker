<?php

return array(
    'Go to first page' => 'Перейти к первой странице',
    'Go to last page' => 'Перейти к последней странице',
    'Next page'=>'Следующая страница',
    'Previous page' => 'Предыдущая страница',
);