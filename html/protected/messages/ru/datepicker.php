<?php

return array(
    "M d, yy"=>"dd.m.yy,",
    "hh:mm:ss TT"=>"H:mm:ss",
    'ampm'=>false,
);