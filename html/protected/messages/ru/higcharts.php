<?php
return array(
    'Export to JPEG'=>'Экспорт в JPEG',
    'Export to PNG'=>'Экспорт в PNG',
    'Export to PDF'=>'Экспорт в PDF',
    'Export to SVG'=>'Экспорт в SVG',
    'Export graph or chart.'=>'Экспортировать график или диаграмму.',
    'Send a graph or chart to print.'=>'Отправить график или диаграмму на печать.',
    'You can filter the items displayed on the chart with the buttons under the header graphic.'=>'У Вас есть возможность фильтровать элементы отображаемые на графике с помощью кнопок под заголовком графика.'
);