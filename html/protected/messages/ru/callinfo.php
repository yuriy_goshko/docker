<?php
// messages to CallInfo model class.
return array(
		'number' => 'номер',
		'outbound number' => 'исходящий номер',
		'destination number' => 'номер назначения',
		'duration' => 'время звонка',
		'billsec' => 'время разговора',
		'status' => 'статус',
		'date' => 'дата',
		'call direction' => 'тип звонка',
		'external channel' => 'внешний канал',
		'queue' => 'очередь',
		'start date' => 'дата начала',
		'end date' => 'дата окончания',
		'agent' => 'агент',
		'page size' => 'количество записей',
		'departments' => 'отделы',
		'department members' => 'отдел пользователей',
		'spetial filter' => 'специальный фильтр');

