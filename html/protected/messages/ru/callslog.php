<?php
return array(
		'attributeLabels' => array(
				'Detail' => 'Подробно',
				'FullNameSrc' => 'Номер|Исходящий',
				'FullNameDst' => 'Номер|Назначения',
				'NumOut' => 'Номер|PBX',
				'NumOutOnly' => 'PBX Номер',
				'ConfNum' => 'Номер|Конференции',
				'Duration' => 'Время|Звонка',
				'BridgeDuration' => 'Время|Разговора',
				'CallDateTime' => 'Время|Начала',
				'CallEndDateTime' => 'Время|Завершения',
				'StateName' => 'Статус',
				'CanceledOutgoing' => 'Сброс звонящим',
				'DirName' => 'Тип звонка',
				'Deps' => 'Отделы',
				'TransferFrom' => 'Переадресован|От',
				'TransferTo' => 'Переадресован|К'),
		'hints' => array('ANSWERED_PART' => 'Не отвечен последним адресатом','CanceledOutgoing' => 'Сброс звонящим'));


