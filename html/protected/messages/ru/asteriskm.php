<?php

return array(
    'Device' => 'Модем',
    'State' => 'Статус',
    'RSSI' => 'Уровень сигнала',
    'AudioState' => 'Аудио',
    'DataState' => 'Дата',
    'Voice' => 'Передача голоса',
    'SMS' => 'СМС',
    'Model' => 'Модель',
    'Mode' => 'Режим',
    'ProviderName' => 'Провайдер',
);