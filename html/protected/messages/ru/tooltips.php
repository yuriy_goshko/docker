<?php

// messages to CallInfo model class.
return array(
		'Set the client data in the phone book.' => 'Установить данные клиента в телефонной книге.',
		'Edit customer details in the phone book.' => 'Редактировать данные клиента в телефонной книге.',
		'Update customer details in the phone book.' => 'Обновить данные клиента в телефонной книге.',
		'Add customer details to the phone book.' => 'Добавить данные клиента в телефонную книгу.',
		"Edit agent's data." => 'Редактировать данные агента.',
		"Set agent's data." => 'Установить данные агента.',
		"Update agent's data." => 'Обновить данные агента.');