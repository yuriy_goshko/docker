<?php
return array(
    'FAILED'=>'неудавшийся',
    'BUSY'=>'занято',
    'ANSWERED'=>'отвечен',
    'UNKNOWN'=>'неизвестно',
    'NO ANSWER'=>'не отвечен',
    'uFAILED'=>'не обработан',
    'uHANGUP'=>'сброс звонящим',
);