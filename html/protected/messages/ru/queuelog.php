<?php
//messages to CallInfo model class.
return array(
    'department queues'=>'отдел очередей',
    'event'=>'событие',
    'queue name'=>'имя очереди',
    'period'=>'интервал времени',
    'period days a week' => "интервал дней недели",
    'system name' => 'системное имя',
    'missed' => 'не принятые',
    'recieved' => 'принятые',
    'transfer' => 'трансфер',
    'waittime' => 'время ожидания',
    'average talk time' => 'среднее время разговора',
    'failed' => 'пропущенные',
    'talk time'=>'время разговора',
    
    
);