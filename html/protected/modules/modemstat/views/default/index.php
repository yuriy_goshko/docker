<?php
$this->finalCss = Yii::app()->assetManager->publish(implode('/', array(Yii::app()->basePath,'..','css'))) . '/' . 'summary_grid.css';
?>

<div class="transformed main-content">
	<div class="fhead">
		<h1>
			<img src="/images/design/blue-monitoring.png" /><?= $h1 ?></h1>
	</div>

    <?php
				$this->widget('bootstrap.widgets.TbGridView', array(
						'template' => '{items}<div class="gwfooter">{pager}{summary}</div>',
						'dataProvider' => $dataProvider,
						'enableHistory' => true,
						'enableSorting' => true,
						'columns' => array(
								array(
										'header' => 'Модем',
										'name' => "Device",
										'value' => function ($data, $row, $column) {
											$controller = $column->grid->owner;
											return $controller->_modems[$data['Device']];
										}),
								array(
										'header' => Yii::t('asteriskm', "State"),
										'name' => "State",
										'value' => function ($data) {
											return Yii::t('ami_data', $data['State']);
										}),
								array(
										'type' => 'raw',
										'header' => Yii::t('asteriskm', "RSSI"),
										'name' => "RSSI",
										'value' => function ($data, $row, $column) {
											$controller = $column->grid->owner;
											$rssi = $controller->parseRssi($data['RSSI']);
											$res = "<div class='progress progress-" . $rssi['class'] . "'>
                                <div class='bar' style='color:#000; width: " . $rssi['percent'] . "%'>" . $rssi['percent'] . "% </div>
                            </div>";
											return $res;
										}),
								array('header' => Yii::t('asteriskm', "AudioState"),'name' => "AudioState"),
								array('header' => Yii::t('asteriskm', "DataState"),'name' => "DataState"),
								array(
										'header' => Yii::t('asteriskm', "Voice"),
										'name' => "Voice",
										'value' => function ($data) {
											return Yii::t('ami_data', $data['Voice']);
										}),
								array(
										'header' => Yii::t('asteriskm', "SMS"),
										'name' => "SMS",
										'value' => function ($data) {
											return Yii::t('ami_data', $data['SMS']);
										}),
								array('header' => Yii::t('asteriskm', "Model"),'name' => "Model"),
								array(
										'header' => Yii::t('asteriskm', "Mode"),
										'name' => "Mode",
										'value' => function ($data) {
											return Yii::t('ami_data', $data['Mode']);
										}),
								array(
										'header' => Yii::t('asteriskm', "ProviderName"),
										'name' => "ProviderName",
										'value' => function ($data) {
											return Yii::t('ami_data', $data['ProviderName']);
										}),
								array(
										'header' => 'USSD',
										'type' => 'raw',
										'value' => function ($data) {
											return "<a href='/ussd/index/id/" . $data['Device'] . "'>" . Yii::t('m', "Send") . "</a>";
										}))));
				?>
</div>