<?php
use PAMI\Listener\IEventListener;
use PAMI\Message\Event;
use PAMI\Message\Action;
class DefaultController extends Controller implements IEventListener {
	public $layout = '//layouts/front/fixed';
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rViewReport")'),array('deny','users' => array('*')));
	}
	protected $_result = array();
	protected $_fields = array('State','AudioState','DataState','Voice','SMS','Device','Model','RSSI','Mode','ProviderName');
	protected $_percenatgeRssi = array(
			'info' => array('max' => 100,'min' => 31),
			'success' => array('max' => 31,'min' => 23.25),
			'warning' => array('max' => 23.25,'min' => 15.5),
			'danger' => array('max' => 7.75,'min' => 0));
	public $_modems = array();
	public function handle(Event\EventMessage $event) {
		if ($event instanceof Event\DongleDeviceEntryEvent)
			$this->_result[] = array_intersect_key($event->getKeys(), array_flip($this->_fields));
	}
	protected function groupFilter($item, $key) {
		foreach ( $item as $field => $val )
			if ($field == 'Device' && ! in_array($val, $this->_modems)) {
				unset($this->_result[$key]);
			}
	}
	protected function getRssiClass($val) {
		foreach ( $this->_percenatgeRssi as $class => $data ) {
			if ($data['min'] <= $val && $val < $data['max'])
				return $class;
		}
	}
	public function parseRssi(&$item) {
		preg_match('/(?P<rssi>\d+).*(?P<sign>-)(?P<dbm>\d+) (?P<amount>\w+)/i', $item, $matches);
		$per = $matches['rssi'] / 31 * 100;
		$class = $this->getRssiClass($matches['rssi']);
		
		return array('dbm' => $matches['dbm'],'percent' => $per,'class' => $class,'sign' => $matches['sign'],'amount' => $matches['amount']);
		// $item = $matches['sign'].$matches['value'].' '.$matches['amount'];
	}
	public function actionIndex() {
		$sp = StaticPages::model()->findByAttributes(array('spExtUrl' => '/modemstat'));
		$this->setPageTitle($sp["spTitle"]);	
		
		
		$sql = 'select `name` as "key", 
				-- IFNULL(`DisplayName`,`name`) as "DVal"
				 (case when COALESCE(DisplayName,"")="" then `Number` else CONCAT(`Number`," (",DisplayName,")") end) as "DVal"
				from `PBXNumbers` c where c.`Type`="DONGLE"';
		$command = Yii::app()->db->createCommand($sql);
		$modems = $command->queryAll();
		foreach ( $modems as $modem ) {
			$this->_modems[] = $modem['key'];
			$this->_modems[$modem['key']] = $modem['DVal'];
		}
		
		$response = AManager::getInstance()->send(new Action\DongleShowDevicesAction());
		
		if (count($response->getEvents()) != false && $response->isComplete()) {
			foreach ( $response->getEvents() as $event )
				$this->handle($event);
		}
		
		array_walk($this->_result, array($this,'groupFilter'));
		
		$dataProvider = new CArrayDataProvider($this->_result, array(
				'keyField' => 'Device',
				'sort' => array('attributes' => array('State','Device','RSSI')),
				'pagination' => false));
		
		$this->render('index', array('dataProvider' => $dataProvider,'h1' => $sp["spTitle"]));
	}
}