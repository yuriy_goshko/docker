$(document).ready(function() {   
    $('#pexport-button').on('click', function() {
        var href = window.location.href;
        if (href.indexOf('?') == -1) {
            href = window.location.href + '?';
        }
        window.location.href = href + '&export=true';
        console.log(window.location.href);
    });
    
	/*jQuery.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<p>").append(this.eq(0).clone()).html();
    };   */ 
    
    $(document).on('show', '.gridplayer', function(e) {
        $("script").each(function() {
            if (/.*jouele\.js$/.test($(this).attr("src"))) {
                jQuery.getScript($(this).attr("src"), function(data) {
                    initPlayer();
                });
            }
        });        
    });

   /* $(document).bind('DOMNodeInserted', function(e) {
        var element = e.target;
        console.log(element); 

    });*/
    
    $(document).on('click', '.modalpop', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'html',
            success: function(data) {
                var data = jQuery.parseJSON(data);
                $('#' + data.modalId).find('.modal-header h4').html(data.header);
                if (data.body.forms != undefined) 
                    $.each(data.body.forms, function(id, val) {
                        $("#" + id).attr('action', url);
                        var jsform = $("#" + id).jsForm({prefix: ''}).jsForm('fill', val);                       
                    });
                $('#' + data.modalId).modal();
            },
            error: function(xhr, textStatus, errorThrown) {
                $.notify(xhr.responseText, "error");
            }
        });
    });
});

function submitModalForm(form, data, hasError)
{	
    if (!hasError)
    {
        var url = form.attr('action');
        $.ajax({
            url: url,
            type: 'post',
            data: $(form).serialize(),
            dataType: 'html',
            success: function(data) {
                var data = jQuery.parseJSON(data);                
                if (data.yiiGridViewUpdate != false)
                  $.fn.yiiGridView.update(data.yiiGridViewUpdate);
                	//$.fn.yiiGridView.update('grdCallsDetailLog???');
                	
                $('#' + data.modalId).modal('hide');
            },
            error: function(xhr, textStatus, errorThrown) {
                $.notify(xhr.responseText, "error");
            }
        });
    }
}

function tbRelationTableRowClick(par) {  
	  elem = document.getElementById($(par).attr('id')); 
	  elem.className +=' selected';
	}
