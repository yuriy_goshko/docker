$(document).ready(function() {

	$(document).on('click', ':not(#anything)', function(e) {
		hidePopOvers(e);
	});
});

$(window).load(function() {

	init();
	$('.hasDatepicker').on("change paste keyup", function() {
		if ($(this).val() === "")
			$(this).val($(this).prop('defaultValue'));
		$(this).next().val(getDateTimeFormat(this));
	});

	$('.hasDatepicker').on("change", function(e) {
		if ($(this).val() === "")
			$(this).val($(this).prop('defaultValue'));
	});

});

function init() {
	$('.hasDatepicker').each(function() {
		$(this).next().val(getDateTimeFormat(this));	
	});
}

function ajaxSearch(form, data, hasError) {
	if (!hasError) {
		url = form.serialize();
		params = $.deparam.querystring('?' + url);
		window.History.pushState(null, document.title,
				decodeURIComponent($.param.querystring(
						window.location.pathname, params)));
		return false;
	}
}

/*
 * jQuery.fn.outerHTML = function(s) { return s ? this.before(s).remove() :
 * jQuery("<p>").append(this.eq(0).clone()).html(); };
 * 
 * $('#filters-form #bQuikFilter').click(function(evt) { //не исп. var btn =
 * $(this); evt.preventDefault();
 * 
 * elem = document.getElementById('filters-form'); PNode= elem.parentNode;
 * 
 * //btn.button('loading');
 * 
 * var val = $( "#bQuikFiltersList" ).val(); var aurl = ["/callslog", "ajax",
 * "GetQuikFilter"]; url = aurl.join('/'); console.log(val); if (val != null) {
 * $.post(url, {value: val}, function(data) { // PNode.removeChild(elem); //
 * PNode.outerHTML=data;
 * 
 * var jsonData = $(data).serializeObject(); var jsform = $("#" +
 * "filters-form").jsForm({prefix: ''}).jsForm('fill', jsonData); init();
 * //reinit btn.button('reset'); }); } });
 */

$('#bResetFilters').click(function(evt) {
	var btn = $(this);
	btn.button('loading'); // call the loading function
	evt.preventDefault();
	var formAction = evt.target.form.action;
	var view = formAction.substr(formAction.lastIndexOf('/') + 1);
	var formName = evt.target.form.name;
	var filters = btn.attr("filters");
	var fileroute = btn.attr("fileroute");
	var actionpage = btn.attr("actionpage");
	var datemode = btn.attr("datemode");
	var servUrlPath = [ "/callslog", "ajax", "GetResetedForm" ];
	var url = servUrlPath.join('/');
	$.post(url, {
		view : view,
		filters : filters,
		fileroute : fileroute,
		actionpage : actionpage,
		datemode : datemode,
		class : formName
	}, function(data) {
		var jsonData = $(data).serializeObject();
		var jsform = $("#" + "filters-form").jsForm({
			prefix : ''
		}).jsForm('fill', jsonData);
		init(); // reinit
		btn.button('reset');
	});
});
