<?php
// use cdbl\media\Console;
class AjaxController extends CController {
	public $layout = false;
	public function filters() {
		return array('ajaxOnly','accessControl');
	}
	public function accessRules() {
		return array(
				array('allow','users' => array('@'),'expression' => '$user->getState("rViewStat") || $user->getState("rViewReport")'),
				array('deny','users' => array('*')));
	}
	public function actionGetResetedForm() {
		// ToDo: перенести в CallsLogSearchForm
		$class = $_REQUEST['class'];
		$SearchForm = new $class();
		$rootView = $_REQUEST['view'];
		
		if (isset($_REQUEST['datemode'])) {
			$SearchForm->dateMode = $_REQUEST['datemode'];
			if ($SearchForm->dateMode)
				$SearchForm->SetToday();
		}
		
		if (isset($_REQUEST['filters'])) {
			$filters = $_REQUEST['filters'];
			$SearchForm->searchFilters = json_decode($filters);
		}
		
		if (isset($_REQUEST['fileroute']))
			$fileroute = $_REQUEST['fileroute'];
		else
			$fileroute = "/" . $rootView . '/filters-form';
		
		if (isset($_REQUEST['actionpage']))
			$actionpage = $_REQUEST['actionpage'];
		else
			$actionpage = false;
		
		$this->renderPartial($fileroute, array('SearchForm' => $SearchForm,'ActionPage' => $actionpage));
	}
	public function actionModalAgent($num = false) {
		// Yii::log(file_get_contents('php://input'), 'info');
		$model = new Agents();
		if ($num != false)
			$model = Agents::model()->find('name = :num', array('num' => $num));
		if (isset($_POST['ajax']) && Yii::app()->getRequest()->getIsAjaxRequest()) {
			$model->setScenario('ajaxValidate');
			echo CActiveForm::validate(array($model));
			Yii::app()->end();
		}
		if (isset($_POST['Agents'])) {
			$model->attributes = $_POST['Agents'];
			if (! $model->save()) {
				throw new CHttpException(500, Yii::t('m', 'At runtime error occurred'));
			}
			echo json_encode(array('modalId' => 'AgentModalForm','yiiGridViewUpdate' => 'grdCallsLog'));
			exit();
		}
		echo json_encode(array(
				'modalId' => 'AgentModalForm',
				'header' => Yii::t('m', 'Change the name of an agent') . ' ' . $model->name,
				'body' => array('forms' => array('AgentForm' => array('Agents[fullname]' => $model->fullname)))));
	}
	public function actionModalPhone($num = false) {
		$model = PhoneBook::model()->mine()->find('pbNumber = :id', array('id' => $num));
		if ($model == null) {
			$model = new PhoneBook();
		}
		if (isset($_POST['ajax']) && Yii::app()->getRequest()->getIsAjaxRequest()) {
			echo CActiveForm::validate(array($model));
			Yii::app()->end();
		}
		if (isset($_POST['PhoneBook'])) {
			$model->attributes = $_POST['PhoneBook'];
			$model->pbNumber = $num;
			if (! $model->save()) {
				throw new CHttpException(500, Yii::t('m', 'At runtime error occurred'));
			}
			echo json_encode(array('modalId' => 'PhoneModalForm','yiiGridViewUpdate' => 'grdCallsLog'));
			exit();
		}
		if ($model->isNewRecord) {
			$header = implode(' ', array(Yii::t('m', 'Add a number'),$num,Yii::t('m', 'in phonebook', 1)));
		} else {
			$header = implode(' ', array(Yii::t('m', 'Update a number'),$num,Yii::t('m', 'in phonebook', 2)));
		}
		echo json_encode(array(
				'modalId' => 'PhoneModalForm',
				'header' => $header,
				'body' => array(
						'forms' => array(
								'PhoneForm' => array(
										'PhoneBook[pbOwner]' => $model->pbOwner,
										'PhoneBook[pbBlacked]' => $model->pbBlacked,
										'PhoneBook[pbReason]' => $model->pbReason)))));
	}
}
