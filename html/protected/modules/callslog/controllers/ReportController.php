<?php
use cdbl\abstracts\Logger;
class ReportController extends Controller {
	private $reportPage = false;
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rViewReport")'),array('deny','users' => array('*')));
	}
	public function actionGeneral() {
		return $this->actionIndex();
	}
	public function actionDepartments() {
		$this->reportPage = $this->action->id;
		return $this->actionIndex();
	}
	public function actionChanout() {
		$this->reportPage = $this->action->id;
		return $this->actionIndex();
	}
	public function actionAgents() {
		$this->reportPage = $this->action->id;
		if (isset($_REQUEST['showdetails']))
			return $this->renderAgentsDetail();
		else
			return $this->actionIndex();
	}
	public function actionMissedcalls() {
		$this->reportPage = $this->action->id;
		return $this->actionIndex();
	}
	public function actionLongtimecalls() {
		$this->reportPage = $this->action->id;
		return $this->actionIndex();
	}
	public function actionCitychoose() {
		$this->reportPage = $this->action->id;
		return $this->actionIndex();
	}
	public function actionCallback() {
		$this->reportPage = $this->action->id;
		if (isset($_REQUEST['showdetails']))
			return $this->renderCallbackDetail();
		else
			return $this->actionIndex();
	}
	public function actionByhours() {
		$this->reportPage = $this->action->id;
		return $this->actionIndex();
	}
	public function actionBydays() {
		$this->reportPage = $this->action->id;
		return $this->actionIndex();
	}
	public function actionIndex() {
		if (! $this->reportPage)
			$this->reportPage = 'general';
		
		$sp = StaticPages::model()->findByAttributes(array('spUrl' => 'reports-' . $this->reportPage));
		$this->setPageTitle($sp["spTitle"]);
		$isExportCSV = Yii::app()->request->getParam('export');
		
		$SearchForm = new CallsLogSearchForm();
		
		if (isset($_REQUEST[get_class($SearchForm)]))
			$SearchForm->attributes = $_REQUEST[get_class($SearchForm)];
		else if ($this->reportPage == 'missedcalls') {
			$SearchForm->SetToday();
			$SearchForm->sfTimeOutSel = '30';
			$SearchForm->sfTimeOutSelIntType = 'MINUTE';
		} else if ($this->reportPage == 'longtimecalls') {
			$SearchForm->SetToday();
			$SearchForm->sfTimeOutSel = '30';
			$SearchForm->sfTimeOutSelIntType = 'SECOND';
		} else if ($this->reportPage == 'byhours') {
			$SearchForm->SetToday();
		}
		
		$keyField = 'name';
		$pageSize = 10;
		$renderEmptyModel = false;
		
		$model = new DataCallsReports();
		$model->fillConditions($SearchForm->attributes, ($this->reportPage == 'agents'), ($this->reportPage == 'byhours'));
		switch ($this->reportPage) {
			case 'general' :
				$SearchForm->searchFilters = array('sfDirections','sfDepartments','sfPBXNum');
				$modelReport = $model->General_fetchAllByDirection();
				if (! $modelReport->isEmpty())
					$GridColumns = $modelReport->General_getGridColumns();
				$sortAttributes = array('TitleName','Duration','BridgeDuration','ANSWERED','NO_ANSWER','BUSY','FAILED','allCount');
				break;
			case 'departments' :
				$SearchForm->searchFilters = array('sfDepartments','sfAgents');
				$modelReport = $model->Departments_fetchAllByDepartments();
				if (! $modelReport->isEmpty())
					$GridColumns = $modelReport->Departments_getGridColumns();
				$sortAttributes = array('TitleName','Outbound','Inbound','Local','Conference','Callback','allCount');
				break;
			case 'agents' :
				$SearchForm->filterDirections = array('Outbound','Inbound','Local','Callback');
				$SearchForm->searchFilters = array('sfDirections','sfDepartments','sfAgents');
				$modelReport = $model->Agents_fetchAll();
				break;
			case 'chanout' :
				$SearchForm->searchFilters = array('sfPBXNum','sfDirections');
				$modelReport = $model->Chanout_fetchAll();
				if (! $modelReport->isEmpty())
					$GridColumns = $modelReport->Chanout_getGridColumns();
				$sortAttributes = array('name','Duration','BridgeDuration','ANSWERED','NO_ANSWER','BUSY','FAILED','allCount');
				break;
			case 'missedcalls' :
				$SearchForm->filterDirections = array('Inbound','Callback');
				$SearchForm->searchFilters = array('sfNum','sfDepartments','sfQueues','sfDirections','sfTimeOutSel');
				$SearchForm->sfTimeOutSelLabel = 'Время дозвона:';
				$keyField = 'NumSrc';
				$pageSize = 20;
				$renderEmptyModel = true;
				$modelReport = $model->MissedCalls_fetchAll();
				if (! $modelReport->isEmpty())
					$GridColumns = $modelReport->MissedCalls_getGridColumns();
				$sortAttributes = array('NumSrc','DirectionId','minDT','AllWaitTime','WaitTime','count','OutboundCount','AgentsCount');
				break;
			case 'longtimecalls' :
				$SearchForm->filterDirections = array('Inbound','Callback');
				$SearchForm->searchFilters = array('sfNum','sfDepartments','sfQueues','sfDirections','sfTimeOutSel');
				$SearchForm->sfTimeOutSelLabel = 'Время вызова больше:';
				$keyField = 'NumSrc';
				$pageSize = 20;
				$renderEmptyModel = true;
				$modelReport = $model->LongTimeCalls_fetchAll();
				if (! $modelReport->isEmpty())
					$GridColumns = $modelReport->LongTimeCalls_getGridColumns();
				$sortAttributes = array('NumSrc','DirectionId','minDT','AllWaitTime','WaitTime','count','AgentsCount');
				break;
			case 'citychoose' :
				$SearchForm->searchFilters = array('sfAllTime');
				$modelReport = $model->Citychoose_fetchAll();
				if (! $modelReport->isEmpty())
					$GridColumns = $modelReport->Citychoose_getGridColumns();
				$sortAttributes = array('name','allCount');
				break;
			case 'callback' :
				$SearchForm->searchFilters = array('sfPBXNum');
				$modelReport = $model->Callback_fetchAll();
				if (! $modelReport->isEmpty())
					$GridColumns = $modelReport->Callback_getGridColumns();
				$sortAttributes = array('name','INIT','CALLBACK','Duration','BridgeDuration','ANSWERED','NO_ANSWER','BUSY','FAILED','allCount');
				break;
			case 'bydays' :
				$modelReport = $model->Bydays_fetchAll();
				$SearchForm->searchFilters = array('sfDepartments','sfQueues','sfDirections');
				$sortAttributes = array();
				break;
			case 'byhours' :
				$modelReport = $model->Byhours_fetchAll();
				$SearchForm->searchFilters = array('sfDepartments','sfQueues','sfDirections');
				$SearchForm->dateMode = true;
				$sortAttributes = array();
				break;
		}
		
		if ($this->reportPage == 'agents') {
			$dcs = $modelReport->Agents_getDirectionKeys();
			foreach ( $dcs as $key ) {
				$dataProvider[$key] = new CArrayDataProvider($modelReport->result[$key], array(
						'keyField' => 'name',
						'sort' => array('attributes' => $modelReport->Agents_getSortAttributes($key))));
			}
		} else
			$dataProvider = new CArrayDataProvider($modelReport->result, array(
					'keyField' => $keyField,
					'pagination' => array('pageSize' => $pageSize),
					'sort' => array('attributes' => $sortAttributes)));
		
		if (($isExportCSV) && (! $modelReport->isEmpty())) {
			if ($this->reportPage == 'agents') {
				foreach ( $dataProvider as $key => $dp ) {
					if (count($dp->rawData) > 0)
						DataModule::csvExport($dp, $sp["spTitle"] . '_' . $key, $modelReport->Agents_getGridColumns($key), array());
					Yii::app()->end(); // несколько не поддерживает
				}
			} else
				DataModule::csvExport($dataProvider, $sp["spTitle"], $GridColumns, array());
			Yii::app()->end();
		}
		
		$this->render('main-form', array(
				'reportPage' => $this->reportPage,
				'modelReport' => $modelReport,
				'dataProvider' => $dataProvider,
				'SearchForm' => $SearchForm,
				'renderEmptyModel' => $renderEmptyModel,
				'needCSVExport' => ($this->reportPage != 'bydays') && ($this->reportPage != 'byhours'),
				'h1' => $sp["spTitle"]));
	}
	public function renderAgentsDetail() {
		$sp = StaticPages::model()->findByAttributes(array('spUrl' => 'reports-' . $this->reportPage));
		$this->setPageTitle($sp["spTitle"]);
		$isExportCSV = Yii::app()->request->getParam('export');
		
		$modelSql = new DataCallsReports();
		$dirkey = $_REQUEST['dirkey'];
		
		$s = $modelSql->Agents_getDirSQL($dirkey, true, $_REQUEST);
		$model = CallsLogModel::model()->default();
		$model->dbCriteria->addCondition($s);
		$pagination = array('pageSize' => 20);
		$dataProvider = new CActiveDataProvider($model, array('sort' => CallsLogModel::GetDefaultSortConfig(),'pagination' => $pagination));
		
		$agent = $_REQUEST['agent'];
		$subtype = $_REQUEST['subtype'];
		$subtype = yii::t('reports', $subtype);
		
		$dirType = $modelSql->GetDirectionCaption($dirkey, true);
		
		if ($isExportCSV) {
			$fn = $sp["spTitle"] . '.' . $dirType . '.' . $agent . '.' . $subtype;
			DataModule::csvExport($dataProvider, $fn, CallsUtils::GetCallslogMainGridColumns(true), Yii::t('callslog', 'attributeLabels'));
			Yii::app()->end();
		}
		$this->render('reports/agents_detail', array(
				'dataProvider' => $dataProvider,
				'model' => $model,
				'rTitles' => array('dirType' => $dirType,'agentFN' => $agent,'subtype' => $subtype),
				'h1' => $sp["spTitle"]));
	}
	public function renderCallbackDetail() {
		$sp = StaticPages::model()->findByAttributes(array('spUrl' => 'reports-' . $this->reportPage));
		$this->setPageTitle($sp["spTitle"]);
		$isExportCSV = Yii::app()->request->getParam('export');
		
		$modelSql = new DataCallsReports();
		
		$s = $modelSql->Callback_getCallsLogSQL($_REQUEST);
		$model = CallsLogModel::model()->default();
		$model->dbCriteria->addCondition($s);
		$pagination = array('pageSize' => 20);
		$dataProvider = new CActiveDataProvider($model, array('sort' => CallsLogModel::GetDefaultSortConfig(),'pagination' => $pagination));
		
		$pbxNum = $_REQUEST['pbxNum'];
		$actionName = $_REQUEST['column'];
		
		if ($actionName == 'INIT')
			$actionName = 'Запрос';
		else if ($actionName == 'CALLBACK')
			$actionName = 'Отзвон';
		else
			$actionName = $modelSql->GetStateCaption($actionName);
		
		if ($isExportCSV) {
			$fn = $sp["spTitle"] . '.CALLBACK.' . $pbxNum . '.' . $actionName;
			DataModule::csvExport($dataProvider, $fn, CallsUtils::GetCallslogMainGridColumns(true), Yii::t('callslog', 'attributeLabels'));
			Yii::app()->end();
		}
		$this->render('reports/callback_detail', array(
				'dataProvider' => $dataProvider,
				'model' => $model,
				'rTitles' => array('pbxNum' => $pbxNum,'actionName' => $actionName),
				'h1' => $sp["spTitle"]));
	}
}