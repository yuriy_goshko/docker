<?php
class ApiController extends Controller {
	private $HTTP_AUTH_USER = '';
	private $HTTP_AUTH_PW = '';
	private $userRole = '';
	private $userSipId = 0;
	private $userId = 0;
	protected function loadIdentityStates($states) {
		$names = array();
		if (is_array($states)) {
			foreach ( $states as $name => $value ) {
				yii::app()->user->setState($name, $value);
				$names[$name] = true;
			}
		}
		yii::app()->user->setState(CWebUser::STATES_VAR, $names);
	}
	private function WebUserAuthorization() {
		// $s = yii::app()->user->getState('rHash');
		yii::app()->user->setName($this->HTTP_AUTH_USER);
		yii::app()->user->setId(- $this->userId);
		yii::app()->user->init();
		// $s .= '<br/>'.yii::app()->user->getState('rHash');
		
		return yii::app()->user->isInitialized;
	}
	private function DBAuthorization() {
		$sql = 'select s.`id`, s.`userRole`, u.`uID` 
		 FROM `SIP` s
		 left join `Users` u on u.`uIdentity` = s.`userRole` and (u.`IsRole` = 1)	
		 where s.`name` = :pName 
		 and md5(CONCAT("s1",s.`name`,"u2",s.`secret`,"p3")) = :pHash';
		
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":pName", $this->HTTP_AUTH_USER, PDO::PARAM_STR);
		$command->bindParam(":pHash", $this->HTTP_AUTH_PW, PDO::PARAM_STR);
		$qr = $command->queryRow();
		
		if (! empty($qr)) {
			$this->userSipId = $qr['id'];
			$this->userRole = $qr['userRole'];
			$this->userId = $qr['uID'];
		}
		
		return (! empty($this->userId)) && ($this->userSipId > 0) && ($this->userId > 0);
	}
	private function CheckAuthorization($showInfo = false) {
		$s = 'Welcome to AMI';
		$auth = False;
		
		if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
			$this->HTTP_AUTH_USER = $_SERVER['PHP_AUTH_USER'];
			$this->HTTP_AUTH_PW = $_SERVER['PHP_AUTH_PW'];
			$s .= '<br/>PHP_AUTH_USER: ' . $this->HTTP_AUTH_USER;
			$s .= '<br/>PHP_AUTH_PW: ' . $this->HTTP_AUTH_PW;
			$auth = True;
		}
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			$HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];
			// $s .= '<br/>HTTP_USER_AGENT: ' . $_SERVER['HTTP_USER_AGENT'];
		}
		
		if ($auth && (! $this->DBAuthorization())) {
			$auth = false;
			$s = 'UserNotExists';
		}
		
		if (! $auth)
			$this->_sendResponse(401, $s);
		
		if (! $showInfo)
			$s = '';
		
		return $s;
	}
	private static function ClearAppEnd() {
		ob_start();
		Yii::app()->end(0, false);
		ob_end_clean();
	}
	private function sendFile($fn, $delAfterSend = False) {
		$this::ClearAppEnd();
		$fp = fopen($fn, 'r');
		Yii::app()->request->sendFile(basename($fn), stream_get_contents($fp), null, false);
		fclose($fp);
		if ($delAfterSend)
			unlink($fn);
		Yii::app()->end();
	}
	public function getFileName() {
		if (! isset($_GET['sipcallid']))
			$this->_sendResponse(207, 'SipCallIdNotExists');
		
		$sipcallid = $_GET['sipcallid'];
		$sql = 'select `CallId` from `CI_SipCalls` where `SipCallId` = "' . $sipcallid . '"';
		$CallId = Yii::app()->db->createCommand($sql)->queryScalar();
		
		if (! isset($CallId) || ($CallId <= 0))
			$this->_sendResponse(207, 'CallNotFound');
		
		$model = CallsLogModel::model()->default();
		$model = $model->findByPk($CallId);
		
		if ($model == false)
			$this->_sendResponse(207, 'CallNotFound_');
		
		$fn = CallsUtils::GenerateBridgeDurationContent($model, true);
		
		if ($fn == 'rListenAudioAbsent')
			$this->_sendResponse(207, 'NoPrivileges');
		
		if (file_exists($fn)) {
			return $fn;
		} else {
			$this->_sendResponse(207, 'FileNotExists');
		}
	}
	public function actionCallRecord() {
		$this->CheckAuthorization();
		if (! $this->WebUserAuthorization())
			$this->_sendResponse(207, 'AuthorizationError');
		
		$fn = $this->getFileName();
		$fn = $this->ConvertMp3ToWav($fn);
		$this->sendFile($fn, True);
	}
	private function ConvertMp3ToWav($fn) {
		$path_info = pathinfo($fn);
		if (strtolower($path_info['extension'] == 'wav'))
			return $fn;
		$fnWav = '/tmp/' . $path_info['filename'] . '.wav';
		if (! file_exists($fnWav))
			exec('lame -h ' . $fn . ' ' . $fnWav . ' --decode');
		if (file_exists($fnWav)) {
			return $fnWav;
		} else {
			$this->_sendResponse(207, 'ConvertError');
		}
	}
	public function actionIndex() {
		$s = $this->CheckAuthorization(true);
		
		$s .= '<br/>Ok!';
		if ($this->WebUserAuthorization())
			$s .= '<br/>rHash: ' . yii::app()->user->getState('rHash');
		if (isset($_GET['sipcallid']))
			$s .= '<br/>sipcallid=' . $_GET['sipcallid'];
		$this->_sendResponse(200, $s);
	}
	private function _sendResponse($status = 200, $body = '', $content_type = 'text/html', $fname = '') {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		header($status_header);
		header('Content-type: ' . $content_type);
		$this::ClearAppEnd();
		
		if ($body != '') {
			echo $body;
		} else {
			$message = '';
			
			switch ($status) {
				case 401 :
					$message = 'You must be authorized to view this page.';
					break;
				case 404 :
					$message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
					break;
				case 500 :
					$message = 'The server encountered an error processing your request.';
					break;
				case 501 :
					$message = 'The requested method is not implemented.';
					break;
			}
			
			// servers don't always have a signature turned on
			// (this is an apache directive "ServerSignature On")
			// $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
			$signature = '';
			
			$body = '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
</head>
<body>
    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
    <p>' . $message . '</p>
    <hr />
    <address>' . $signature . '</address>
</body>
</html>';
			echo $body;
		}
		Yii::app()->end();
	}
	private function _getStatusCodeMessage($status) {
		$codes = Array(
				200 => 'OK',
				400 => 'Bad Request',
				401 => 'Unauthorized',
				402 => 'Payment Required',
				403 => 'Forbidden',
				404 => 'Not Found',
				500 => 'Internal Server Error',
				501 => 'Not Implemented');
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
}