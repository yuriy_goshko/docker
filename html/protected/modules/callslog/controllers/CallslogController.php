<?php
class CallslogController extends Controller {
	public function filters() {
		return array('accessControl'); // perform access control for CRUD operations
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rViewStat")'),array('deny','users' => array('*')));
	}
	public function getPageSize($SearchForm) {
		$pagsize = 0;
		if (isset($_REQUEST[get_class($SearchForm)]['sfPagesize'])) {
			$pagsize = $_REQUEST[get_class($SearchForm)]['sfPagesize'];
			Yii::app()->request->cookies['callslog_pagesize'] = new CHttpCookie('callslog_pagesize', $pagsize);
		}
		if ($pagsize < 1)
			if (isset(Yii::app()->request->cookies['callslog_pagesize']))
				$pagsize = Yii::app()->request->cookies['callslog_pagesize']->value;
			else
				$pagsize = 20;
		return $pagsize;
	}
	public function actionIndex() {
		$sp = StaticPages::model()->findByAttributes(array('spExtUrl' => '/callslog'));
		$this->setPageTitle($sp["spTitle"]);
		$isExportCSV = Yii::app()->request->getParam('export');
		
		$SearchForm = new CallsLogSearchForm();
		$SearchForm->ajaxSearch = true;
		$SearchForm->searchFilters = array('sfNum','sfDepartments','sfStates','sfDirections','sfAgents','sfQueues','sfPBXNum','sfPagesize');
		$pagsize = $this->getPageSize($SearchForm);
		
		$command = Yii::app()->db->createCommand('select value from Settings where param = "FindByCallbakInit"');
		Yii::app()->params['FindByCallbakInit'] = $command->queryScalar();
		$command = Yii::app()->db->createCommand('select value from Settings where param = "ShowLinkDeps"');
		Yii::app()->params['ShowLinkDeps'] = $command->queryScalar();
		$command = Yii::app()->db->createCommand('select value from Settings where param = "FindAudioArchOnly"');
		Yii::app()->params['FindAudioArchOnly'] = $command->queryScalar();
		if (Yii::app()->params['ShowLinkDeps'] == 1)
			$model = CallsLogModel::model()->defaultDeps();
		else
			$model = CallsLogModel::model()->default();
		
		if (isset($_REQUEST[get_class($SearchForm)])) {
			$SearchForm->setAttributes($_REQUEST[get_class($SearchForm)]);
			if (! $SearchForm->validate())
				throw new CHttpException(404, 'Model validation failed');
		}
		$model->search($SearchForm);
		
		if ($isExportCSV)
			$pagination = false;
		else
			$pagination = array('pageSize' => $pagsize);
		$dataProvider = new CActiveDataProvider($model, array('sort' => CallsLogModel::GetDefaultSortConfig(),'pagination' => $pagination));
		
		if ($isExportCSV) {
			DataModule::csvExport($dataProvider, $sp["spTitle"], CallsUtils::GetCallslogMainGridColumns(true), Yii::t('callslog', 'attributeLabels'));
			Yii::app()->end();
		}
		
		$this->render('callslog', array(
				'dataProvider' => $dataProvider,
				'SearchForm' => $SearchForm,
				'model' => $model,
				'h1' => array('title' => $sp["spTitle"],'class' => $sp["spHtmlClass"])));
	}
	public function init() {
		// $str = '<script>js:$.notify(" ' .$str .' ", "info");</script>';
	}
	public function actionDetail($id) {
		$model = CallsLogModel::GetDetailModel()->default();
		$dataProvider = new CActiveDataProvider($model, array(
				'sort' => $model::GetDefaultSortConfig(),
				'criteria' => array("params" => array(':cid' => $id)),
				'pagination' => false));
		$this->renderPartial('/layouts/callsgrid_detail', array('dataProvider' => $dataProvider,'model' => $model));
	}
}
