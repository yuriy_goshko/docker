<?php
class CitychooseController extends Controller {
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rViewReport")'),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		$sp = StaticPages::model()->findByAttributes(array('spExtUrl' => '/callslog/citychoose'));
		$this->setPageTitle($sp["spTitle"]);
		$header = array('title' => $sp["spTitle"],'class' => $sp["spHtmlClass"],'wikipage' => 'citychoose','image' => 'blue-pb');
		$isExportCSV = Yii::app()->request->getParam('export');
		$model = CityChoose::model()->CityNames();
		
		if (isset($_GET['CityChoose'])) {
			$model->attributes = $_GET['CityChoose'];
			$model->search();
		}
		
		$pagsize = 20;
		if ($isExportCSV)
			$pagination = false;
		else
			$pagination = array('pageSize' => $pagsize);
		
		$dataProvider = new CActiveDataProvider($model, array(
				'sort' => array('attributes' => array('*',"CityName" => array())),
				'pagination' => $pagination));
		
		if ($isExportCSV) {
			DataModule::csvExport($dataProvider, $sp["spTitle"], $model->GetDefaultGridColumns(true), $model->attributeLabels());
			Yii::app()->end();
		}
		
		$this->render('citychoose', array('dataProvider' => $dataProvider,'model' => $model,'header' => $header));
	}
	public function loadModel($id) {
		$model = CityChoose::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
	public function actionDelete($id) {
		$this->loadModel($id)->delete();
		if (! isset($_GET['ajax']))
			$this->redirect(array('/callslog/citychoose/'));
	}
	public function actionUpdate($id) {
		$model = $this->loadModel($id);
		if (Yii::app()->request->isPostRequest) {
			$model->city = $_REQUEST['frmCitychoose']['city_ed'];
			if ($model->save())
				Yii::app()->user->setFlash('success', "Успешно обновлен!");
			else
				Yii::app()->user->setFlash('error', $model);
		}
		$this->redirect(array('/callslog/citychoose/'));
	}
}