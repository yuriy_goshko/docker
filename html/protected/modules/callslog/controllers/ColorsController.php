<?php
class ColorsController extends Controller {
	public function filters() {
		return array('accessControl'); // perform access control for CRUD operations
	}
	public function accessRules() {
		return array(
				array('allow','users' => array('@')),
				array('allow','actions' => array('makecsv'),'users' => array('admin')),
				array('deny','users' => array('*')));
	}
	public function ColorRow($Key, $Name, $Color) {
		$res = CHtml::openTag('tr',array('class'=>'rtrdown'));
		$res .= CHtml::tag('td', array(), $Key);
		$res .= CHtml::tag('td', array(), $Name);
		$res .= CHtml::tag('td', array('bgcolor' => $Color,'width' => "120"));
		$res .= CHtml::tag('td', array(), $Color);
		
		//$s= CHtml::tag('tbrelational-columnimg', array(),'  ');
		//$res .= CHtml::tag('td', array(), $s);
		//$res .= CHtml::tag('td', array('class'=>'tbrelational-columnimg'));
		$res .= CHtml::closeTag('tr');
		return $res;
	}
	public function actionIndex() {
		$content = '';
		$BaseColors = array('#4572A7','#AA4643','#89A54E','#80699B','#3D96AE','#DB843D','#92A8CD','#A47D7C','#B5CA92');
		$content .= CHtml::tag('h2', array(), 'Базовые');
		$content .= CHtml::openTag('table');
		$i = 0;
		foreach ( $BaseColors as $value ) {
			$i ++;
			$content .= $this->ColorRow($i, '-', $value);
		}
		$content .= CHtml::closeTag('table');
		
		$s= CHtml::tag('tbrelational-columnimg', array(),'');
		$content .= CHtml::tag('div', array(), $s);
		
		$content .= CHtml::tag('h2', array(), 'Статусы');
		$content .= CHtml::openTag('table');
		$content .= $this->ColorRow('ANSWERED', 'Отвечен', DataCallsReports::GetStateColor('ANSWERED'));
		$content .= $this->ColorRow('BUSY', 'Занято', DataCallsReports::GetStateColor('BUSY'));
		$content .= $this->ColorRow('NO_ANSWER', 'Не отвечен', DataCallsReports::GetStateColor('NO_ANSWER'));
		$content .= $this->ColorRow('FAILED', 'Неудавшийся', DataCallsReports::GetStateColor('FAILED'));
		$content .= $this->ColorRow('', '', '');
		$content .= $this->ColorRow('ANSWERED_PART', 'Отвечен *', DataCallsReports::GetStateColor('ANSWERED_PART'));
		$content .= $this->ColorRow('UNKNOWN', 'Неизвестен', DataCallsReports::GetStateColor('UNKNOWN'));
		$content .= CHtml::closeTag('table');
		
		$content .= CHtml::tag('h2', array(), 'Типы');
		$content .= CHtml::openTag('table');
		$content .= $this->ColorRow('Outbound', 'Исходящий', DataCallsReports::GetDirectionColor('Outbound'));
		$content .= $this->ColorRow('Inbound', 'Входящий', DataCallsReports::GetDirectionColor('Inbound'));
		$content .= $this->ColorRow('Local', 'Внутренний', DataCallsReports::GetDirectionColor('Local'));
		$content .= $this->ColorRow('Conference', 'Конференция', DataCallsReports::GetDirectionColor('Conference'));
		$content .= $this->ColorRow('ConfOut', 'Внешняя конференция', DataCallsReports::GetDirectionColor('ConfOut'));
		$content .= $this->ColorRow('AutoCall', 'Автозвонок', DataCallsReports::GetDirectionColor('AutoCall'));
		$content .= $this->ColorRow('Callback', 'Отзвон', DataCallsReports::GetDirectionColor('Callback'));
		$content .= $this->ColorRow('', '', '');
		$content .= $this->ColorRow('CallbackInit', 'Запрос отзвона', DataCallsReports::GetDirectionColor('CallbackInit'));
		$content .= CHtml::closeTag('table');
		$this->renderText($content);
	}
}
