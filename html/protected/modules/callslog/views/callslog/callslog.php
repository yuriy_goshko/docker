<div class="row-fluid fhead">
	<div class="fhead-content">
		<div class="span10 <?= $h1['class'] ?>">
			<h1>
				<img src="/images/design/blue-cdr.png" /><?= $h1['title'] ?></h1>
		</div>
		<div class="buttons" style="float: right">            
			<?php
			if (yii::app()->user->getState('rCSVExport'))
				echo CHtml::button(Yii::t('m', 'Export CSV'), array('id' => 'pexport-button','class' => 'btn export-btn','style' => "float:right;"));
			echo CHtml::tag('a', array(
					'class' => "btn btn-primary",
					'target' => "_blank",
					'href' => Yii::app()->params['wikilink'] . 'callslog',
					'style' => "float:right;margin-right: 10px;"), CHtml::Tag('div', array('class' => "wikihelp"), null) . Yii::t('m', 'Wiki Help'));			
			?>    	
		</div>
	</div>
</div>

<div id="bottombar"
	class="row-fluid filter <? if (isset($_REQUEST[get_class($SearchForm)])) : ?>active<? endif; ?>">
	<div class="filter-content">
        <?=$this->renderPartial('/layouts/filters-form', array('SearchForm' => $SearchForm,'ActionPage' => 'callslog'));?>
    </div>
</div>

<div class="row-fluid main-content">		
  <?=$this->renderPartial('/layouts/callsgrid_main', array('dataProvider' => $dataProvider,'model' => $model));?>  
</div>


