<?php
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
?>
<?php

$form = $this->beginWidget('ActiveForm', array(
		'id' => 'filters-form',
		'enableClientValidation' => true,
		'clientOptions' => array('validateOnSubmit' => true,'afterValidate' => 'js:ajaxSearch'),
		'htmlOptions' => array('name' => 'CallsLogSearchForm'),
		'method' => 'get',
		'action' => 'callslog'));
?>

<div
	class="toggled-title pull-left
     <? if (isset($_REQUEST[get_class($SearchForm)])) : ?>active<? endif;?>">

	<h3 class="lever pull-left">
		<span
			class="icon-chevron-<? if (isset($_REQUEST[get_class($SearchForm)])) : ?>up<? else: ?>down<? endif; ?>"></span>
        <?= Yii::t('m', 'Filter')?>
    </h3>

	<div class="pull-left">
        <?php
								$this->widget('CJuiDateTimePicker', array(
										'name' => '_sfCallDateTime',
										'value' => Yii::app()->dateFormatter->formatDateTime($SearchForm->sfCallDateTime),
										'mode' => 'datetime',
										'htmlOptions' => array(
												'data-toggle' => "tooltip",
												'data-placement' => 'top',
												'title' => Yii::t('callinfo', 'start date'))));
								echo $form->hiddenField($SearchForm, 'sfCallDateTime');
								?>
								-
        <?php
								$this->widget('CJuiDateTimePicker', array(
										'name' => "_sfCallEndDateTime",
										'value' => Yii::app()->dateFormatter->formatDateTime($SearchForm->sfCallEndDateTime),
										'mode' => 'datetime',
										'htmlOptions' => array(
												'data-toggle' => "tooltip",
												'data-placement' => 'top',
												'title' => Yii::t('callinfo', 'end date'))));
								echo $form->hiddenField($SearchForm, 'sfCallEndDateTime');
								?> 
        <?php
								$this->widget('bootstrap.widgets.TbButton', array(
										'buttonType' => 'submit',
										'type' => 'primary',
										'label' => Yii::t('m', 'Search')));
								?>
        <?php
								$this->widget('bootstrap.widgets.TbButton', array(
										'id' => 'bResetFilters',
										'buttonType' => 'reset',
										'loadingText' => Yii::t('m', 'Reset') . '...',
										'label' => Yii::t('m', 'Reset')));
								?>
    </div>
</div>

<div
	class="form toggled-content <? if (!isset($_REQUEST[get_class($SearchForm)])) : ?>collapsed<? endif; ?>">
	<div class="row-fluid line"></div>

	<div class="row-fluid">
		<div class="span3 controls">
            <?php echo $form->labelEx($SearchForm, 'sfNum', array('class' => 'span5')); ?>
            <?php echo $form->textField($SearchForm, 'sfNum', array('value' => $SearchForm->sfNum, 'class' => 'span7'));?>
            <?php echo $form->error($SearchForm, 'sfNum'); ?>
        </div>
		<div class="span3 controls">
            <?php echo $form->labelEx($SearchForm, 'sfDepartments', array('class' => 'span5')); ?>
            <?php
												$this->widget('ext.bootstrap-select.TbSelect', array(
														'model' => $SearchForm,
														'attribute' => 'sfDepartments',
														'data' => Chtml::listData(DataModule::getDepartmentsList(), 'dID', 'dName'),
														'htmlOptions' => array('class' => 'span7 designed-select','multiple' => true)))?>
        </div>
		<div class="span3 controls">
            <?php echo $form->labelEx($SearchForm, 'sfStates', array('class' => 'span5')); ?>
            <?php
												$this->widget('ext.bootstrap-select.TbSelect', array(
														'model' => $SearchForm,
														'attribute' => 'sfStates',
														'data' => Chtml::listData(DataModule::getStatesList(), 'Id', 'Name'),
														'htmlOptions' => array('class' => 'span7 designed-select','multiple' => true)))?>
        </div>
	</div>
	<div class="row-fluid">
		<div class="span3 controls">
	        <?php echo $form->labelEx($SearchForm, 'sfDirections', array('class' => 'span5')); ?>
            <?php
												$this->widget('ext.bootstrap-select.TbSelect', array(
														'model' => $SearchForm,
														'attribute' => 'sfDirections',
														'data' => Chtml::listData(DataModule::getDirectionsList(), 'Id', 'Name'),
														'htmlOptions' => array('class' => 'span7 designed-select','multiple' => true)))?>
		</div>
		<div class="span3 controls">
            <?php echo $form->labelEx($SearchForm, 'sfAgents', array('class' => 'span5')); ?>
            <?php
												$this->widget('ext.bootstrap-select.TbSelect', array(
														'model' => $SearchForm,
														'attribute' => 'sfAgents',
														'data' => Chtml::listData(DataModule::getAgentsList($SearchForm->sfDepartments), 'FN', 'FN'),
														'htmlOptions' => array('class' => 'span7 designed-select','multiple' => true)))?>
        </div>
		<div class="span3 controls">
            <?php echo $form->labelEx($SearchForm, 'sfChanNameOut', array('class' => 'span5')); ?>
            <?php
												$this->widget('ext.bootstrap-select.TbSelect', array(
														'model' => $SearchForm,
														'attribute' => 'sfChanNameOut',
														'data' => Chtml::listData(DataModule::getOutChannelsList(), 'cName', 'FullName'),
														'htmlOptions' => array('class' => 'span7 designed-select','multiple' => true)))?>
        </div>
	</div>
	<div class="row-fluid line nextline"></div>
	<div class="row-fluid ">
<? if (false): ?>	
		<div class="span3 controls ">
			<?php echo $form->labelEx($SearchForm, 'sfSpecFilter', array('class' => 'span5')); ?>												            
            <?php
	$this->widget('ext.bootstrap-select.TbSelect', array(
			
			'model' => $SearchForm,
			'attribute' => 'sfSpecFilter',
			'data' => CallsLogSearchForm::getSpecFilterList(),
			'htmlOptions' => array('id' => 'bQuikFiltersList','class' => 'span7 designed-select','multiple' => true)))?>
        </div>
<?endif;?>        
		<div class="span3 controls">
            <?php echo $form->labelEx($SearchForm, 'sfPagesize', array('class' => 'span5')); ?>
            <?php
												echo $form->textField($SearchForm, 'sfPagesize', array(
														'value' => $SearchForm->sfPagesize,
														'class' => 'span7'));
												?>
            <?php echo $form->error($SearchForm, 'cfPagesize'); ?>
        </div>

	</div>
</div>
<?php $this->endWidget(); ?>



