<?php
$DetailGridColumns = array(
		
		// array('name' => 'FullNameSrc','type' => 'raw','value' => '$data->FullNameSrcContent'),
		// array('name' => 'FullNameDst','type' => 'raw','value' => '$data->FullNameDstContent'),
		array('name' => 'FullNameSrc'),
		array('name' => 'FullNameDst'),	
		array('name' => 'NumOut', 'value' => '$data->FullNameOut'),
		// array('name' => 'ConfNum'),
		//array('name' => 'StateName','type' => 'raw','value' => '$data->StateNameContent'),
		array('name' => 'StateName'),
		array('name' => 'DirName'),				
		array('name' => 'CallDateTime','value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data->CallDateTime)'),
		array('name' => 'Duration','value' => 'sec2hms($data->Duration,true)'),
		array('name' => 'BridgeDuration','value' => 'sec2hms($data->BridgeDuration,true)'),		
		//array('name' => 'CallEndDateTime','value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data->CallDateTime)'),
		array('name' => 'TransferFrom'),
		array('name' => 'TransferTo'));

?>

<div class="DetailGrid effect8 ">
<?php
// $this->widget('bootstrap.widgets.TbGridView', array(
$this->widget('TbGridViewExtTitle', array(
		'id' => 'grdCallsDetailLog',
		'dataProvider' => $dataProvider,
		'enableSorting' => false,
		'ajaxUpdate' => true,
		
		// 'summaryText' => Yii::t ( 'm', 'Calls' ) . ' {start}-{end} ' . Yii::t ( 'm', 'of' ) . ' {count}',
		'template' => '{items}',
		'columns' => $DetailGridColumns));
?>
</div>