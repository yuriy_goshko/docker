<?php
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');

if ($SearchForm->ajaxSearch)
	$co = array('validateOnSubmit' => true,'afterValidate' => 'js:ajaxSearch');
else
	$co = array('validateOnSubmit' => true);

$form = $this->beginWidget('ActiveForm', array(
		'id' => 'filters-form',
		'enableClientValidation' => true,
		'clientOptions' => $co,
		'htmlOptions' => array('name' => 'CallsLogSearchForm'),
		'method' => 'get',
		'action' => $ActionPage));
?>

<div
	class="toggled-title pull-left
     <? if (isset($_REQUEST[get_class($SearchForm)])) : ?>active<? endif; ?>">

	<h3 class="lever pull-left">
		<span
			class="icon-chevron-<? if (!isset($_REQUEST[get_class($SearchForm)])) : ?>down<? else: ?>up<? endif; ?>"></span>
        <?= Yii::t('m', 'Filter')?>
    </h3>

	<div class="pull-left">
		<? $SearchForm->GenerateHTMLTimeFilters($form, $SearchForm); ?>      

        <?php								
								$this->widget('bootstrap.widgets.TbButton', array(
										'buttonType' => 'submit',
										'type' => 'primary',
										'label' => Yii::t('m', 'Search')));
								?>
        <?php
								$this->widget('bootstrap.widgets.TbButton', array(
										'id' => 'bResetFilters',
										'buttonType' => 'reset',
										'htmlOptions' => array(
												'filters' => json_encode($SearchForm->searchFilters),
												'fileroute' => '/layouts/filters-form',
												'datemode' => $SearchForm->dateMode,
												'actionpage' => $ActionPage),
										
										'loadingText' => Yii::t('m', 'Reset') . '...',
										'label' => Yii::t('m', 'Reset')));
								?>
    </div>
</div>

<div
	class="form toggled-content <? if (!isset($_REQUEST[get_class($SearchForm)])) : ?>collapsed<? endif; ?>">
	<div class="row-fluid line"></div>
   <? $SearchForm->GenerateHTMLFilters($form, $SearchForm); ?>	
<? if (in_array('sfPagesize', $SearchForm->searchFilters)): ?>
<div class="row-fluid line nextline"></div>
	<div class="row-fluid ">
		<div class="span3 controls">
            <?php echo $form->labelEx($SearchForm, 'sfPagesize', array('class' => 'span5')); ?>
            <?php echo $form->textField($SearchForm, 'sfPagesize', array('value' => $SearchForm->sfPagesize,'class' => 'span7')); ?>
            <?php echo $form->error($SearchForm, 'sfPagesize'); ?>
        </div>
	</div>
<?endif;?>  
</div>


<?php $this->endWidget(); ?>

