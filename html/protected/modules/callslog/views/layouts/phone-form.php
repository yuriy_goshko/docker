<?php
$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'PhoneModalForm'));
?>

<script type="text/javascript">
function setVisibleReason() {
    if ($('#PhoneBook_pbBlacked').val() == 'no') {
        $('#PhoneBook_pbReason').parent().parent().addClass('hidden');
    } else {
        $('#PhoneBook_pbReason').parent().parent().removeClass('hidden');
    }
}
</script>

<?

$form = $this->beginWidget('ActiveForm', array(
		'id' => 'PhoneForm',
		'type' => 'horizontal',
		'method' => 'post',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'validationUrl' => Yii::app()->urlManager->createUrl('callslog/ajax/modalphone/validate'),
				'afterValidate' => 'js:submitModalForm')));
?>

<div class="modal-header">
	<a class="close" data-dismiss="modal">×</a>
	<h4></h4>
</div>

<div class="modal-body">
	<div class="control-group">
        <?= $form->labelEx($model, 'pbOwner', array('class' => 'control-label')); ?>
        <div class="controls">
            <?= $form->textField($model, 'pbOwner'); ?>
            <?= $form->error($model, 'pbOwner'); ?>
        </div>
	</div>
	<div class="control-group">
        <?php echo $form->labelEx($model, 'pbBlacked', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
												echo $form->wrappedDownList(array(
														$model,
														'pbBlacked',
														array('yes' => Yii::t('pb_data', 'yes'),'no' => Yii::t('pb_data', 'no')),
														array(
																'onChange' => "setVisibleReason()",
																'options' => array('no' => array('selected' => true)))));
												?>
            <?php echo $form->error($model, 'pbBlacked'); ?>
        </div>
	</div>
	<div class="control-group hidden">
        <?php echo $form->labelEx($model, 'pbReason', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
												echo $form->textArea($model, 'pbReason', array('maxlength' => 80,'style' => 'height:86px'));
												?>
            <?php echo $form->error($model, 'pbReason'); ?>
        </div>
	</div>
</div>

<div class="modal-footer">
    <?php
				$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit','type' => 'primary','label' => Yii::t('m', 'Save')));
				?>

    <a data-dismiss="modal" class="btn" id="yw7" href="#"><?= Yii::t('m', 'Close') ?></a>
</div>

<?php $this->endWidget(); ?>

<?php $this->endWidget(); ?>
