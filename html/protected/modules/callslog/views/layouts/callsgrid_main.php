<?php
$this->finalCss = Yii::app()->assetManager->publish(implode('/', array(Yii::app()->basePath,'..','css'))) . '/' . 'summary_grid.css';
Yii::import('application.extensions.stanislavkrsv-jouele-6fec871.Jouele');

//для активации скритпов(CLEV-205) 
$this->widget('Jouele', array('file' => '','htmlOptions' => array('hidden' => 'true')));

$this->widget('TbGridViewExtTitle', array(
		'id' => 'grdCallsLog',
		'dataProvider' => $dataProvider,
		'enableSorting' => true,
		'ajaxUpdate' => true,
		'rowCssClass' => array('odd','even'),
		'rowCssClassExpression' => function ($row, $data, $component) {
			$css = $component->rowCssClass[$row % 2];
			if (($data->ConvErr > 0) && (DataModule::GetSettingValue('MarkWarnings') === "1"))
				$css = $css . ' c_redtext';
			return $css;
		},
		'enableHistory' => true,
		'template' => '{items}<div class="gwfooter">{pager}{summary}</div>',
		'summaryText' => Yii::t('m', 'Calls') . ' {start}-{end} ' . Yii::t('m', 'of') . ' {count}',
		
		'responsiveTable' => true,
		'columns' => array_merge(array(
				array(
						'class' => 'TbRelationalColumnExt',
						'header' => CHtml::tag('img', array('src' => "/images/ic_more-info.png")),
						'headerHtmlOptions' => array('style' => 'max-width: 20px'),
						'name' => 'Detail',
						'StyleTableRow' => '"padding-left:15px !important; padding-top:5px; padding-bottom:5px;  padding-right: 15px !important;"',
						'cssClass' => 'tbrelational-columnimg',
						'url' => $this->createUrl('/callslog/callslog/detail', array('ID' => $dataProvider->Id)),
						'value' => '$data->HasDetailsCaption',
						'OnClick' => '"tbRelationTableRowClick($(this))"',
						'UseIcons' => false,
						'imgUp' => '/images/20/ic_less.png',
						'imgDown' => '/images/20/ic_more.png')), CallsUtils::GetCallslogMainGridColumns())));

$this->renderPartial('/layouts/agent-form', array('model' => Agents::model()));
$this->renderPartial('/layouts/phone-form', array('model' => PhoneBook::model()));