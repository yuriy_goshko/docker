<div class="row-fluid fhead">
	<div class="fhead-content">
		<div class="span10 <?= $header['class'] ?>">
			<h1>
				<img src="/images/design/<?= $header['image']?>.png" /><?= $header['title']?>
			</h1>
		</div>
		<div class="buttons" style="float: right">            
			<?php
			if (yii::app()->user->getState('rCSVExport'))
				echo CHtml::button(Yii::t('m', 'Export CSV'), array('id' => 'pexport-button','class' => 'btn export-btn','style' => "float:right;"));
			echo CHtml::tag('a', array(
					'class' => "btn btn-primary",
					'target' => "_blank",
					'href' => Yii::app()->params['wikilink'] . $header['wikipage'],
					'style' => "float:right;margin-right: 10px;"), CHtml::Tag('div', array('class' => "wikihelp"), null) . Yii::t('m', 'Wiki Help'));
			?>    	
		</div>
	</div>
</div>