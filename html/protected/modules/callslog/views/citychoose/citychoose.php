<?php
$this->renderPartial('/layouts/header_page', array('header' => $header));
$this->finalCss = Yii::app()->assetManager->publish(implode('/', array(Yii::app()->basePath,'..','css'))) . '/' . 'summary_grid.css';
Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(YiiBase::getPathOfAlias('CallslogPath'),'js'))) . '/' . 'citychoose.js', CClientScript::POS_END);

$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => true,
		'fade' => true,
		'closeText' => '&times;',
		'alerts' => array('success' => array('block' => true,'fade' => true,'closeText' => '&times;'))));

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id' => 'frmCitychoose',
		'enableClientValidation' => true,
		'enableAjaxValidation' => true,
		'clientOptions' => array('validateOnSubmit' => true),
		'method' => 'post'));

?>
<div id="bottombar" class="row-fluid filter active">
	<div class="filter-content">
		<h3 class="toggled-title active">
			</span><?= 'Изменить город' ?></h3>
		<div class="form toggled-content">
			<div class="row-fluid line"></div>
        <? if (Yii::app()->user->hasFlash('error')): ?>
            <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
        <? endif; ?>
			<div class="row-fluid">
				<div class="span3 controls">
				<?php echo $form->errorSummary($model); ?>
        		<?php
										echo $form->labelEx($model, 'number');
										echo '<input readonly="readonly" class="span7" name="frmCitychoose[number_ed]" id="CityChooseEd_number_ed" type="text">';
										?>
                </div>
				<div class="span3 controls">
				<?php
				echo $form->labelEx($model, 'CityName');
				echo $model->CitysDropDownList();				
				//echo '<input class="span7" name="frmCitychoose[city_ed]" id="CityChooseEd_city_ed" type="text">';
				?>		
        		</div>
				<div class="span3 controls">
                <?php
																
																$this->widget('bootstrap.widgets.TbButton', array(
																		'buttonType' => 'submit',
																		'type' => 'primary',
																		'htmlOptions' => array('id' => 'edit','class' => 'hide'),
																		'label' => 'Добавить'));
																?>
                <?php
																$this->widget('bootstrap.widgets.TbButton', array(
																		'buttonType' => 'reset',
																		'label' => 'Сброс',
																		'htmlOptions' => array('id' => 'reset')));
																?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row-fluid main-content">

<?php $this->endWidget(); ?>
<?php

$this->widget('bootstrap.widgets.TbGridView', array(
		'id' => 'grdCityChoose',
		'dataProvider' => $dataProvider,
		'filter' => $model,
		
		'ajaxUpdate' => false,
		'enableSorting' => true,
		'template' => '{items}<div class="gwfooter">{pager}{summary}</div>',
		'columns' => $model->GetDefaultGridColumns()));

?>
</div>
