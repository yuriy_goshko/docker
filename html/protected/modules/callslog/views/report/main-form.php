<div class="row-fluid fhead">
	<div class="fhead-content">
		<div class="span10">
			<h1>
				<img src="/images/design/blue-reports.png" /><?= $h1 ?></h1>
		</div>
		<div class="buttons" style="float: right">             
			<?php
			if (yii::app()->user->getState('rCSVExport') && $needCSVExport)
				echo CHtml::button(Yii::t('m', 'Export CSV'), array('id' => 'pexport-button','class' => 'btn export-btn','style' => "float:right;"));
			echo CHtml::tag('a', array(
					'class' => "btn btn-primary",
					'target' => "_blank",
					'href' => Yii::app()->params['wikilink'] . 'report_' . $reportPage,
					'style' => "float:right;margin-right: 10px;"), CHtml::Tag('div', array('class' => "wikihelp"), null) . Yii::t('m', 'Wiki Help'));
			?>  					
		</div>
	</div>
</div>

<div id="bottombar"
	class="row-fluid filter <? if (isset($_REQUEST[get_class($SearchForm)])) : ?>active<? endif; ?>">
	<div class="filter-content">
        <?=$this->renderPartial('/layouts/filters-form', array('SearchForm' => $SearchForm,'ActionPage' => '/callslog/report/' . $reportPage));?>
    </div>
</div>

<? if (!$modelReport->isEmpty() || $renderEmptyModel): ?>
  <?$this->renderPartial('reports/' . $reportPage, array('modelReport' => $modelReport,'dataProvider' => $dataProvider));?>
<?else:?>	
<?echo CHtml::tag('span', array('class' => "empty"), Yii::t('m', 'No results.'));?>
<?endif;?>
