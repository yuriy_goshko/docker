<?php
$dcs = $modelReport->Agents_getDirectionKeys();
foreach ( $dcs as $DirKey ) {
	echo CHtml::openTag('div', array('class' => 'row-fluid main-content'));
	echo CHtml::tag('div', array('class' => 'subreptitle'), $modelReport->GetDirectionCaption($DirKey, true));
	if (key_exists($DirKey, $dataProvider) && count($dataProvider[$DirKey]->rawData) > 0)
		$this->widget('TbGridViewExtTitle', array(
				'template' => '{items}<div class="gwfooter">{pager}</div>',
				'dataProvider' => $dataProvider[$DirKey],
				'columns' => $modelReport->Agents_getGridColumns($DirKey)));
	else
		echo CHtml::tag('span', array('class' => "empty"), Yii::t('m', 'No results.'));
	echo CHtml::closeTag('div');
}
	
	
