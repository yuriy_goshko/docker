<div class="row-fluid highcharts">
	<div class="span7">
     <?
					echo $modelReport->Byhours_GenerateLinksContent();
					$dtStr = $modelReport->sfCallDateTime;
					$dt = strtotime($dtStr);
					$dtStr = strftime('%A, %d %B %Y', $dt);
					$dtStr = ucfirst(strtolower($dtStr));
					//$dtStr = iconv('windows-1251', 'UTF-8', $dtStr);
					$this->Widget('application.extensions.highcharts.HighchartsWidget', array(
							'skin' => 'filtered',
							'options' => array(
									'title' => array('text' => Yii::t('m', 'Call status')),
									'subtitle' => array('text' => $dtStr,'align' => 'left'),
									'chart' => array('type' => 'column'),
									'xAxis' => array(
											'title' => array(
													'text' => Yii::t('m', 'Total') . ": " . $modelReport->allData['allCount'],
													'align' => 'left'),
											'categories' => $modelReport->Byhours_getExistingHours()),
									'yAxis' => array('min' => 0,'title' => array('text' => Yii::t('m', 'Quantity'))),
									'plotOptions' => array(
											'series' => array(
													'cursor' => 'pointer',
													'point' => array('events' => array('click' => $modelReport->GenerateJS_OnColumnClick())),
													'stacking' => 'normal')),
									'series' => $modelReport->Byhours_getColumnSeries())));
					?>
    </div>
	<div class="span4">
      <?
						$this->Widget('application.extensions.highcharts.HighchartsWidget', array(
								'options' => array(
										'title' => array('text' => Yii::t('m', 'The ratio of the call status,%')),
										'chart' => array('type' => 'pie'),
										'tooltip' => array('valueSuffix' => '%'),
										// 'plotOptions' => array('pie' => array('allowPointSelect' => true,'cursor' => 'pointer')),
										'series' => $modelReport->Byhours_getPieSeries())));
						?>										
     </div>
</div>