<div class="row-fluid fhead">
	<div class="fhead-content">
		<div class="span10 ">
			<h1>
				<img src="/images/design/blue-reports.png" /><?= $h1 ?></h1>
		</div>
        <? if (yii::app()->user->getState('rCSVExport')): ?>
            <div class="span2 buttons">
                <?php echo CHtml::button(Yii::t('m', 'Export CSV'), array('id' => 'pexport-button', 'class' => 'btn export-btn', 'style' => "float:right")); ?>                	  
            </div>
        <? endif; ?>
    </div>

</div>

<div class="filter nextline"></div>
<div style="height: 10px"></div>
<div class="row-fluid">
	<div class="row-fluid fhead-content">
		
		<?php
		echo CHtml::tag('div', array('class' => 'labelname'), Yii::t('reports', 'pbxNum') . ': ');
		echo CHtml::tag('div', array('class' => 'labevalue'), $rTitles['pbxNum']);
		?>			

		<?php
		echo CHtml::tag('div', array('class' => 'labelname'), ';  ');
		echo CHtml::tag('div', array('class' => 'labelname'), Yii::t('reports', 'Status') . ': ');
		echo CHtml::tag('div', array('class' => 'labevalue'), $rTitles['actionName']);
		?>		

	</div>
</div>
<div style="height: 7px"></div>



<div class="row-fluid main-content">
<?php
echo PHP_EOL;
$this->renderPartial('/layouts/callsgrid_main', array('dataProvider' => $dataProvider,'model' => $model));
?>  
</div>
