
<div class="row-fluid main-content">
        <?
								$this->widget('TbGridViewExtTitle', array(
										'template' => '{items}<div class="gwfooter">{pager}</div>',
										'dataProvider' => $dataProvider,
										'columns' => $modelReport->General_getGridColumns()));
								?>
    </div>

<div class="row-fluid highcharts">
	<div class="span5">
            <?
												$this->Widget('application.extensions.highcharts.HighchartsWidget', array(
														'options' => array(
																'title' => array('text' => Yii::t('m', 'The ratio of call types,%')),
																'chart' => array('type' => 'pie'),
																'tooltip' => array('valueSuffix' => '%'),
																'series' => $modelReport->General_getPieSeries())));
												?>										
        </div>
	<div class="span7">
            <?
												$this->Widget('application.extensions.highcharts.HighchartsWidget', array(
														'skin' => 'filtered',
														'options' => array(
																'title' => array('text' => Yii::t('m', 'Call status')),
																'chart' => array('type' => 'column'),
																'xAxis' => array(
																		'categories' => $modelReport->General_getExistingDirectionCategores()),
																'yAxis' => array('min' => 0,'title' => array('text' => Yii::t('m', 'Quantity'))),
																'plotOptions' => array('series' => array('stacking' => 'normal')),
																'series' => $modelReport->General_getColumnSeries())));
												
												?>
        </div>
</div>