
<div class="row-fluid main-content">
        <?
								$this->widget('TbGridViewExtTitle', array(
										'template' => '{items}<div class="gwfooter">{pager}</div>',
										'dataProvider' => $dataProvider,
										'columns' => $modelReport->Citychoose_getGridColumns()));
								
								?>
    </div>

<div class="row-fluid highcharts">
	<div class="span5">
            <?
												$this->Widget('application.extensions.highcharts.HighchartsWidget', array(
														'options' => array(
																'title' => array('text' => 'Соотношение между городами, %'),
																'chart' => array('type' => 'pie'),
																'tooltip' => array('valueSuffix' => '%'),
																'series' => $modelReport->Citychoose_getPieSeries())));
																
												?>

        </div>
	<div class="span7">
            <?
            /*
												$this->Widget('application.extensions.highcharts.HighchartsWidget', array(
														'skin' => 'filtered',
														'options' => array(
																'title' => array('text' => Yii::t('m', 'Types of calls')),
																'chart' => array('type' => 'column'),
																'xAxis' => array('categories' => $modelReport->Departments_getExistingDepartments()),
																'yAxis' => array('min' => 0,'title' => array('text' => Yii::t('m', 'Quantity'))),
																'plotOptions' => array('series' => array('stacking' => 'normal')),
																'series' => $modelReport->Departments_getColumnSeries())));
																*/												
												?>
        </div>
</div>
