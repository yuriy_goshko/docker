<div class="row-fluid main-content">
        <?
								$this->widget('TbGridViewExtTitle', array(
										'template' => '{items}<div class="gwfooter">{pager}</div>',
										'dataProvider' => $dataProvider,
										'columns' => $modelReport->Chanout_getGridColumns()));
								?>
</div>

<div class="row-fluid highcharts">
	<div class="span6">
            <?
												$this->Widget('application.extensions.highcharts.HighchartsWidget', array(
														'skin' => 'filtered',
														'options' => array(
																'title' => array('text' => Yii::t('m', 'Types of calls')),
																'chart' => array('type' => 'column'),
																'xAxis' => array('categories' => $modelReport->Chanout_getExistingChans(true)),
																'yAxis' => array('min' => 0,'title' => array('text' => Yii::t('m', 'Quantity'))),
																'plotOptions' => array('series' => array('stacking' => 'normal')),
																'series' => $modelReport->Chanout_getColumnSeriesDirection())));
												?>
        </div>
	<div class="span6">
            <?
												$this->Widget('application.extensions.highcharts.HighchartsWidget', array(
														'skin' => 'filtered',
														'options' => array(
																'title' => array('text' => Yii::t('m', 'Call status')),
																'chart' => array('type' => 'column'),
																'xAxis' => array('categories' => $modelReport->Chanout_getExistingChans(true)),
																'yAxis' => array('min' => 0,'title' => array('text' => Yii::t('m', 'Quantity'))),
																'plotOptions' => array('series' => array('stacking' => 'normal')),
																'series' => $modelReport->Chanout_getColumnSeriesStates())));
												?>
        </div>        
</div>
