<?php
$this->finalCss = Yii::app()->assetManager->publish(implode('/', array(Yii::app()->basePath,'..','css'))) . '/' . 'summary_grid.css';
?>
<div class="row-fluid main-content">
        <?
								$this->widget('TbGridViewExtTitle', array(
										'template' => '{items}<div class="gwfooter">{pager}</div>',
										'dataProvider' => $dataProvider,
										'template' => '{items}<div class="gwfooter">{pager}{summary}</div>',
										'summaryText' => Yii::t('m', 'Calls') . ' {start}-{end} ' . Yii::t('m', 'of') . ' {count}',
										'columns' => $modelReport->MissedCalls_getGridColumns()));
								?>
</div>
<?php
$this->renderPartial('/layouts/phone-form', array('model' => PhoneBook::model()));