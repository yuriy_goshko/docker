<?php
class CallslogModule extends CWebModule {
	public $defaultController = 'callslog';
	public function init() {
		$this->setImport(array('callslog.models.*','callslog.components.*'));
		
		Yii::setPathOfAlias('CallslogPath', implode('/', array(YiiBase::getPathOfAlias('webroot'),'protected','modules','callslog')));
		
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
				YiiBase::getPathOfAlias('CallslogPath'),
				'js'))) . '/' . 'filtersmodule.js', CClientScript::POS_END);
		
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
				YiiBase::getPathOfAlias('CallslogPath'),
				'js'))) . '/' . 'grids.js', CClientScript::POS_END);
		
		$cssPath = Yii::app()->assetManager->publish(implode('/', array(Yii::app()->basePath,'..','css'))) . '/';
		Yii::app()->clientScript->registerCssFile($cssPath . 'grids.css');
		Yii::app()->clientScript->registerCssFile($cssPath . 'styles.css');
		Yii::app()->gettext->setLocale();
	}
	public function beforeControllerAction($controller, $action) {
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else
			return false;
	}
}
