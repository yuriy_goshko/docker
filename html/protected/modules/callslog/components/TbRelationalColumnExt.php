<?php
/**
 * Доработанный напильником bootstrap.widgets.TbRelationalColumn из набора YiiBooster 2.0.0
 */
/**
 *## TbRelationalColumn class file
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * @copyright Copyright &copy; Clevertech 2012-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

/**
 *## TbRelationalColumn class
 *
 * Displays a clickable column that will make an ajax request and display its resulting data
 * into a new row.
 *
 * @package booster.widgets.grids.columns
 */
class TbRelationalColumnExt extends TbDataColumn {
	public $StyleTableRow = '';
	public $OnClick = false;
	public $UseIcons = false;
	public $ShowValue = false;	
	public $imgUp;
	public $imgDown;
	
	/**
	 * @var string $url the route to call via AJAX to get the data from
	 */
	public $url;
	
	/**
	 * @var string $cssClass the class name that will wrap up the cell content.
	 * Important Note: this class will be used as the trigger for the AJAX call, so make sure is unique for the
	 * column.
	 */
	public $cssClass = 'tbrelational-column';
	
	/**
	 * @var bool $cacheData if set to true, there won't be more than one AJAX request. If set to false, the widget will
	 * continuously make AJAX requests. This is useful if the data could vary. If the data doesn't change then is better
	 * to set it to true. Defaults to true.
	 */
	public $cacheData = true;
	
	/**
	 * @var string a javascript function that will be invoked if an AJAX call occurs.
	 *
	 * The function signature is <code>function(tr, rowid, data)</code>
	 * <ul>
	 * <li><code>tr</code> is the newly created TR HTML object that will display the returned server data.</li>
	 * <li><code>rowid</code> the model id of the row.</li>
	 * <li><code>data</code> is the data returned by the server that is already displayed on the row.</li>
	 * </ul>
	 * Note: This handler is not called for JSONP requests.
	 *
	 * Example (add in a call to TbRelationalColumn):
	 * <pre>
	 *  ...
	 *  'afterAjaxUpdate'=>'js:function(tr,rowid, data){ console.log(rowid); }',
	 *  ...
	 * </pre>
	 */
	public $afterAjaxUpdate;
	
	/**
	 * @var string $ajaxErrorMessage the message that is displayed on the newly created row in case there is an AJAX
	 * error.
	 */
	public $ajaxErrorMessage = 'Error';
	
	/**
	 * widget initialization
	 */
	public function init() {
		parent::init();
		
		if (empty($this->url))
			$this->url = Yii::app()->getRequest()->requestUri;
		
		$this->registerClientScript();
	}
	
	/**
	 * Overrides CDataColumn renderDataCell in order to wrap up its content with the object that will be used as a
	 * trigger.
	 * Important: Making use of links as a content for this of column is an error.
	 *
	 * @param int $row
	 */
	public function renderDataCell($row) {
		$data = $this->grid->dataProvider->data[$row];
		$options = $this->htmlOptions;
		
		if ($this->cssClassExpression !== null) {
			$class = $this->evaluateExpression($this->cssClassExpression, array('row' => $row,'data' => $data));
			if (isset($options['class'])) {
				$options['class'] .= ' ' . $class;
			} else {
				$options['class'] = $class;
			}
		}
		
		if ($this->value !== null)
			$value = $this->evaluateExpression($this->value, array('data' => $data,'row' => $row));
		elseif ($this->name !== null)
			$value = CHtml::value($data, $this->name);
		if ($value != null) {
			$options['id'] = 'rtr'.$this->getPrimaryKey($data);
			if (isset($options['class'])) {
				$options['class'] .= ' rtrdown';
			} else {
				$options['class'] = 'rtrdown';
			}
		}
		
		echo CHtml::openTag('td', $options);
		if ($value != null) {
			echo CHtml::openTag('span', array('class' => $this->cssClass,'data-rowid' => $this->getPrimaryKey($data)));
		
			if ($this->UseIcons == false) {
				if ($this->ShowValue)
			 	 	$this->renderDataCellContent($row, $data);
			} else {			
					echo CHtml::tag('img', array('src' => $this->imgDown,'id' => 'relatedimg' . $this->getPrimaryKey($data)));			
			}			
			echo CHtml::closeTag('span');
		}
		echo CHtml::closeTag('td');
	}
	
	/**
	 * Helper function to return the primary key of the $data
	 *  * IMPORTANT: composite keys on CActiveDataProviders will return the keys joined by two dashes: `--`
	 *
	 * @param CActiveRecord $data
	 *
	 * @return null|string
	 */
	protected function getPrimaryKey($data) {
		if ($this->grid->dataProvider instanceof CActiveDataProvider) {
			$key = $this->grid->dataProvider->keyAttribute === null ? $data->getPrimaryKey() : $data->{$this->grid->dataProvider->keyAttribute};
			return is_array($key) ? implode('--', $key) : $key;
		}
		
		if ($this->grid->dataProvider instanceof CArrayDataProvider || $this->grid->dataProvider instanceof CSqlDataProvider) {
			return is_object($data) ? $data->{$this->grid->dataProvider->keyField} : $data[$this->grid->dataProvider->keyField];
		}
		
		return null;
	}
	
	/**
	 * Register script that will handle its behavior
	 */
	public function registerClientScript() {
		Yii::app()->bootstrap->registerAssetCss('bootstrap-relational.css');
		
		/** @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		if ($this->afterAjaxUpdate !== null) {
			if ((! $this->afterAjaxUpdate instanceof CJavaScriptExpression) && (strpos($this->afterAjaxUpdate, 'js:') !== 0)) {
				$this->afterAjaxUpdate = new CJavaScriptExpression($this->afterAjaxUpdate);
			}
		} else {
			$this->afterAjaxUpdate = 'js:$.noop';
		}
		
		$this->ajaxErrorMessage = CHtml::encode($this->ajaxErrorMessage);
		$afterAjaxUpdate = CJavaScript::encode($this->afterAjaxUpdate);
		$span = count($this->grid->columns);
		$loadingPic = CHtml::image(Yii::app()->bootstrap->getAssetsUrl() . '/img/loading.gif');
		$cache = $this->cacheData ? 'true' : 'false';
		$UseIcons = $this->UseIcons ? 'true' : 'false';
		$data = ! empty($this->submitData) && is_array($this->submitData) ? $this->submitData : 'js:{}';
		$data = CJavascript::encode($data);
		list ($parentId) = explode('_', $this->id);
		$js = <<<EOD
$(document).on('click','#{$parentId} .{$this->cssClass}', function(){
	var span = $span;
	var that = $(this);
	var status = that.data('status');
	var rowid = that.data('rowid');
	var tr = $('#relatedinfo'+rowid);
	var parent = that.parents('tr').eq(0);
	var afterAjaxUpdate = {$afterAjaxUpdate};		

	function CnangeState(isDown) {
		if (isDown==true) {
			$('#rtr'+rowid).removeClass('rtrdown');
			$('#rtr'+rowid).addClass('rtrup');
			if ({$UseIcons}) document.getElementById('relatedimg'+rowid).src = "{$this->imgUp}";
			}
		else {
			$('#rtr'+rowid).removeClass('rtrup');
			$('#rtr'+rowid).addClass('rtrdown');
			if ({$UseIcons}) document.getElementById('relatedimg'+rowid).src = "{$this->imgDown}";
			}		
	}
	
	if (status && status=='on'){return}
	that.data('status','on');

	if (tr.length && !tr.is(':visible') && {$cache})
	{
		tr.slideDown();
		that.data('status','off');									
		CnangeState(true);
		return;
	}else if (tr.length && tr.is(':visible'))
	{	
		tr.slideUp();
		that.data('status','off');		
		CnangeState(false);
		return;
	}
	if (tr.length)
	{	
		tr.find('td').html('{$loadingPic}');
		if (!tr.is(':visible')){
			tr.slideDown();			
			CnangeState(true);
		}
	}
	else
	{								
		var td = $('<td/>').html('{$loadingPic}').attr({'colspan':$span, 'style':{$this->StyleTableRow}});
		tr = $('<tr/>').prop({'id':'relatedinfo'+rowid}).append(td);				 		
			
		oncl = {$this->OnClick} ;
		if (oncl != false) tr.attr({'onclick':oncl});		
		
		/* we need to maintain zebra styles :) */
		var fake = $('<tr class="hide"/>').append($('<td/>').attr({'colspan':$span}));
		parent.after(tr);
		tr.after(fake);		
		CnangeState(true);
	}
	var data = $.extend({$data}, {id:rowid});
	$.ajax({
		url: '{$this->url}',
		data: data,
		success: function(data){
			tr.find('td').html(data);
			that.data('status','off');
			if ($.isFunction(afterAjaxUpdate))
			{
				afterAjaxUpdate(tr,rowid,data);
			}
		},
		error: function()
		{
			tr.find('td').html('{$this->ajaxErrorMessage}');
			that.data('status','off');
		}
	});
});
EOD;
		$cs->registerScript(__CLASS__ . '#' . $this->id, $js);
	}
}
