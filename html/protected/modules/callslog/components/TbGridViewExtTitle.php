<?php
Yii::import('bootstrap.widgets.TbGridView');
class TbGridViewExtTitle extends TbGridView {
	public function renderTableHeader() {
		ob_start();
		$this->SetGroupHeaders();
		$cntGrH = ob_get_contents();
		ob_clean();
		parent::renderTableHeader();
		$cntH = ob_get_contents();
		ob_end_clean();
		echo $this->RegroupTitle($cntGrH, $cntH);
	}
	public function RegroupTitle($cntGrH, $cntH) {
		// load
		$domGrH = new domDocument();
		$domGrH->loadXML($cntGrH);
		$domH = new domDocument();
		$domH->loadXML($cntH);
		// merge
		$els = $domH->getElementsByTagName('tr')->item(0);
		$els = $domGrH->importNode($els, true);
		$domGrH->documentElement->appendChild($els);
		// grup replace tags
		$tr1 = $domGrH->getElementsByTagName('tr')->item(0);
		$tr2 = $domGrH->getElementsByTagName('tr')->item(1);
		
		$tr1_th_list = $tr1->getElementsByTagName('th');
		$tr2_th_list = $tr2->getElementsByTagName('th');
		
		$size = $tr1_th_list->length;
		$i2 = 0;
		$i = 0;
		while ( $i < $size ) {
			$Tag1 = $tr1_th_list->item($i);
			$Tag2 = $tr2_th_list->item($i2);
			$step = ($Tag1->hasAttribute('colspan')) ? $Tag1->getAttribute('colspan') : 1;
			if ($Tag1->nodeValue === '') {
				// replace
				$CloneTag = $Tag2->cloneNode(true);
				$rs_attr = $domGrH->createAttribute('rowspan');
				$rs_attr->value = 2;
				$CloneTag->appendChild($rs_attr);
				$tr1->replaceChild($CloneTag, $Tag1);
				$tr2->removeChild($Tag2);
				$i2 --;
			}
			$i2 = $i2 + $step;
			$i ++;
		}
		return $domGrH->saveXML();
	}
	public function GetColumnCaption($colum) {
		if ($colum->name !== null && $colum->header === null) {
			if ($colum->grid->dataProvider instanceof CActiveDataProvider) {
				return CHtml::encode($colum->grid->dataProvider->model->getAttributeLabel($colum->name));
			} else {
				return CHtml::encode($colum->name);
			}
		} else {
			return trim($colum->header) !== '' ? $colum->header : $colum->grid->blankDisplay;
		}
	}
	public function SetGroupHeaders() {
		$addColumn = function ($colspan, $gTextLast) {
			$options = array('style' => 'text-align:center;font-weight:normal;');
			$options = array();
			$options = array_merge($options, array('colspan' => $colspan));
			echo CHtml::openTag('th', $options);
			echo $gTextLast;
			echo CHtml::closeTag('th');
		};
		echo CHtml::openTag('thead') . "\n";
		echo CHtml::openTag('tr') . "\n";
		$gTextLast = '';
		$colspan = - 1;
		$i = 0;
		foreach ( $this->columns as $colum ) {
			$colspan ++;
			$i ++;
			$cc = $this->GetColumnCaption($colum);
			$pieces = explode("|", $cc);
			$gText = $pieces[0];
			$cText = '';
			if (count($pieces) > 1)
				$cText = $pieces[1];
			if ($cText === '') {
				$cText = $gText;
				$gText = '';
			}
			$colum->header = $cText;
			if (($gTextLast != $gText && $i > 1) || ($gTextLast == '' && $i > 1)) {
				$addColumn($colspan, $gTextLast);
				$colspan = 0;
			}
			$gTextLast = $gText;
			if ($i == count($this->columns)) {
				$colspan ++;
				$addColumn($colspan, $gTextLast);
			}			
		}		
		echo CHtml::closeTag('tr') . "\n";
		echo CHtml::closeTag('thead') . "\n";
	}
}
?>