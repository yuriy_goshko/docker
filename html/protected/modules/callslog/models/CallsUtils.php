<?php
class CallsUtils {
	public static function isSipNum($Num) {
		return ($Num >= 200) && ($Num <= 999);
	}
	public static function user_browser() {
		$agent = $_SERVER['HTTP_USER_AGENT'];
		preg_match("/(MSIE|Opera|Firefox|Chrome|Version)(?:\/| )([0-9.]+)/", $agent, $browser_info);
		list (, $browser, $version) = $browser_info;
		return $browser;
		
		/*	
		 if ($browser == 'Opera' && $version == '9.80')
		 return 'Opera ' . substr($agent, - 5);
		 if ($browser == 'Version')
		 return 'Safari ' . $version;
		 if (! $browser && strpos($agent, 'Gecko'))
		 return 'Browser based on Gecko';
		 return $browser . ' ' . $version;
		 */
	}
	protected static function GenereteSIPFullNameContent($Num, $Name, $exName) {
		if ($Name === '') {
			return $Num;
		} else if (($exName != null) && yii::app()->user->getState('rEditUsers')) {
			$content = CHtml::link($Name, '/callslog/ajax/modalagent/num/' . $Num, array(
					'class' => "modalpop",
					'data-toggle' => "tooltip",
					'data-placement' => "bottom",
					'title' => Yii::t('tooltips', "Update agent's data.")));
			return $Num . ' (' . $content . ')';
		} else
			return $Num . ' (' . $Name . ')';
	}
	protected static function GeneretePhoneFullNameContent($Num, $exName, $btnAdd) {
		if ($exName === null) {
			if ((in_array($Num, array('-','*','s'))) || $btnAdd === false)
				return $Num;
			else
				$content = CHtml::link('', '/callslog/ajax/modalphone/num/' . $Num, array(
						'class' => "modalpop addphoneimg",
						'data-toggle' => "tooltip",
						'data-placement' => "bottom",
						'title' => Yii::t('tooltips', 'Add customer details to the phone book.')));
			$Num = '<div style="float:left;">' . $Num . '</div>';
			return $Num . $content;
		} else {
			$content = CHtml::link($exName, '/callslog/ajax/modalphone/num/' . $Num, array(
					'class' => "modalpop",
					'data-toggle' => "tooltip",
					'data-placement' => "bottom",
					'title' => Yii::t('tooltips', 'Update customer details in the phone book.')));
			return $Num . ' "' . $content . '"';
		}
	}
	public static function GenereteFullNameContent($Num, $Name, $exName, $btnAdd = true) {
		if (CallsUtils::isSipNum($Num))
			return CallsUtils::GenereteSIPFullNameContent($Num, $Name, $exName);
		else
			return CallsUtils::GeneretePhoneFullNameContent($Num, $exName, $btnAdd);
	}
	public static function GenerateBridgeDurationContent($record, $onlyfilename = False) {
		$text = $onlyfilename ? '' : sec2hms($record->BridgeDuration, true);
		
		if ((! yii::app()->user->getState('rListenAudio')) && (! yii::app()->user->getState('rListenAudioTransfer'))) 
			return $onlyfilename?'rListenAudioAbsent':$text;
		
		if ((! yii::app()->user->getState('rListenAudioTransfer')) && ((! empty($record->TransferFrom) || ! empty($record->TransferTo))))
			return $onlyfilename?'rListenAudioAbsent':$text;
		
		$agNum = yii::app()->user->getState(WebUser::$KEY_AGENT, 0);
		if ($agNum > 0) {
			if ($record->IsAgAnsw != 1)
				return $text;
		}
		
		if ($record->ConfUnId == '')
			$AudioRecordName = $record->audioFile;
		else
			$AudioRecordName = 'confbridge-' . $record->ConfNum . '-' . substr($record->ConfUnId, 0, strpos($record->ConfUnId, '.'));
		
		$f = $AudioRecordName;
		
		$FindAudioArchOnly = (Yii::app()->params['FindAudioArchOnly'] == 1);
		if ($FindAudioArchOnly == True) {
			$REAL_PATH = REAL_SNDARCH_PATH;
			$LINK_PATH = '/sndsarch/';
		} else {
			$f = date("Y/m/d/", strtotime($record->CallDateTime)) . $f;
			$REAL_PATH = REAL_SNDARCH_PATH;
			$LINK_PATH = '/sndsarch/';
		}
		$fn = $REAL_PATH . $f;
		$fnPlay = $LINK_PATH . $f;
		
		$ext_mp3 = '.mp3';
		$ext_wav = '.wav';
		$FindWavAlso = false; // установить в true
		
		if (file_exists($fn . $ext_mp3)) {
			$fn .= $ext_mp3;
			$fnPlay .= $ext_mp3;
		} else if ($FindWavAlso && (file_exists($fn . $ext_wav))) {
			$fn .= $ext_wav;
			$fnPlay .= $ext_wav;
		} else
			return $text;
		
		if ($onlyfilename)
			return $fn;
		
		$controller = Yii::app()->controller;
		return CHtml::link($text, '', array(
				'class' => 'link-popover gridplayer',
				'data-html' => true,
				'data-title' => Yii::t('m', 'Listen to the recording'),
				'data-placement' => 'top',
				'data-content' => file_exists($fn) ? $controller->widget('Jouele', array(
						'file' => $fnPlay,
						'htmlOptions' => array('style' => 'min-width: 206px')), true) : Yii::t('m', 'Record is not exists'),
				'data-toggle' => 'popover'));
	}
	public static function GenereteStateNameContent($record) {
		$text = $record->StateName;
		if ($record->StateId == 'ANSWERED_PART') {
			$arr = Yii::t('callslog', 'hints');
			$text = CHtml::tag('text', array('title' => $arr['ANSWERED_PART']), $text);
		} elseif ($record->CanceledOutgoing == true) {
			$arr = Yii::t('callslog', 'hints');
			$text = CHtml::tag('text', array('title' => $arr['CanceledOutgoing']), $text . ' *');
		}
		return $text;
	}
	public static function GetCallslogMainGridColumns($IsExport = false) {
		if ($IsExport)
			$BridgeDurationValue = 'sec2hms($data->BridgeDuration,true)';
		else
			$BridgeDurationValue = '$data->BridgeDurationContent';
			
			// array('name' => 'CallEndDateTime','value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data->CallDateTime)'),
		$columns = array(
				array('name' => 'FullNameSrc','type' => 'raw','value' => '$data->FullNameSrcContent'),
				array('name' => 'FullNameDst','type' => 'raw','value' => '$data->FullNameDstContent'),
				array('name' => 'NumOut','value' => '$data->FullNameOut'),
				array('name' => 'StateName','type' => 'raw','value' => '$data->StateNameContent'),
				array('name' => 'DirName'),
				array('name' => 'CallDateTime','value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data->CallDateTime)'),
				array('name' => 'Duration','value' => 'sec2hms($data->Duration,true)'),
				array('name' => 'BridgeDuration','type' => 'raw','value' => $BridgeDurationValue),
				array('name' => 'TransferFrom'),
				array('name' => 'TransferTo'));
		if (Yii::app()->params['ShowLinkDeps'] == 1)
			$columns[] = array('name' => 'Deps');
		
		return $columns;
	}
}
