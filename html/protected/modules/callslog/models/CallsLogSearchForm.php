<?php
class CallsLogSearchForm extends CFormModel {
	public $sfCallDateTime;
	public $sfCallEndDateTime;
	public $sfNum;
	public $sfDepartments;
	public $sfStates;
	public $sfDirections;
	public $sfAgents;
	public $sfPBXNum;
	public $sfPagesize;
	public $sfSpecFilter;
	public $sfLinkedId;
	public $sfTimeOutSelLabel;
	public $sfTimeOutSel;
	public $sfTimeOutSelIntType;
	public $sfTimeOutSel2Label;
	public $sfTimeOutSel2;
	public $sfTimeOutSel2IntType;
	public $sfQueues;
	public $sfAllTime;
	public $searchFilters = array();
	public $filterDirections = array();
	public $ajaxSearch = false;
	// ToDo: перенести все проперти (dateMode, filters, fileroute и пр.) в одну струтуру для передачи и инициализации виджета
	public $dateMode = false;
	public function rules() {
		return array(
				array('sfAllTime, sfCallDateTime, sfCallEndDateTime, sfNum, sfDepartments, sfStates','safe'),
				array('sfDirections, sfAgents, sfPBXNum, sfPagesize, sfSpecFilter, sfLinkedId','safe'),
				array('sfTimeOutSel, sfTimeOutSelIntType, sfTimeOutSel2, sfTimeOutSel2IntType, sfQueues, dateMode','safe'),
				array('ajaxSearch','safe'));
	}
	public function attributeLabels() {
		return array(
				'sfAllTime' => 'За все время',
				'sfTimeOutSel' => $this->sfTimeOutSelLabel,
				'sfTimeOutSel2' => $this->sfTimeOutSel2Label,
				'sfQueues' => 'Очереди: ',
				'sfNum' => mb_ucfirst(Yii::t('callinfo', 'number')) . ':',
				'sfDepartments' => mb_ucfirst(Yii::t('callinfo', 'departments')) . ':',
				'sfStates' => mb_ucfirst(Yii::t('callinfo', 'status')) . ':',
				'sfDirections' => mb_ucfirst(Yii::t('callinfo', 'call direction')) . ':',
				'sfAgents' => mb_ucfirst(Yii::t('callinfo', 'agent')) . ':',
				'sfPBXNum' => 'PBX Номер',
				'sfSpecFilter' => mb_ucfirst(Yii::t('callinfo', 'spetial filter')) . ':',
				'sfPagesize' => mb_ucfirst(Yii::t('callinfo', 'page size')) . ':');
	}
	public function init() {
		$this->sfCallDateTime = date('Y-m-1');
		$d = strtotime("+1 day");
		$this->sfCallEndDateTime = date('Y-m-d', $d);
		if (isset(Yii::app()->request->cookies['callslog_pagesize']))
			$this->sfPagesize = Yii::app()->request->cookies['callslog_pagesize']->value;
		$this->sfTimeOutSelLabel = 'Время дозвона:';
		$this->sfTimeOutSel = '30';
		$this->sfTimeOutSelIntType = 'MINUTE';
	}
	public function SetToday() {
		$this->sfCallDateTime = date('Y-m-d');
		$this->sfCallEndDateTime = date('Y-m-d', strtotime("+1 day"));
	}
	public static function getSpecFilterList() {
		return Array('MissedCalls' => 'Пропущенные'); // 'OnlyMy' => 'Только мои'
	}
	public static function GenerateHTMLSelectTag($form, $SearchForm, $field, $data) {
		echo $form->labelEx($SearchForm, $field, array('class' => 'span5'));
		yii::app()->controller->widget('ext.bootstrap-select.TbSelect', array(
				'model' => $SearchForm,
				'attribute' => $field,
				'data' => $data,
				'htmlOptions' => array('class' => 'span7 designed-select','multiple' => true)));
		echo $form->error($SearchForm, $field);
	}
	public static function GetListData($field, $SearchForm) {
		switch ($field) {
			case 'sfDepartments' :
				$data = Chtml::listData(DataModule::getDepartmentsList(), 'dID', 'dName');
				break;
			case 'sfStates' :
				$data = Chtml::listData(DataModule::getStatesList(), 'Id', 'Name');
				break;
			case 'sfDirections' :
				$data = Chtml::listData(DataModule::getDirectionsList(), 'Id', 'Name');
				if (! empty($SearchForm->filterDirections))
					$data = array_intersect_key($data, array_flip($SearchForm->filterDirections));
				break;
			case 'sfAgents' :
				$data = Chtml::listData(DataModule::getAgentsList($SearchForm->sfDepartments), 'FN', 'FN');
				break;
			case 'sfPBXNum' :
				$data = Chtml::listData(DataModule::getPBXNumsList(), 'Number', 'DisplayNumber');
				break;
			case 'sfQueues' :
				$data = Chtml::listData(DataModule::getQueuesList(), 'name', 'qName');
				break;
		}
		return $data;
	}
	public static function GenerateHTMLTimeOutTag($form, $SearchForm, $field) {
		$data = array('SECOND' => 'Секунд','MINUTE' => 'Минут','HOUR' => 'Часов','DAY' => 'Дней');
		echo $form->labelEx($SearchForm, $field, array('class' => 'span6'));
		echo $form->textField($SearchForm, $field, array('value' => $SearchForm->$field,'class' => 'span2','style' => 'float:left'));
		echo $form->dropDownList($SearchForm, $field . 'IntType', $data, array('class' => 'span4 designed-select','style' => 'float:right'));
	}
	public static function GenerateHTMLCheckBox($form, $SearchForm, $field) {
		echo $form->CheckBox($SearchForm, $field, false, array('style' => 'float: left'));
		echo $form->labelEx($SearchForm, $field, array('class' => 'span6'));
	}
	public static function GenerateHTMLFilters($form, $SearchForm) {
		$SelectFields = array('sfDepartments','sfStates','sfDirections','sfAgents','sfPBXNum','sfQueues');
		$i = 0;
		foreach ( $SearchForm->searchFilters as $field ) {
			if ($field == 'sfPagesize')
				continue;
			if (($field == 'sfQueues') && (DataModule::GetSettingValue('ShowQueueFilters') != "1"))
				continue;
			
			$i += 1;
			if ($i == 1)
				echo CHtml::openTag('div', array('class' => 'row-fluid'));
			echo CHtml::openTag('div', array('class' => 'span3 controls'));
			if (in_array($field, $SelectFields)) {
				$data = self::GetListData($field, $SearchForm);
				self::GenerateHTMLSelectTag($form, $SearchForm, $field, $data);
			} else if ($field == 'sfNum') {
				echo $form->labelEx($SearchForm, 'sfNum', array('class' => 'span5'));
				echo $form->textField($SearchForm, 'sfNum', array('value' => $SearchForm->sfNum,'class' => 'span7'));
				echo $form->error($SearchForm, 'sfNum');
			} else if (($field == 'sfTimeOutSel') || ($field == 'sfTimeOutSel2')) {
				self::GenerateHTMLTimeOutTag($form, $SearchForm, $field);
			} else if ($field == 'sfAllTime') {
				self::GenerateHTMLCheckBox($form, $SearchForm, $field);
			}
			echo CHtml::closeTag('div');
			if ($i == 3) {
				echo CHtml::closeTag('div');
				$i = 0;
			}
		}
		if ($i > 0)
			echo CHtml::closeTag('div');
	}
	public static function GenerateHTMLTimeFilters($form, $SearchForm) {
		// CJuiDateTimePicker
		$dtVal = Yii::app()->dateFormatter->formatDateTime($SearchForm->sfCallDateTime, 'medium', ($SearchForm->dateMode) ? '' : 'medium');
		if ($SearchForm->dateMode)
			$dtVal .= ','; // костыль, иначе слетает на тек. дату (см. filtersmodule.js: $(this).next().val(getDateTimeFormat(this));)
		$form->widget('CJuiDateTimePicker', array(
				'name' => 'sfCallDateTime',
				'value' => $dtVal,
				'mode' => ($SearchForm->dateMode) ? 'date' : 'datetime',
				'htmlOptions' => array('data-toggle' => "tooltip",'data-placement' => 'top','title' => Yii::t('callinfo', 'start date'))));
		echo $form->hiddenField($SearchForm, 'sfCallDateTime');
		
		if ($SearchForm->dateMode)
			return;
		
		echo ' - ';
		
		$form->widget('CJuiDateTimePicker', array(
				'name' => "sfCallEndDateTime",
				'value' => Yii::app()->dateFormatter->formatDateTime($SearchForm->sfCallEndDateTime),
				'mode' => 'datetime',
				'htmlOptions' => array('data-toggle' => "tooltip",'data-placement' => 'top','title' => Yii::t('callinfo', 'end date'))));
		echo $form->hiddenField($SearchForm, 'sfCallEndDateTime');
	}
}
