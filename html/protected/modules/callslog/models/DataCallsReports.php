<?php
class DataCallsReports extends DAOCharts {
	private static $StatesColors = array(
			'ANSWERED' => '#89A54E',
			'BUSY' => '#80699B',
			'NO_ANSWER' => '#AA4643',
			'FAILED' => '#A47D7C',
			'ANSWERED_PART' => '#B5CA92',
			'UNKNOWN' => '#ceb1aa');
	private static $DirectionsColors = array(
			'Outbound' => '#89A54E',
			'Inbound' => '#4572A7',
			'Local' => '#DB843D',
			'Conference' => '#ab6280',
			'ConfOut' => '#73b49e',
			'AutoCall' => '#B5CA92',
			'Callback' => '#3D96AE',
			'CallbackInit' => '#3D96AE');
	private static $StatesCaptions = array();
	private static $DirectionsCaptions = array();
	private static $DepartmentsColors = array();
	private static $UsesDirections = array();
	private static $UsesStates = array('ANSWERED','NO_ANSWER','BUSY','FAILED');
	private static $ShortDirectionKeys = array('IN' => 'Inbound','OUT' => 'Outbound','LOC' => 'Local','CLB' => 'Callback');
	public static function model($sql = '', $className = __CLASS__) {
		return parent::model($sql, $className);
	}
	public function __construct() {
		parent::__construct();
		if (empty(self::$StatesCaptions)) {
			$sl = DataModule::getStatesList(true);
			foreach ( $sl as $row ) {
				self::$StatesCaptions[$row['Id']] = $row['Name'];
			}
		}
		if (empty(self::$DirectionsCaptions)) {
			$sl = DataModule::getDirectionsList(true);
			foreach ( $sl as $row ) {
				self::$DirectionsCaptions[$row['Id']] = $row['Name'];
			}
		}
		if (empty(self::$UsesDirections)) {
			$sl = DataModule::getDirectionsList(false);
			foreach ( $sl as $row ) {
				self::$UsesDirections[] = $row['Id'];
			}
		}
		if (empty(self::$DepartmentsColors)) {
			$command = Yii::app()->db->createCommand('select dID, dColor from Departments');
			$sl = $command->queryAll();
			foreach ( $sl as $row ) {
				self::$DepartmentsColors[$row['dID']] = $row['dColor'];
			}
		}
	}
	public static function GetDepartmentColor($DepId) {
		if (array_key_exists($DepId, self::$DepartmentsColors))
			return self::$DepartmentsColors[$DepId];
		else
			return 'black';
	}
	public static function GetStateColor($StateId) {
		if (array_key_exists($StateId, self::$StatesColors))
			return self::$StatesColors[$StateId];
		else
			return 'black';
	}
	public static function GetDirectionColor($DirectionId) {
		return self::$DirectionsColors[$DirectionId];
	}
	public static function GetStateCaption($StateId) {
		if (array_key_exists($StateId, self::$StatesCaptions))
			return self::$StatesCaptions[$StateId];
		else
			return $StateId;
	}
	public static function GetDirectionCaption($DirectionId, $isShortKey = false) {
		if ($isShortKey)
			$DirectionId = self::$ShortDirectionKeys[$DirectionId];
		if (array_key_exists($DirectionId, self::$DirectionsCaptions))
			return self::$DirectionsCaptions[$DirectionId];
		else
			return $DirectionId;
	}
	public static function GenerateLinkContent($text, $SearchTmp, $IconFind = true) {
		$text = '<div style="float:left;">' . $text . '</div>';
		$ico = $IconFind ? '/images/16/tablefind.png' : '/images/16/table.png';
		
		$content = CHtml::Tag('a', array(
				'href' => '/callslog?' . http_build_query(array(get_class($SearchTmp) => $SearchTmp)),
				'target' => '_blank',
				'style' => 'float:right'), CHtml::Tag('img', array('src' => $ico)));
		return $text . $content;
	}
	public function GetHeader($FieldName) {
		$arr = Yii::t('callslog', 'attributeLabels');
		return $arr[$FieldName];
	}
	public function fillConditions($attributes, $OnlyPeriod = false, $OnlyDay = false) {
		$this->attributes = $attributes;
		$this->command->reset();
		if ($OnlyDay)
			$this->sfCallEndDateTime = date("Y-m-d", strtotime("+1 day", strtotime($this->sfCallDateTime)));
		$this->command->andWhere('c.CallDateTime >= "' . $this->sfCallDateTime . '"');
		$this->command->andWhere('c.CallDateTime < "' . $this->sfCallEndDateTime . '"');
		$this->command->andWhere(DataModule::DepsCond());
		
		if ($OnlyPeriod)
			return $this;
		
		if ($this->sfStates != false) {
			$items = $this->sfStates;
			$i = 0;
			foreach ( $items as $State ) {
				$items[$i] = '(c.StateId like "' . $State . '%")';
				$i ++;
			}
			$s = implode(' or ', $items);
			$this->command->andWhere($s);
		}
		
		if ($this->sfDirections != false) {
			$items = $this->sfDirections;
			$i = 0;
			foreach ( $items as $Direction ) {
				$items[$i] = '(c.DirectionId like "' . $Direction . '%")';
				$i ++;
			}
			$s = implode(' or ', $items);
			$this->command->andWhere($s);
		}
		
		if ($this->sfPBXNum != false) {
			$s = implode('","', $this->sfPBXNum);
			$this->command->andWhere('(c.Numout in ("' . $s . '")
			or exists (select 1 from CI_CallsDetail cd where c.Id=cd.CallId and cd.Numout in ("' . $s . '")))');
		}
		
		if ($this->sfNum != false) {
			$s = '
			(((NumSrc like "[Number]") or (NumDst like "[Number]") or (NumOut like "[Number]") or (TransferFrom like "[Number]") or (TransferTo like "[Number]")) or
			(exists (select 1 from CI_CallsDetail cd where c.Id=cd.CallId and
			((cd.NumSrc like "[Number]") or (cd.NumDst like "[Number]") or (cd.NumOut like "[Number]") or (cd.TransferFrom like "[Number]") or (cd.TransferTo like "[Number]")))))';
			$s = str_replace('[Number]', '%' . $this->sfNum . '%', $s);
			$this->command->andWhere($s);
		}
		
		if ($this->sfQueues != false) {
			$s = '(exists (select 1 from `CI_CallsInfo` ci where ci.CallId = c.`Id`
				  and ci.`InfoValue` in	("[Queues]") and ci.InfoType in ("ENTERQUEUE", "QUEUE_NAME")))';
			$s = str_replace('[Queues]', implode('","', $this->sfQueues), $s);
			$this->command->andWhere($s);
		}
		
		if ($this->sfDepartments != false) {
			$s = DataModule::DepsCondByArr($this->sfDepartments);
			$this->command->andWhere($s);
		}
		if ($this->sfAgents != false) {
			$s = '(((case when c.NameSrc="" then c.NumSrc else CONCAT(c.NumSrc," (",c.NameSrc,")") end) in ([Agents]))
				  or ((case when c.NameDst="" then c.NumDst else CONCAT(c.NumDst," (",c.NameDst,")") end) in ([Agents])) 
				  or (exists (select 1 from CI_CallsDetail cd where c.Id=cd.CallId
 				  and (((case when cd.NameSrc="" then cd.NumSrc else CONCAT(cd.NumSrc," (",cd.NameSrc,")") end) in ([Agents]))
				  or ((case when cd.NameDst="" then cd.NumDst else CONCAT(cd.NumDst," (",cd.NameDst,")") end) in ([Agents]))))))';
			$agnts = '"' . implode('","', $this->sfAgents) . '"';
			$s = str_replace('[Agents]', $agnts, $s);
			$this->command->andWhere($s);
		}
		return $this;
	}
	public function General_getExistingDirectionCategores($keys = false) {
		if (! $keys)
			return array_values(array_intersect_key(self::$DirectionsCaptions, $this->namedData));
		else
			return array_keys(array_intersect_key(self::$DirectionsCaptions, $this->namedData));
	}
	public function General_fetchAllByDirection() {
		$this->command->select('if(c.DirectionId="CallbackInit","Callback",c.DirectionId) name,				 
								SUM(c.Duration) Duration, sum(c.BridgeDuration) BridgeDuration, 
								SUM(if(c.StateId like "ANSWERED%", 1, 0)) ANSWERED,
								SUM(if(c.StateId = "NO_ANSWER", 1, 0)) NO_ANSWER,
								SUM(if(c.StateId = "BUSY", 1, 0)) BUSY,
								SUM(if(c.StateId = "FAILED", 1, 0)) FAILED,							
								IFNULL(count(1),0) allCount');
		$this->command->from('CI_Calls c');
		$this->command->join('CI_Directions cd', 'cd.Id=c.DirectionId and c.DirectionId<>"UNKNOWN"');
		$this->command->group('if(c.DirectionId="CallbackInit","Callback",c.DirectionId)');
		$this->command->order('cd.OrderKey');
		$this->result = $this->command->queryAll();
		$this->processDataByName($this->result);
		$this->processAllData();
		return $this;
	}
	public function General_getPieSeries() {
		$res = array();
		$dataAll = array();
		$dataGroup = array();
		
		if (count($this->namedData != false) && count($this->allData) != false) {
			foreach ( $this->namedData as $name => $v ) {
				$color = $this->GetDirectionColor($name);
				if ($v['allCount'] != 0)
					$dataAll[] = array(
							'name' => $this->GetDirectionCaption($name),
							'color' => $color,
							'y' => getPercent($v['allCount'], $this->allData['allCount']));
				foreach ( self::$UsesStates as $StateId ) {
					if ($v[$StateId] != 0) {
						$dataGroup[] = array(
								'name' => $this->GetDirectionCaption($name) . '<br>' . $this->GetStateCaption($StateId),
								'color' => $this->GetStateColor($StateId),
								'y' => getPercent($v[$StateId], $v['allCount'], getPercent($v['allCount'], $this->allData['allCount'])));
					}
				}
			}
		}
		$res[] = array(
				'name' => Yii::t('m', 'All calls'),
				'data' => $dataAll,
				'size' => '80%',
				'dataLabels' => array('distance' => 20,'color' => 'black'));
		/* убрано пока не попросят */
		// $res[] = array('name' => ' ','data' => $dataGroup,'innerSize' => '80%','dataLabels' => array(),'allowPointSelect' => true,'cursor' => 'pointer');
		return $res;
	}
	public function General_getColumnSeries() {
		$ExDirs = $this->General_getExistingDirectionCategores(true);
		$res = array();
		foreach ( self::$UsesStates as $StateId ) {
			$row = array();
			$row['name'] = $this->GetStateCaption($StateId);
			$row['color'] = $this->GetStateColor($StateId);
			$data = array();
			foreach ( $ExDirs as $DirectionId ) {
				$data[] = intval($this->namedData[$DirectionId][$StateId]);
			}
			$row['data'] = $data;
			$res[] = $row;
		}
		return $res;
	}
	public static function General_GenerateLinkContent($Column, $data) {
		$text = $data[$Column];
		$directionId = $data['name'];
		$StateId = $Column;
		
		if ($directionId == null)
			return $text;
		
		$SearchTmp = new CallsLogSearchForm();
		if (isset($_REQUEST[get_class($SearchTmp)])) {
			$SearchTmp->attributes = $_REQUEST[get_class($SearchTmp)];
		}
		$SearchTmp->sfStates = array($StateId);
		$SearchTmp->sfDirections = array($directionId);
		return self::GenerateLinkContent($text, $SearchTmp);
	}
	public function General_getGridColumns() {
		return array(
				array('name' => 'TitleName','header' => $this->GetHeader('DirName'),'value' => 'DataCallsReports::GetDirectionCaption($data["name"])'),
				array(
						'name' => 'ANSWERED',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('ANSWERED'),
						'footer' => $this->allData['ANSWERED'],
						'type' => 'raw',
						'value' => 'DataCallsReports::General_GenerateLinkContent("ANSWERED", $data)'),
				
				array(
						'name' => 'NO_ANSWER',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('NO_ANSWER'),
						'footer' => $this->allData['NO_ANSWER'],
						'type' => 'raw',
						'value' => 'DataCallsReports::General_GenerateLinkContent("NO_ANSWER", $data)'),
				array(
						'name' => 'BUSY',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('BUSY'),
						'footer' => $this->allData['BUSY'],
						'type' => 'raw',
						'value' => 'DataCallsReports::General_GenerateLinkContent("BUSY", $data)'),
				array(
						'name' => 'FAILED',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('FAILED'),
						'footer' => $this->allData['FAILED'],
						'type' => 'raw',
						'value' => 'DataCallsReports::General_GenerateLinkContent("FAILED", $data)'),
				array('name' => 'allCount','header' => Yii::t('m', 'Total'),'footer' => $this->allData['allCount']),
				array(
						'name' => 'Duration',
						'header' => $this->GetHeader('Duration'),
						'value' => function ($data) {
							return sec2hms($data['Duration'], true);
						},
						'footer' => sec2hms($this->allData['Duration'], true)),
				array(
						'name' => 'BridgeDuration',
						'header' => $this->GetHeader('BridgeDuration'),
						'value' => function ($data) {
							return sec2hms($data['BridgeDuration'], true);
						},
						'footer' => sec2hms($this->allData['BridgeDuration'], true)));
	}
	public function Departments_getUsesDirections($fullArr = true) {
		$arr = array_intersect_key(array('Outbound','Inbound','Local','Conference','Callback','CallbackInit'), self::$UsesDirections);
		if (in_array('Callback', self::$UsesDirections) && ! in_array('CallbackInit', $arr))
			$arr[] = 'CallbackInit';
		if (! $fullArr) {
			$arr = array_flip($arr);
			unset($arr['CallbackInit']);
			$arr = array_flip($arr);
		}
		return $arr;
	}
	public function Departments_fetchAllByDepartments() {
		$ud = implode($this->Departments_getUsesDirections(), '", "');
		$where = 'c.DirectionId in ("' . $ud . '")';
		$GenDepIds = yii::app()->user->getState('depIds');
		$conditions = $this->command->where;
		if (($this->sfDepartments != false) || ($GenDepIds != '*')) {
			$s = '';
			if ($this->sfDepartments != false) {
				$s = ' and (d.dID in ([DepartsIs])) ';
				$s = str_replace('[DepartsIs]', implode(',', $this->sfDepartments), $s);
			}
			if ($GenDepIds != '*') {
				$s .= ' and (d.dID in (' . $GenDepIds . ')) ';
			}
			$s = ' having 1=1 ' . $s;
			$conditions .= $s;
		}
		// можно убрать union после релиза CLEV-151
		$this->command->text = '
		select name, TitleName, COUNT(1) allCount,
		SUM(if(DirectionId = "Outbound", 1, 0)) Outbound,
		SUM(if(DirectionId = "Inbound", 1, 0)) Inbound,
		SUM(if(DirectionId = "Local", 1, 0)) Local,
		SUM(if(DirectionId = "Conference", 1, 0)) Conference,
		SUM(if(DirectionId like "Callback%", 1, 0)) Callback
		from (
			select distinct c.id, d.dID as name, d.dName as TitleName, c.DirectionId
			from CI_Calls c
			left join CI_CallsInfo ci on ci.CallId = c.Id and ci.InfoType in ("ENTERQUEUE", "QUEUE_NAME") 
			left join CI_CallsDetail cd on c.Id=cd.CallId				
			left join SIP s on ((s.name = c.NumDst) or (s.name = c.NumSrc) or (s.name = cd.NumDst) or (s.name = cd.NumSrc))
			left join DepartmentAgents da on (s.id = da.SIPid)
			left join Departments d on d.dID = da.DepartmentId
			where ' . $where . '
			and ci.Id is null 
			and ' . $conditions . '		

			union 
            
			select distinct c.id, d.dID as name, d.dName as TitleName, c.DirectionId
			from CI_Calls c
            inner join `CI_CallsInfo` ci on ci.CallId = c.`Id` and ci.InfoType in ("ENTERQUEUE", "QUEUE_NAME")
            inner join `DepartmentQueues` dq on ci.`InfoValue` = dq.dqQueues_qName 
            inner join Departments d on d.dID = dq.`dqDepartments_dID`
			where ' . $where . '
			and ' . $conditions . '					
		) as callsDist
		group by 1,2
		order by TitleName';
		
		$this->result = $this->command->queryAll();
		$this->processDataByName($this->result);
		$this->processAllData();
		return $this;
	}
	public static function Departments_GenerateLinkContent($Column, $data) {
		$text = $data[$Column];
		$depId = $data['name'];
		$directionId = $Column;
		
		if ($depId == null)
			return $text;
		
		$SearchTmp = new CallsLogSearchForm();
		if (isset($_REQUEST[get_class($SearchTmp)])) {
			$SearchTmp->attributes = $_REQUEST[get_class($SearchTmp)];
		}
		$SearchTmp->sfDirections = array($directionId);
		$SearchTmp->sfDepartments = array($depId);
		return self::GenerateLinkContent($text, $SearchTmp);
	}
	public function Departments_getGridColumns() {
		$ud = $this->Departments_getUsesDirections();
		return array(
				array(
						'name' => 'TitleName',
						'header' => mb_ucfirst(Yii::t('callinfo', 'departments')),
						'value' => function ($data) {
							$val = $data['TitleName'];
							if ($val == false)
								$val = '-';
							return $val;
						}),
				array(
						'name' => 'Outbound',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('Outbound'),
						
						// 'footer' => $this->allData['Outbound'],
						'visible' => in_array('Outbound', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Departments_GenerateLinkContent("Outbound", $data)'),
				array(
						'name' => 'Inbound',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('Inbound'),
						
						// 'footer' => $this->allData['Inbound'],
						'visible' => in_array('Inbound', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Departments_GenerateLinkContent("Inbound", $data)'),
				array(
						'name' => 'Local',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('Local'),
						
						// 'footer' => $this->allData['Local'],
						'visible' => in_array('Local', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Departments_GenerateLinkContent("Local", $data)'),
				array(
						'name' => 'Conference',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('Conference'),
						
						// 'footer' => $this->allData['Conference'],
						'visible' => in_array('Conference', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Departments_GenerateLinkContent("Conference", $data)'),
				array(
						'name' => 'Callback',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('Callback'),
						
						// 'footer' => $this->allData['Callback'],
						'visible' => in_array('Callback', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Departments_GenerateLinkContent("Callback", $data)'),
				array('name' => 'allCount','header' => Yii::t('m', 'Total')));
		// 'footer' => $this->allData['allCount']
	}
	public function Departments_getExistingDepartments($keys = false) {
		$res = array();
		if (count($this->namedData != false) && count($this->allData) != false) {
			foreach ( $this->namedData as $name => $v ) {
				if (! $keys)
					$res[] = $v['TitleName'];
				else
					$res[] = $name;
			}
		}
		return $res;
	}
	public function Departments_getColumnSeries() {
		$ExDeps = $this->Departments_getExistingDepartments(true);
		$ud = $this->Departments_getUsesDirections(false);
		$res = array();
		foreach ( $ud as $DirectionId ) {
			$row = array();
			$row['name'] = $this->GetDirectionCaption($DirectionId);
			$row['color'] = $this->GetDirectionColor($DirectionId);
			$data = array();
			
			foreach ( $ExDeps as $DepId ) {
				$data[] = intval($this->namedData[$DepId][$DirectionId]);
			}
			$row['data'] = $data;
			$res[] = $row;
		}
		return $res;
	}
	public function Departments_getPieSeries() {
		$res = array();
		$dataAll = array();
		$dataGroup = array();
		$ud = $this->Departments_getUsesDirections(false);
		
		if (count($this->namedData != false) && count($this->allData) != false) {
			foreach ( $this->namedData as $name => $v ) {
				$color = $this->GetDepartmentColor($name);
				if ($v['allCount'] != 0)
					$dataAll[] = array('name' => $v['TitleName'],'color' => $color,'y' => getPercent($v['allCount'], $this->allData['allCount']));
				
				foreach ( $ud as $DirectionId ) {
					if ($v[$DirectionId] != 0) {
						$dataGroup[] = array(
								'name' => $v['TitleName'] . '<br>' . $this->GetDirectionCaption($DirectionId),
								'color' => $this->GetDirectionColor($DirectionId),
								'y' => getPercent($v[$DirectionId], $v['allCount'], getPercent($v['allCount'], $this->allData['allCount'])));
					}
				}
			}
		}
		$res[] = array(
				'name' => Yii::t('m', 'All calls'),
				'data' => $dataAll,
				'size' => '80%',
				'dataLabels' => array('distance' => 20,'color' => 'black'));
		/* убрано пока не попросят */
		// $res[] = array('name' => ' ','data' => $dataGroup,'innerSize' => '80%','dataLabels' => array(),'allowPointSelect' => true,'cursor' => 'pointer');
		return $res;
	}
	public function Agents_getDirectionKeys() {
		$res = array();
		if (is_array($this->sfDirections)) {
			$res = array_flip(self::$ShortDirectionKeys);
			$res = array_intersect_key($res, array_flip($this->sfDirections));
			$res = array_values($res);
		}
		if (empty($res))
			$res = array_keys(self::$ShortDirectionKeys);
		
		$act_arr = array_flip(self::$ShortDirectionKeys);
		$act_arr = array_intersect_key($act_arr, array_flip(self::$UsesDirections));
		if ($this->sfDirections != false)
			$act_arr = array_intersect_key($act_arr, array_flip($this->sfDirections));
		$res = array_values($act_arr);
		return ($res);
	}
	public function Agents_getSQLConditions($DirKey) {
		$res = array();
		$sqlCond = '';
		if (in_array($DirKey, array('LOC','OUT'))) {
			if (($this->sfDepartments != false) && ($this->sfAgents == false)) {
				$s = 'and (exists (select 1 from SIP s
				  inner join DepartmentAgents da on s.id = da.SIPId and da.DepartmentId in ([DepartsIs])
				  where s.name = c.NumSrc))
				  ';
				$s = str_replace('[DepartsIs]', implode(',', $this->sfDepartments), $s);
				$sqlCond .= $s;
			}
			if ($this->sfAgents != false) {
				$s = 'and FullName(c.NumSrc, c.NameSrc) in ([Agents])					
				 ';
				$agnts = '"' . implode('","', $this->sfAgents) . '"';
				$s = str_replace('[Agents]', $agnts, $s);
				$sqlCond .= $s;
			}
			$res['LOC'] = $sqlCond;
			$res['OUT'] = $sqlCond;
		}
		if (in_array($DirKey, array('IN','CLB'))) {
			if (($this->sfDepartments != false) && ($this->sfAgents == false)) {
				$s = 'and (exists (select 1 from SIP s
				  inner join DepartmentAgents da on s.id = da.SIPId and da.DepartmentId in ([DepartsIs])
				  where s.name = [tblAlias].NumDst))
				  ';
				$s = str_replace('[DepartsIs]', implode(',', $this->sfDepartments), $s);
				$sqlCond .= $s;
			}
			if ($this->sfAgents != false) {
				$s = 'and FullName([tblAlias].NumDst, [tblAlias].NameDst) in ([Agents])
				 ';
				$agnts = '"' . implode('","', $this->sfAgents) . '"';
				$s = str_replace('[Agents]', $agnts, $s);
				$sqlCond .= $s;
			}
			$res['IN'] = str_replace('[tblAlias]', 'c', $sqlCond);
			$res['IN_D'] = str_replace('[tblAlias]', 'cd', $sqlCond);
			$res['CLB'] = $res['IN'];
			$res['CLB_D'] = $res['IN_D'];
		}
		return $res;
	}
	public function Agents_getDirSQL($DirKey, $GetCondition = false, $params = array()) {
		$res = '';
		if ($GetCondition) {
			// fill only period
			$attributes = array('sfCallDateTime' => $params['datebegin'],'sfCallEndDateTime' => $params['dateend']);
			$this->fillConditions($attributes, true);
		} else
			$dirCond = $this->Agents_getSQLConditions($DirKey);
		$conditions = $this->command->where;
		/*
		 'agent' => $AgentFN,
		 'subtype' => $Column,
		 'dirkey' => $dirTag,
		 'datebegin' => $SearchTmp->sfCallDateTime,
		 'dateend' => $SearchTmp->sfCallEndDateTime)),
		 */
		
		if ($DirKey == 'LOC') {
			if (! $GetCondition)
				$res = '
				select FullName(c.NumSrc, c.NameSrc) name,
				sum(c.Duration) DurationLOC,
				sum(c.BridgeDuration) BridgeDurationLOC,
				avg(c.BridgeDuration) avgBridgeDurationLOC,
				SUM(if(c.StateId like "ANSWERED%", 1, 0)) countAnsweredLOC,
				count(1) countAllLOC
				from CI_Calls c
				where isSipNum(c.NumSrc)
				and c.DirectionId = "local"
				and ' . $conditions . '
				' . $dirCond['LOC'] . '
				group by 1
				order by 1';
			else {
				$res = '1=1
				and ' . $conditions . '
				and c.DirectionId = "local"
				and FullName(c.NumSrc, c.NameSrc) = "' . $params['agent'] . '"																								
				';
				if ($params['subtype'] == 'countAnswered')
					$res .= 'and c.StateId like "ANSWERED%"';
			}
		}
		if ($DirKey == 'OUT') {
			if (! $GetCondition)
				$res = '      
				select FullName(c.NumSrc, c.NameSrc) name,
 				sum(c.Duration) DurationOUT, 
				sum(c.BridgeDuration) BridgeDurationOUT, 
				avg(c.BridgeDuration) avgBridgeDurationOUT, 
 				SUM(if(c.StateId like "ANSWERED%", 1, 0)) countAnsweredOUT,
 				count(1) countAllOUT 
				from CI_Calls c
				where isSipNum(c.NumSrc)
				and c.DirectionId = "outbound"
				and ' . $conditions . '
				' . $dirCond['OUT'] . '				
				group by 1
				order by 1';
			else {
				$res = '1=1
				and ' . $conditions . '
				and c.DirectionId = "outbound"
				and FullName(c.NumSrc, c.NameSrc) = "' . $params['agent'] . '"
				';
				if ($params['subtype'] == 'countAnswered')
					$res .= 'and c.StateId like "ANSWERED%"';
			}
		}
		if (in_array($DirKey, array('IN','CLB'))) {
			$DirIdCondition = ($DirKey == 'CLB') ? 'like "Callback%"' : '= "Inbound"';
			if (! $GetCondition)
				$res = '
				select FullNameSrc name, 
				sum(Duration) Duration' . $DirKey . ', 
				sum(BridgeDuration) BridgeDuration' . $DirKey . ',
				sum(avgBridgeDuration) avgBridgeDuration' . $DirKey . ', 
				sum(countANSWERED) countAnswered' . $DirKey . ', 
				sum(countAll) countAll' . $DirKey . '
				from
				(
				select FullName(c.NumDst, c.NameDst) FullNameSrc,
				sum(c.Duration) Duration,
				sum(c.BridgeDuration) BridgeDuration,
				avg(c.BridgeDuration) avgBridgeDuration,
				SUM(if(c.StateId like "ANSWERED%", 1, 0)) countANSWERED,
				count(1) countAll
				from CI_Calls c
				where isSipNum(c.NumDst) 
				and c.DirectionId ' . $DirIdCondition . '
				and not exists(select 1 from CI_CallsDetail cd where c.Id=cd.CallId)
				and ' . $conditions . '
				' . $dirCond['IN'] . '							
				group by 1						
				union all			
				select FullName(cd.NumDst, cd.NameDst) FullNameSrc,
				sum(cd.Duration) Duration,
				sum(cd.BridgeDuration) BridgeDuration,
				avg(cd.BridgeDuration) avgBridgeDuration,
				SUM(if(cd.StateId like "ANSWERED%", 1, 0)) countANSWERED,
				count(distinct cd.CallId) countAll
				from CI_CallsDetail cd
				inner join CI_Calls c on c.Id = cd.CallId and c.DirectionId ' . $DirIdCondition . '
				where isSipNum(cd.NumDst) 
				and cd.Id<=COALESCE((select cd2.Id from CI_CallsDetail cd2 where cd.CallId=cd2.CallId and cd2.StateId = "ANSWERED" limit 1), cd.Id)
				and ' . $conditions . '
				' . $dirCond['IN_D'] . '							
				group by 1
				) sumTable where FullNameSrc is not null
				group by 1
				order by 1';
			else {
				$conditionsM = str_replace('c.', 'cm.', $conditions);
				$conditionsD = str_replace('c.', 'cI.', $conditions);
				$conditionsMAnsw = '';
				$conditionsDAnsw = '';
				if ($params['subtype'] == 'countAnswered') {
					$conditionsMAnsw .= 'and cm.StateId like "ANSWERED%"';
					$conditionsDAnsw .= 'and cdm.StateId like "ANSWERED%"';
				}
				$res = '1=1
				and ' . $conditions . '
				and c.DirectionId ' . $DirIdCondition . '
				and c.Id in (
				select distinct cm.Id 
				from CI_Calls cm
				where cm.DirectionId ' . $DirIdCondition . '
				and not exists(select 1 from CI_CallsDetail cdm where cm.Id=cdm.CallId)
				and ' . $conditionsM . '
				' . $conditionsMAnsw . ' 
				and FullName(cm.NumDst, cm.NameDst) = "' . $params['agent'] . '"						
				union						
				select distinct cdm.CallId
				from CI_CallsDetail cdm
				inner join CI_Calls cI on cI.Id = cdm.CallId and cI.DirectionId ' . $DirIdCondition . '
				where ' . $conditionsD . '
				' . $conditionsDAnsw . '
				and FullName(cdm.NumDst, cdm.NameDst) = "' . $params['agent'] . '"
				and cdm.Id<=COALESCE((select cd2.Id from CI_CallsDetail cd2 where cdm.CallId=cd2.CallId and cd2.StateId = "ANSWERED" limit 1), cdm.Id)				 												
				)';
			}
		}
		return $res;
	}
	public function Agents_fetchAll() {
		$this->Agents_getDirectionKeys();
		$this->result = array();
		$dcs = $this->Agents_getDirectionKeys();
		
		if (in_array('LOC', $dcs)) {
			$this->command->text = $this->Agents_getDirSQL('LOC');
			$this->result['LOC'] = $this->command->queryAll();
		}
		
		if (in_array('OUT', $dcs)) {
			$this->command->text = $this->Agents_getDirSQL('OUT');
			$this->result['OUT'] = $this->command->queryAll();
		}
		
		if (in_array('IN', $dcs)) {
			$this->command->text = $this->Agents_getDirSQL('IN');
			$this->result['IN'] = $this->command->queryAll();
		}
		
		if (in_array('CLB', $dcs)) {
			$this->command->text = $this->Agents_getDirSQL('CLB');
			$this->result['CLB'] = $this->command->queryAll();
		}
		$this->processDataByName($this->result);
		$this->processAllData();
		return $this;
	}
	public function Agents_getSortAttributes($dirTag) {
		return array(
				'name',
				'Duration' . $dirTag,
				'BridgeDuration' . $dirTag,
				'avgBridgeDuration' . $dirTag,
				'countAnswered' . $dirTag,
				'countAll' . $dirTag);
	}
	public static function Agents_GenerateLinkContent($Column, $dirTag, $data) {
		$text = $data[$Column . $dirTag];
		$AgentFN = $data['name'];
		
		if ($AgentFN == null)
			return $text;
		
		$SearchTmp = new CallsLogSearchForm();
		if (isset($_REQUEST[get_class($SearchTmp)])) {
			$SearchTmp->attributes = $_REQUEST[get_class($SearchTmp)];
		}
		$content = CHtml::Tag('a', array(
				'href' => '/callslog/report/agents?' . http_build_query(array(
						'showdetails' => '1',
						'agent' => $AgentFN,
						'subtype' => $Column,
						'dirkey' => $dirTag,
						'datebegin' => $SearchTmp->sfCallDateTime,
						'dateend' => $SearchTmp->sfCallEndDateTime)),
				'target' => '_blank',
				'style' => 'float:right'), CHtml::Tag('img', array('src' => '/images/16/tablefind.png')));
		$text = '<div style="float:left;">' . $text . '</div>';
		return $text . ' ' . $content;
	}
	public function Agents_getGridColumns($dirTag) {
		Yii::app()->params['CurrDirTag'] = $dirTag;
		return array(
				array('name' => 'name','header' => 'Агент'),
				array(
						'name' => 'Duration' . $dirTag,
						'header' => 'Общая длительность|Звонка',
						
						// 'footer' => sec2hms($this->allData['Duration' . $dirTag], true),
						'value' => function ($data) {
							return sec2hms($data['Duration' . Yii::app()->params['CurrDirTag']], true);
						}),
				array(
						'name' => 'BridgeDuration' . $dirTag,
						'header' => 'Общая длительность|Разговора',
						
						// 'footer' => sec2hms($this->allData['BridgeDuration' . $dirTag], true),
						'value' => function ($data) {
							return sec2hms($data['BridgeDuration' . Yii::app()->params['CurrDirTag']], true);
						}),
				array(
						'name' => 'avgBridgeDuration' . $dirTag,
						'header' => 'Общая длительность|Разговора(средн)',
						
						// 'footer' => sec2hms($this->allData['avgBridgeDuration' . $dirTag], true),
						'value' => function ($data) {
							return sec2hms($data['avgBridgeDuration' . Yii::app()->params['CurrDirTag']], true);
						}),
				array(
						'name' => 'countAnswered' . $dirTag,
						'header' => 'Отвечено',
						
						// 'footer' => $this->allData['countAnswered' . $dirTag],
						'type' => 'raw',
						'value' => 'DataCallsReports::Agents_GenerateLinkContent("countAnswered", Yii::app()->params["CurrDirTag"], $data)'),
				array('name' => 'countAll' . $dirTag,'header' => Yii::t('m', 'Total'),
						// 'footer' => $this->allData['countAll' . $dirTag],
						'type' => 'raw','value' => 'DataCallsReports::Agents_GenerateLinkContent("countAll", Yii::app()->params["CurrDirTag"], $data)'));
	}
	public function Chanout_fetchAll() {
		$where = $this->command->where;
		$where .= ' and c.DirectionId in ("' . implode('","', $this->Chanout_getUsesDirections()) . '")';
		
		$globalWhere = 'where 1=1';
		if ($this->sfPBXNum != false) {
			$s = implode('","', $this->sfPBXNum);
			$globalWhere .= ' and (name in ("' . $s . '")) ';
		}
		
		$this->command->text = '
				
		select 
		name,
		sum(Duration) Duration,
		sum(BridgeDuration) BridgeDuration,
		sum(Outbound) Outbound,
		sum(Inbound) Inbound,
		sum(Callback) Callback,
		sum(ConfOut) ConfOut,
		sum(AutoCall) AutoCall,
		sum(ANSWERED) ANSWERED,
		sum(NO_ANSWER) NO_ANSWER,
		sum(BUSY) BUSY,
		sum(FAILED) FAILED,
		sum(allCount) allCount				  
		from (
			SELECT cd.NumOut name,			
			sum(0) Duration, 
            sum(0) BridgeDuration,
			count(distinct concat(c.Id,if((c.DirectionId = "Outbound"),1,null))) Outbound, 
            count(distinct concat(c.Id,if((c.DirectionId = "Inbound"),1,null))) Inbound, 
            count(distinct concat(c.Id,if((c.DirectionId like "Callback%"),1,null))) Callback, 
            count(distinct concat(c.Id,if((c.DirectionId = "ConfOut"),1,null))) ConfOut, 
            count(distinct concat(c.Id,if((c.DirectionId = "AutoCall"),1,null))) AutoCall, 								
            count(distinct concat(c.Id,if((c.StateId like "ANSWERED%"),1,null))) ANSWERED, 
            count(distinct concat(c.Id,if((c.StateId = "NO_ANSWER"),1,null))) NO_ANSWER, 
            count(distinct concat(c.Id,if((c.StateId = "BUSY"),1,null))) BUSY, 
            count(distinct concat(c.Id,if((c.StateId = "FAILED"),1,null))) FAILED,             
            count(distinct c.Id) allCount				
			FROM CI_Calls c
			inner join `CI_CallsDetail` cd on cd.`CallId` = c.`Id`
            where 1=1
			and' . $where . '						
            group by 1                        
		union all
			SELECT c.NumOut name,			
			sum(c.Duration) Duration, 
            sum(c.BridgeDuration) BridgeDuration,
			count(distinct concat(c.Id,if((c.DirectionId = "Outbound"),1,null))) Outbound, 
            count(distinct concat(c.Id,if((c.DirectionId = "Inbound"),1,null))) Inbound, 
            count(distinct concat(c.Id,if((c.DirectionId like "Callback%"),1,null))) Callback, 
            count(distinct concat(c.Id,if((c.DirectionId = "ConfOut"),1,null))) ConfOut, 
            count(distinct concat(c.Id,if((c.DirectionId = "AutoCall"),1,null))) AutoCall, 								
            count(distinct concat(c.Id,if((c.StateId like "ANSWERED%"),1,null))) ANSWERED, 
            count(distinct concat(c.Id,if((c.StateId = "NO_ANSWER"),1,null))) NO_ANSWER, 
            count(distinct concat(c.Id,if((c.StateId = "BUSY"),1,null))) BUSY, 
            count(distinct concat(c.Id,if((c.StateId = "FAILED"),1,null))) FAILED,             
            count(distinct c.Id) allCount				
			FROM CI_Calls c
			left join `CI_CallsDetail` cd on cd.`CallId` = c.`Id` 
            where cd.`CallId` is null
			and' . $where . ' 
            group by 1					
		union all
			SELECT name, sum(Duration) Duration, sum(BridgeDuration) BridgeDuration, 
					Outbound, Inbound, Callback, ConfOut, AutoCall, ANSWERED, NO_ANSWER, BUSY, FAILED, allCount
				from (
					SELECT cd.NumOut name, c.`Id`,			
					TIMESTAMPDIFF(SECOND,min(cd.CallDateTime),max(cd.CallEndDateTime)) Duration,
            		sum(cd.BridgeDuration) BridgeDuration,
					sum(0) Outbound, 
            		sum(0) Inbound, 
            		sum(0) Callback, 
            		sum(0) ConfOut, 
            		sum(0) AutoCall, 								
            		sum(0) ANSWERED, 
            		sum(0) NO_ANSWER, 
            		sum(0) BUSY, 
          			sum(0) FAILED,             
            		sum(0) allCount				
					FROM CI_Calls c
					inner join `CI_CallsDetail` cd on cd.`CallId` = c.`Id`            
					where 1=1            
					and' . $where . '
					group by 1,2) st 
			group by 1					
     	) t ' . $globalWhere . '
		group by name
		order by name';
		$this->result = $this->command->queryAll();
		$this->processDataByName($this->result);
		$this->processAllData();
		return $this;
	}
	public function Chanout_getUsesDirections($fullArr = true) {
		$arr = array_intersect_key(array('Outbound','Inbound','Callback','CallbackInit','ConfOut','AutoCall'), self::$UsesDirections);
		if ($fullArr) {
			if (in_array('Callback', self::$UsesDirections) && ! in_array('CallbackInit', $arr))
				$arr[] = 'CallbackInit';
		} else {
			$arr = array_flip($arr);
			unset($arr['CallbackInit']);
		}
		return $arr;
	}
	public static function Chanout_GenerateLinkContent($Column, $data, $isState = false) {
		$text = $data[$Column];
		// $chanId = $data['ChanNameOut'];
		$numOut = $data['name'];
		
		if ($numOut == null)
			return $text;
		
		$SearchTmp = new CallsLogSearchForm();
		if (isset($_REQUEST[get_class($SearchTmp)])) {
			$SearchTmp->attributes = $_REQUEST[get_class($SearchTmp)];
		}
		if ($isState)
			$SearchTmp->sfStates = array($Column);
		else
			$SearchTmp->sfDirections = array($Column);
		$SearchTmp->sfPBXNum = array($numOut);
		return self::GenerateLinkContent($text, $SearchTmp);
	}
	public function Chanout_getGridColumns() {
		$ud = $this->Chanout_getUsesDirections();
		return array(
				array(
						'name' => 'name',
						'header' => $this->GetHeader('NumOutOnly'),
						'value' => function ($data) {
							$val = $data['name'];
							if ($val == false)
								$val = '-';
							return $val;
						}),
				
				array(
						'name' => 'Outbound',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('Outbound'),
						'footer' => $this->allData['Outbound'],
						'visible' => in_array('Outbound', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Chanout_GenerateLinkContent("Outbound", $data)'),
				array(
						'name' => 'Inbound',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('Inbound'),
						'footer' => $this->allData['Inbound'],
						'visible' => in_array('Inbound', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Chanout_GenerateLinkContent("Inbound", $data)'),
				array(
						'name' => 'Callback',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('Callback'),
						'footer' => $this->allData['Callback'],
						'visible' => in_array('Callback', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Chanout_GenerateLinkContent("Callback", $data)'),
				array(
						'name' => 'ConfOut',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('ConfOut'),
						'footer' => $this->allData['ConfOut'],
						'visible' => in_array('ConfOut', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Chanout_GenerateLinkContent("ConfOut", $data)'),
				array(
						'name' => 'AutoCall',
						'header' => $this->GetHeader('DirName') . '|' . $this->GetDirectionCaption('AutoCall'),
						'footer' => $this->allData['AutoCall'],
						'visible' => in_array('AutoCall', $ud),
						'type' => 'raw',
						'value' => 'DataCallsReports::Chanout_GenerateLinkContent("AutoCall", $data)'),
				array(
						'name' => 'ANSWERED',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('ANSWERED'),
						'footer' => $this->allData['ANSWERED'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Chanout_GenerateLinkContent("ANSWERED", $data, true)'),
				array(
						'name' => 'NO_ANSWER',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('NO_ANSWER'),
						'footer' => $this->allData['NO_ANSWER'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Chanout_GenerateLinkContent("NO_ANSWER", $data, true)'),
				array(
						'name' => 'BUSY',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('BUSY'),
						'footer' => $this->allData['BUSY'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Chanout_GenerateLinkContent("BUSY", $data, true)'),
				array(
						'name' => 'FAILED',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('FAILED'),
						'footer' => $this->allData['FAILED'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Chanout_GenerateLinkContent("FAILED", $data, true)'),
				array('name' => 'allCount','header' => Yii::t('m', 'Total'),'footer' => $this->allData['allCount']),
				array(
						'name' => 'Duration',
						'header' => $this->GetHeader('Duration'),
						'value' => function ($data) {
							return sec2hms($data['Duration'], true);
						},
						'footer' => sec2hms($this->allData['Duration'], true)),
				array(
						'name' => 'BridgeDuration',
						'header' => $this->GetHeader('BridgeDuration'),
						'value' => function ($data) {
							return sec2hms($data['BridgeDuration'], true);
						},
						'footer' => sec2hms($this->allData['BridgeDuration'], true)));
	}
	public function Chanout_getExistingDirectionCategores($keys = false) {
		$usD = $this->Chanout_getUsesDirections(false);
		if (! $keys)
			return array_values(array_intersect_key(self::$DirectionsCaptions, $usD));
		else
			return array_keys(array_intersect_key(self::$DirectionsCaptions, $usD));
	}
	public function Chanout_getExistingChans($title = false) {
		$res = array();
		if (count($this->namedData != false) && count($this->allData) != false) {
			foreach ( $this->namedData as $name => $v ) {
				if (($name === '') && $title)
					$res[] = '-';
				else
					$res[] = $name;
			}
		}
		return $res;
	}
	public function Chanout_getColumnSeriesDirection() {
		$ExChans = $this->Chanout_getExistingChans();
		$usDirs = $this->Chanout_getExistingDirectionCategores(true);
		$res = array();
		foreach ( $usDirs as $DirectionId ) {
			$row = array();
			$row['name'] = $this->GetDirectionCaption($DirectionId);
			$row['color'] = $this->GetDirectionColor($DirectionId);
			$data = array();
			foreach ( $ExChans as $ChanN ) {
				$data[] = intval($this->namedData[$ChanN][$DirectionId]);
			}
			$row['data'] = $data;
			$res[] = $row;
		}
		return $res;
	}
	public function Chanout_getColumnSeriesStates() {
		$ExChans = $this->Chanout_getExistingChans();
		$res = array();
		foreach ( self::$UsesStates as $StateId ) {
			$row = array();
			$row['name'] = $this->GetStateCaption($StateId);
			$row['color'] = $this->GetStateColor($StateId);
			$data = array();
			foreach ( $ExChans as $ChanN ) {
				$data[] = intval($this->namedData[$ChanN][$StateId]);
			}
			$row['data'] = $data;
			$res[] = $row;
		}
		return $res;
	}
	public function Chanout_getPieSeries() {
		$res = array();
		$dataAll = array();
		
		if (count($this->namedData != false) && count($this->allData) != false) {
			foreach ( $this->namedData as $name => $v ) {
				$color = getRandomColor();
				if ($v['allCount'] != 0)
					$dataAll[] = array('name' => $name,'color' => $color,'y' => getPercent($v['allCount'], $this->allData['allCount']));
			}
		}
		$res[] = array(
				'name' => Yii::t('m', 'All calls'),
				'data' => $dataAll,
				'size' => '80%',
				'dataLabels' => array('distance' => 20,'color' => 'black'));
		
		return $res;
	}
	public function Callback_getCallsLogSQL() {
		$pbxNum = $_REQUEST['pbxNum'];
		$column = $_REQUEST['column'];
		$attributes = array('sfCallDateTime' => $_REQUEST['datebegin'],'sfCallEndDateTime' => $_REQUEST['dateend']);
		$this->fillConditions($attributes, true);
		
		$s = $this->command->where . ' and (c.DirectionId like "CALLBACK%")';
		$s .= ' and exists (select 1 from CI_CallsDetail cd where cd.CallId = c.Id and cd.DirectionId = "Callback';
		if ($column == 'INIT')
			$s .= 'Init';
		$s .= '"';
		
		$s .= ' and cd.NumOut =' . $pbxNum;
		if (($column != 'INIT') && ($column != 'CALLBACK'))
			$s .= ' and cd.`StateId`="' . $column . '"';
		
		$s .= ")";
		
		return $s;
	}
	public static function Callback_GenerateLinkContent($Column, $data) {
		$text = $data[$Column];
		$pbxNum = $data['name'];
		
		if ($pbxNum == null)
			return $text;
		
		$SearchTmp = new CallsLogSearchForm();
		if (isset($_REQUEST[get_class($SearchTmp)])) {
			$SearchTmp->attributes = $_REQUEST[get_class($SearchTmp)];
		}
		$content = CHtml::Tag('a', array(
				'href' => '/callslog/report/callback?' . http_build_query(array(
						'showdetails' => '1',
						'pbxNum' => $pbxNum,
						'column' => $Column,
						'datebegin' => $SearchTmp->sfCallDateTime,
						'dateend' => $SearchTmp->sfCallEndDateTime)),
				'target' => '_blank',
				'style' => 'float:right'), CHtml::Tag('img', array('src' => '/images/16/tablefind.png')));
		$text = '<div style="float:left;">' . $text . '</div>';
		return $text . ' ' . $content;
	}
	public function Callback_fetchAll() {
		$where = $this->command->where;
		$globalWhere = 'where 1=1';
		if ($this->sfPBXNum != false) {
			$s = implode('","', $this->sfPBXNum);
			$globalWhere .= ' and (name in ("' . $s . '")) ';
		}
		$this->command->text = '
			SELECT name, INIT,  CALLBACK, ANSWERED, NO_ANSWER, BUSY, FAILED, Duration, BridgeDuration FROM(
				select cd.NumOut name,		
				count(distinct concat(c.Id,if((cd.DirectionId = "CallbackInit"),1,null))) INIT, 
		        count(distinct concat(c.Id,if((cd.DirectionId = "Callback"),1,null))) CALLBACK,

				count(distinct concat(c.Id,if((cd.DirectionId = "Callback") and (cd.StateId like "ANSWERED%"),1,null))) ANSWERED,
                count(distinct concat(c.Id,if((cd.DirectionId = "Callback") and (cd.StateId =  "NO_ANSWER"),1,null))) NO_ANSWER,
                count(distinct concat(c.Id,if((cd.DirectionId = "Callback") and (cd.StateId =  "BUSY"),1,null))) BUSY,
                count(distinct concat(c.Id,if((cd.DirectionId = "Callback") and (cd.StateId =  "FAILED"),1,null))) FAILED,			
				
				SUM(distinct if(cd.DirectionId="Callback", c.Duration,0)) Duration, 
				SUM(distinct if(cd.DirectionId="Callback", c.BridgeDuration,0)) BridgeDuration
	
				from `CI_Calls` c
				inner join `CI_CallsDetail` cd on cd.`CallId` = c.`Id`
				where c.DirectionId like "Callback%"				
				and ' . $where . '
				GROUP BY 1
				ORDER BY 1
			) t ' . $globalWhere;
		
		$this->result = $this->command->queryAll();
		$this->processDataByName($this->result);
		$this->processAllData();
		return $this;
	}
	public function Callback_getGridColumns() {
		// $ud = $this->Chanout_getUsesDirections();
		return array(
				array(
						'name' => 'name',
						'header' => $this->GetHeader('NumOutOnly'),
						'value' => function ($data) {
							$val = $data['name'];
							if ($val == false)
								$val = '-';
							return $val;
						}),
				array(
						'name' => 'INIT',
						'header' => Yii::t('m', 'Total') . '|' . "Запросов",
						'footer' => $this->allData['INIT'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Callback_GenerateLinkContent("INIT", $data)'),
				array(
						'name' => 'INIT',
						'header' => Yii::t('m', 'Total') . '|' . "Отзвонов",
						'footer' => $this->allData['CALLBACK'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Callback_GenerateLinkContent("CALLBACK", $data)'),
				array(
						'name' => 'ANSWERED',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('ANSWERED'),
						'footer' => $this->allData['ANSWERED'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Callback_GenerateLinkContent("ANSWERED", $data)'),
				array(
						'name' => 'NO_ANSWER',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('NO_ANSWER'),
						'footer' => $this->allData['NO_ANSWER'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Callback_GenerateLinkContent("NO_ANSWER", $data)'),
				array(
						'name' => 'BUSY',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('BUSY'),
						'footer' => $this->allData['BUSY'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Callback_GenerateLinkContent("BUSY", $data)'),
				array(
						'name' => 'FAILED',
						'header' => $this->GetHeader('StateName') . '|' . $this::GetStateCaption('FAILED'),
						'footer' => $this->allData['FAILED'],
						'type' => 'raw',
						'value' => 'DataCallsReports::Callback_GenerateLinkContent("FAILED", $data)'),
				array(
						'name' => 'Duration',
						'header' => $this->GetHeader('Duration'),
						'value' => function ($data) {
							return sec2hms($data['Duration'], true);
						},
						'footer' => sec2hms($this->allData['Duration'], true)),
				array(
						'name' => 'BridgeDuration',
						'header' => $this->GetHeader('BridgeDuration'),
						'value' => function ($data) {
							return sec2hms($data['BridgeDuration'], true);
						},
						'footer' => sec2hms($this->allData['BridgeDuration'], true)));
	}
	public function MissedCalls_fetchAll() {
		$where = $this->command->where;
		$dt1 = $this->sfCallDateTime;
		$dt2 = $this->sfCallEndDateTime;
		$TimeOutSel = (int) $this->sfTimeOutSel;
		$addInterval = 'INTERVAL ' . $TimeOutSel . ' ' . $this->sfTimeOutSelIntType;
		$this->command->text = '		
		select c.`NumSrc`, if(c.DirectionId="CallbackInit","Callback",c.DirectionId) DirectionId,
			   c.`NameSrc`, isExistingNum(NumSrc, NameSrc) exSrc, 
			   MIN(c.`CallDateTime`) minDT, 
			   (TIME_TO_SEC(TIMEDIFF(max(c.`CallEndDateTime`), min(c.`CallDateTime`))) - SUM(c.`BridgeDuration`)) AllWaitTime,
			   SUM(c.`Duration`-c.`BridgeDuration`) WaitTime, count(1) count,
				(select count(distinct cOutb.`Id`)
                from `CI_Calls` cOutb 
                where cOutb.`CallDateTime` between :dt1 and :dt2 + ' . $addInterval . ' -- /ind
                and ' . DataModule::DepsCond('cOutb') . '
                and cOutb.`CallDateTime` between min(c.`CallDateTime`) and max(c.`CallDateTime`) + ' . $addInterval . '
                and cOutb.`DirectionId` = "Outbound"
                and cOutb.`NumDst` = min(c.`NumSrc`)                
                ) OutboundCount,
                (select count(distinct IF(`isSipNum`(COALESCE(cdAg.`NumDst`, cAg.`NumDst`)),COALESCE(cdAg.`NumDst`, cAg.`NumDst`),null))
                from `CI_Calls` cAg
                left join `CI_CallsDetail` cdAg on `cAg`.`Id`=`cdAg`.`CallId`               
				where cAg.`CallDateTime` between :dt1 and :dt2 + ' . $addInterval . ' -- /ind
				and ' . DataModule::DepsCond('cAg') . '
                and cAg.`CallDateTime` between MIN(c.`CallDateTime`) and MAX(c.`CallDateTime`) + ' . $addInterval . '
				and cAg.`StateId`<>"ANSWERED"
				and ((cAg.`DirectionId` = "Inbound") or (cAg.`DirectionId` like "Callback%"))
                and ((((cAg.`DirectionId` = "Inbound") or (cAg.`DirectionId` = "Callback")) and cAg.`NumSrc`=min(c.`NumSrc`))
			 		or (cAg.`DirectionId` = "Outbound" and cAg.`NumDst`=min(c.`NumSrc`)))	             
                ) AgentsCount
		from `CI_Calls` c
		where ' . $where . '
		and c.`StateId`<>"ANSWERED"
		and ((c.`DirectionId` = "Inbound") or (c.`DirectionId` like "Callback%"))
		
		and not exists (
		 select 1 from `CI_Calls` c2
		 where c2.`CallDateTime` between c.`CallDateTime` and c.`CallDateTime` + ' . $addInterval . '
		 and c2.`CallDateTime` between :dt1 and :dt2 + ' . $addInterval . ' -- /ind
		 and c2.`StateId`="ANSWERED"
		 and ((((c2.`DirectionId` = "Inbound") or (c2.`DirectionId` = "Callback")) and c2.`NumSrc`=c.`NumSrc`)
			 or (c2.`DirectionId` = "Outbound" and c2.`NumDst`=c.`NumSrc`)))
		group by 1, 2, 3, 4
		order by minDT desc';
		$this->command->bindParam(':dt1', $dt1, PDO::PARAM_STR);
		$this->command->bindParam(':dt2', $dt2, PDO::PARAM_STR);
		$this->result = $this->command->queryAll();
		return $this;
	}
	public static function MissedCalls_GenerateLinkContent($data) {
		$num = $data['NumSrc'];
		$text = CallsUtils::GenereteFullNameContent($data['NumSrc'], $data['NameSrc'], $data['exSrc'], false);
		$directionId = $data['DirectionId'];
		if ($directionId == null)
			return $text;
		$SearchTmp = new CallsLogSearchForm();
		if (isset($_REQUEST[get_class($SearchTmp)]))
			$SearchTmp->attributes = $_REQUEST[get_class($SearchTmp)];
		else
			$SearchTmp->SetToday();
		$SearchTmp->sfDirections = array($directionId,'Outbound');
		$SearchTmp->sfNum = $num;
		return self::GenerateLinkContent($text, $SearchTmp, true);
	}
	public function MissedCalls_getGridColumns() {
		return array(
				array(
						'name' => 'NumSrc',
						'header' => 'Номер',
						'type' => 'raw',
						'value' => function ($data) {
							return DataCallsReports::MissedCalls_GenerateLinkContent($data);
						}),
				array('name' => 'NumSrc','header' => 'Тип звонка','value' => 'DataCallsReports::GetDirectionCaption($data["DirectionId"])'),
				array(
						'name' => 'minDT',
						'header' => 'Время первого звонка',
						'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data["minDT"])'),
				array('name' => 'AllWaitTime','header' => 'Время дозвона|Общее','value' => 'sec2hms($data["AllWaitTime"],true)'),
				array('name' => 'WaitTime','header' => 'Время дозвона|На линии','value' => 'sec2hms($data["WaitTime"],true)'),
				array('name' => 'count','header' => 'Количество|Звонков'),
				array('name' => 'OutboundCount','header' => 'Количество|Перезвонов'),
				array('name' => 'AgentsCount','header' => 'Количество|Агентов'));
		// 'htmlOptions' => array('style'=>'width:10%'),
	}
	public function LongTimeCalls_fetchAll() {
		$where = $this->command->where;
		$dt1 = $this->sfCallDateTime;
		$dt2 = $this->sfCallEndDateTime;
		$WaitTimeSeconds = (int) $this->sfTimeOutSel;
		switch ($this->sfTimeOutSelIntType) {
			case 'MINUTE' :
				$WaitTimeSeconds = $WaitTimeSeconds * 60;
				break;
			case 'HOUR' :
				$WaitTimeSeconds = $WaitTimeSeconds * 60 * 60;
				break;
			case 'DAY' :
				$WaitTimeSeconds = $WaitTimeSeconds * 60 * 60 * 24;
				break;
		}
		$TimeOutSel = (int) $this->sfTimeOutSel;
		$addInterval = 'INTERVAL ' . $TimeOutSel . ' ' . $this->sfTimeOutSelIntType;
		$this->command->text = '				
		select c.`NumSrc`, if(c.DirectionId="CallbackInit","Callback",c.DirectionId) DirectionId,
	    	c.`NameSrc`, isExistingNum(NumSrc, NameSrc) exSrc,
       		if (min(c.`StateId`) = max(c.`StateId`), max(cs.`Name`), "*") StateName,				 
	    	MIN(c.`CallDateTime`) minDT, 
       		(TIME_TO_SEC(TIMEDIFF(max(c.`CallEndDateTime`), min(c.`CallDateTime`))) - SUM(c.`BridgeDuration`)) AllWaitTime,
	   		SUM(c.`Duration`-c.`BridgeDuration`) WaitTime, count(1) count,
				
			(select count(distinct IF(`isSipNum`(COALESCE(cdAg.`NumDst`, cAg.`NumDst`)),COALESCE(cdAg.`NumDst`, cAg.`NumDst`),null))
             from `CI_Calls` cAg
             left join `CI_CallsDetail` cdAg on `cAg`.`Id`=`cdAg`.`CallId`               
			 where 1=1
			 and ' . DataModule::DepsCond('cAg') . '
			 and (TIME_TO_SEC(TIMEDIFF(c.`CallEndDateTime`, c.`CallDateTime`)) - c.`BridgeDuration` >= :wts)
			 and ((c.`DirectionId` = "Inbound") or (c.`DirectionId` like "Callback%"))
			 and cAg.`NumSrc`=min(c.`NumSrc`)
			 and cAg.`CallDateTime` between MIN(c.`CallDateTime`) and MAX(c.`CallDateTime`)
			 and cAg.`CallDateTime` between :dt1 and :dt2 	             
                ) AgentsCount
		from `CI_Calls` c
       	left join `CI_States` cs on cs.`Id`=c.`StateId`
		where ' . $where . '
		and (TIME_TO_SEC(TIMEDIFF(c.`CallEndDateTime`, c.`CallDateTime`)) - c.`BridgeDuration` >= :wts)
		and ((c.`DirectionId` = "Inbound") or (c.`DirectionId` like "Callback%"))
		group by 1, 2, 3, 4
		order by minDT desc';
		$this->command->bindParam(':wts', $WaitTimeSeconds, PDO::PARAM_INT);
		$this->command->bindParam(':dt1', $dt1, PDO::PARAM_STR);
		$this->command->bindParam(':dt2', $dt2, PDO::PARAM_STR);
		$this->result = $this->command->queryAll();
		return $this;
	}
	public static function LongTimeCalls_GenerateLinkContent($data) {
		$num = $data['NumSrc'];
		$text = CallsUtils::GenereteFullNameContent($data['NumSrc'], $data['NameSrc'], $data['exSrc'], false);
		$directionId = $data['DirectionId'];
		if ($directionId == null)
			return $text;
		$SearchTmp = new CallsLogSearchForm();
		if (isset($_REQUEST[get_class($SearchTmp)]))
			$SearchTmp->attributes = $_REQUEST[get_class($SearchTmp)];
		else
			$SearchTmp->SetToday();
		$SearchTmp->sfDirections = array($directionId);
		$SearchTmp->sfNum = $num;
		return self::GenerateLinkContent($text, $SearchTmp, true);
	}
	public function LongTimeCalls_getGridColumns() {
		return array(
				array(
						'name' => 'NumSrc',
						'header' => 'Номер',
						'type' => 'raw',
						'value' => function ($data) {
							return DataCallsReports::LongTimeCalls_GenerateLinkContent($data);
						}),
				array('name' => 'NumSrc','header' => 'Тип звонка','value' => 'DataCallsReports::GetDirectionCaption($data["DirectionId"])'),
				array('name' => 'StateName','header' => 'Статус'),
				array(
						'name' => 'minDT',
						'header' => 'Время первого звонка',
						'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $data["minDT"])'),
				array('name' => 'AllWaitTime','header' => 'Время дозвона|Общее','value' => 'sec2hms($data["AllWaitTime"],true)'),
				array('name' => 'WaitTime','header' => 'Время дозвона|На линии','value' => 'sec2hms($data["WaitTime"],true)'),
				array('name' => 'count','header' => 'Количество|Звонков'),
				array('name' => 'AgentsCount','header' => 'Количество|Агентов'));
	}
	public function Citychoose_fetchAll() {
		$dt1 = $this->sfCallDateTime;
		$dt2 = $this->sfCallEndDateTime;
		if ($this->sfAllTime)
			$cond = '';
		else
			$cond = 'and `InsertDateTime` between :dt1 and :dt2 ';
		$this->command->text = '
				select COALESCE(cn.`DisplayName`, cc.`city`) name, count(cc.id) allCount
				from `CityChoose` cc 				
				left join `CityNames` cn on cn.`Name`=cc.`city`
				where 1=1
				' . $cond . '
				group by 1';
		$this->command->bindParam(':dt1', $dt1, PDO::PARAM_STR);
		$this->command->bindParam(':dt2', $dt2, PDO::PARAM_STR);
		$this->result = $this->command->queryAll();
		$this->processDataByName($this->result);
		$this->processAllData();
		return $this;
	}
	public static function Citychoose_CityName($Name) {
		if ($Name === '')
			$Name = 'Без города';
		return $Name;
	}
	public function Citychoose_getGridColumns() {
		return array(
				array('name' => 'name','header' => 'Город','value' => 'DataCallsReports::Citychoose_CityName($data["name"])'),
				array('name' => 'allCount','header' => 'Всего','footer' => $this->allData['allCount']));
	}
	public function Citychoose_getPieSeries() {
		$res = array();
		$dataAll = array();
		
		if (count($this->namedData != false) && count($this->allData) != false) {
			foreach ( $this->namedData as $name => $v ) {
				$color = getRandomColor();
				if ($v['allCount'] != 0)
					$dataAll[] = array(
							'name' => DataCallsReports::Citychoose_CityName($name),
							'color' => $color,
							'y' => getPercent($v['allCount'], $this->allData['allCount']));
			}
		}
		$res[] = array('name' => 'Зарегистрировано','data' => $dataAll,'size' => '80%','dataLabels' => array('distance' => 20,'color' => 'black'));
		return $res;
	}
	public function Bydays_fetchAll() {
		$this->command->select('DATE(c.`CallDateTime`) name,
								SUM(if(c.StateId like "ANSWERED%", 1, 0)) ANSWERED,
								SUM(if(c.StateId = "NO_ANSWER", 1, 0)) NO_ANSWER,
								SUM(if(c.StateId = "BUSY", 1, 0)) BUSY,
								SUM(if(c.StateId = "FAILED", 1, 0)) FAILED,
								IFNULL(count(1),0) allCount');
		$this->command->from('CI_Calls c');
		$this->command->group('name');
		$this->command->order('name');
		$tmpResult = $this->command->queryAll();
		$this->result = array();
		$days = $this->Bydays_getDays();
		foreach ( $days as $day ) {
			// доставляем пустые дни
			$r = array(array('name' => $day,'ANSWERED' => 0,'NO_ANSWER' => 0,'BUSY' => 0,'FAILED' => 0,'allCount' => 0));
			foreach ( $tmpResult as $item ) {
				if ($item['name'] == $day) {
					$r = $item;
					break;
				}
			}
			$this->result[] = $r;
		}
		
		$this->processDataByName($this->result);
		$this->processAllData();
		return $this;
	}
	public function Bydays_getExisitingDays($comp = false) {
		$res = array();
		if (count($this->namedData != false) && count($this->allData) != false) {
			foreach ( $this->namedData as $name => $v ) {
				if (! $comp)
					$res[] = $name;
				else {
					$dtStr = $name;
					$dt = strtotime($dtStr);
					$dtStr = strftime('%d', $dt) . '   ' . ucfirst(strftime('%a', $dt)) . '   ';
					// $dtStr = iconv('windows-1251', 'UTF-8', $dtStr);
					$res[] = $dtStr;
				}
			}
		}
		return $res;
	}
	public function Bydays_getDays() {
		$date1 = new DateTime($this->sfCallDateTime);
		$date2 = new DateTime($this->sfCallEndDateTime);
		
		// echo $this->sfCallDateTime . '</br>';
		// echo $this->sfCallEndDateTime . '</br>';
		
		$datediff = date_diff($date2, $date1)->days - 1;
		
		// echo $datediff . '</br>';
		
		$res = array();
		$res[] = $date1->format('Y-m-d');
		for($n = 1; $n <= $datediff; $n ++) {
			$date1 = date_add($date1, new DateInterval('P1D'));
			$res[] = $date1->format('Y-m-d');
		}
		
		return $res;
	}
	public function Bydays_getColumnSeries() {
		$dates = $this->Bydays_getDays();
		$res = array();
		foreach ( self::$UsesStates as $StateId ) {
			$row = array();
			$row['name'] = $this->GetStateCaption($StateId);
			$row['color'] = $this->GetStateColor($StateId);
			$data = array();
			foreach ( $dates as $date ) {
				$key = $date;
				if (array_key_exists($key, $this->namedData))
					$data[] = intval($this->namedData[$key][$StateId]);
			}
			$row['data'] = $data;
			$res[] = $row;
		}
		return $res;
	}
	public function GenerateJS_OnColumnClick() {
		return 'js:function() {
				var colNo = this.x;
				$links = document.getElementById("links");			
				$spans = links.getElementsByTagName("span");
				$link = $spans[colNo].innerHTML;
				$link = $link .replace(/&amp;/g, "&");
				console.log($link);								
 				window.open($link);
				}';
	}
	public function Bydays_GenerateLinksContent() {
		$res = '';
		$dates = $this->Bydays_getDays();
		
		$hbq = '';
		$SearchTmp = new CallsLogSearchForm();
		if (isset($_REQUEST[get_class($SearchTmp)]))
			$SearchTmp->attributes = $_REQUEST[get_class($SearchTmp)];
		$res .= CHtml::openTag('div', array('id' => 'links','style' => 'display:none'));
		foreach ( $dates as $date ) {
			$res .= CHtml::openTag('span');
			$res .= '/callslog/report/byhours?';
			$SearchTmp->sfCallDateTime = $date;
			$hbq = http_build_query(array(get_class($SearchTmp) => $SearchTmp));
			$res .= $hbq;
			$res .= CHtml::closeTag('span');
		}
		$res .= CHtml::closeTag('div');
		return $res;
	}
	public function Byhours_fetchAll() {
		$this->command->select('EXTRACT(HOUR FROM c.`CallDateTime`) + 1 name,
								SUM(if(c.StateId like "ANSWERED%", 1, 0)) ANSWERED,
								SUM(if(c.StateId = "NO_ANSWER", 1, 0)) NO_ANSWER,
								SUM(if(c.StateId = "BUSY", 1, 0)) BUSY,
								SUM(if(c.StateId = "FAILED", 1, 0)) FAILED,
								IFNULL(count(1),0) allCount');
		$this->command->from('CI_Calls c');
		$this->command->group('name');
		$this->command->order('name');
		$tmpResult = $this->command->queryAll();
		$this->result = array();
		for($n = 1; $n < 25; $n ++) {
			// доставляем пустые часы
			$r = array(array('name' => $n,'ANSWERED' => 0,'NO_ANSWER' => 0,'BUSY' => 0,'FAILED' => 0,'allCount' => 0));
			foreach ( $tmpResult as $item ) {
				if ($item['name'] == $n) {
					$r = $item;
					break;
				}
			}
			$this->result[] = $r;
		}
		$this->processDataByName($this->result);
		$this->processAllData();
		return $this;
	}
	public function Byhours_getColumnSeries() {
		$hours = array();
		for($n = 1; $n < 25; $n ++) {
			$hours[] = $n;
		}
		// echo json_encode($hours);
		$res = array();
		foreach ( self::$UsesStates as $StateId ) {
			$row = array();
			$row['name'] = $this->GetStateCaption($StateId);
			$row['color'] = $this->GetStateColor($StateId);
			$data = array();
			foreach ( $hours as $hour ) {
				// if (array_key_exists($hour, $this->namedData))
				$data[] = intval($this->namedData[$hour][$StateId]);
			}
			$row['data'] = $data;
			$res[] = $row;
		}
		return $res;
	}
	public function Byhours_getExistingHours() {
		$res = array();
		if (count($this->namedData != false) && count($this->allData) != false) {
			foreach ( $this->namedData as $name => $v ) {
				$res[] = $name . Yii::t('m', 'h');
			}
		}
		return $res;
	}
	public function Byhours_getPieSeries() {
		$res = array();
		$dataAll = array();
		$dataGroup = array();
		
		if (count($this->namedData != false) && count($this->allData) != false) {
			$d = array();
			foreach ( self::$UsesStates as $StateId )
				$d[$StateId] = 0;
			foreach ( $this->namedData as $name => $v ) {
				foreach ( self::$UsesStates as $StateId )
					$d[$StateId] += $v[$StateId];
			}
			
			foreach ( $d as $StateId => $v ) {
				if ($v != 0)
					$dataAll[] = array(
							'y' => getPercent($v, $this->allData['allCount']),
							'name' => $this->GetStateCaption($StateId),
							'color' => $this->GetStateColor($StateId));
			}
		}
		
		$res[] = array(
				'name' => Yii::t('m', 'All calls'),
				'data' => $dataAll,
				'size' => '80%',
				'dataLabels' => array('distance' => 20,'color' => 'black'));
		
		return $res;
	}
	public function Byhours_GenerateLinksContent() {
		$res = '';
		$hours = array();
		for($n = 1; $n < 25; $n ++) {
			$hours[] = $n;
		}
		
		$hbq = '';
		$SearchTmp = new CallsLogSearchForm();
		if (isset($_REQUEST[get_class($SearchTmp)]))
			$SearchTmp->attributes = $_REQUEST[get_class($SearchTmp)];
		$res .= CHtml::openTag('div', array('id' => 'links','style' => 'display:none'));
		$date = new DateTime($this->sfCallDateTime);
		foreach ( $hours as $hour ) {
			$res .= CHtml::openTag('span');
			$res .= '/callslog?';
			$date1 = $date;
			$SearchTmp->sfCallDateTime = $date1->format('Y-m-d H:i:s');
			$date = date_add($date, new DateInterval('PT1H'));
			$date2 = $date;
			$SearchTmp->sfCallEndDateTime = $date2->format('Y-m-d H:i:s');
			yii::log($hour . ': ' . $SearchTmp->sfCallDateTime . ' - ' . $SearchTmp->sfCallEndDateTime);
			$hbq = http_build_query(array(get_class($SearchTmp) => $SearchTmp));
			$res .= $hbq;
			$res .= CHtml::closeTag('span');
		}
		$res .= CHtml::closeTag('div');
		return $res;
	}
}

