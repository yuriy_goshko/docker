<?php
class CallsLogModel extends CActiveRecord {
	public $DetCount;
	public $FullNameSrc;
	public $FullNameDst;
	public $FullNameOut;
	public $StateName;
	public $DirName;
	public $ConfNum;
	public $ConfUnId;
	public $ConvErr;
	public $exSrc;
	public $exDst;
	public $audioFile;
	public $IsAgAnsw;
	public $Deps;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'CI_Calls';
	}
	public function getFullNameDstContent() {
		return CallsUtils::GenereteFullNameContent($this->NumDst, $this->NameDst, $this->exDst);
	}
	public function getFullNameSrcContent() {
		return CallsUtils::GenereteFullNameContent($this->NumSrc, $this->NameSrc, $this->exSrc);
	}
	public function getBridgeDurationContent() {
		return CallsUtils::GenerateBridgeDurationContent($this);
	}
	public function getStateNameContent() {
		return CallsUtils::GenereteStateNameContent($this);
	}
	public function rules() {
		return array(array('Id','integerOnly' => true),array('DetCount','safe'),);
	}
	public function getHasDetailsCaption() {
		$has = ($this->DetCount > 0);
		$has = $has && ! (($this->DetCount == 1) && ($this->DirectionId == 'Conference'));
		$has = $has && ! (($this->DetCount == 1) && ($this->DirectionId == 'CallbackInit'));
		return $has;
	}
	public function scopes() {
		$agNum = yii::app()->user->getState(WebUser::$KEY_AGENT, 0);
		if ($agNum > 0) {
			$IsAgAnswField = 'if((c.`StateId` like "ANSWERED%") and ((c.`NumDst`="[Num]") or (c.`NumSrc`="[Num]")), 1, 
 			(select 1 from `CI_CallsDetail` cda where cda.`CallId`=c.`Id` 
			and (cda.`StateId` like "ANSWERED%") and ((cda.`NumDst`="[Num]") or (cda.`NumSrc`="[Num]")) limit 1))';
			$IsAgAnswField = str_replace('[Num]', $agNum, $IsAgAnswField);
		} else
			$IsAgAnswField = '0';
		
		$selectDefault = array(
				'c.Id, c.DirectionId, CallDateTime, CallEndDateTime, NumSrc, NumDst, TransferFrom, TransferTo',
				'NumOut, Duration, BridgeDuration, CanceledOutgoing, NameSrc, NameDst, LinkedId',
				'(case when NameSrc="" then NumSrc else CONCAT(NumSrc," (",NameSrc,")") end) FullNameSrc',
				'(case when NameDst="" then NumDst else CONCAT(NumDst," (",NameDst,")") end) FullNameDst',
				'(case when COALESCE(pn.DisplayName,"")="" then NumOut else CONCAT(NumOut," (",pn.DisplayName,")") end) FullNameOut',
				'(select count(1) from `CI_CallsDetail` cd where c.`Id`=cd.`CallId`) DetCount',
				'cs.Id StateId, cs.Name StateName, cd.Name DirName, cc.RoomNum ConfNum, cc.RecordUniqueId ConfUnId',
				'COALESCE(ca.FileName, LinkedId) audioFile',
				$IsAgAnswField . ' IsAgAnsw',
				'isExistingNum(NumSrc, NameSrc) exSrc, isExistingNum(NumDst, NameDst) exDst',
				'(select 1 from CI_Warnings w where w.LinkedId=c.LinkedId and w.Type<>"Info" limit 1) ConvErr');
		$selectDefaultDeps = $selectDefault;
		$selectDefaultDeps[] = '(select GROUP_CONCAT(distinct ci.`InfoValue` SEPARATOR ", ") from `CI_CallsInfo` ci where ci.`CallId`=c.`Id` and ci.`InfoType`="DEP_NAME") Deps';
		
		$joinDefault = implode(array(
				'left join CI_States cs on cs.Id=c.StateId ',
				'left join CI_Directions cd on cd.Id=c.DirectionId ',
				'left join CI_Conferences cc on cc.CallId=c.Id ',
				'left join PBXNumbers pn on pn.Number=c.NumOut ',
				'left join CI_AudioFiles ca on ca.CallId=c.Id and ca.Parsed="Y" '));
		return array(
				'default' => array('select' => $selectDefault,'join' => $joinDefault,'alias' => 'c'),
				'defaultDeps' => array('select' => $selectDefaultDeps,'join' => $joinDefault,'alias' => 'c'));
	}
	public static function GetDefaultSortConfig() {
		return array(
				'attributes' => array('*',"StateName" => array(),"DirName" => array(),"ConfNum" => array(),"FullNameSrc" => array(),"FullNameDst" => array()),
				'defaultOrder' => array('CallDateTime' => true));
	}
	public function attributeLabels() {
		return Yii::t('callslog', 'attributeLabels');
	}
	private function addHavingCondition($Cond) {
		if ($this->dbCriteria->having === '')
			$this->dbCriteria->having = '1=1';
		$this->dbCriteria->having = $this->dbCriteria->having . ' ' . $Cond;
	}
	public function search($form = false) {
		if ($form->sfLinkedId != false) {
			// для перехода/просмотра из руут раздела
			$this->dbCriteria->addCondition('c.LinkedId = "' . $form->sfLinkedId[0] . '"');
			return;
		}
		
		$this->dbCriteria->addCondition('CallDateTime >= "' . $form->sfCallDateTime . '"');
		$this->dbCriteria->addCondition('CallDateTime < "' . $form->sfCallEndDateTime . '"');
		$this->dbCriteria->addCondition(DataModule::DepsCond());
		
		if ($form->sfSpecFilter != false) {
			if (in_array('MissedCalls', $form->sfSpecFilter)) {
				$s = '(c.StateId <> "ANSWERED") and (c.DirectionId = "Inbound")';
				$this->dbCriteria->addCondition($s);
			}
		}
		
		if ($form->sfStates != false) {
			$items = $form->sfStates;
			$i = 0;
			foreach ( $items as $State ) {
				$items[$i] = '(c.StateId like "' . $State . '%")';
				$i ++;
			}
			$s = implode(' or ', $items);
			$this->dbCriteria->addCondition($s);
		}
		
		if ($form->sfDirections != false) {
			$items = $form->sfDirections;
			$i = 0;
			foreach ( $items as $Direction ) {
				$items[$i] = '(c.DirectionId like "' . $Direction . '%")';
				$i ++;
			}
			$s = implode(' or ', $items);
			$this->dbCriteria->addCondition($s);
		}
		
		if ($form->sfPBXNum != false) {
			$s = implode('","', $form->sfPBXNum);
			$s = 'c.Numout in ("' . $s . '") or exists (select 1 from CI_CallsDetail cd where c.Id=cd.CallId and cd.Numout in ("' . $s . '")';
			if ((Yii::app()->params['FindByCallbakInit'] == 1) && ($form->sfDirections == array('Callback')))
				$s .= ' and cd.DirectionId="CallbackInit")';
			else
				$s .= ')';
			$s = '(' . $s . ')';
			$this->dbCriteria->addCondition($s);
		}
		
		$agNum = yii::app()->user->getState(WebUser::$KEY_AGENT, 0);
		if ($agNum > 0) {
			$s = '
			and (((NumSrc = "[Number]") or (NumDst = "[Number]") or (TransferFrom = "[Number]") or (TransferTo = "[Number]")) or
			(exists (select 1 from CI_CallsDetail cd where c.Id=cd.CallId and
			((cd.NumSrc = "[Number]") or (cd.NumDst = "[Number]") or (cd.TransferFrom = "[Number]") or (cd.TransferTo = "[Number]")))))';
			$s = str_replace('[Number]', $agNum, $s);
			$this->addHavingCondition($s);
		}
		
		if ($form->sfNum != false) {
			$s = '
			and (((NumSrc like "[Number]") or (NumDst like "[Number]") or (NumOut like "[Number]") or (TransferFrom like "[Number]") or (TransferTo like "[Number]")) or 
			(exists (select 1 from CI_CallsDetail cd where c.Id=cd.CallId and 
			((cd.NumSrc like "[Number]") or (cd.NumDst like "[Number]") or (cd.NumOut like "[Number]") or (cd.TransferFrom like "[Number]") or (cd.TransferTo like "[Number]")))))';
			$s = str_replace('[Number]', '%' . $form->sfNum . '%', $s);
			$this->addHavingCondition($s);
		}
		if (isset($_REQUEST['nod'])) {
			// для поиска звонков без отделов
			$s = 'and not (exists (select 1 from `CI_CallsInfo` ci where ci.CallId = c.`Id` and ci.InfoType = "DEP_NAME"))';
			$this->addHavingCondition($s);
		}
		if (isset($_REQUEST['noq'])) {
			// для поиска звонков без очередей
			$s = 'and not (exists (select 1 from `CI_CallsInfo` ci 				   
				  where ci.CallId = c.`Id` and ci.InfoType in ("ENTERQUEUE", "QUEUE_NAME")))';
			$this->addHavingCondition($s);
		}
		if ($form->sfQueues != false) {
			$s = 'and (exists (select 1 from `CI_CallsInfo` ci where ci.CallId = c.`Id` 
				  and ci.`InfoValue` in	("[Queues]") and ci.InfoType in ("ENTERQUEUE", "QUEUE_NAME")))';
			$s = str_replace('[Queues]', implode('","', $form->sfQueues), $s);
			$this->addHavingCondition($s);
		}
		
		if ($form->sfDepartments != false) {
			$s = 'and ' . DataModule::DepsCondByArr($form->sfDepartments);
			$this->addHavingCondition($s);
		}
		if ($form->sfAgents != false) {
			$s = 'and ((FullNameSrc in ([Agents])) or (FullNameDst in ([Agents])) or 
				  (exists (select 1 from CI_CallsDetail cd where c.Id=cd.CallId
 				  and (((case when cd.NameSrc="" then cd.NumSrc else CONCAT(cd.NumSrc," (",cd.NameSrc,")") end) in ([Agents]))
				  or ((case when cd.NameDst="" then cd.NumDst else CONCAT(cd.NumDst," (",cd.NameDst,")") end) in ([Agents]))))))';
			$agnts = '"' . implode('","', $form->sfAgents) . '"';
			$s = str_replace('[Agents]', $agnts, $s);
			$this->addHavingCondition($s);
		}
	}
	public static function GetDetailModel() {
		return CallsDetailLogModel::model();
	}
}
class CallsDetailLogModel extends CActiveRecord {
	public $FullNameSrc;
	public $FullNameDst;
	public $FullNameOut;
	public $StateName;
	public $DirName;
	public $ConfNum;
	public $exSrc;
	public $exDst;
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function tableName() {
		return 'CI_CallsDetail';
	}
	public function getFullNameDstContent() {
		return CallsUtils::GenereteFullNameContent($this->NumDst, $this->NameDst, $this->exDst);
	}
	public function getFullNameSrcContent() {
		return CallsUtils::GenereteFullNameContent($this->NumSrc, $this->NameSrc, $this->exSrc);
	}
	public function getStateNameContent() {
		return CallsUtils::GenereteStateNameContent($this);
	}
	public function rules() {
		return array('Id, CallId','integerOnly' => true);
	}
	public function attributeLabels() {
		return Yii::t('callslog', 'attributeLabels');
	}
	public static function GetDefaultSortConfig() {
		return array(
				'attributes' => array('*',"StateName" => array(),"DirName" => array(),"ConfNum" => array(),"FullNameSrc" => array(),"FullNameDst" => array()),
				'defaultOrder' => 'c.Id');
	}
	public function scopes() {
		return array(
				'default' => array(
						'select' => array(
								'c.CallId, CallDateTime, CallEndDateTime, NumSrc, NumDst, TransferFrom, TransferTo',
								'NumOut, Duration, BridgeDuration, CanceledOutgoing, NameSrc, NameDst',
								'(case when NameSrc="" then NumSrc else CONCAT(NumSrc," (",NameSrc,")") end) FullNameSrc',
								'(case when NameDst="" then NumDst else CONCAT(NumDst," (",NameDst,")") end) FullNameDst',
								'(case when COALESCE(pn.DisplayName,"")="" then NumOut else CONCAT(NumOut," (",pn.DisplayName,")") end) FullNameOut',
								'cs.Name StateName, cd.Name DirName, cc.RoomNum ConfNum',
								'isExistingNum(NumSrc, NameSrc) exSrc, isExistingNum(NumDst, NameDst) exDst'),
						'join' => implode(array(
								'left join CI_States cs on cs.Id=c.StateId ',
								'left join CI_Directions cd on cd.Id=c.DirectionId ',
								'left join PBXNumbers pn on pn.Number=c.NumOut ',
								'left join CI_Conferences cc on cc.CallId=c.CallId ')),
						'condition' => 'c.CallId=:cid',
						'alias' => 'c',
						'order' => 'c.Id'));
	}
}
