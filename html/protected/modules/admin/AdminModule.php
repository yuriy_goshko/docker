<?php
class AdminModule extends CWebModule {
	public $defaultController = 'agents';
	public function init() {
		Yii::app()->theme = 'admin';
		$this->setImport(array('admin.models.*','admin.components.*'));
		$cssPath = Yii::app()->assetManager->publish(implode('/', array(Yii::app()->basePath,'..','css'))) . '/';
		Yii::app()->clientScript->registerCssFile($cssPath . 'styles.css');
	}
	public function beforeControllerAction($controller, $action) {
		if (parent::beforeControllerAction($controller, $action)) {
			$controller->setPageTitle('Администраторский раздел');
			$controller->layout = '//layouts/admin';
			$controller->menu = array();
			$controller->breadcrumbs = array();
			return true;
		} else
			return false;
	}
}
