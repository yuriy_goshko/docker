<?php
class RootModule extends CWebModule {
	// public $urlRules = array('root/login' => 'root/default/login');
	public $urlRules = array(
			'root/login' => 'root/default/login',
			'root/delete/id/<id>' => 'root/default/delete',
			'root/<m:(addedit|addeditm|addeditr)>/id/<id>' => 'root/default/addedit',
			'root/<m:(addedit|addeditm|addeditr)>' => 'root/default/addedit');
	public $modules = array('groups','channels','channelsmembers','providers','statistic');
	public function init() {
		$this->setModules($this->modules);
		$this->setImport(array('root.models.*','root.components.*'));
		$cssPath = Yii::app()->assetManager->publish(implode('/', array(Yii::app()->basePath,'..','css'))) . '/';
		Yii::app()->clientScript->registerCssFile($cssPath . 'styles.css');
	}
	public function beforeControllerAction($controller, $action) {
		if (parent::beforeControllerAction($controller, $action)) {
			Yii::app()->user->loginUrl = '/root/login';
			$controller->setPageTitle('Рут раздел');
			
			if (trim($controller->layout) != '//layouts/root/2column')
				$controller->layout = '//layouts/root';
			$controller->menu = array();
			$controller->breadcrumbs = array();
			return true;
		} else
			return false;
	}
}
