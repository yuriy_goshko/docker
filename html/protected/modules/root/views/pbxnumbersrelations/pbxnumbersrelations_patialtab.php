<?
$checkBoxHtmlOptions = array('data-toggle' => 'tooltip','data-placement' => 'right','data-title' => Yii::t('m', 'Select an item'));

$this->widget('bootstrap.widgets.TbGridView', array(
		'dataProvider' => $dataProvider,
		'template' => '{items}',
		'enableHistory' => true,
		'enableSorting' => true,
		'selectableRows' => 0,
		'id' => 'gm_' . $PBXnum['Number'],
		'afterAjaxUpdate' => 'js:init',
		'filter' => $filtersForm,
		'columns' => array(
				array(
						'header' => 'Тип',
						'name' => "TypeCaption",
						'cssClassExpression' => '"TypeCaption"',
						'filter' => CHtml::tag('label', array('class' => 'designed-select'), CHtml::dropDownList('FiltersForm[Type]', $filtersForm->Type, array(
								null => 'Все значения',
								0 => 'Отдел',
								1 => 'Канал',
								2 => 'Очередь')))),
				
				array(
						'header' => 'Название',
						'name' => "Name",
						'cssClassExpression' => '"Name"',
						'filter' => CHtml::tag('label', array('class' => 'designed-select'), CHtml::dropDownList('FiltersForm[CheckedAny]', $filtersForm->CheckedAny, array(
								null => 'Все значения',
								1 => 'Отмеченные')))),
				array(
						'class' => 'CCheckBoxColumn',
						'selectableRows' => 2,
						'disabled' => '($data["Type"]!=PbxnumbersrelationsController::$rt_DEP)',
						'cssClassExpression' => '($data["Type"]!=PbxnumbersrelationsController::$rt_DEP)?"hideCB":""',
						'headerTemplate' => '<div data-toggle = "tooltip" data-placement="right" data-title="Входящие/Отзвон звонки">Входящие</div>',
						'checkBoxHtmlOptions' => $checkBoxHtmlOptions,
						'id' => 'gm' . $PBXnum['Number'] . '_' . 'IcomChecked',
						'checked' => '($data["Type"]===PbxnumbersrelationsController::$rt_DEP)?$data["IcomChecked"]:false'),
				array(
						'class' => 'CCheckBoxColumn',
						'selectableRows' => 2,
						'disabled' => '($data["Type"]!=PbxnumbersrelationsController::$rt_DEP)',
						'cssClassExpression' => '($data["Type"]!=PbxnumbersrelationsController::$rt_DEP)?"hideCB":""',
						'headerTemplate' => '<div data-toggle = "tooltip" data-placement="right" data-title="Только исходящие звонки">Исходящие</div>',
						'checkBoxHtmlOptions' => $checkBoxHtmlOptions,
						'id' => 'gm' . $PBXnum['Number'] . '_' . 'OutcomChecked',
						'checked' => '($data["Type"]===PbxnumbersrelationsController::$rt_DEP)?$data["OutcomChecked"]:false'),
				array(
						'class' => 'CCheckBoxColumn',
						'selectableRows' => 2,
						'disabled' => '($data["Type"]===PbxnumbersrelationsController::$rt_DEP)',
						'cssClassExpression' => '($data["Type"]===PbxnumbersrelationsController::$rt_DEP)?"hideCB":""',
						'headerTemplate' => '<div data-toggle = "tooltip" data-placement="right" data-title="На один тип одна связь">Связь</div>',
						'checkBoxHtmlOptions' => $checkBoxHtmlOptions,
						'id' => 'gm' . $PBXnum['Number'] . '_' . 'checked',
						'checked' => '($data["Type"]!=PbxnumbersrelationsController::$rt_DEP)?$data["Checked"]:false'))));

?>
