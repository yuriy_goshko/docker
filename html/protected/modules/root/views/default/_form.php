<?php
/* @var $this StaticPagesController */
/* @var $model StaticPages */
/* @var $form CActiveForm */
?>

<?
$flashes = array(
    'addedit' => array(
        'update' => 'Обновить страницу',
        'add' => 'Добавить новую страницу'
    ),
    'addeditr' => array(
        'update' => 'Обновить ссылку',
        'add' => 'Добавить новую ссылку'
    ),
    'addeditm' => array(
        'update' => 'Обновить меню',
        'add' => 'Добавить новое меню'
    )
        )
?>

<script type="text/javascript">
    menuParentID = <?= StaticPages::menuParentID ?>
</script>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'static-pages-_form-form',
    'type' => 'horizontal',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>
<fieldset>

    <legend><? if ($model->isNewRecord): ?><?= $flashes[$model->spType]['add'] ?><? else: ?><?= $flashes[$model->spType]['update'] ?><? endif; ?></legend>

    <?php echo $form->errorSummary($model); ?>

    <?php
    $this->widget('bootstrap.widgets.TbAlert', array(
        'block' => true, // display a larger alert block?
        'fade' => true, // use transitions?
        'closeText' => '&times;', // close link text - if set to false, no close link is displayed
        'alerts' => array(// configurations per alert type
            'success' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
        // success, info, warning, error or danger
        ),
    ));
    ?>

    <?php echo $form->textFieldRow($model, 'spTitle'); ?>
    <?php if ($model->isNewRecord && $model->spType != "addeditm") echo $form->dropDownListRow($model, 'spParentID', StaticPages::menuList($ismenu)); ?>
    <? if ($model->spType != "addeditm"): ?>
        <?php if ($model->spType != "addedit") echo $form->textFieldRow($model, 'spExtUrl', array('hint' => 'Укажите внешнюю ссылку или относительную на екшин (пример admin/users)')); ?>
        <? if ($model->spType != "addeditr"): ?>    
            <?php echo $form->textAreaRow($model, 'spKey'); ?>
            <?php echo $form->textAreaRow($model, 'spDesc'); ?>

            <?
            $this->widget('application.extensions.yii-redactor.ERedactorWidget', array(
                'model' => $model,
                'attribute' => 'spText',
                'options' => array(
                    'lang' => Yii::app()->language,
                    'toolbar'=>'custom',
                    'fileUpload' => Yii::app()->createUrl('/root/default/fileupload', array(
                        'attr' => 'spText',
                    )),
                    'fileUploadErrorCallback' => new CJavaScriptExpression(
                            'function(obj,json) { alert(json.error); }'
                    ),
                    'imageUpload' => Yii::app()->createUrl('/root/default/imageupload', array(
                        'attr' => 'spText'
                    )),
                    'imageGetJson' => Yii::app()->createUrl('/root/default/imagelist', array(
                        'attr' => 'spText'
                    )),
                    'imageUploadErrorCallback' => new CJavaScriptExpression(
                            'function(obj,json) { alert(json.error); }'
                    ),
                ),
            ));
            ?>

        <? endif; ?>
    <? endif; ?>
    <?php echo $form->checkBoxRow($model, 'spVisible') ?>
    <?php echo $form->textFieldRow($model, 'spHtmlClass') ?>




</fieldset>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $model->isNewRecord ? "Добавить" : "Обновить")); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Сброс')); ?>
</div>

<?php $this->endWidget(); ?>
