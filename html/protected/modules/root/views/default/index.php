<?php
/* @var $this DefaultController */
?>
<? if (count($rows) != false): ?>
    <div id="pages">
        <div class="grid-view">
            <ol class="sortable">
                <? foreach ($rows as $row): ?>
                    <?php
                    echo $this->renderPartial('_pages', array('model' => $row,
                        'incLevel' => 0,
                        'urlParts' => array('p0' => $row->spUrl),
                        'childs' => StaticPages::model()->ordered()->findAllByAttributes(array("spParentID" => $row->spID))
                    ));
                    ?>
                <? endforeach; ?>
            </ol>
        </div>
    </div>
<? endif; ?>
