<?$incLevel++?>
<li id="page_<?= $model->spID; ?>" >

    <div class=<?= !$model->spVisible ? 'invizibil' : "" ?> <?= $model->spParentID == StaticPages::menuParentID ? 'menu' : "" ?>>
        <?= $model->spTitle ?>
        <? if ($model->spParentID != StaticPages::menuParentID): ?>
            <?php if ($model->spType == "addeditr"): ?> 
                <span>Exturl: <? echo $model->spExtUrl; ?></span>
            <? else: ?>
                <input type="text" value="<?=Yii::app()->request->hostInfo.Yii::app()->getUrlManager()->createUrl('site/view', $urlParts)?>" />
            <? endif; ?>
        <? endif; ?>

        <? if ($model->spParentID == StaticPages::menuParentID && StaticPages::model()->menus()->count() > 1): ?>
            <a href="<?= Yii::app()->getUrlManager()->createUrl($this->module->id.'/delete', array('id' => $model->spID)) ?>" onclick="return confirm('Вы уверены, что хотите удалить меню?')" class="del">Удалить</a>
        <? endif; ?>

        <? if ($model->spParentID != StaticPages::menuParentID ): ?>
            <a href="<?= Yii::app()->getUrlManager()->createUrl($this->module->id.'/delete', array('id' => $model->spID)) ?>" onclick="return confirm('Вы уверены, что хотите удалить страницу?')" class="del">Удалить</a>
        <? endif; ?>

        <a href="<?= Yii::app()->getUrlManager()->createUrl($this->module->id.'/' . $model->spType, array('id' => $model->spID)) ?>" class="edit">Ред.</a>

    </div>
    <? if (count($childs) != false): ?>
        <ol class="sortable_<?= $model->spID ?>">
            <? foreach ($childs as $row): ?>
                <?php
                $urlParts['p'.$incLevel] = $row->spUrl;
                echo $this->renderPartial('_pages', array(
                    'model' => $row,
                    'incLevel'=>$incLevel,
                    'urlParts'=>$urlParts,
                    'childs' => StaticPages::model()->ordered()->findAllByAttributes(array("spParentID" => $row->spID))
                ));
                ?>
            <? endforeach; ?>
        </ol>
    <? endif; ?>
</li>

