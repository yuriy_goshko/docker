<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
		YiiBase::getPathOfAlias('webroot'),
		'protected',
		'modules',
		'root',
		'js'))) . '/' . 'PBXNumbers.js', CClientScript::POS_BEGIN);

$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => true,
		'fade' => true,
		'closeText' => '&times;',
		'alerts' => array('success' => array('block' => true,'fade' => true,'closeText' => '&times;'))));
?>
<div class="row">
	<div class="form">
    <?php
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
						'id' => 'PBXNumbers',
						'enableClientValidation' => true,
						'clientOptions' => array('validateOnSubmit' => true),
						'method' => 'post',
						'action' => '/root/PBXNumbers/add'));
				
				?>
    
    <? if (Yii::app()->user->hasFlash('error')): ?>
        <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
    <? endif; ?>

    <div class="row">
			<div class="span3">
            <?=$form->textFieldRow($model, 'Number', array('value' => $model->Number));?>
        </div>
			<div class="span3">            	
            	<?= $form->labelEx($model, 'Type'); ?>
            	<?php $TypesList = array('SIP' => 'SIP','Dongle' => 'Dongle','IAX' => 'IAX','DAHDI' => 'DAHDI');?>
            	<?= $form->dropDownList($model, 'Type', $TypesList); ?>          	            	
        	</div>
			<div class="span3">
            	<?=$form->textFieldRow($model, 'Name', array('value' => $model->Name));?>
        	</div>
		</div>
		<div class="row">
			<div class="span3">
            	<?=$form->textFieldRow($model, 'DisplayName', array('value' => $model->DisplayName));?>
        	</div>
			<div class="span3">                  
                <?= $form->labelEx($model, 'ProviderId'); ?>
                <?php
																$prList = Providers::model()->findAll();
																array_unshift($prList, array('pID' => null,'pName' => '-'));
																$prList = Chtml::listData($prList, 'pID', 'pName');
																?>
                
                <?= $form->dropDownList($model, 'ProviderId', $prList); ?>
            </div>
			<div class="span3">
            <?=$form->textFieldRow($model, 'Note', array('value' => $model->Note));?>
        </div>
        	<?php wu::getDepartmensCB($form, $model); ?>    
		</div>

		<div class="row buttons">
			<div class="span3">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Добавить')); ?>
            <?php
												
												$this->widget('bootstrap.widgets.TbButton', array(
														'buttonType' => 'reset',
														'label' => 'Сброс',
														'htmlOptions' => array('id' => 'reset')));
												?>
        </div>
		</div>

    <?php $this->endWidget(); ?>
</div>
</div>
<div class="row">
    <?php
				$gridId = 'pbxnumbers-grid';
				$this->widget('bootstrap.widgets.TbGridView', array(
						'dataProvider' => $dataProvider,
						'ajaxUpdate' => false,
						'id' => $gridId,
						'rowCssClassExpression' => '$data["wDep"] != 1 ? "c_nodep" : ""',
						'columns' => array(
								array('name' => 'Number','cssClassExpression' => '"Number"','value' => '$data->Number'),
								array('name' => 'Type','cssClassExpression' => '"Type"'),
								array('name' => 'Name','cssClassExpression' => '"Name"'),
								array(
										'name' => 'DisplayName',
										'cssClassExpression' => '"DisplayName"',
										'value' => '($data->DisplayName === null) ? "" : $data->DisplayName'),
								array(
										'name' => 'ProviderId',
										'cssClassExpression' => '"ProviderId"',
										'htmlOptions' => array('style' => 'display:none'),
										'headerHtmlOptions' => array('style' => 'display:none')),
								array('name' => 'ProviderName'),
								array('name' => 'Note','cssClassExpression' => '"Note"','value' => '$data->Note'),
								array(
										'header' => wu::pagPageSizeList($gridId, 'pbxNumbersPGS'),
										'class' => 'CButtonColumn',
										'template' => '{update} {delete}',
										'buttons' => array(
												'update' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-edit addpbn')),
												'delete' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-remove-circle')))))));
				?>
</div>