<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
		YiiBase::getPathOfAlias('webroot'),
		'protected',
		'modules',
		'root',
		'js'))) . '/' . 'settings.js', CClientScript::POS_BEGIN);

?>
<div class="row">
	<div class="form">
    	<?php echo $content;?>
	</div>
</div>
