<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
		YiiBase::getPathOfAlias('webroot'),
		'protected',
		'modules',
		'root',
		'js'))) . '/' . 'citynames.js', CClientScript::POS_BEGIN);

$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => true,
		'fade' => true,
		'closeText' => '&times;',
		'alerts' => array('success' => array('block' => true,'fade' => true,'closeText' => '&times;'))));
?>
<div class="row">
	<div class="form">
    <?php
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
						'id' => 'CityNames',
						'enableClientValidation' => true,
						'clientOptions' => array('validateOnSubmit' => true),
						'method' => 'post',
						'action' => '/root/citynames/add'));
				
				?>
    
    <? if (Yii::app()->user->hasFlash('error')): ?>
        <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
    <? endif; ?>

    <div class="row">
			<div class="span3">
            <?=$form->textFieldRow($model, 'Name', array('value' => $model->Name));?>
        </div>
			<div class="span3">
            <?=$form->textFieldRow($model, 'DisplayName', array('value' => $model->DisplayName));?>
        </div>
		</div>

		<div class="row buttons">
			<div class="span3">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Добавить')); ?>
            <?php
												
												$this->widget('bootstrap.widgets.TbButton', array(
														'buttonType' => 'reset',
														'label' => 'Сброс',
														'htmlOptions' => array('id' => 'reset')));
												?>
        </div>
		</div>

    <?php $this->endWidget(); ?>
</div>
</div>
<div class="row">
    <?php
				$this->widget('bootstrap.widgets.TbGridView', array(
						'dataProvider' => $dataProvider,
						'ajaxUpdate' => false,
						'columns' => array(
								
								array('name' => 'Name','cssClassExpression' => '"Name"'),
								array('name' => 'DisplayName','cssClassExpression' => '"DisplayName"'),
								array(
										'header' => 'Действия',
										'class' => 'CButtonColumn',
										'template' => '{update} {delete}',
										'buttons' => array(
												'update' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-edit addpbn')),
												'delete' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-remove-circle')))))));
				?>
</div>