<div class="row">
	<div class="form">
        <?php
								$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
										'id' => 'directionstodepsForm',
										'enableClientValidation' => true,
										'clientOptions' => array('validateOnSubmit' => true),
										'method' => 'post',
										'action' => 'directionstodeps/save'));
								?>

        <div class="row">
			<div class="span3">         
               <?php echo CHtml::label('Ситуации (шаблоны)', ''); ?>
	           <?php
												
												$itemsarr = $this->GetItems();
												echo CHtml::dropDownList('listTamplates', 'joint_A', $itemsarr, array('style' => 'width:100%'));
												?>               
            </div>
			<div class="span3" align="left" style="margin-top: 25px">                            	                       
                <?php
																$this->widget('bootstrap.widgets.TbButton', array(
																		'buttonType' => 'reset',
																		'label' => 'Заполнить',
																		'htmlOptions' => array('id' => 'reset','style' => '')));
																?>
            </div>
		</div>

	</div>
</div>
<div class="row">
    <?php
				$this->widget('bootstrap.widgets.TbGridView', array(
						'dataProvider' => $dataProvider,
						'ajaxUpdate' => false,
						'id' => 'grdDirections',
						'columns' => array(
								array(
										'id' => '"Id"',
										'header' => $model->getAttributeLabel("Id"),
										'name' => '"Id"',
										'cssClassExpression' => '"Id"',
										'type' => 'raw',
										'value' => function ($data) {
											if ($data->Used == 1)
												return CHtml::tag('color ', array('style' => ''), $data->Id);
											else
												return CHtml::tag('color ', array('style' => 'font-style: italic;'), $data->Id);
										}),
								array(
										'header' => $model->getAttributeLabel("Name"),
										'name' => '"Name"',
										'cssClassExpression' => '"Name"',
										'type' => 'raw',
										'value' => function ($data) {
											if ($data->Used == 1)
												return CHtml::tag('color ', array('style' => ''), $data->Name);
											else
												return CHtml::tag('color ', array('style' => 'font-style: italic;'), $data->Name);
										}),
								array(
										'id' => 'DepsLinkByPBX',
										'value' => '$data->Id',
										'cssClassExpression' => '($data["Id"] == "Local")?"hideCB":""',
										'class' => 'CCheckBoxColumn',
										'selectableRows' => $selectableRows,
										'name' => '"DepsLinkByPBX"',
										'headerTemplate' => '<div data-toggle = "tooltip" data-placement="right" data-title="По PBX номерам">PBX</div>',
										'checkBoxHtmlOptions' => array(
												'data-toggle' => 'tooltip',
												'data-placement' => 'right',
												'data-title' => Yii::t('m', 'Select an item')),
										'checked' => function ($data, $row, $column) use($model) {
											return $data->DepsLinkByPBX;
										}),
								array(
										'id' => 'DepsLinkByAgents',
										'value' => '$data->Id',
										'cssClassExpression' => '(($data["Id"] == "ConfOut") || ($data["Id"] == "AutoCall") || ($data["Id"] == "CallbackInit"))?"hideCB":""',
										'class' => 'CCheckBoxColumn',
										'selectableRows' => $selectableRows,
										'name' => '"DepsLinkByAgents"',
										'headerTemplate' => '<div data-toggle = "tooltip" data-placement="right" data-title="По номерам агентов">Агенты</div>',
										'checkBoxHtmlOptions' => array(
												'data-toggle' => 'tooltip',
												'data-placement' => 'right',
												'data-title' => Yii::t('m', 'Select an item')),
										'checked' => function ($data, $row, $column) use($model) {
											return $data->DepsLinkByAgents;
										}))));
				?>
</div>
<div class="row">
	<div class="span5" align="justify">
		<NOBR>
		<?php echo CHtml::label('ТЕКУЩАЯ Ситуация: ', '',array('style'=>"font-weight: bold; display: inline")); ?>		
		<?php echo CHtml::label($this->GetSelectItem($dataProvider), '', array('style'=>"color: navy; font-weight: bold; display: inline")); ?>
		</NOBR>
	</div>
	<div class="row buttons" align="right">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Сохранить')); ?>
	<?php $this->endWidget(); ?>
</div>
</div>