<?php
class PbxnumbersrelationsController extends rootController {
	public static $rt_DEP = 0;
	public static $rt_CHANNEL_DEPRECATED = 1;
	public static $rt_QUEUE = 2;
	private static $rtKeysFields = array('DepartmentId','ChannelId','QueueName');
	public function actionIndex() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
				YiiBase::getPathOfAlias('webroot'),
				'protected',
				'modules',
				'root',
				'js'))) . '/' . 'pbxnumbersrelations.js', CClientScript::POS_BEGIN);
		
		$PBXNumbersDefaultScope = PBXNumbers::model()->defaultScope();
		
		$sql = 'select ' . $PBXNumbersDefaultScope['select'] . ' from `PBXNumbers` n order by Type,Number';
		$command = Yii::app()->db->createCommand($sql);
		$numbers = $command->queryAll();
		if ($numbers == null)
			Yii::app()->user->setFlash('info', "Еще нет записей");
		$dataProvider = new CArrayDataProvider($numbers, array('keyField' => 'Number','pagination' => false));
		$page = 'pbxnumbersrelations_page';
		$this->render($page, array('dataProvider' => $numbers));
	}
	public function actionAddedit($id = false) {
		if (Yii::app()->request->isAjaxRequest) {
			$ids = Yii::app()->request->getParam('ids');
			$ids_i = Yii::app()->request->getParam('ids_i');
			$ids_o = Yii::app()->request->getParam('ids_o');
			$num = Yii::app()->request->getParam('num');
			$ft = Yii::app()->request->getParam('ft');
			$ids_all = array();
			if (isset($ids))
				foreach ( $ids as $value ) {
					$r = json_decode($value, true);
					$r['In'] = null;
					$r['Out'] = null;
					$ids_all[] = $r;
				}
			if (isset($ids_i))
				foreach ( $ids_i as $value ) {
					$r = json_decode($value, true);
					$r['In'] = 1;
					$r['Out'] = null;
					$ids_all[] = $r;
				}
			if (isset($ids_o))
				foreach ( $ids_o as $value ) {
					$r = json_decode($value, true);
					$r['In'] = null;
					$r['Out'] = 1;
					$ids_all[] = $r;
				}
			$transaction = Yii::app()->db->beginTransaction();
			try {
				if ($num === null)
					throw new CHttpException(404, 'cat is null.');
				$sql = 'delete from PBXNumbersRelations where Number = :pNumber ';
				if ($ft != null) {
					$keyName = self::$rtKeysFields[$ft];
					$sql .= 'and ' . $keyName . ' is not null';
				}
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(":pNumber", $num, PDO::PARAM_STR);
				$command->execute();
				
				$Check_OneNumberOneQueue = false;
				$Check_OneNumberOneDepIn = false;
				if (is_array($ids_all)) {
					foreach ( $ids_all as $value ) {
						$TypeKey = (object) ($value);
						$keyName = self::$rtKeysFields[$TypeKey->Type];
						
						if ($TypeKey->Type === self::$rt_QUEUE) {
							if ($Check_OneNumberOneQueue)
								throw new CHttpException(404, 'На один номер одна очередь!');
							else
								$Check_OneNumberOneQueue = true;
						}
						if (($TypeKey->Type === self::$rt_DEP) && ($TypeKey->In === 1)) {
							if ($Check_OneNumberOneDepIn)
								throw new CHttpException(404, 'На один номер однин Отдел-Входящие!');
							else
								$Check_OneNumberOneDepIn = true;
						}
						
						$sql = 'insert into PBXNumbersRelations(Number, ' . $keyName . ', DepartmentIncoming, DepartmentOutcoming) 
								values (:pNumber, :pKey, :pDepartmentIncoming, :pDepartmentOutcoming);';
						$command = Yii::app()->db->createCommand($sql);
						$command->bindParam(":pNumber", $num, PDO::PARAM_STR);
						if ($TypeKey->Type === self::$rt_QUEUE)
							$command->bindParam(":pKey", $TypeKey->Key, PDO::PARAM_STR);
						else
							$command->bindParam(":pKey", $TypeKey->Key, PDO::PARAM_INT);
						$command->bindParam(":pDepartmentIncoming", $TypeKey->In, PDO::PARAM_INT);
						$command->bindParam(":pDepartmentOutcoming", $TypeKey->Out, PDO::PARAM_INT);
						$command->execute();
					}
				}
				$transaction->commit();
			} catch ( Exception $e ) {
				$transaction->rollback();
				echo json_encode(array($e->getMessage(),'error'));
				return;
			}
			echo json_encode(array(Yii::t('m', 'The operation completed successfully'),'info'));
		} else {
			$this->redirect($this->redirectUrl);
		}
	}
	private function FillNumberRelations($data, $sql, $pNumber, $rt) {
		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(":pNumber", $pNumber, PDO::PARAM_STR);
		$dbdata = $command->queryAll();
		$newdata = array();
		foreach ( $dbdata as $r ) {
			$rtCaption = '???';
			switch ($rt) {
				case self::$rt_DEP :
					$rtCaption = 'Отдел';
					break;
				case self::$rt_QUEUE :
					$rtCaption = 'Очередь';
					break;
			}
			$r['TypeCaption'] = $rtCaption;
			$r['Type'] = $rt;
			$r['CheckedAny'] = $r['Checked'] || $r['IcomChecked'] || $r['OutcomChecked'];
			$r['Id'] = json_encode(array('Type' => $rt,'Key' => $r['Key']));
			$newdata[] = $r;
		}
		
		$data = array_merge($data, $newdata);
		return $data;
	}
	protected function getArrayFromAR($model = false) {
		$tId = Yii::app()->request->getParam('tabid');
		$filtersForm = new FiltersForm();
		if (isset($_GET['FiltersForm']))
			$filtersForm->filters = $_GET['FiltersForm'];
		
		$res = array();
		$page = 'pbxnumbersrelations_patialtab.php';
		if (count($model) != false) {
			foreach ( $model as $k => $v ) {
				$data = array();
				$sql = 'select d.`dName` "Name", d.`dID` "Key", sum(COALESCE(nr.`DepartmentIncoming`,0)+COALESCE(nr.`DepartmentOutcoming`,0)) "Checked",
						max(nr.`DepartmentIncoming`) "IcomChecked", max(nr.`DepartmentOutcoming`) "OutcomChecked"  
						from `PBXNumbersRelations` nr
						right join `Departments` d on `d`.`dID`=nr.`DepartmentId` and  nr.`Number` = :pNumber
						group by 1,2
						order by Checked desc, Name';
				$data = $this->FillNumberRelations($data, $sql, $v['Number'], self::$rt_DEP);
				$sql = 'select q.`qName` "Name", q.`name`"Key", if(nr.`QueueName` is null, 0, 1) "Checked",
						nr.`DepartmentIncoming` "IcomChecked", nr.`DepartmentOutcoming` "OutcomChecked" 
						from `PBXNumbersRelations` nr
						right join `Queue` q on `q`.`name` = nr.`QueueName` and nr.`Number` = :pNumber
						order by Checked desc, q.`qName`';
				$data = $this->FillNumberRelations($data, $sql, $v['Number'], self::$rt_QUEUE);
				
				$filteredData = $filtersForm->filter($data);
				$dataProvider = new CArrayDataProvider($filteredData, array('keyField' => 'Id','pagination' => false));
				
				$label = $v['Number'] . ' - ' . $v['Type'];
				$label = (trim($v['DisplayName']) === '') ? $label : $label . ' (' . $v['DisplayName'] . ')';
				
				$itemOptions = array();
				$css = "";
				if ($v['wDep'] != '1')
					$css .= ' c_nodep';
				if ($css != "")
					$itemOptions = array('class' => $css);
				
				$res[$k] = array(
						'label' => $label,
						'id' => $v['Number'],
						'itemOptions' => $itemOptions,
						'content' => $this->renderPartial('pbxnumbersrelations_patialtab', array(
								'filtersForm' => $filtersForm,
								'dataProvider' => $dataProvider,
								'PBXnum' => $v), true));
				if ((($tId === null) && ($k == 0)) || ($tId == $v['Number']))
					$res[$k]['active'] = true;
			}
		}
		return $res;
	}
}