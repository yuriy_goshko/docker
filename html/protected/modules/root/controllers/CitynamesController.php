<?php
class CitynamesController extends rootController {
	public function actionIndex() {
		$model = CityNames::model();
		
		$dataProvider = new CActiveDataProvider('CityNames', array('sort' => array('defaultOrder' => 'Name')));
		$this->render('citynames', array('dataProvider' => $dataProvider,'model' => $model));
	}
	public function AddEdit($id = false) {
		$model = $id != false ? $this->loadModel($id) : new CityNames();
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_REQUEST[get_class($model)];
			if ($model->save()) {
				$id == false ? Yii::app()->user->setFlash('success', "Успешно добавлен!") : Yii::app()->user->setFlash('success', "Успешно обновлен!");
			} else {
				Yii::app()->user->setFlash('error', $model);
			}
		}
		$this->redirect('/root/citynames/');
	}
	public function actionAdd($id = false) {
		$this->AddEdit($id);
	}
	public function actionUpdate($id) {
		$this->AddEdit($id);
	}
	public function actionDelete($id) {
		$this->loadModel($id)->delete();
		
		if (! isset($_GET['ajax']))
			$this->redirect('/root/citynames');
	}
	public function loadModel($id) {
		$model = CityNames::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
}