<?php
class SettingsController extends rootController {
	private $settings = array();
	private function FillSettings() {
		$sql = 'select param, value from Settings;';
		$command = Yii::app()->db->createCommand($sql);
		$data = $command->queryAll();
		$this->settings = CHtml::listData($data, 'param', 'value');
	}
	private function CheckBoxContext($Name, $Caption, $hint = "") {
		$v = false;
		if (isset($this->settings[$Name]))
			if ($this->settings[$Name] === '1')
				$v = true;
		
		$hint = str_replace('/n', chr(13), $hint);
		$content = CHtml::openTag('div', array('class' => 'row-fluid'));
		$content .= CHtml::CheckBox($Name, $v, array('style' => 'float: left','action' => 'settings/edit','title' => $hint));
		$content .= CHtml::label('&nbsp;' . $Caption, $Name, array('style' => 'float: left;','title' => $hint));
		$content .= CHtml::closeTag('div');
		return $content;
	}
	private function EditContext($Name, $Caption, $hint = "") {
		$v = '';
		if (isset($this->settings[$Name]))
			$v = $this->settings[$Name];
		$hint = str_replace('/n', chr(13), $hint);
		$content = CHtml::openTag('div', array('class' => 'row-fluid'));
		$content .= CHtml::label($Caption . '&nbsp;&nbsp;', $Name, array('style' => 'float: left; padding-top: 4px;','title' => $hint));
		$content .= CHtml::textField($Name, $v, array('style' => 'float: left','title' => $hint));
		$content .= '&nbsp;&nbsp;';
		$content .= CHtml::button('Обновить', array('name' => $Name,'action' => 'settings/edit','style' => 'padding-top: 4px;'));
		$content .= CHtml::closeTag('div');
		return $content;
	}
	private function Header($Caption) {
		return "<legend>$Caption</legend>";
	}
	public function actionIndex() {
		$adminOption = 'Опция для отладки./nНе оставлять включенной без необходимости';
		$this->FillSettings();
		$content = '';
		$content .= $this->Header('Статистика');
		$content .= $this->EditContext('StatCaption', 'Название');
		$content .= $this->CheckBoxContext('ShowQueueFilters', 'Отображать фильтры по очередям');
		$content .= $this->CheckBoxContext('FindByCallbakInit', 'При фильтре "Отзвон + PBX номер" поиск по "Инициализации отзвона"');
		$content .= $this->CheckBoxContext('FindAudioArchOnly', 'Искть аудиофайлы только в "архиве"(без дат)');
		$content .= $this->CheckBoxContext('MarkWarnings', 'Подкрашивать звонки с варнингами', $adminOption);
		$content .= $this->Header('Нотификатор автозвонков');
		$content .= $this->CheckBoxContext('tnsNotifierActive', 'Включить нотификатор автозвонков');
		$content .= $this->EditContext('tnsNotifierHost', 'Нотификатор, хост');
		$content .= $this->EditContext('tnsNotifierPort', 'Нотификатор, порт');
		$content .= $this->Header('Отделы');
		$content .= $this->CheckBoxContext('DirToDepsSelect', 'Разрешить редактировать "Разделение по отделам"');
		$content .= $this->EditContext('DefDepKeyName', 'Отдел по умолчанию');
		$content .= $this->CheckBoxContext('ShowLinkDeps', 'Отображать поле "Отделы"(только для отладки)', $adminOption);
		$content .= $this->Header('Внешняя конференция');
		$content .= $this->CheckBoxContext('CCSCommandsListen', 'Включить CCS прием комманд(вн.конференция..)');
		$content .= $this->EditContext('CCSCommandsPort', 'CCS прием комманд, порт');
		$content .= $this->EditContext('ContextConfOut', 'Контекст "Внешняя конф."');
		$content .= $this->EditContext('ContextConfOutAudio', 'Контекст "Внешняя конф. запись"');
		$content .= $this->CheckBoxContext('ContextConfConvNums', '"Внешняя конф." - дозвон наоборот');
		$content .= $this->Header('Доступ агентов');
		$content .= $this->CheckBoxContext('AgentCanAccess', 'Разрешить доступ', 'Необходимо задать роль агенту');
		$content .= $this->EditContext('AgentIpFilters', 'IP Filters(для агентов)', 'Задаются через запятую, допустимы маски');
		$content .= $this->EditContext('AgentRoleDefault', 'Роль по умолчанию', 'Автоподставляется при создании');
		
		$content .= $this->Header('CCS/C-Phone');
		$content .= $this->CheckBoxContext('AMIEnabledLog', 'Логирование всех AMI событий', 'CCS пишет в лог все что получает от AMI./nАвтоматическая очистка логов за неделю и более. ');
		$content .= $this->CheckBoxContext('CPAutoAnswerDisable', 'Запретить использование "Автоответ" в C-Phone');
		$this->render('settings', array('content' => $content));
	}
	public function actionEdit() {
		if (Yii::app()->request->isAjaxRequest) {
			try {
				$param = Yii::app()->request->getParam('Name');
				$val = Yii::app()->request->getParam('Value');
				$sql = 'INSERT INTO Settings (param, value) VALUES(:param, :val)
						ON DUPLICATE KEY UPDATE param=:param, value=:val;';
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(":val", $val, PDO::PARAM_STR);
				$command->bindParam(":param", $param, PDO::PARAM_STR);
				$command->execute();
				echo json_encode(array(Yii::t('m', 'The operation completed successfully'),'info'));
			} catch ( Exception $e ) {
				echo json_encode(array($e->getMessage(),'error'));
				return;
			}
		} else {
			$this->redirect('settings');
		}
	}
}