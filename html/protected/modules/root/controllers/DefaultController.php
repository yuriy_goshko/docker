<?php
class DefaultController extends rootController {
	public $layout = '//layouts/root/2column';
	public function actions() {
		return array(
				'fileupload' => array('class' => 'application.extensions.yii-redactor.actions.FileUpload'),
				'imageupload' => array('class' => 'application.extensions.yii-redactor.actions.ImageUpload'),
				'imagelist' => array('class' => 'application.extensions.yii-redactor.actions.ImageList'));
	}
	public function actionIndex() {
		$this->registerScripts();
		if (Yii::app()->request->isAjaxRequest) {
			if (count($_POST["q"]) != false) {
				$res = explode('&', $_POST["q"]);
				if (count($res)) {
					$pr = 0;
					foreach ( $res as $i => $b ) {
						$pr ++;
						$k[$i] = explode('=', $b);
						if ($k[$i][1] == 'null') {
							$k[$i][1] = '-1';
						}
						$model = StaticPages::model()->findByPk(preg_replace("/\D/", "", $k[$i][0]));
						$model->spParentID = $k[$i][1];
						$model->spOrder = $pr;
						$model->save();
					}
				}
			}
			Yii::app()->end();
		}
		$this->render('index', array('rows' => StaticPages::model()->ordered()->findAllByAttributes(array("spParentID" => - 1))));
	}
	public function actionAddedit($id = false, $m = "addedit") {
		$model = $id != false ? $this->loadModel($id) : new StaticPages();
		
		$model->spType = $m;
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_POST[get_class($model)];
			if ($model->validate()) {
				$model->save();
				if (! $id) {
					Yii::app()->user->setFlash('success', "Успешно добавлено!");
					$this->redirect('/root/' . $m);
				} else {
					Yii::app()->user->setFlash('success', "Успешно обновлено!");
				}
			}
		}
		
		$this->render('_form', array('model' => $model,'ismenu' => $m == "addeditm" ? true : false));
	}
	public function actionDelete($id) {
		$this->recDel($id);
		if (! isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('/root'));
	}
	public function actionLogin() {
		$this->forward('/site/login');
	}
	public function actionLogout() {
		$this->forward('/site/login');
	}
	protected function recDel($id) {
		$childs = StaticPages::model()->childs()->findAll(array('params' => array('pid' => $id)));
		$this->loadModel($id)->delete();
		if (count($childs) != false) {
			foreach ( $childs as $k => $child ) {
				$this->recDel($child->spID);
			}
		}
	}
	public function loadModel($id) {
		$model = StaticPages::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
	protected function registerScripts() {
		Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::app()->basePath . '/../css/ui-lightness') . '/jquery-ui-1.10.0.custom.css', CClientScript::POS_BEGIN);
		
		/* Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(
          Yii::app()->basePath . '/../js/admin') .
          '/jquery-1.9.0.js', CClientScript::POS_BEGIN); */
		
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->basePath . '/../js/admin') . '/jquery-ui-1.10.0.custom.js', CClientScript::POS_HEAD);
		
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::app()->basePath . '/../js/admin') . '/jquery.mjs.nestedSortable.js', CClientScript::POS_HEAD);
	}
}