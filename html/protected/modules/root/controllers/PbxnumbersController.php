<?php
class PbxnumbersController extends rootController {
	public function actionIndex() {
		$model = PBXNumbers::model()->withproviders();
		
		$dataProvider = new CActiveDataProvider('PBXNumbers', array(
				'pagination' => array('pageSize' => wu::getRequestPagPageValue('pbxNumbersPGS')),
				'sort' => array(
						'attributes' => array('*','ProviderName' => array('asc' => 'pr.pName','desc' => 'pr.pName desc')),
						'defaultOrder' => 'Type,Number')));
		$this->render('pbxnumbers', array('dataProvider' => $dataProvider,'model' => $model));
	}
	public function AddEdit($id = false) {
		$model = $id != false ? $this->loadModel($id) : new PBXNumbers();
		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_REQUEST[get_class($model)];
			if ($model->save()) {
				$id == false ? Yii::app()->user->setFlash('success', "Успешно добавлен!") : Yii::app()->user->setFlash('success', "Успешно обновлен!");
			} else {
				Yii::app()->user->setFlash('error', $model);
			}
		}
		$this->redirect('/root/pbxnumbers/');
	}
	public function actionAdd($id = false) {
		$this->AddEdit($id);
	}
	public function actionUpdate($id) {
		$this->AddEdit($id);
	}
	public function actionDelete($id) {
		$this->loadModel($id)->delete();
		
		if (! isset($_GET['ajax']))
			$this->redirect('/root/pbxnumbers');
	}
	public function loadModel($id) {
		$model = PBXNumbers::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
}