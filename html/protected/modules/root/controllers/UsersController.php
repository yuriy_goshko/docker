<?php
Yii::import('application.common.users.*');
// ROOT
class UsersController extends Users_Controller {
	public function filters() {
		return array('accessControl -login');
	}
	public function accessRules() {
		return array_merge(array(array('deny','users' => array('*'),'expression' => '$user->getState("isRoot_") != true')), parent::accessRules());
	}
}