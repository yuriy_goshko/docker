<?php
class DirectionstodepsController extends rootController {
	public static $items = array(
			'ideal' => array(
					'name' => 'Разные PBX и Агенты',
					'rules' => array(
							'PBX' => array('Outbound','Inbound','Conference','ConfOut','AutoCall','Callback','CallbackInit'),
							'Agents' => array('Outbound','Inbound','Local','Conference','Callback'))),
			'joint_A' => array(
					'name' => 'Общие агенты',
					'rules' => array('PBX' => array('Outbound','Inbound','ConfOut','AutoCall','CallbackInit'),'Agents' => array('Local','Conference'))),
			'joint_AO' => array(
					'name' => 'Общие агенты и исходящие PBX',
					'rules' => array('PBX' => array('Inbound','CallbackInit'),'Agents' => array('Local','Conference'))),
			'joint_O' => array(
					'name' => 'Общие исходящие PBX',
					'rules' => array('PBX' => array('Inbound','CallbackInit'),'Agents' => array('Outbound','Inbound','Local','Conference','Callback'))),
			'joint_PBX' => array(
					'name' => 'Общие PBX',
					'rules' => array('PBX' => array(),'Agents' => array('Outbound','Inbound','Local','Conference','Callback'))));
	static public function GetItems() {
		$arr = array();
		foreach ( self::$items as $k => $v ) {
			$arr[$k] = $v['name'];
		}
		return $arr;
	}
	static public function GetSelectItem($dataProvider) {
		$dataProvider->setPagination(false);
		$count = $dataProvider->getTotalItemCount();
		$dtPBXArr = array();
		$dtAgentsArr = array();
		for($i = 0; $i < $count; $i ++) {
			$Id = $dataProvider->data[$i]->Id;
			if ($dataProvider->data[$i]->DepsLinkByPBX == 1)
				$dtPBXArr[] = $Id;
			if ($dataProvider->data[$i]->DepsLinkByAgents == 1)
				$dtAgentsArr[] = $Id;
		}
		sort($dtPBXArr);
		sort($dtAgentsArr);
		$res = 'Выборочная';
		foreach ( self::$items as $k => $v ) {
			$PBXArr = $v['rules']['PBX'];
			$AgentsArr = $v['rules']['Agents'];
			sort($PBXArr);
			sort($AgentsArr);
			if (($dtPBXArr == $PBXArr) && ($dtAgentsArr == $AgentsArr)) {
				$res = $v['name'];
				break;
			}
		}
		return $res;
		// return $count . ' ' . json_encode($dtPBXArr) . ' ' . json_encode($dtAgentsArr);
	}
	public function actionIndex() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(implode('/', array(
				YiiBase::getPathOfAlias('webroot'),
				'protected',
				'modules',
				'root',
				'js'))) . '/' . 'directionstodeps.js', CClientScript::POS_BEGIN);
		$model = ciDirections::model();
		$dataProvider = new CActiveDataProvider('ciDirections', array('sort' => array('defaultOrder' => 'OrderKey')));
		
		$command = Yii::app()->db->createCommand('select value from Settings where param = "DirToDepsSelect"');
		if ($command->queryScalar() == 1)
			$selectableRows = 2;
		else
			$selectableRows = 0;
		$this->render('directionstodeps', array('dataProvider' => $dataProvider,'model' => $model,'selectableRows' => $selectableRows));
	}
	public function actionSave() {
		if (Yii::app()->request->isPostRequest) {
			$PBX_Ids = Yii::app()->request->getParam('DepsLinkByPBX');
			$Agents_Ids = Yii::app()->request->getParam('DepsLinkByAgents');
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$sql = 'update CI_Directions set DepsLinkByPBX = 0, DepsLinkByAgents = 0';
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
				if (is_array($PBX_Ids)) {
					foreach ( $PBX_Ids as $v ) {
						$sql = 'update CI_Directions set DepsLinkByPBX = 1 where Id = :pId ';
						$command = Yii::app()->db->createCommand($sql);
						$command->bindParam(":pId", $v, PDO::PARAM_INT);
						$command->execute();
					}
				}
				if (is_array($Agents_Ids)) {
					foreach ( $Agents_Ids as $v ) {
						$sql = 'update CI_Directions set DepsLinkByAgents = 1 where Id = :pId ';
						$command = Yii::app()->db->createCommand($sql);
						$command->bindParam(":pId", $v, PDO::PARAM_INT);
						$command->execute();
					}
				}
				$transaction->commit();
				$this->redirect('/root/directionstodeps');
			} catch ( Exception $e ) {
				$transaction->rollback();
				echo $e->getMessage();
			}
		}
	}
	public function actionGetTemplate() {
		if (Yii::app()->request->isAjaxRequest) {
			$id = Yii::app()->request->getParam('id');
			$arr = array();
			foreach ( self::$items as $k => $v ) {
				if ($id == $k) {
					$arr = $v['rules'];
					break;
				}
			}
			echo json_encode($arr);
		}
	}
}