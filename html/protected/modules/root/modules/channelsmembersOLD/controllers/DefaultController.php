<?php

class DefaultController extends rootController {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'AjaxOnly+addedit'
        );
    }

    protected $arrayOfAr = array();
    public $existsChannel = array();
    public $groupName;

    public function actionIndex() {

        $model = Groups::model();
        //$sort = new CSort('Agents');
        $crit = new CDbCriteria(
                array(
            'with' => 'groupschannels',
                )
        );
        $rows = $model->findAll($crit);
        if ($rows == null)
            Yii::app()->user->setFlash('info', "Еще нет записей");
        $this->render('index', array(
            'model' => $rows
        ));
    }

    public function actionAddedit($id = false) {
        if (Yii::app()->request->isAjaxRequest) {
            $dis = Yii::app()->request->getParam('ids');
            $gID = Yii::app()->request->getParam('cat');
            $co = ChannelOwners::model();
            if (is_array($dis)) {
                $cond = 'cType = :c AND coGroups_gID = :g AND channels.cName NOT IN(' . implode(', ', array_map(function($el) {
                                            return '"' . $el . '"';
                                        }, $dis)) . ')';
            } else {
                $cond = 'cType = :c AND coGroups_gID = :g';
            }
            $crit = new CDbCriteria(
                    array(
                'condition' => $cond,
                'together' => true,
                'params' => array(
                    'c' => Channels::OUT_CHAN,
                    'g' => $gID,
                ),
                'with' => 'channels',
                    )
            );
            $rows = $co->findAll($crit);
            if (is_array($rows))
                array_walk($rows, function($item, $key) {
                            $item->delete();
                        });

            if (is_array($dis)) {
                foreach ($dis as $v) {
                    $co = new ChannelOwners;
                    $params = array('coGroups_gID' => $gID, 'coChannels_cID' => Channels::model()->findByPk($v)->cID);
                    $co->attributes = $params;
                    if (!$co->exists(array('condition' => 'coGroups_gID = :coGroups_gID AND coChannels_cID = :coChannels_cID', 'params' => $params)))
                        $co->save();
                }
            }
            echo Yii::t('m', 'The operation completed successfully');
        } else {
            $this->redirect(array('index'));
        }
    }

    protected function getArrayFromAR($model = false) {
        if (count($model) != false) {

            foreach ($model as $k => $v) {
                $this->groupName = $v->gName;
                $dataProvider = new CActiveDataProvider('Channels', array(
                    'criteria' => array(
                        'select' => '*, belongToGr.coID as checked',
                        'join' => 'LEFT OUTER JOIN `ChannelOwners` `belongToGr` 
                            ON (belongToGr.coChannels_cID = t.cID) 
                            AND `belongToGr`.coGroups_gID = :gId',
                        'condition' => 'cType = :c',
                        'params' => array(
                            'gId'=>$v->gID,
                            'c' => Channels::OUT_CHAN
                        ),
                    ),
                    'sort' => array(
                        'attributes' => array(
                            '*',
                            'checked' => array(
                                'asc' => 'checked asc',
                                'desc' => 'checked desc'
                            ),
                        ),
                        'defaultOrder' => array(
                            'checked' => true,
                        ),
                    ),
                    'pagination' => false));
                foreach ($v->groupschannels as $g) {
                    $this->existsChannel[$this->groupName][] = $g->cID;
                }

                $this->arrayOfAr[$k] = array('label' => $v->gName, 'content' =>
                    $this->renderPartial('patialtab', array(
                        'dataProvider' => $dataProvider,
                        'group' => $v,
                            ), true)
                );
                if ($k == 0) {
                    $this->arrayOfAr[$k]['active'] = true;
                }
            }
        }
        return $this->arrayOfAr;
    }

}