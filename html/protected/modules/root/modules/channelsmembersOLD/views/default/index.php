<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model array */

$user = Yii::app()->user;
Yii::import('application.extensions.stanislavkrsv-jouele-6fec871.Jouele');
?>

<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'),// success, info, warning, error or danger
        ),
    )); 
?>

<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'id'=>'groupschannels',
    'placement'=>'left', // 'above', 'right', 'below' or 'left'
    'tabs'=>  $this->getArrayFromAR($model)
)); ?>
