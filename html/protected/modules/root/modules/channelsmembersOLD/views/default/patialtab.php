<?

$this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $dataProvider,
    'template' => '{items}',
    'enableHistory' => true,
    'enableSorting' => true,
    'selectableRows' => 0,
    'id' => 'gm_' . $group->gID,
    'afterAjaxUpdate' => 'js:init',
    'columns' => array(
        'cName',
        array(
            'class' => 'CCheckBoxColumn',
            'selectableRows' => 2,
            'name' => 'Чек',
            'headerTemplate' => '
                <div data-toggle = "tooltip" data-placement="right" data-title="' .
            Yii::t('m', 'Select all members') . '">{item}</div>',
            'checkBoxHtmlOptions' => array(
                'data-toggle' => 'tooltip',
                'data-placement' => 'right',
                'data-title' => Yii::t('m', 'Select an item'),
            ),
            'id' => 'gm' . $group->gID . '_' . 'membertd',
            'checked' => function($data, $row, $column) {
                $controller = $column->grid->owner;
                if (isset($controller->existsChannel[$controller->groupName])) {

                    return in_array($data->cID, $controller->existsChannel[$controller->groupName]);
                }
            }
        )
    )
));
?>

