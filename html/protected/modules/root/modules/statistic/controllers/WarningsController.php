<?php
class WarningsController extends rootController {
	public function actionIndex() {
		$model = ciWarnings::model();
		if (isset($_GET['ciWarnings'])) {
			$model->attributes = $_GET['ciWarnings'];
		}
		$dataProvider = new CActiveDataProvider('ciWarnings', array(
				'criteria' => $model->search(),
				'sort' => array('defaultOrder' => 'Id'),
				'pagination' => array('pageSize' => 15)));
		$this->render('index', array('dataProvider' => $dataProvider,'model' => $model));
	}
	public function actionUpdateByLinkedsId() {
		/*if (isset($_POST['btnNoMarkWarnings']))
			ciWarnings::model()->MarkedWarnings("0");
		else if (isset($_POST['btnMarkWarnings']))
			ciWarnings::model()->MarkedWarnings("1");
		else*/
		if (isset($_POST['SelLIds'])) {
			$Lids = '';
			if (isset($_POST['btnRecalc'])) {
				foreach ( $_POST['SelLIds'] as $LId ) {
					$Lids .= $LId . ',';
					ciWarnings::model()->RecalcWarningsByLinkedId($LId, true);
				}
				$Lids = 'Будут пересчитаны <pre>' . $Lids;
			}
			if (isset($_POST['btnDelete'])) {
				foreach ( $_POST['SelLIds'] as $LId ) {
					$Lids .= $LId . ',';
					ciWarnings::model()->RecalcWarningsByLinkedId($LId, false);
				}
				$Lids = 'Успешно удалены <pre>' . $Lids;
			}
			if (isset($_POST['btnDeleteInfo'])) {
				foreach ( $_POST['SelLIds'] as $LId ) {
					$Lids .= $LId . ',';
					ciWarnings::model()->DeleteInfoWarnings($LId);
				}
				$Lids = 'Успешно удалены <pre>' . $Lids;
			}
			Yii::app()->user->setFlash('success', $Lids);
		}
		$this->redirect(Yii::app()->request->getUrlReferrer() ? Yii::app()->request->getUrlReferrer() : 'index');
	}
	public function loadModel($id) {
		$model = ciWarnings::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
}