<?php

class DirectionsController extends rootController {

 
    public function actionIndex() {
        $model = ciDirections::model();
        $dataProvider = new CActiveDataProvider('ciDirections', array(
            'sort'=>array(
                'defaultOrder'=>'OrderKey'
            ),
        ));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }

    public function actionAddedit($id = false) {
        $model = $id != false ? $this->loadModel($id) : new ciDirections();
        if (Yii::app()->request->isPostRequest) {        	
            $model->attributes = $_REQUEST[get_class($model)];
            
            if ($model->validate() && $model->save()) {
                $id == false ? Yii::app()->user->setFlash('success', "Успешно добавлен!") : Yii::app()->user->setFlash('success', "Успешно обновлен!");
            } else {
                Yii::app()->user->setFlash('error', $model);
            }
        }
        //$this->redirect(array('index'));
        $this->redirect(Yii::app()->request->getUrlReferrer()?Yii::app()->request->getUrlReferrer():'index');
    }

    public function loadModel($id) {
        $model = ciDirections::model()->findByPk($id);        
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');      
        return $model;
    }

}