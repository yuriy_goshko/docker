<?php

class RulesnumbersController extends rootController {
	
 
    public function actionIndex() {        
        $model = ciRulesForWritingNumbers::model();
        $dataProvider = new CActiveDataProvider('ciRulesForWritingNumbers', array(
            'sort'=>array(
                'defaultOrder'=>'OrderKey'
            ),
        ));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }

    public function actionAddedit($id = false) {
        $model = $id != false ? $this->loadModel($id) : new ciRulesForWritingNumbers();      
       
        if (Yii::app()->request->isPostRequest) {        	
            $model->attributes = $_REQUEST[get_class($model)];
            
            if ($model->validate() && $model->save()) {
                $id == false ? Yii::app()->user->setFlash('success', "Успешно добавлен!") : Yii::app()->user->setFlash('success', "Успешно обновлен!");
            } else {
                Yii::app()->user->setFlash('error', $model);
            }
        }
        $this->redirect(Yii::app()->request->getUrlReferrer()?Yii::app()->request->getUrlReferrer():'index');
    //     $this->redirect(array('index'));         
    }
    
    public function actionDelete($id) {    	
    	$this->loadModel($id)->delete();
    
    	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
    	if (!isset($_GET['ajax']))
    		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function loadModel($id) {
        $model = ciRulesForWritingNumbers::model()->findByPk($id);        
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');      
        return $model;
    } 
    
}