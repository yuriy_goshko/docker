<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model CActiveRecord */

$user = Yii::app()->user;

?>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => true, // use transitions?
    'closeText' => '&times;', // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
    ),
));
?>

<div class="row">
    <div class="form">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'RulesForWritingNumbersForm',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'method' => 'post',
            'action' => '/root/statistic/Rulesnumbers/addedit',
        ));
        ?>

        <? if (Yii::app()->user->hasFlash('error')): ?>
            <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
        <? endif; ?>

        <div class="row">
        	<div class="span3">
                <?=$form->textFieldRow($model, 'OrderKey');?>
            </div>    
            <div class="span3">
                <?=$form->textFieldRow($model, 'CharsLength');?>
            </div>
            <div class="span3" >
       		    <?php echo $form->labelEx($model, 'OnlyRealNumbers'); ?>
	            <?=$form->dropDownList($model, 'OnlyRealNumbers', array('1'=>'Да','0'=>'Нет'))?> 
            </div>    
        </div> 
        <div class="row">      
            <div class="span3">
	            <?php echo $form->labelEx($model, 'ActionType'); ?>
	            <?=$form->dropDownList($model, 'ActionType', ciRulesForWritingNumbers::getActionTypesArray())?>                
            </div>
            <div class="span3">
                <?=$form->textFieldRow($model, 'OldValue');?>
            </div>
            <div class="span3">
                <?= $form->textFieldRow($model, 'NewValue');?>
            </div>                                                         
        </div> 
  
        <div class="row buttons">
            <div class="span3">
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Добавить')); ?>
                <?php
                $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Сброс', 'htmlOptions' => array(
                        'id' => 'reset'
                )));
                ?>
            </div>
        </div>                 

        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="row">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
    	'id'=>'RulesForWritingNumbers-grid',		
     	'dataProvider' => $dataProvider,
        'ajaxUpdate' => false,    		
        'columns' => array(            
        		array(
        				'header'=>$model->getAttributeLabel("OrderKey"),
        				'name' => 'OrderKey',
        				'cssClassExpression' => '"OrderKey"',
        				'value' => '$data->OrderKey',
        		),        	
        		array(
        				'header'=>$model->getAttributeLabel("CharsLength"),
        				'name' => 'CharsLength',
        				'cssClassExpression' => '"CharsLength"',
        				'value' => '$data->CharsLength',
        		),
        		array(
        				'header'=>$model->getAttributeLabel("OnlyRealNumbers"),
        				'name' => 'OnlyRealNumbers',        				
        				'cssClassExpression' => '"OnlyRealNumbers"',
        				//'value' => '$data->OnlyRealNumbers',
        				'value'=>'($data->OnlyRealNumbers=="0")?"Нет":"Да"',       				
        		),   
        		array(
        				'header'=>$model->getAttributeLabel("ActionType"),
        				'name' => 'ActionType',
        				'cssClassExpression' => '"ActionType"',
        				//'value' => '$data->ActionType',
        				'value' => '$data::getActionTypeNameByKey($data->ActionType)',    				        				
        		),  		
        		array(
        				'header'=>$model->getAttributeLabel("OldValue"),
        				'name' => 'OldValue',
        				'cssClassExpression' => '"OldValue"',
        				'value' => '$data->OldValue',
        		),
        		array(
        				'header'=>$model->getAttributeLabel("NewValue"),
        				'name' => 'NewValue',
        				'cssClassExpression' => '"NewValue"',
        				'value' => '$data->NewValue',
        		),
        		array(
        				'header' => 'Действия',
        				'class' => 'CButtonColumn',
        				'template' => '{editpbn} {delete}',
        				'buttons' => array(
        						'editpbn' => array(
        								'label' => '',
        								'url' => function($data){
        									return Yii::app()->urlManager->createUrl('/root/statistic/Rulesnumbers/addedit', array('id'=>$data->Id));
        								},
        								'options' => array('class' => 'icon-edit addpbn'),
        								'visible' => '$data->Id',
        						),
        						'delete' => array('label' => '', 'imageUrl' => '', 'options' => array('class' => 'icon-remove-circle'),),
        				),
        		)    		        		
        )
    ));
    ?>
</div>

