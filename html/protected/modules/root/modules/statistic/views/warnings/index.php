<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model CActiveRecord */
$user = Yii::app()->user;

?>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => true, // display a larger alert block?
		'fade' => true, // use transitions?
		'closeText' => '&times;', // close link text - if set to false, no close link is displayed
		'alerts' => array( // configurations per alert type
'success' => array('block' => true,'fade' => true,'closeText' => '&times;'))));
?>


<div class="row">
	<div class="form" style="min-width: 1500px">
        <?php
								$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
										'id' => 'ciWarningsForm',
										'enableClientValidation' => true,
										'clientOptions' => array('validateOnSubmit' => true),
										'method' => 'post',
										'action' => '/root/statistic/Warnings/UpdateByLinkedsId'));
								?>
        <? if (Yii::app()->user->hasFlash('error')): ?>
            <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
        <? endif; ?>    
<div class="row">        
    <?php
				/*if ($model->MarkWarnings === "1")
					echo CHtml::submitButton('Не подкрашивать звонки с варнингами', array('name' => 'btnNoMarkWarnings'));
				else
					echo CHtml::submitButton('Подкрашивать звонки с варнингами', array('name' => 'btnMarkWarnings'));
					*/
				?>
</div>


		<div class="row">
    <?php
				$this->widget('bootstrap.widgets.TbGridView', array(
						'id' => 'ciWarningsGrid',
						'dataProvider' => $dataProvider,
						'ajaxUpdate' => false,
						'selectableRows' => 2,
						'enableSorting' => true,
						'filter' => $model,
						
						// 'template' => '{items}<div class="gwfooter">{pager}{summary}</div>',
						'columns' => array(
								array('id' => 'SelLIds','value' => '$data->LinkedId','class' => 'CCheckBoxColumn'),
								array('name' => 'Id','filter' => false),
								array('name' => 'UniqueId'),
								
								// array('name' => 'LinkedId'),
								array('name' => 'LinkedId','type' => 'raw','value' => 'ciWarnings::GenerateLinkContent("LinkedId", $data)'),
								array(
										'name' => 'Type',
										'cssClassExpression' => '"Type"',
										'filter' => CHtml::tag('label', array('class' => 'designed-select'), CHtml::dropDownList(CHtml::activeName(ciWarnings::model(), 'Type'), $model->Type, mPop(array(
												'Info' => 'Info',
												'Warning' => 'Warning',
												'Ignored' => 'Ignored',
												'Error' => 'Error'))))),
								array('name' => 'Info'),
								array('name' => 'Count','filter' => false),
								array('name' => 'InsertDateTime'),
								array(
										'name' => 'UpdateDateTime',
										'filter' => false,
										'value' => function ($data) {
											if ($data->UpdateDateTime != 0)
												return $data->UpdateDateTime;
										}),
								array('name' => 'CallDateTime'))));
				?>
        <div class="row buttons">
				<div class="span3">
					<NOBR>
					<?php echo CHtml::submitButton('Recalc selected Calls', array('name' => 'btnRecalc', 'confirm' => "Будут удалены и пересчитаны заново все логи статистики связанные с выделенным(и) LinkedId. \nПродолжить?"));?>				
					<?php echo CHtml::submitButton('Delete selected warnings and Calls', array('name' => 'btnDelete', 'confirm' => 'Удалить все логи статистики связанные с выделенным(и) LinkedId?')); ?>
					<?php echo CHtml::submitButton('Delete only selected "Info" warnings', array('name' => 'btnDeleteInfo', 'confirm' => 'Удалить только информационные варнинги статистики связанные с выделенным(и) LinkedId?')); ?>
					</NOBR>
				</div>     

        	<?php $this->endWidget(); ?>
    </div>
		</div>
	</div>
</div>

