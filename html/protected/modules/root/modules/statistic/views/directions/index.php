<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model CActiveRecord */

$user = Yii::app()->user;
?>



<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => true, // use transitions?
    'closeText' => '&times;', // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
    ),
));
?>
<div class="row">
    <div class="form">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'ciDirectionsForm',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'method' => 'post',
            'action' => '/root/statistic/directions/addedit',
        ));
        ?>

        <? if (Yii::app()->user->hasFlash('error')): ?>
            <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
        <? endif; ?>

        <div class="row">
            <div class="span3">
                <?=$form->textFieldRow($model, 'Id', array('readonly'=>true));?>
            </div>
            
            <div class="span3">
               <?=$form->textFieldRow($model, 'Name');?>                
            </div>  
            
            <div class="span3">         
               <?php echo $form->labelEx($model, 'Used'); ?>
	           <?=$form->dropDownList($model, 'Used', array('1'=>'Да','0'=>'Нет'))?>               
            </div>  
            
            <div class="span3">
               <?=$form->textFieldRow($model, 'OrderKey');?>                
            </div>
            
        </div>        


        <div class="row buttons">
            <div class="span3">                
            	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Добавить')); ?>                       
                <?php
                $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Сброс', 'htmlOptions' => array('id' => 'reset')));
                ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="row">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' => $dataProvider,
        //'enableHistory' => true,
        'ajaxUpdate' => false,
        'columns' => array(
            array(
                'header'=>$model->getAttributeLabel("Id"),
                'name' => '"Id"',
                'cssClassExpression' => '"Id"',
                'value' => '$data->Id',
            ),
            array(
                'header' => $model->getAttributeLabel("Name"),
                'name'=> '"Name"',
                'cssClassExpression' => '"Name"',
                'value' => '$data->Name'
            ),
            array(
                'name'=> '"Used"',
                'header' => $model->getAttributeLabel("Used"),
                'cssClassExpression' => '"Used"',
                //'value' => '$data->Used',
            	'value'=>'($data->Used=="0")?"Нет":"Да"',   
            ),
            array(
                'name'=> '"OrderKey"',
                'header' => $model->getAttributeLabel("OrderKey"),
                'cssClassExpression' => '"OrderKey"',
                'value' => '$data->OrderKey'
            ),
        		array(
        				'header' => 'Действия',
        				'class' => 'CButtonColumn',
        				'template' => '{editpbn}',
        				'buttons' => array(
        						'editpbn' => array(
        								'label' => '',
        								'url' => function($data){
        									return Yii::app()->urlManager->createUrl('/root/statistic/directions/addedit', array('id'=>$data->Id));
        								},
        								'options' => array('class' => 'icon-edit addpbn'),
        								'visible' => '$data->Id',
        						),
        						'delete' => array('label' => '', 'imageUrl' => '', 'options' => array('class' => 'icon-remove-circle'),),
        				),
        		)    		        		
        )
    ));
    ?>
</div>