<?php
$user = Yii::app()->user;
?>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
		'block' => true,
		'fade' => true,
		'closeText' => '&times;',
		'alerts' => array('success' => array('block' => true,'fade' => true,'closeText' => '&times;'))));
?>
<div class="row">
	<div class="form">
        <?php
								$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
										'id' => 'providers',
										'enableClientValidation' => true,
										'clientOptions' => array('validateOnSubmit' => true),
										'method' => 'post',
										'action' => '/root/providers/default/addedit'));
								?>

        <? if (Yii::app()->user->hasFlash('error')): ?>
            <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
        <? endif; ?>

        <div class="row">
			<div class="span3">
                <?= $form->textFieldRow($model, 'pName'); ?>
            </div>
		</div>

		<div class="row buttons">
			<div class="span3">
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Добавить')); ?>
                <?php
																$this->widget('bootstrap.widgets.TbButton', array(
																		'buttonType' => 'reset',
																		'label' => 'Сброс',
																		'htmlOptions' => array('id' => 'reset')));
																?>
            </div>
		</div>
<?php $this->endWidget(); ?>
    </div>
</div>
<div class="row">
    <?php
				$this->widget('bootstrap.widgets.TbGridView', array(
						'dataProvider' => $dataProvider,
						'ajaxUpdate' => false,
						'columns' => array(
								array(
										'name' => 'pName',
										'header' => $model->getAttributeLabel("pName"),
										'cssClassExpression' => '"pName"',
										'value' => '$data->pName'),
								array(
										'header' => 'Действия',
										'class' => 'CButtonColumn',
										'template' => '{editpbn} {delete}',
										'buttons' => array(
												'editpbn' => array(
														'label' => '',
														'url' => function ($data) {
															return Yii::app()->urlManager->createUrl('/root/providers/default/addedit', array(
																	'id' => $data->pID));
														},
														'options' => array('class' => 'icon-edit addpbn'),
														'visible' => '$data->pID'),
												'delete' => array('label' => '','imageUrl' => '','options' => array('class' => 'icon-remove-circle')))))));
				?>
</div>