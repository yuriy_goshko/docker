<?php
class ProvidersModule extends CWebModule {
	public function init() {
		$this->setImport(array('providers.models.*','providers.components.*'));
	}
	public function beforeControllerAction($controller, $action) {
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else
			return false;
	}
}
