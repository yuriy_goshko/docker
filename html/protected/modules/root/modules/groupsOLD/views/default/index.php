<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model CActiveRecord */

$user = Yii::app()->user;
?>



<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'),
        ),
    )); 
?>
<div class="row">
<div class="form">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'groups',

        'enableClientValidation' => true,
        //'enableAjaxValidation'=>true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'method' => 'post',
        'action' => '/root/groups/default/addedit',
            ));
    ?>
    
    <? if (Yii::app()->user->hasFlash('error')): ?>
        <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
    <? endif; ?>

    <div class="row">
        <div class="span3">
            <?=$form->textFieldRow($model, 'gName');?>
        </div>
        <div class="span3">
            <?=$form->textFieldRow($model, 'gID');?>
        </div>
    </div>
    
    <div class="row buttons">
        <div class="span3">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Добавить')); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset',  'label'=>'Сброс', 'htmlOptions'=>array(
                'id'=>'reset'
            ))); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>
</div>
<div class="row">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' => $dataProvider,
        //'enableHistory' => true,
        'ajaxUpdate' => false,
        'columns'=>array(
            
            array(
                'header'=>Groups::model()->getAttributeLabel("gName"),
                'name'=>"gName",
                'cssClassExpression'=>'"gName"',
                'value'=>'$data->gName',
            ),
            array(
                'header'=>Groups::model()->getAttributeLabel("gID"),
                'name'=>"gID",
                'cssClassExpression'=>'"gID"',
                'value'=>'$data->gID',
            ),
            array(
                'header' => 'Действия',
                'class' => 'CButtonColumn',
                'template' => '{editpbn}',
                'buttons' => array(
                    'editpbn' => array(
                        'label' => '',
                        'url' => function($data){
                            return Yii::app()->urlManager->createUrl('/root/groups/default/addedit', array('id'=>$data->gID));
                        },
                        'options' => array('class' => 'icon-edit addpbn'),
                    ),
                ),
            )
        )
        ));
        ?>
</div>