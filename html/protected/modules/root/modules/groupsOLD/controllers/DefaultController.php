<?php

class DefaultController extends rootController {

    public function actionIndex() {
        $model = Groups::model();

        //$sort->applyOrder($crit);
        $dataProvider = new CActiveDataProvider('Groups', array(
            'sort'=>array(
                'defaultOrder'=>'gName'
            ),
        ));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    
    public function actionAddedit($id = false) {
    	if ($id != false)
        	$model = $this->loadModel($id) ;
    	else        
        	throw new CHttpException(500, 'Нельзя добавлять группы.');
        if (Yii::app()->request->isPostRequest) {
            $model->attributes = $_REQUEST[get_class($model)];
            if ($model->validate() && $model->save()) {
                $id == false ? Yii::app()->user->setFlash('success', "Успешно добавлен!") : Yii::app()->user->setFlash('success', "Успешно обновлен!");
            } else {
                Yii::app()->user->setFlash('error', $model);
            }
        }
        $this->redirect(array('index'));
    }
    
    public function loadModel($id) {
        $model = Groups::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }



}