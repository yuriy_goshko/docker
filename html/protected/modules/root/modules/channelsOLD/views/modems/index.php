<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model CActiveRecord */

$user = Yii::app()->user;
?>



<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => true, // use transitions?
    'closeText' => '&times;', // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
    ),
));
?>
<div class="row">
    <div class="form">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'modems',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'method' => 'post',
            'action' => '/root/channels/modems/addedit',
        ));
        ?>

        <? if (Yii::app()->user->hasFlash('error')): ?>
            <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
        <? endif; ?>

        <div class="row">
            <div class="span3">
                <?= $form->textFieldRow($model, 'mName', array(
                    'value' => $model->mName
                ));
                ?>
            </div>
            
            <div class="span3">
                <?= $form->textFieldRow($model, 'mNumber', array(
                    'value' => $model->mNumber
                ));
                ?>
            </div>

            <div class="span3">
                <?= $form->labelEx($model, 'mProviders_pID'); ?>
                <?= $form->dropDownList($model, 'mProviders_pID', 
                        Chtml::listData(Providers::model()->findAll(), 'pID', 'pName')) ?>
            </div>
            
            <div class="span3">
                <?= $form->textFieldRow(Channels::model(), 'cDescription') ?>
            </div>
            
        </div>
        


        <div class="row buttons">
            <div class="span3">
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Добавить')); ?>
                <?php
                $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Сброс', 'htmlOptions' => array(
                        'id' => 'reset'
                )));
                ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="row">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' => $dataProvider,
        //'enableHistory' => true,
        'ajaxUpdate' => false,
        'columns' => array(
            array(
                'header'=>$model->getAttributeLabel("mName"),
                'name' => "mName",
                'cssClassExpression' => '"mName"',
                'value' => '"$data->mName"',
            ),
            array(
                'header' => $model->getAttributeLabel("mNumber"),
                'name'=>'mNumber',
                'cssClassExpression' => '"mNumber"',
                'value' => '"$data->mNumber"'
            ),
            array(
                'name'=>'cDescription',
                'header' => Channels::model()->getAttributeLabel("cDescription"),
                'cssClassExpression' => '"cDescription"',
                'value' => '$data->channels->cDescription'
            ),
            array(
                'name' => Yii::t('m','Groups'),
                'cssClassExpression' => '"cGroups"',
                'value' => '$data->attachedGroups'
            ),
            array(
                'name' => Providers::model()->getAttributeLabel("pName"),
                'cssClassExpression' => '"mProviders_pID-map"',
                'value' => '$data->providers->pName'
            ),
            array(
                'header' => 'Действия',
                'class' => 'CButtonColumn',
                'template' => '{editpbn} {delete}',
                'buttons' => array(
                    'editpbn' => array(
                        'label' => '',
                        'url' => function($data){
                            return Yii::app()->urlManager->createUrl('/root/channels/modems/addedit', array('id'=>$data->mID));
                        },
                        'options' => array('class' => 'icon-edit addpbn'),
                        'visible' => '$data->mID',
                    ),
                    'delete' => array('label' => '', 'imageUrl' => '', 'options' => array('class' => 'icon-remove-circle'),),
                ),
            )
        )
    ));
    ?>
</div>