<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model CActiveRecord */

$user = Yii::app()->user;
?>



<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true, // display a larger alert block?
    'fade' => true, // use transitions?
    'closeText' => '&times;', // close link text - if set to false, no close link is displayed
    'alerts' => array(// configurations per alert type
        'success' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
    ),
));
?>
<div class="row">
    <div class="form">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'outers',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'method' => 'post',
            'action' => '/root/channels/outersip/addedit',
        ));
        ?>

        <? if (Yii::app()->user->hasFlash('error')): ?>
            <?php echo $form->errorSummary(Yii::app()->user->getFlash('error')); ?>
        <? endif; ?>

        <div class="row">
            <div class="span3">
                <?= $form->textFieldRow($model, 'name', array(
                    'value' => $model->name
                ));
                ?>
            </div>
            
            <div class="span3">
                <?= $form->textFieldRow(Channels::model(), 'cDescription') ?>
            </div>
            
        </div>
        


        <div class="row buttons">
            <div class="span3">
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Добавить')); ?>
                <?php
                $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Сброс', 'htmlOptions' => array(
                        'id' => 'reset'
                )));
                ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="row">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' => $dataProvider,
        //'enableHistory' => true,
        'ajaxUpdate' => false,
        'columns' => array(
            array(
                'name'=>'name',
                'header' => $model->getAttributeLabel("name"),
                'cssClassExpression' => '"name"',
                'value' => '$data->name',
            ),
            array(
                'name' => Channels::model()->getAttributeLabel("cDescription"),
                'cssClassExpression' => '"cDescription"',
                'value' => '$data->channels->cDescription'
            ),
            array(
                'header' => 'Действия',
                'class' => 'CButtonColumn',
                'template' => '{editpbn} {delete}',
                'buttons' => array(
                    'editpbn' => array(
                        'label' => '',
                        'url' => function($data){
                            return Yii::app()->urlManager->createUrl('/root/channels/outersip/addedit', array('id'=>$data->id));
                        },
                        'options' => array('class' => 'icon-edit addpbn'),
                        'visible' => '$data->id',
                    ),
                    'delete' => array('label' => '', 'imageUrl' => '', 'options' => array('class' => 'icon-remove-circle'),),
                ),
            )
        )
    ));
    ?>
</div>