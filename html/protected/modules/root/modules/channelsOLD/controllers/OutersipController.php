<?php

class OutersipController extends rootController {
 
    public function actionIndex() {
        $model = OuterSIP::model();
        $dataProvider = new CActiveDataProvider('OuterSIP', array(
            'sort'=>array(
                'defaultOrder'=>'cName'
            ),
        ));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }

    public function actionAddedit($id = false) {
        $model = $id != false ? $this->loadModel($id) : new OuterSIP;
        if (Yii::app()->request->isPostRequest) {
            if($model->isNewRecord) $model->channels = new Channels();
            $model->attributes = $_REQUEST[get_class($model)];
            $model->channels->attributes = $_REQUEST[get_class($model->channels)];
            if ($model->validate() && $model->save()) {
                $id == false ? Yii::app()->user->setFlash('success', "Успешно добавлен!") : Yii::app()->user->setFlash('success', "Успешно обновлен!");
            } else {
                Yii::app()->user->setFlash('error', $model);
            }
        }
        $this->redirect(Yii::app()->request->getUrlReferrer()?Yii::app()->request->getUrlReferrer():'index');
    }

    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function loadModel($id) {
        $model = OuterSIP::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}