<?php
class rootController extends Controller {
	public function filters() {
		return array('accessControl -login');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("isRoot_")'),array('deny','users' => array('*')));
	}
}