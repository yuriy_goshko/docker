$(document).ready(function() {
	$(':checkbox').on('change', function(e) {
		e.preventDefault();
		var cbName = $(this).attr('id');
		var cbValue = $(this).prop('checked') ? 1 : 0;		 
		var cbAction = $(this).attr('action');
		$.post(cbAction, {Name:cbName, Value:cbValue}, function(data) {
			var newarr = $.parseJSON(data);
			$.notify(newarr[0], newarr[1]);
		});
	});
	$(':button').click(function(e){		
		e.preventDefault();
		e.stopPropagation();
		var bName = $(this).attr('name');
		var tValue = $('#'+bName).attr('value');		 
		var bAction = $(this).attr('action');		
		$.post(bAction, {Name:bName, Value:tValue}, function(data) {
			var newarr = $.parseJSON(data);
			$.notify(newarr[0], newarr[1]);
		});
	});
});
