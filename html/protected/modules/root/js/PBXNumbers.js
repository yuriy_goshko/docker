$(document).ready(function() {
	action = $('#PBXNumbers').attr('action');
	$('.addpbn').click(function(e) {
		e.preventDefault();
		$data = pGVfields('PBXNumbers', 'PBXNumbers_', this);
		$list = $('#PBXNumbers SELECT[name="PBXNumbers[ProviderId]"]');
		if ($list.attr('value') != $data['ProviderId'])
			$list.val(null);
		$('#PBXNumbers button[type="submit"]').html(buttons.upd);
	});

	$('#reset').click(function(e) {
		$('#PBXNumbers').attr('action', action);
		$('#PBXNumbers button[type="submit"]').html(buttons.add);
	});

});

$('#PBXNumbers').submit(function(e) {
	e.preventDefault();
	$.post($(this).attr('action'), $(this).serializeArray(), function() {

	});

});