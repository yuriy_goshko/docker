
$(document).ready(function() {
    
    init();
    
    $(document).on('click', "input:regex(id, ^gm\\d+_*)", function(e) {
        setRowSelected(e.target);
        var id = $(e.target).attr('id').substring(0, $(e.target).attr('id').lastIndexOf('_'));
        var num_pr = id.substring(0,id.indexOf('_') + 1);
        updMembers(num_pr);
    });

    $(document).on('click', 'input:regex(id, ^gm\\d+_membertd_all)', function(e) {
        $(e.target).closest('table').find('tbody').find('input:regex(id, ^gm\\d+_membertd_\\d+)').each(function(key, val) {
            $(val).attr('checked', e.target.checked);
            setRowSelected(val);
        });
        var id = $(e.target).attr('id').substring(0, $(e.target).attr('id').lastIndexOf('_'));
        updMembers(id);
    });


});
function updMembers(num_pr) {	
    var gridId = $('#pbxnumbersrelations .tab-content .active .grid-view').attr('id');
    var num = gridId.substring(gridId.indexOf('_') + 1);  
    var ids = $('#' + gridId).yiiGridView('getChecked', num_pr+'checked');
    var ids_i = $('#' + gridId).yiiGridView('getChecked', num_pr+'IcomChecked');
    var ids_o = $('#' + gridId).yiiGridView('getChecked', num_pr+'OutcomChecked');    
    var ft = $('#FiltersForm_Type').val();
    $('#pbxnumbersrelations .grid-view').addClass("grid-view-loading");
    $.post('/root/pbxnumbersrelations/addedit', {'ids': ids, 'ids_i': ids_i, 'ids_o': ids_o, 'num': num, 'ft': ft}, function(data) {
    	var newarr = $.parseJSON(data);    	
    	$.notify(newarr[0], newarr[1]);
        $('#pbxnumbersrelations .grid-view').removeClass("grid-view-loading");
    });
}

function init() {
    $("input:regex(id, ^gm\\d+_*)").not('#membertd_all').each(function(key, val) {
    	//console.log(key);
    	console.log(val);
        setRowSelected(val);
    });
}