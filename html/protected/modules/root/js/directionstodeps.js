$(document).ready(function(){	
    //test();    
     $('#reset').click(function(e){             	 
    	 e.preventDefault();  
         var e = document.getElementById("listTamplates");
         var val = e.options[e.selectedIndex].value;
         console.log(val);
         $.post('/root/directionstodeps/GetTemplate', {'id': val}, function(data) {
        	 console.log(data);
         	var arr = $.parseJSON(data);    	
         	
         	// DepsLinkByPBX
         	var arrPBX = arr['PBX']; 
         	var arrAgents = arr['Agents'];
         	$("input:regex(id, ^DepsLinkByPBX*)").not('#DepsLinkByPBX_all').each(function(key, val) {         		
         		if (arrPBX.indexOf(val.value)>-1)
         			val.checked = 1;
         		else
         			val.checked = 0;         			
         	});
         	$("input:regex(id, ^DepsLinkByAgents*)").not('#DepsLinkByAgents_all').each(function(key, val) {         		
         		if (arrAgents.indexOf(val.value)>-1)
         			val.checked = 1;
         		else
         			val.checked = 0;         			
         	});
         });
     });
    
});

function test() {
	console.log('init');
	$("input:regex(id, ^SelLIds*)").not('#SelLIds_all').each(function(key, val) {    	                  
        setRowSelected(val);
        if (val.checked) 
        {val.checked = 0}        
         else 
        {val.checked = 1};
        console.log(val.value);        
    });
}
