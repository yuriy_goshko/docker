<?php
class DefaultController extends Controller {
	public $layout = '//layouts/front/fixed';
	public function filters() {
		return array('accessControl');
	}
	public function accessRules() {
		return array(array('allow','users' => array('@'),'expression' => '$user->getState("rViewReport")'),array('deny','users' => array('*')));
	}
	public function actionIndex() {
		$model = Agents::model();		
		$sp = StaticPages::model()->findByAttributes(array('spExtUrl' => '/memberstat'));
		$this->setPageTitle($sp["spTitle"]);
		$dataProvider = new CActiveDataProvider('Agents', array('sort' => array('defaultOrder' => array('port' => true)),'pagination' => false));
		$this->render('index', array('dataProvider' => $dataProvider,'model' => $model,'h1' => $sp["spTitle"]));
	}
}