<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model Agents */
/* @var $h1 string */

$this->finalCss = Yii::app()->assetManager->publish(
                implode('/', array(Yii::app()->basePath, '..', 'css'))
        ) . '/' . 'summary_grid.css';
?>
<div class="transformed main-content">

    <div class="fhead">
        <h1><img src="/images/design/blue-monitoring.png"/><?= $h1 ?></h1>
    </div>

    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'template' => '{items}<div class="gwfooter">{pager}{summary}</div>',
        'dataProvider' => $dataProvider,
        'enableHistory' => true,
        'enableSorting' => true,
        'columns' => array(
            array(
                'header' => Agents::model()->getAttributeLabel("name"),
                'name' => "name",
                'cssClassExpression' => '"name"',
                'value' => '$data->name',
            ),
            array(
                'header' => Agents::model()->getAttributeLabel("fullname"),
                'name' => "fullname",
                'cssClassExpression' => '"fullname"',
                'value' => '$data->fullname'
            ),
            array(
                'header' => Agents::model()->getAttributeLabel("ipaddr"),
                'name' => "ipaddr",
                'cssClassExpression' => '"ipaddr"',
                'value' => '$data->ipaddr'
            ),
            array(
                'header' => Agents::model()->getAttributeLabel("useragent"),
                'name' => 'useragent',
                'cssClassExpression' => '"useragent"',
                'value' => '$data->useragent'
            ),
            array(
                'header' => Agents::model()->getAttributeLabel("status"),
                'name' => 'port',
                'cssClassExpression' => '"status"',
                'value' => function($model) {
                    if ($model->status) {
                        return'<img src="/images/design/monitoring/on.png"/>';
                    } else {
                        return '<img src="/images/design/monitoring/off.png"/>';
                    }
                },
                'sortable' => TRUE,
                'type' => 'html',
            ),
        )
    ));
    ?>
</div>
