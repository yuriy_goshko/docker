<?php

class CallinfoModule extends CWebModule {

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components

        $this->setImport(array(
            'callinfo.models.*',
            'callinfo.components.*',
        ));
        
        // register module js file.
        Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                        implode('/', array(YiiBase::getPathOfAlias('webroot'), 'js', 'callinfo'))
                ) . '/' . '_module.js', CClientScript::POS_END);
        
        $cssPath = Yii::app()->assetManager->publish(implode('/', array(Yii::app()->basePath,'..','css'))) . '/';
        Yii::app()->clientScript->registerCssFile($cssPath . 'oldgrids.css');        
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }

}
