<?php

use callinfo\components\collections\direction;

$controller = $this;

Yii::import('application.extensions.stanislavkrsv-jouele-6fec871.Jouele');

//init js for player (only ajax)
ob_start();
$controller->widget('Jouele');
ob_end_clean();

$this->widget('bootstrap.widgets.TbGridView', array(
    'summaryText' => Yii::t('m', 'Calls') . ' {start}-{end} ' . Yii::t('m', 'of') . ' {count}',
    'dataProvider' => $dataProvider,
    'enableSorting' => true,
    'ajaxUpdate' => true,
    'enableHistory' => true,
    'id' => 'calls',
    'template' => '{items}<div class="gwfooter">{pager}{summary}</div>',
    'rowHtmlOptionsExpression' => function($row, $data) {
return array('id' => 'rowid_' . $data->cID);
},
    'columns' => array(
        array(
            'header' => CallInfo::model()->getAttributeLabel("cCalldate"),
            'name' => 'cCalldate',
            'value' => function($data) {
        return Yii::app()->dateFormatter->formatDateTime($data->cCalldate);
    }
        ),
        array(
            'header' => CallInfo::model()->getAttributeLabel("cSrc"),
            'name' => "prepared.prNameSrc",
            'type' => 'raw',
            'value' => function($data) {

        if (Users::isAdmin()) {
            $dir = new direction\TemplateMap(
                    new CConfiguration(Yii::getPathOfAlias('callinfo.config.direction.gwparams.admin.src') . '.php')
            );
        } else {
            $dir = new direction\TemplateMap(
                    new CConfiguration(Yii::getPathOfAlias('callinfo.config.direction.gwparams.user.src') . '.php')
            );
        }
        $content = $dir->itemAt($data->cDirection)->populateString(array(
            ':id' => $data->cSrc,
            ':name' => $data->prepared->prNameSrc,
            ':agentid' => $data->cChannelChannels_cID,
        ));        
        return $content;
    }
        ),
        array(
            'header' => CallInfo::model()->getAttributeLabel("cDst"),
            'name' => "prepared.prNameDst",
            'type' => 'raw',
            'value' => function($data) {

        if (Users::isAdmin()) {
            $dir = new direction\TemplateMap(
                    new CConfiguration(Yii::getPathOfAlias('callinfo.config.direction.gwparams.admin.dst') . '.php')
            );
        } else {
            $dir = new direction\TemplateMap(
                    new CConfiguration(Yii::getPathOfAlias('callinfo.config.direction.gwparams.user.dst') . '.php')
            );
        }
        $content = $dir->itemAt($data->cDirection)->populateString(array(
            ':id' => $data->cDst,
            ':name' => $data->prepared->prNameDst,
            ':agentid' => $data->cDstchannelChannels_cID,
        ));
        return $content;
    }
        ),
        array(
            'header' => CallInfo::model()->getAttributeLabel("cDisposition"),
            'name' => "cDisposition",
            'value' => function($data) {
        return Yii::t('dis_data', $data->cDisposition);
    }
        ),
        array(
            'header' => CallInfo::model()->getAttributeLabel("cDirection"),
            'name' => "cDirection",
            'value' => function($data) {
        return Yii::t('dir_data', $data->cDirection);
    }
        ),
        array(
            'header' => CallInfo::model()->getAttributeLabel("cOutchanChannels_cID"),
            'name' => "prepared.prNameExtChan",
            'value' => '$data->prepared->prNameExtChan'
        ),
        array(
            'header' => CallInfo::model()->getAttributeLabel("cDuration"),
            'name' => "cDuration",
            'value' => '$data->cDuration'
        ),
        array(
            'header' => CallInfo::model()->getAttributeLabel("cBillsec"),
            'name' => "cBillsec",
            'type' => 'raw',
            'value' => function($data) use ($controller) {
        if (!Yii::app()->user->checkAccess('canListenRecords'))
            return $data->cBillsec;
        return CHtml::link($data->cBillsec, '', array(
                    'class' => 'link-popover gridplayer',
                    'data-html' => true,
                    'data-title' => Yii::t('m', 'Listen to the recording'),
                    'data-placement' => 'top',
                    'data-content' => file_exists(REAL_SND_PATH . $data->cUniqueid . SND_EXT) ?
                            $controller->widget('Jouele', array(
                                'file' => SND_PATH . $data->cUniqueid . SND_EXT,
                                'htmlOptions' => array('style' => 'min-width: 206px')), true) :
                            Yii::t('m', 'Record is not exists'),
                    'data-toggle' => 'popover'
        ));
    }
        ),
    )
));
?>