<?php
/*
 * @var model CallInfoForm
 */
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
?>


<?php
$form = $this->beginWidget('ActiveForm', array(
    'id' => 'search-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'afterValidate' => 'js:ajaxSearch' //comment if need send form with reload page
    ),
    'htmlOptions' => array(
        'name' => 'CallInfoForm',
    ),
    'method' => 'get',
    'action' => '/callinfo/default',
        ));
?>

<div class="toggled-title pull-left
     <? if (isset($_REQUEST[get_class($model)])) : ?>active<? endif; ?>">

    <h3  class="lever pull-left">
        <span class="icon-chevron-<? if (!isset($_REQUEST[get_class($model)])) : ?>down<? else: ?>up<? endif; ?>"></span>
        <?= Yii::t('m', 'Filter') ?>
    </h3>

    <div class="pull-left">
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'name' => '_cfStartDate', //attribute name
            'value' => Yii::app()->dateFormatter->formatDateTime($model->cfStartDate),
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'htmlOptions' => array(
                'data-toggle' => "tooltip",
                'data-placement' => 'top',
                'title' => Yii::t('callinfo', 'start date')
            ),
        ));
        echo $form->hiddenField($model, 'cfStartDate');
        ?>
        -
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'name' => "_cfEndDate",
            'value' => Yii::app()->dateFormatter->formatDateTime($model->cfEndDate),
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'htmlOptions' => array(
                'data-toggle' => "tooltip",
                'data-placement' => 'top',
                'title' => Yii::t('callinfo', 'end date')
            ),
        ));
        echo $form->hiddenField($model, 'cfEndDate');
        ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => Yii::t('m', 'Search')));
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'reset',
            'loadingText' => Yii::t('m', 'Reset') . '...',
            'label' => Yii::t('m', 'Reset')
        ));
        ?>
    </div>


</div>
<div class="form toggled-content <? if (!isset($_REQUEST[get_class($model)])) : ?>collapsed<? endif; ?>">
    <div class="row-fluid line"></div>

    <div class="row-fluid">
        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfSrc', array('class' => 'span5')); ?>
            <?php
            echo $form->textField($model, 'cfSrc', array(
                'value' => $model->cfSrc,
                'class' => 'span7'
            ));
            ?>
            <?php echo $form->error($model, 'cfSrc'); ?>
        </div>
        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfDirection', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'cfDirection',
                'data' => array(
                    'outbound' => mb_ucfirst(Yii::t('dir_data', 'outbound'), 'utf8'),
                    'inbound' => mb_ucfirst(Yii::t('dir_data', 'inbound'), 'utf8'),
                    'local' => mb_ucfirst(Yii::t('dir_data', 'local'), 'utf8'),
                    'callback' => mb_ucfirst(Yii::t('dir_data', 'callback'), 'utf8'),
                ),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'cfDirection'); ?>
        </div>
        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfOutchan', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'cfOutchan',
                'data' => Channels::model()->gPopOUT(),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'cfOutchan'); ?>
        </div>
    </div>
    <div class="row-fluid">

        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfQueue', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'cfQueue',
                'data' => Chtml::listData(Queue::model()->mine()->findAll(), 'name', 'qName'),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'cfQueue'); ?>
        </div>
        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfAgent', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'cfAgent',
                'data' => Channels::model()->gPopIN(),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'cfAgent'); ?>
        </div>

        <div class="span3 controls">

            <?php echo $form->labelEx($model, 'cfPagesize', array('class' => 'span5')); ?>
            <?php
            echo $form->textField($model, 'cfPagesize', array(
                'value' => $model->cfPagesize,
                'class' => 'span7'
            ));
            ?>
            <?php echo $form->error($model, 'cfPagesize'); ?>
        </div>

    </div>

    <div class="row-fluid">


        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfDepartmentMb', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'cfDepartmentMb',
                'data' => Chtml::listData(DepartmentMembers::getPopDepartments(), 'dID', 'dName'),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'cfDepartmentMb'); ?>

        </div>

        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfDisposition', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'cfDisposition',
                'data' => array(
                    'FAILED' => mb_ucfirst(Yii::t('dis_data', 'FAILED'), 'utf8'),
                    'BUSY' => mb_ucfirst(Yii::t('dis_data', 'BUSY'), 'utf8'),
                    'ANSWERED' => mb_ucfirst(Yii::t('dis_data', 'ANSWERED'), 'utf8'),
                    'NO ANSWER' => mb_ucfirst(Yii::t('dis_data', 'NO ANSWER'), 'utf8'),
                    'uFAILED' => mb_ucfirst(Yii::t('dis_data', 'uFAILED'), 'utf8'),
                    'uHANGUP' => mb_ucfirst(Yii::t('dis_data', 'uHANGUP'), 'utf8'),
                ),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'cfDisposition'); ?>
        </div>
    </div>


</div>

<?php $this->endWidget(); ?>