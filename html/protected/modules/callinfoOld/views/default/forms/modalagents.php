<?php
$this->beginWidget(
        'bootstrap.widgets.TbModal', array('id' => 'modalagents')
);
?>
<?
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'agentsform',
    'type' => 'horizontal',
    'method' => 'post',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'validationUrl' => Yii::app()->urlManager->createUrl('callinfo/ajax/modalagents'),
        'afterValidate' => 'js:submitModalForm'
    ),
        ));
?>

<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h4></h4>
</div>

<div class="modal-body">
    <?php echo $form->textFieldRow($model, 'fullname'); ?>
</div>

<div class="modal-footer">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => Yii::t('m', 'Save'),
    ));
    ?>

    <a data-dismiss="modal" class="btn" id="yw7" href="#"><?= Yii::t('m', 'Close') ?></a>
</div>

<?php $this->endWidget(); ?>

<?php $this->endWidget(); ?>
