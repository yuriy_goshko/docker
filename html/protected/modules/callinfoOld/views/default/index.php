<?php
/* @var $this CallInfoController 
 * @var $dataProvider CActiveDataProvider
 * @var $model CallInfoForm
 */

$this->finalCss = Yii::app()->assetManager->publish(
                implode('/', array(Yii::app()->basePath, '..', 'css'))
        ) . '/' . 'summary_grid.css';

?>
<div class="row-fluid fhead">
    <div class="fhead-content">
        <div class="span10 <?= $h1['class'] ?>"><h1><img src="/images/design/blue-cdr.png"/><?= $h1['title'] ?></h1></div>
        <? if (Users::isAdmin()): ?>
            <div class="span2 buttons">
                <?php echo CHtml::button(Yii::t('m', 'Export CSV'), array('id' => 'pexport-button', 'class' => 'btn export-btn', 'style' => "float:right")); ?>
            </div>
        <? endif; ?>
    </div>
</div>
<div id="bottombar" class="row-fluid filter <? if (isset($_REQUEST[get_class($model)])) : ?>active<? endif; ?>">
    <div class="filter-content">
        <?= $this->renderPartial('forms/search-form', array('model' => $model)); ?>
    </div>
</div>


<div class="row-fluid main-content">
    <? $this->renderPartial('index/gw', array('dataProvider' => $dataProvider)) ?> 
</div>

<?= $this->renderPartial('forms/modalagents', array('model' => Agents::model())); ?>

<?= $this->renderPartial('forms/modalpb', array('model' => new PhoneBookForm)); ?>