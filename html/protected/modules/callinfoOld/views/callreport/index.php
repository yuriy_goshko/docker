<?php
/* @var $this CallInfoController 
 * @var $provider CActiveDataProvider
 * @var $callinfoData DAO
 * @var $h1 string
 */
?>

<div class="row-fluid fhead">
    <div class="fhead-content">
        <div class="span10 <?= $h1 ?>"><h1><img src="/images/design/blue-reports.png"/><?= $h1 ?></h1></div>
        <? if (Users::isAdmin()): ?>
            <div class="span2 buttons">
                <?php echo CHtml::button(Yii::t('m', 'Export CSV'), array('id' => 'export-button', 'class' => 'btn export-btn', 'style' => "float:right")); ?>
            </div>
        <? endif; ?>
    </div>
</div>
<div id="bottombar" class="row-fluid filter <? if (isset($_REQUEST[get_class($model)])) : ?>active<? endif; ?>">
    <div class="filter-content">
        <?= $this->renderPartial('forms/search-form', array('model' => $model)); ?>
    </div>
</div>
<? if (!$callinfoData->isEmpty()): ?>
    <div class="row-fluid main-content">
        <?
        $this->widget('bootstrap.widgets.TbGridView', array(
            'template' => '{items}<div class="gwfooter">{pager}</div>',
            'dataProvider' => $provider,
            'columns' => array(
                array(
                    'header' => mb_ucfirst(Yii::t('callinfo', 'call direction')),
                    'name' => 'name',
                    'value' => function($data) {
                return Yii::t('dir_data', $data['name']);
            }),
                array(
                    'header' => mb_ucfirst(Yii::t('dis_data', 'ANSWERED')),
                    'name' => 'answered',
                    'footer' => $callinfoData->allData['answered']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('dis_data', 'NO ANSWER')),
                    'name' => 'noanswer',
                    'footer' => $callinfoData->allData['noanswer']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('dis_data', 'BUSY')),
                    'name' => 'busy',
                    'footer' => $callinfoData->allData['busy']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('dis_data', 'uFAILED')),
                    'name' => 'ufailed',
                    'footer' => $callinfoData->allData['ufailed']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('dis_data', 'uHANGUP')),
                    'name' => 'uhangup',
                    'footer' => $callinfoData->allData['uhangup']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('dis_data', 'FAILED')),
                    'name' => 'failed',
                    'footer' => $callinfoData->allData['failed']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('m', 'All calls')),
                    'name' => 'allcalls',
                    'footer' => $callinfoData->allData['allcalls']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('callinfo', 'duration')),
                    'name' => 'duration',
                    'value' => function($data) {
                return sec2hms($data['duration']);
            },
                    'footer' => sec2hms($callinfoData->allData['duration'])
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('callinfo', 'billsec')),
                    'name' => 'billsec',
                    'value' => function($data) {
                return sec2hms($data['billsec']);
            },
                    'footer' => sec2hms($callinfoData->allData['billsec'])
                ),
            ),
        ));
        ?>
    </div>
    <div class="row-fluid highcharts">
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'options' => array(
                    'title' => array(
                        'text' => Yii::t('m', 'The ratio of the call status,%'),
                    ),
                    'chart' => array(
                        'type' => 'pie',
                    ),
                    'tooltip' => array('valueSuffix' => '%'),
                    'series' => $callinfoData->getSeriesByNameDirection()
                )
            ));
            ?>

        </div>
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'skin' => 'filtered',
                'options' => array(
                    'title' => array(
                        'text' => Yii::t('m', 'Call status')),
                    'chart' => array(
                        'type' => 'column',
                    ),
                    'xAxis' => array('categories' => array_values(array_intersect_key(array(
                            'outbound' => mb_ucfirst(Yii::t('dir_data', 'outbound'), 'utf8'),
                            'inbound' => mb_ucfirst(Yii::t('dir_data', 'inbound'), 'utf8'),
                            'local' => mb_ucfirst(Yii::t('dir_data', 'local'), 'utf8'),
                            'callback' => mb_ucfirst(Yii::t('dir_data', 'callback'), 'utf8'),
                                        ), $callinfoData->namedData))),
                    'yAxis' => array('min' => 0, 'title' => array('text' => Yii::t('m', 'Quantity'))),
                    'plotOptions' => array('series' => array('stacking' => 'normal')),
                    'series' => $callinfoData->getSeries(
                            array(
                        'outbound' => mb_ucfirst(Yii::t('dir_data', 'outbound'), 'utf8'),
                        'inbound' => mb_ucfirst(Yii::t('dir_data', 'inbound'), 'utf8'),
                        'local' => mb_ucfirst(Yii::t('dir_data', 'local'), 'utf8'),
                        'callback' => mb_ucfirst(Yii::t('dir_data', 'callback'), 'utf8'),
                            ), array(
                        'ufailed' => Yii::t('dis_data', 'uFAILED'),
                        'answered' => Yii::t('dis_data', 'ANSWERED'),
                        'busy' => Yii::t('dis_data', 'BUSY'),
                        'failed' => Yii::t('dis_data', 'FAILED'),
                        'noanswer' => Yii::t('dis_data', 'NO ANSWER'),
                        'uhangup' => Yii::t('dis_data', 'uHANGUP'),
                    )),
                ),
            ));
            ?>
        </div>
    </div>

<? else: ?>

    <?
    Yii::app()->user->setFlash('info', Yii::t('m', 'There are no entries'));
    $this->widget('bootstrap.widgets.TbAlert', array(
        'block' => true, // display a larger alert block?
        'fade' => true, // use transitions?
        'closeText' => '&times;', // close link text - if set to false, no close link is displayed
        'alerts' => array(// configurations per alert type
            'info' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
        ),
    ));
    ?>

<? endif; ?>
