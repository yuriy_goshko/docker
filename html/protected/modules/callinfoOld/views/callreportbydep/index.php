<?php
/* @var $this CallInfoController 
  @var $provider CActiveDataProvider
  @var $callinfoData DAO
  @var $h1 string
 * 
 */

?>

<div class="row-fluid fhead">
    <div class="fhead-content">
        <div class="span10 <?= $h1 ?>"><h1><img src="/images/design/blue-reports.png"/><?= $h1 ?></h1></div>
        <? if (Users::isAdmin()): ?>
            <div class="span2 buttons">
                <?php echo CHtml::button(Yii::t('m', 'Export CSV'), array('id' => 'export-button', 'class' => 'btn export-btn', 'style' => "float:right")); ?>
            </div>
        <? endif; ?>
    </div>
</div>
<div id="bottombar" class="row-fluid filter <? if (isset($_REQUEST[get_class($model)])) : ?>active<? endif; ?>">
    <div class="filter-content">
        <?= $this->renderPartial('forms/search-form', array('model' => $model)); ?>
    </div>
</div>
<? if (!$callinfoData->isEmpty()): ?>
    <div class="row-fluid main-content">
        <?
        $this->widget('bootstrap.widgets.TbGridView', array(
            'template' => '{items}<div class="gwfooter">{pager}</div>',
            'dataProvider' => $provider,
            'columns' => array(
                array(
                    'header' => mb_ucfirst(Yii::t('callinfo', 'departments')),
                    'name' => 'name',
                    'value' => function($data) {
                        $dep = CHtml::listData(Departments::getPopDepartments(), 'dID', 'dName');
                        return $dep[$data['name']];
                    }),
                array(
                    'header' => mb_ucfirst(Yii::t('dir_data', 'outbound'), 'utf8'),
                    'name' => 'call_0',
                    'footer' => $callinfoData->allData['call_0']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('dir_data', 'inbound'), 'utf8'),
                    'name' => 'call_1',
                    'footer' => $callinfoData->allData['call_1']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('dir_data', 'local'), 'utf8'),
                    'name' => 'call_2',
                    'footer' => $callinfoData->allData['call_2']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('dir_data', 'callback'), 'utf8'),
                    'name' => 'call_3',
                    'footer' => $callinfoData->allData['call_3']
                ),
                array(
                    'header' => Yii::t('m', 'All calls'),
                    'name' => 'allcalls',
                    'footer' => $callinfoData->allData['allcalls']
                ),
            ),
        ));
        ?>
    </div>
    <div class="row-fluid highcharts">
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'options' => array(
                    'title' => array(
                        'text' => Yii::t('m', 'The ratio of call types,%'),
                    ),
                    'chart' => array(
                        'type' => 'pie',
                    ),
                    'tooltip' => array('valueSuffix' => '%'),
                    'series' => $callinfoData->getSeriesByNameDep(array(
                        'call_0', 'call_1', 'call_2', 'call_3'
                    ))
                )
            ));
            ?>

        </div>
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'skin' => 'filtered',
                'options' => array(
                    'title' => array(
                        'text' => Yii::t('m', 'Types of calls')),
                    'chart' => array(
                        'type' => 'column',
                    ),
                    'xAxis' => array('categories' => array_values(array_intersect_key(Chtml::listData(Departments::getPopDepartments(), 'dID', 'dName'), $callinfoData->namedData))),
                    'yAxis' => array('min' => 0, 'title' => array('text' => Yii::t('m', 'Quantity'))),
                    'plotOptions' => array('series' => array('stacking' => 'normal')),
                    'series' => $callinfoData->getSeries(Chtml::listData(Departments::getPopDepartments(), 'dID', 'dName'), array(
                        'call_0' => Yii::t('dir_data', 'outbound'),
                        'call_1' => Yii::t('dir_data', 'inbound'),
                        'call_2' => Yii::t('dir_data', 'local'),
                        'call_3' => Yii::t('dir_data', 'callback'),
                    ))
                )
            ));
            ?>
        </div>
    </div>

<? else: ?>

    <?
    Yii::app()->user->setFlash('info', Yii::t('m', 'There are no entries'));
    $this->widget('bootstrap.widgets.TbAlert', array(
        'block' => true, // display a larger alert block?
        'fade' => true, // use transitions?
        'closeText' => '&times;', // close link text - if set to false, no close link is displayed
        'alerts' => array(// configurations per alert type
            'info' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
        ),
    ));
    ?>

<? endif; ?>
