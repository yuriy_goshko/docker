<?php
/*
 * @var model CallInfoForm
 */
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
?>


<?php
$form = $this->beginWidget('ActiveForm', array(
    'id' => 'search-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    //'afterValidate' => 'js:ajaxSearch' //comment if need send form with reload page
    ),
    'htmlOptions' => array(
        'name' => 'CallInfoForm',
    ),
    'method' => 'get',
    'action' => '/callinfo/callreportbydep'
        ));
?>



<div class="toggled-title pull-left
     <? if (isset($_REQUEST[get_class($model)])) : ?>active<? endif; ?>">

    <h3  class="lever pull-left">
        <span class="icon-chevron-<? if (!isset($_REQUEST[get_class($model)])) : ?>down<? else: ?>up<? endif; ?>"></span>
        <?= Yii::t('m', 'Filter') ?>
    </h3>

    <div class="pull-left">
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'name' => '_cfStartDate', //attribute name
            'value' => Yii::app()->dateFormatter->formatDateTime($model->cfStartDate),
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'htmlOptions' => array(
                'data-toggle' => "tooltip",
                'data-placement' => 'top',
                'title' => Yii::t('callinfo', 'start date')
            ),
        ));
        echo $form->hiddenField($model, 'cfStartDate');
        ?>
        -
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'name' => "_cfEndDate",
            'value' => Yii::app()->dateFormatter->formatDateTime($model->cfEndDate),
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'htmlOptions' => array(
                'data-toggle' => "tooltip",
                'data-placement' => 'top',
                'title' => Yii::t('callinfo', 'end date')
            ),
        ));
        echo $form->hiddenField($model, 'cfEndDate');
        ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => Yii::t('m', 'Search')));
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'reset',
            'loadingText' => Yii::t('m', 'Reset') . '...',
            'label' => Yii::t('m', 'Reset')
        ));
        ?>
    </div>


</div>
<div class="form toggled-content <? if (!isset($_REQUEST[get_class($model)])) : ?>collapsed<? endif; ?>">
    <div class="row-fluid line"></div>

    <div class="row-fluid">

        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfAgent', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'cfAgent',
                'data' => Channels::model()->gPopIN(),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'cfAgent'); ?>
        </div>

        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfDirection', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'cfDirection',
                'data' => array(
                    'outbound' => mb_ucfirst(Yii::t('dir_data', 'outbound'), 'utf8'),
                    'inbound' => mb_ucfirst(Yii::t('dir_data', 'inbound'), 'utf8'),
                    'local' => mb_ucfirst(Yii::t('dir_data', 'local'), 'utf8'),
                    'callback' => mb_ucfirst(Yii::t('dir_data', 'callback'), 'utf8'),
                ),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'cfDirection'); ?>
        </div>

        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'cfDepartment', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'cfDepartment',
                'data' => Chtml::listData(Departments::getPopDepartments(), 'dID', 'dName'),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'cfDepartment'); ?>
        </div>

    </div>



</div>

<?php $this->endWidget(); ?>