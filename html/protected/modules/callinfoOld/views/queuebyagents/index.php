<?php
/* @var $this CallInfoController 
  @var $model QueueLog CFormModel
  @var $queueData DAO
  @var $h1 string
 * 
 */

?>

<div class="row-fluid fhead">
    <div class="fhead-content">
        <div class="span10 <?= $h1 ?>"><h1><img src="/images/design/blue-reports.png"/><?= $h1 ?></h1></div>
        <? if (Users::isAdmin()): ?>
            <div class="span2 buttons">
                <?php echo CHtml::button(Yii::t('m', 'Export CSV'), array('id' => 'export-button', 'class' => 'btn export-btn', 'style' => "float:right")); ?>
            </div>
        <? endif; ?>
    </div>
</div>
<div id="bottombar" class="row-fluid filter <? if (isset($_REQUEST[get_class($model)])) : ?>active<? endif; ?>">
    <div class="filter-content">
        <?= $this->renderPartial('forms/search-form', array('model' => $model)); ?>
    </div>
</div>
<? if (!$queueData->isEmpty()): ?>
    <div class="row-fluid main-content">
        <?
        $this->widget('bootstrap.widgets.TbGridView', array(
            'template' => '{items}<div class="gwfooter">{pager}</div>',
            'dataProvider' => $provider,
            'columns' => array(
                array(
                    'header' => mb_ucfirst(Yii::t('callinfo', 'agent')),
                    'name' => 'name',
                    'value' => function($data) {
                        $channels = Channels::model()->gPopIN();
                        return $channels[$data['name']];
                    }),
                array(
                    'header' => mb_ucfirst(Yii::t('callinfo', 'number')),
                    'name' => 'number'
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('queuelog', 'recieved')),
                    'name' => 'recieved',
                    'footer' => $queueData->allData['recieved']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('queuelog', 'missed')),
                    'name' => 'missed',
                    'footer' => $queueData->allData['missed']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('queuelog', 'transfer')),
                    'name' => 'transfer',
                    'footer' => $queueData->allData['transfer']
                ),
                array(
                    'header' => mb_ucfirst(Yii::t('m', 'All calls')),
                    'name' => 'allcall',
                    'footer' => $queueData->allData['allcall']
                ),
            ),
        ));
        ?>
    </div>
    <div class="row-fluid highcharts">
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'options' => array(
                    'title' => array(
                        'text' => Yii::t('m', 'The ratio of calls in the queue,%'),
                    ),
                    'chart' => array(
                        'type' => 'pie',
                    ),
                    'tooltip' => array('valueSuffix' => '%'),
                    'series' => $queueData->getSeriesByAgents()
                )
            ));
            ?>

        </div>

        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'skin' => 'filtered',
                'options' => array(
                    'dataLabels' => array(),
                    'title' => array('text' => Yii::t('m', 'Types of calls')),
                    'chart' => array('type' => 'column',),
                    'xAxis' => array('categories' => array_values(array_intersect_key(Channels::model()->gPopIN(), $queueData->namedData))),
                    'yAxis' => array('min' => 0, 'title' => array('text' => Yii::t('m', 'Quantity'))),
                    'plotOptions' => array('series' => array('stacking' => 'normal')),
                    'series' => $queueData->getSeries(Channels::model()->gPopIN(), array(
                        'recieved' => mb_ucfirst(Yii::t('queuelog', 'recieved')),
                        'missed' => mb_ucfirst(Yii::t('queuelog', 'missed')),
                        'transfer' => mb_ucfirst(Yii::t('queuelog', 'transfer')),
                    ))
                )
            ));
            ?>
        </div>



    </div>


<? else: ?>

    <?
    Yii::app()->user->setFlash('info', Yii::t('m', 'There are no entries'));
    $this->widget('bootstrap.widgets.TbAlert', array(
        'block' => true, // display a larger alert block?
        'fade' => true, // use transitions?
        'closeText' => '&times;', // close link text - if set to false, no close link is displayed
        'alerts' => array(// configurations per alert type
            'info' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
        ),
    ));
    ?>

<? endif; ?>
