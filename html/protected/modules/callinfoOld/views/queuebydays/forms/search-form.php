<?php
/*
 * @var model QueueLogForm
 */
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
?>


<?php
$form = $this->beginWidget('ActiveForm', array(
    'id' => 'search-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    //'afterValidate' => 'js:ajaxSearch' //comment if need send form with reload page
    ),
    'htmlOptions' => array(
        'name' => 'QueueLogForm',
    ),
    'method' => 'get',
    'action' => '/callinfo/queuebydays'
        ));
?>



<div class="toggled-title pull-left
     <? if (isset($_REQUEST[get_class($model)])) : ?>active<? endif; ?>">

    <h3  class="lever pull-left">
        <span class="icon-chevron-<? if (!isset($_REQUEST[get_class($model)])) : ?>down<? else: ?>up<? endif; ?>"></span>
        <?= Yii::t('m', 'Filter') ?>
    </h3>

    <div class="pull-left">
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'name' => '_startDate', //attribute name
            'value' => Yii::app()->dateFormatter->formatDateTime($model->startDate),
            'mode' => 'datetime', //use "time","date" or "datetime" (default),
            'htmlOptions' => array(
                'data-toggle' => "tooltip",
                'data-placement' => 'top',
                'title' => Yii::t('callinfo', 'start date')
            ),
        ));
        echo $form->hiddenField($model, 'startDate');
        ?>
        -
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'name' => "_endDate",
            'value' => Yii::app()->dateFormatter->formatDateTime($model->endDate),
            'mode' => 'datetime', //use "time","date" or "datetime" (default),
            'htmlOptions' => array(
                'data-toggle' => "tooltip",
                'data-placement' => 'top',
                'title' => Yii::t('callinfo', 'end date')
            ),
        ));
        echo $form->hiddenField($model, 'endDate');
        ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => Yii::t('m', 'Search')));
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'reset',
            'loadingText' => Yii::t('m', 'Reset') . '...',
            'label' => Yii::t('m', 'Reset')
        ));
        ?>
    </div>


</div>
<div class="form toggled-content <? if (!isset($_REQUEST[get_class($model)])) : ?>collapsed<? endif; ?>">
    <div class="row-fluid line"></div>

    <div class="row-fluid">
        
        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'departmentQueue', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'departmentQueue',
                'data' => Chtml::listData(DepartmentQueues::getPopDepartments(), 'dID', 'dName'),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>

            <?php echo $form->error($model, 'departmentQueue'); ?>
        </div>

        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'periodD', array('class' => 'span5')); ?>
            <?php
            echo $form->wrappedDownList(array($model, 'period', QueueLogForm::getPeriodListByDays()), array('class' => 'span7'));
            ?>
            <?php echo $form->error($model, 'period'); ?>
        </div>

        <div class="span3 controls">
            <?php echo $form->labelEx($model, 'qlQueuename', array('class' => 'span5')); ?>
            <?php
            $this->widget('ext.bootstrap-select.TbSelect', array(
                'model' => $model,
                'attribute' => 'qlQueuename',
                'data' => Chtml::listData(Queue::model()->mine()->findAll(), 'name', 'qName'),
                'htmlOptions' => array(
                    'class' => 'span7 designed-select',
                    'multiple' => true,
                ),
            ))
            ?>
            <?php echo $form->error($model, 'qlQueuename'); ?>
        </div>
    </div>




</div>

<?php $this->endWidget(); ?>