<?php
/* @var $this CallInfoController 
  @var $model QueueLog CFormModel
  @var $queueData DAO
  @var $h1 string
 * 
 */

?>

<div class="row-fluid fhead">
    <div class="fhead-content">
        <div class="span10 <?= $h1 ?>"><h1><img src="/images/design/blue-reports.png"/><?= $h1 ?></h1></div>
        <? if (Users::isAdmin()): ?>
            <div class="span2 buttons">
                <?php echo CHtml::button(Yii::t('m', 'Export CSV'), array('id' => 'export-button', 'class' => 'btn export-btn', 'style' => "float:right")); ?>
            </div>
        <? endif; ?>
    </div>
</div>
<div id="bottombar" class="row-fluid filter <? if (isset($_REQUEST[get_class($model)])) : ?>active<? endif; ?>">
    <div class="filter-content">
        <?= $this->renderPartial('forms/search-form', array('model' => $model)); ?>
    </div>
</div>
<? if (!$queueData->isEmpty()): ?>
    <div class="row-fluid main-content">
        <? $this->renderPartial('_queue/_qdtable', array('data' => $queueData)) ?> 
    </div>
    <div class="row-fluid highcharts">
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'options' => array(
                    'title' => array(
                        'text' => Yii::t('m', 'The ratio of the number of calls in queue,%'),
                    ),
                    'chart' => array(
                        'type' => 'pie',
                    ),
                    'tooltip' => array('valueSuffix' => '%'),
                    'series' => $queueData->getSeriesByCount(),
                )
            ));
            ?>

        </div>

        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'options' => array(
                    'title' => array(
                        'text' => Yii::t('m', 'The ratio of time in queue,%'),
                    ),
                    'chart' => array(
                        'type' => 'pie',
                    ),
                    'tooltip' => array('valueSuffix' => '%'),
                    'series' => $queueData->getSeriesByTime(),
                )
            ));
            ?>

        </div>


    </div>
    <div class="row-fluid highcharts">
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'skin' => 'filtered',
                'options' => array(
                    'dataLabels' => array(),
                    'title' => array('text' => Yii::t('m', 'Statistics on the received calls')),
                    'chart' => array('type' => 'column',),
                    'xAxis' => array(
                        'categories' => $queueData->getCategoriesByDays(),
                        'labels' => array('enabled' => false),
                    ),
                    'yAxis' => array(
                        'title' => array('text' => Yii::t('m', 'Number of calls received'))
                    ),
                    'series' => $queueData->getSeriesByField(array('recieved')),
                )
            ));
            ?>
        </div>
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'skin' => 'filtered',
                'options' => array(
                    'title' => array('text' => Yii::t('m', 'Statistics on missed calls')),
                    'chart' => array('type' => 'column',),
                    'xAxis' => array(
                        'categories' => $queueData->getCategoriesByDays(),
                        'labels' => array('enabled' => false),
                    ),
                    'yAxis' => array(
                        'title' => array('text' => Yii::t('m', 'Number of missed calls'))
                    ),
                    'series' => $queueData->getSeriesByField(array('failed')),
                )
            ));
            ?>
        </div>
    </div>
    <div class="row-fluid highcharts">
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'skin' => 'filtered',
                'options' => array(
                    'title' => array('text' => Yii::t('m', 'Statistics on the talk time')),
                    'chart' => array('type' => 'column',),
                    'xAxis' => array(
                        'categories' => $queueData->getCategoriesByDays(),
                        'labels' => array('enabled' => false),
                    ),
                    'yAxis' => array(
                        'title' => array('text' => Yii::t('m', 'Time(c)'))
                    ),
                    'series' => array_merge($queueData->getSeriesByField(array('fullDuration')), $queueData->getSeriesByField(array('avgDuration', 'options' => array('type' => 'spline')))),
                )
            ));
            ?>
        </div>
        <div class="span6">
            <?
            $this->Widget('application.extensions.highcharts.HighchartsWidget', array(
                'skin' => 'filtered',
                'options' => array(
                    'title' => array('text' => Yii::t('m', 'Statistics on waiting times')),
                    'chart' => array('type' => 'column',),
                    'xAxis' => array(
                        'categories' => $queueData->getCategoriesByDays(),
                        'labels' => array('enabled' => false),
                    ),
                    'yAxis' => array(
                        'title' => array('text' => Yii::t('m', 'Time(c)'))
                    ),
                    'series' => $queueData->getSeriesByField(array('waitTime')),
                )
            ));
            ?>
        </div>
    </div>

<? else: ?>

    <?
    Yii::app()->user->setFlash('info', Yii::t('m', 'There are no entries'));
    $this->widget('bootstrap.widgets.TbAlert', array(
        'block' => true, // display a larger alert block?
        'fade' => true, // use transitions?
        'closeText' => '&times;', // close link text - if set to false, no close link is displayed
        'alerts' => array(// configurations per alert type
            'info' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
        ),
    ));
    ?>

<? endif; ?>
