<?php
/* $this CallInfoController
 * $data array
 */
?>
<div class="grid-view">
    <table class="items table cust">
        <thead>
            <tr>
                <th><?= mb_ucfirst(Yii::t('queuelog', 'period days a week')) ?></th>
                <th><?= mb_ucfirst(Yii::t('queuelog', 'queue name')) ?></th>
                <th><?= mb_ucfirst(Yii::t('queuelog', 'recieved')) ?></th>
                <th><?= mb_ucfirst(Yii::t('queuelog', 'failed')) ?></th>
                <th><?= mb_ucfirst(Yii::t('m', 'All calls')) ?></th>
                <th><?= mb_ucfirst(Yii::t('queuelog', 'talk time')) ?></th>
                <th><?= mb_ucfirst(Yii::t('queuelog', 'waittime')) ?></th>
                <th><?= mb_ucfirst(Yii::t('queuelog', 'average talk time')) ?></th>
            </tr>
        </thead>
        <tbody>
            <? $i = 1; ?>
            <? foreach ($data->result as $h => $row): ?>
                <? if (count($row) != false): ?>
                    <? $f = true; ?>
                    <? foreach ($row as $v): ?>
                        <tr  class="<?= $i % 2 ? 'odd' : 'even' ?>">
                            <td><?= preg_replace_callback('/d(\d)+/i', "getDayName", $h) ?></td>
                            <td><?= $v['name']; ?></td>
                            <td><?= $v['recieved']; ?></td>
                            <td><?= $v['failed']; ?></td>
                            <td><?= $v['allcall']; ?></td><? $f = false; ?>
                            <td><?= sec2hms($v['fullDuration']); ?></td>
                            <td><?= sec2hms($v['waitTime']); ?></td>
                            <td><? if ($v['recieved'] != 0) echo sec2hms($v['fullDuration'] / $v['recieved']); ?></td>
                        </tr>
                    <? endforeach; ?>
                    <? $i++ ?>
                <? endif; ?>

            <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td><?= $data->allData['recieved'] ?></td> 
                <td><?= $data->allData['failed'] ?></td> 
                <td><?= $data->allData['allcall'] ?></td> 
                <td><?= sec2hms($data->allData['fullDuration']) ?></td> 
                <td><?= sec2hms($data->allData['waitTime']) ?></td>
                <td><? if ($data->allData['recieved'] != 0) echo sec2hms($data->allData['fullDuration'] / $data->allData['recieved']) ?></td> 
            </tr> 
        </tfoot>
    </table>
</div>