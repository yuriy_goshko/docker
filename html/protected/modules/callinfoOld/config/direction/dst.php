<?php

return array(
    'inbound' => array('template' => '{:id} {(:name)}'),
    'outbound' => array('template' => '{:id} {":name"}'),
    'callback' => array('template' => '{:id} {(:name)}'),
    'local' => array('template' => '{:id} {(:name)}'),
    'autocall' => array('template' => '{:id} {":name"}'),
);