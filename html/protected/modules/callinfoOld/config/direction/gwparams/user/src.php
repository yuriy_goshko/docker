<?php

use string\format;

$parent = require(Yii::getPathOfAlias('callinfo.config.direction.src') . '.php');
return CMap::mergeArray($parent, array(
            'inbound' => array(
                'params' => array(
                    $parent['inbound']['template'] =>
                    CHtml::link($parent['inbound']['template'], '/callinfo/ajax/modalpb/id/:id', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', 'Update customer details in the phone book.'),
                    )),
                )),
            'callback' => array(
                'params' => array(
                    $parent['callback']['template'] =>
                    CHtml::link($parent['callback']['template'], '/callinfo/ajax/modalpb/id/:id', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', 'Update customer details in the phone book.'),
                            )
                    ),
                )),
        ));