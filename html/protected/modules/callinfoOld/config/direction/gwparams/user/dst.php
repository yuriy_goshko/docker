<?php

use string\format;

$parent = require(Yii::getPathOfAlias('callinfo.config.direction.dst') . '.php');
return CMap::mergeArray($parent, array(
            'outbound' => array(
                'params' => array(
                    $parent['outbound']['template'] =>
                    CHtml::link($parent['inbound']['template'], '/callinfo/ajax/modalpb/id/:id', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', 'Update customer details in the phone book.'),
                    )),
                )),
        ));
