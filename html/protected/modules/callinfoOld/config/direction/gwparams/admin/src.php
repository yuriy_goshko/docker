<?php

use string\format;

$parent = require(Yii::getPathOfAlias('callinfo.config.direction.src') . '.php');
return CMap::mergeArray($parent, array(
            'inbound' => array(
                'params' => array(
                    $parent['inbound']['template'] =>
                    CHtml::link($parent['inbound']['template'], '/callinfo/ajax/modalpb/id/:id', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', 'Update customer details in the phone book.'),
                    )),
                )),
            'outbound' => array(
                'params' => array(
                    ':name' => CHtml::link(':name', '/callinfo/ajax/modalagents/id/:agentid', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', "Update agent's data."),
                            )
                    ),
                )),
            'callback' => array(
                'params' => array(
                    $parent['callback']['template'] =>
                    CHtml::link($parent['callback']['template'], '/callinfo/ajax/modalpb/id/:id', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', 'Update customer details in the phone book.'),
                            )
                    ),
                )),
            'local' => array(
                'params' => array(
                    ':name' => CHtml::link(':name', '/callinfo/ajax/modalagents/id/:agentid', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', "Update agent's data."),
                            )
                    ),
                )),
        ));