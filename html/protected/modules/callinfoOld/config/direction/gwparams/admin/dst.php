<?php

use string\format;

$parent = require(Yii::getPathOfAlias('callinfo.config.direction.dst') . '.php');
return CMap::mergeArray($parent, array(
            'inbound' => array(
                'params' => array(
                    ':name' => CHtml::link(':name', '/callinfo/ajax/modalagents/id/:agentid', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', "Update agent's data."),
                            )
                    ),
                )),
            'outbound' => array(
                'params' => array(
                    $parent['outbound']['template'] =>
                    CHtml::link($parent['outbound']['template'], '/callinfo/ajax/modalpb/id/:id', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', 'Update customer details in the phone book.'),
                    )),
                )),
            'callback' => array(
                'params' => array(
                    ':name' => CHtml::link(':name', '/callinfo/ajax/modalagents/id/:agentid', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', "Update agent's data."),
                            )
                    ),
                )),
            'local' => array(
                'params' => array(
                    ':name' => CHtml::link(':name', '/callinfo/ajax/modalagents/id/:agentid', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', "Update agent's data."),
                            )
                    ),
                )),
            'autocall' => array(
                'params' => array(
                    $parent['autocall']['template'] =>
                    CHtml::link($parent['autocall']['template'], '/callinfo/ajax/modalpb/id/:id', array(
                        'class' => "modalpop",
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'title' => Yii::t('tooltips', 'Update customer details in the phone book.'),
                    )),
                )),
        ));
