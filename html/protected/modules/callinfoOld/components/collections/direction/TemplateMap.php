<?php

namespace callinfo\components\collections\direction;

use callinfo\components\string\format;

class TemplateMap extends \cMap {

    /**
     * Get a \string\format\Templater to given direction.
     * 
     * @param string $str
     * @return \string\format\Templater
     */
    public function __get($str) {
        return $this->itemAt($str);
    }

    /**
     * Set a \string\format\Templater to direction.
     * 
     * @param string $name
     * @param string $val
     */
    public function add($name, $val) {
        $params = isset($val['params'])?$val['params']:array();
        parent::add($name, new format\Templater($val['template'], $params));
    }

}