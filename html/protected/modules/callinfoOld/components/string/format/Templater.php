<?php

namespace callinfo\components\string\format;

use string\format as plain;

class Templater extends plain\Templater {
    
    /**
     * Return calculated string.
     * 
     * @return string
     */
    public function populateString($params = array()) {
        if (count($params) != false) {
            preg_match_all('/\{.*(:\w+?).*\}/U', $this->pattern, $match);
            foreach ($match[1] as $key => $val) {
                if (array_key_exists($val, $params) && $params[$val] == false)
                    $this->pattern = preg_replace('/' . preg_quote($match[0][$key], '/') . '/', '', $this->pattern);
            }
        }

        $params['}'] = false;
        $params['{'] = false;
        return parent::getString($params);
    }

}