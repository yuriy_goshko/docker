<?php

use callinfo\components\collections\direction;

/**
 * Detailed CDR controller.
 *
 * @author Kuzich Yurii <qzichs@gmail.com>
 * @since 1.0.0
 */
class DefaultController extends FrontController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array(
                    'makecsv',
                ),
                'users' => array('admin')
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Рендеринг статистики всех звонков, с постраничной разбивкой. 
     */
    public function actionIndex() {

        $sp = StaticPages::model()->findByAttributes(array('spExtUrl' => '/callinfo'));
        $this->setPageTitle($sp["spTitle"]);
        $model = CallInfo::model();
        $form = new CallInfoForm();
        $model->dbCriteria = new CDbCriteria(array(
            'select' => 'UNIX_TIMESTAMP(  `cCalldate` ) AS cCalldate, 
                cSrc, cDst,  SEC_TO_TIME(cDuration) as cDuration,  
                SEC_TO_TIME(cBillsec) as cBillsec, cDisposition, cUniqueid, 
                cDirection, cActiveAgent, cChannelChannels_cID, cDstchannelChannels_cID',
            'with' => array('prepared' => array('select' => 'prNameSrc, prNameDst, prNameExtChan')),
            'condition' => 't.cGroups_gID = :g',
            'params' => array(
                'g' => Yii::app()->user->getState('Groups_gID')
            ),
        ));
        $pagsize = $form->cfPagesize;
        if (isset($_REQUEST[get_class($form)]['cfPagesize']))
            $pagsize = $_REQUEST[get_class($form)]['cfPagesize'];
        /*
         * There is was post request?
         */
        if (isset($_REQUEST[get_class($form)])) {
            $form->setAttributes($_REQUEST[get_class($form)]);
            if(!$form->validate())
                throw new CHttpException(404, 'Model validation failed');
        }
        $model->search($form);

        $pagination = array('pageSize' => $pagsize);
        $dataProvider = new CActiveDataProvider($model, array(
            'pagination' => $pagination,
            'criteria' => $model->dbCriteria,
            'sort' => array(
                'attributes' => array(
                    'cCalldate',
                    'prepared.prNameSrc',
                    'prepared.prNameDst',
                    'cBillsec',
                    'cDuration',
                    'cDisposition',
                    'cDirection',
                    'prepared.prNameExtChan'
                ),
                'defaultOrder' => array(
                    'cCalldate' => true,
                ),
            )
        ));
        if (Yii::app()->request->getParam('export')) {
            if (!Users::isAdmin())
                throw new CHttpException(403, Yii::t('yii', 'You are not authorized to perform this action.'));
            $dataProvider->getSort()->applyOrder($model->dbCriteria);
            $this->csvExport($model->dbCriteria, $dataProvider->getSort());
            Yii::app()->end();
        }
        $this->render('index', array('dataProvider' => $dataProvider, 'model' => $form, 'h1' => array(
                'title' => $sp["spTitle"],
                'class' => $sp["spHtmlClass"]
        )));
    }

    public function actionMakecsv() {
        if (isset($_POST['data'])) {
            $result = urldecode($_POST['data']);
            Yii::app()->getRequest()->sendFile('export.csv', $result, "text/csv; charset=utf8", true);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return CallInfo the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = CallInfo::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CallInfo $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'call-info-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function csvExport(CDbCriteria $criteria, CSort $sort) {
        $fp = fopen('php://temp', 'w');
        $model = Yii::app()->db->createCommand()//DATE_FORMAT(cCalldate,  "%b %e, %Y %r" )
                ->select('
                    DATE_FORMAT(cCalldate,  "' . Yii::t('m', "%b %e, %Y %r") . '" ) AS cCalldate, 
                    cSrc, cDst, SEC_TO_TIME(cDuration) as cDuration, 
                    SEC_TO_TIME(cBillsec) as cBillsec, cDisposition, cUniqueid, 
                    cDirection, prepared.prNameSrc, prepared.prNameDst, 
                    prepared.prNameExtChan
                 ')
                ->from('CallInfo t')
                ->join('PreparedCDR prepared', 'prepared.prID=t.cID');
        $model->where = $criteria->condition;
        $model->params = $criteria->params;
        $dataProvider = new CSqlDataProvider($model, array(
            'sort' => $sort,
            'pagination' => false,
                )
        );

        $headers = array(
            'cCalldate' => CallInfo::model()->getAttributeLabel('cCalldate'),
            'cSrc' => CallInfo::model()->getAttributeLabel('cSrc'),
            'cDst' => CallInfo::model()->getAttributeLabel('cDst'),
            'cDuration' => CallInfo::model()->getAttributeLabel('cDuration'),
            'cBillsec' => CallInfo::model()->getAttributeLabel('cBillsec'),
            'cDisposition' => CallInfo::model()->getAttributeLabel('cDisposition'),
            'cDirection' => CallInfo::model()->getAttributeLabel('cDirection'),
            'cOutchanChannels_cID' => CallInfo::model()->getAttributeLabel('cOutchanChannels_cID'),
        );
        $row = array();
        foreach ($headers as $attr => $header) {
            $row[] = $header;
        }
        fputcsv($fp, $row);

        $directions = array(
            'inbound' => Yii::t('dir_data', 'inbound'),
            'outbound' => Yii::t('dir_data', 'outbound'),
            'local' => Yii::t('dir_data', 'local'),
            'callback' => Yii::t('dir_data', 'callback')
        );
        $dispositions = array(
            'FAILED' => Yii::t('dis_data', 'FAILED'),
            'BUSY' => Yii::t('dis_data', 'BUSY'),
            'ANSWERED' => Yii::t('dis_data', 'ANSWERED'),
            'UNKNOWN' => Yii::t('dis_data', 'UNKNOWN'),
            'NO ANSWER' => Yii::t('dis_data', 'NO ANSWER'),
            'uFAILED' => Yii::t('dis_data', 'uFAILED'),
            'uHANGUP' => Yii::t('dis_data', 'uHANGUP'),
        );

        $srcTempMap = new direction\TemplateMap(
                new CConfiguration(Yii::getPathOfAlias('callinfo.config.direction.src') . '.php')
        );
        $dstTempMap = new direction\TemplateMap(
                new CConfiguration(Yii::getPathOfAlias('callinfo.config.direction.dst') . '.php')
        );


        Yii::beginProfile('blockID');
        foreach ($dataProvider->getData() as $model) {
            fputcsv($fp, array(
                $model['cCalldate'],
                        $srcTempMap->itemAt($model["cDirection"])
                        ->populateString(
                                array(
                                    ':id' => $model['cSrc'],
                                    ':name' => $model['prNameSrc'],
                                )
                        ),
                        $dstTempMap->itemAt($model["cDirection"])
                        ->populateString(
                                array(
                                    ':id' => $model['cDst'],
                                    ':name' => $model['prNameDst'],
                                )
                        ),
                $model['cDuration'],
                $model['cBillsec'],
                $dispositions[$model['cDisposition']],
                $directions[$model['cDirection']],
                $model['prNameExtChan'],
            ));
        }
        Yii::endProfile('blockID');
        rewind($fp);
        Yii::app()->request->sendFile('export.csv', stream_get_contents($fp));
        fclose($fp);
    }

    public function actionModalPb($id = false) {
        var_dump($id);

        /* echo json_encode(array(
          'header'=>'someHeader',
          'body'=>'someBody',
          )); */


        echo '<div id="agentmodal" class="modal hide fade">
<div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h4>Pb</h4>
</div>

<div class="modal-body">
    eees
</div>

<div class="modal-footer">
    <a data-dismiss="modal" class="btn btn-primary" id="yw6" href="#">Save changes</a>    <a data-dismiss="modal" class="btn" id="yw7" href="#">Close</a></div>

</div>';
    }

}
