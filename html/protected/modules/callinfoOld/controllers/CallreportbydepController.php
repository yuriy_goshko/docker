<?php

class CallreportbydepController extends FrontController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Общая статистика отделов
     */
    public function actionIndex() {
        $post = new CallInfoForm;
        $obj = DCallInfo::model();
        $sp = StaticPages::model()->findByAttributes(array('spExtUrl' => '/callinfo/callreportbydep'));
        $this->setPageTitle($sp["spTitle"]);

        /*
         * There is was post/get request?
         */
        if (isset($_REQUEST[get_class($post)])) {
            $post->attributes = $_REQUEST[get_class($post)];
        }
        //setup filter values;
        $obj->attributes = $post->attributes;
        $obj->search();
        $dao = $obj->fetchAllByDepartments();
        $dataProvider = new CArrayDataProvider($dao->result, array(
            'keyField' => 'name',
            'sort' => array(
                'attributes' => array(
                    'name',
                    'call_0', 'call_1', 'call_2', 'call_3',
                    'allcalls'
                ),
            ),));

        $this->render('index', array(
            'model' => $post,
            'callinfoData' => $dao,
            'provider' => $dataProvider,
            'h1' => $sp["spTitle"]
        ));
    }

}