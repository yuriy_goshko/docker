<?php

class QueuebydaysController extends FrontController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Входящие звонки, очереди по дням
     */
    public function actionIndex() {
        $form = new QueueLogForm;
        $obj = DQueueLog::model();
        $sp = StaticPages::model()->findByAttributes(array('spExtUrl' => '/callinfo/queuebydays'));
        $this->setPageTitle($sp["spTitle"]);

        /*
         * There is was post/get request?
         */
        if (isset($_REQUEST[get_class($form)])) {
            $form->attributes = $_REQUEST[get_class($form)];
        }
        //setup filter values;
        $obj->attributes = $form->attributes;
        $this->render('index', array(
            'model' => $form,
            'queueData' => $obj->search()->fetchAllStatByDays($form->period),
            'h1' => $sp['spTitle']
        ));
    }

}