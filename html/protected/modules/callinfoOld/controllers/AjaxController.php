<?php

/**
 * Ajax controller.
 *
 * @author Kuzich Yurii <qzichs@gmail.com>
 * @since 1.0.0
 */
class AjaxController extends CController {

    public $layout = false;

    public function filters() {
        return array(
            'ajaxOnly',
            'accessControl'
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array(
                    'agent',
                ),
                'users' => array('admin')
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionModalAgents($id = false) {

        $model = new Agents();
        if ($id != false)
            $model = Agents::model()->find('sChannels_cID = :id', array('id' => $id));
        $model->setScenario('ajaxValidate');
        if (isset($_POST['ajax']) && Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }

        if (isset($_POST['Agents'])) {
            $model->attributes = $_POST['Agents'];
            if (!$model->save()) {
                throw new CHttpException(500, Yii::t('m', 'At runtime error occurred'));
            }
        }

        echo json_encode(
                array(
                    'modalId' => 'modalagents',
                    'header' => Yii::t('m', 'Change the name of an agent') . ' ' . $model->name,
                    'body' => array(
                        'forms' => array(
                            'agentsform' => array(
                                'Agents[fullname]' => $model->channelowners->coName
                            )
                        )
                    )
                )
        );
    }

    public function actionModalPb($id = false) {

        $model = PhoneBook::model()->mine()->find('pbNumber = :id', array('id' => $id));
        if ($model == null) {
            $model = new PhoneBook();
        }

        if (isset($_POST['ajax']) && Yii::app()->getRequest()->getIsAjaxRequest()) {
            echo CActiveForm::validate(array($model));
            Yii::app()->end();
        }

        if (isset($_POST['PhoneBookForm'])) {
            $model->attributes = $_POST['PhoneBookForm'];
            $model->pbGroups_gID = Yii::app()->user->getState('Groups_gID');
            $model->pbNumber = $id;
            if (!$model->save()) {
                throw new CHttpException(500, Yii::t('m', 'At runtime error occurred'));
            }
        }

        if ($model->isNewRecord) {
            $header = implode(' ', array(Yii::t('m', 'Add a number'), $id, Yii::t('m', 'in phonebook', 1)));
        } else {
            $header = implode(' ', array(Yii::t('m', 'Update a number'), $id, Yii::t('m', 'in phonebook', 2)));
        }

        echo json_encode(
                array(
                    'modalId' => 'modalpb',
                    'header' => $header,
                    'body' => array(
                        'forms' => array(
                            'pbform' => array(
                                'PhoneBookForm[pbOwner]' => $model->pbOwner,
                                'PhoneBookForm[pbBlacked]' => $model->pbBlacked,
                                'PhoneBookForm[pbReason]' => $model->pbReason,
                            )
                        )
                    )
                )
        );
    }

    /**
     * Get populated html form by model init value for reset to default.
     * 
     * Class model determine dynamically (by passed var "class").
     * Can use rapidly with different forms classes.
     */
    public function actionGetResetedForm() {
        $class = $_REQUEST['class'];
        $model = new $class;
        if (isset($_REQUEST[get_class($model)])) {
            $model->setAttributes($_REQUEST[get_class($model)]);
        }
        $rootView = $_REQUEST['view'];
        $this->renderPartial("/" . $rootView . '/forms/search-form', array('model' => $model));
    }

}
