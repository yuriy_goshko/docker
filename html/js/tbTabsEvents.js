$(document).ready(
		function() {
			tbTabClick = function(e) {
				if (e.target.getAttribute('data-toggle') != 'tab')
					return;

				$h = e.target.href;
				$h = $h.split("#")[1];
				$h = "?tabid=" + $h;
				// console.log($h);
				
				params = $.deparam.querystring($h);
				$url = decodeURIComponent($.param.querystring(window.location.pathname, params));
				history.pushState(null, document.title, $url);
			}
		});