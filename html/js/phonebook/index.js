$(document).ready(function() {
    var action = $('#phone-book-form').attr('action');
    $('.update').click(function(e) {
        e.preventDefault();
        pGVfields('phone-book-form', 'PhoneBookForm_', this);
        setVisibleReason();
        $('#phone-book-form button[type="submit"]').html(buttons.upd);
    });
    $('#reset').click(function(e) {
        $('#phone-book-form').attr('action', action);
        $('#phone-book-form button[type="submit"]').html(buttons.add);
        setVisibleReason();
    });
});

function setVisibleReason() {
    if ($('#PhoneBookForm_pbBlacked').val() == 'no') {
        $('#PhoneBookForm_pbReason').parent().addClass('hidden');
    } else {
        $('#PhoneBookForm_pbReason').parent().removeClass('hidden');
    }
}
