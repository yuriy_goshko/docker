$(document).ready(function(){
    $('ol.sortable').nestedSortable({
        protectRoot:true,
        maxLevels:4,
        forcePlaceholderSize: true,
        handle: 'div',
        items: 'li',
        opacity: .6,
        placeholder: 'placeholder',
        tabSize: 25,
        tolerance: 'pointer',
        toleranceElement: '> div',
        'update': function (event, ui) {
            updpgaes();
        }
    });
});


function updpgaes(){
    
    serialize = jQuery('ol.sortable').nestedSortable('serialize');
    
    //document.getElementById('brain-ajax').innerHTML = '<img src="/adm_tmp/i/ajax-loader.gif" />';
     $('#pages .grid-view').addClass("grid-view-loading");   
    $.post
    (
        "root",
        {
            q: serialize
        },
        function(data) {
            $('#page .grid-view').removeClass("grid-view-loading");
        }
        );
}