$(document).ready(function(){
    action = $('#RulesForWritingNumbersForm').attr('action');
    $('.addpbn').click(function(e){
        e.preventDefault();      
        data = pGVfields('RulesForWritingNumbersForm', 'ciRulesForWritingNumbers_', this);
        setDropDownList($('#RulesForWritingNumbersForm SELECT[name="ciRulesForWritingNumbers[OnlyRealNumbers]"]'),data['OnlyRealNumbers']);
        setDropDownList($('#RulesForWritingNumbersForm SELECT[name="ciRulesForWritingNumbers[ActionType]"]'),data['ActionType']);
        $('#RulesForWritingNumbersForm button[type="submit"]').html(buttons.upd);
    }); 
    
     $('#reset').click(function(e){
        $('#RulesForWritingNumbersForm').attr('action', action);
        $('#RulesForWritingNumbersForm button[type="submit"]').html(buttons.add);
     });
    
});


