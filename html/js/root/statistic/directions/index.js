$(document).ready(function(){
    action = $('#ciDirectionsForm').attr('action');
    $('.addpbn').click(function(e){    	
        e.preventDefault();      
        data = pGVfields('ciDirectionsForm', 'ciDirections_', this);     
        
        setDropDownList($('#ciDirectionsForm SELECT[name="ciDirections[Used]"]'),data['Used']);
        $('#ciDirectionsForm button[type="submit"]').html(buttons.upd);

    }); 
    
     $('#reset').click(function(e){
        $('#ciDirectionsForm').attr('action', action);        
        $('#ciDirectionsForm button[type="submit"]').html(buttons.add);      
     });
    
});