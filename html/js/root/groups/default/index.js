$(document).ready(function(){
    action = $('#groups').attr('action');
    $('.addpbn').click(function(e){
        e.preventDefault();
        pGVfields('groups', 'Groups_', this);
        $('#groups input[name="Groups[gID]"]').attr('disabled', 'disabled');
        $('#groups button[type="submit"]').html(buttons.upd);
    }); 
    
     $('#reset').click(function(e){
        $('#groups').attr('action', action);
        $('#groups button[type="submit"]').html(buttons.add);
        $('#groups input[name="Groups[gID]"]').removeAttr('disabled');
     });
    
});
