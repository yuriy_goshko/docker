/*Main js file. It exists across all project*/

buttons = new Object();
buttons = {
	'add' : 'Добавить',
	'upd' : 'Обновить'
};
$(document).ready(
		function() {

			// highlight menu items
			$('.nav li').each(
					function(key, li) {
						path = window.location.pathname;
						if ($(li).find('a').attr('href')[0] != '#'
								&& path == $(li).find('a')[0].pathname) {
							$(li).addClass('active');
							$(li).parents('li').addClass('active');
						}
					});

			$(".tooltip-top").tooltip({
				placement : "top"
			});
			$(".tooltip-right").tooltip({
				placement : "right"
			});
			$(".tooltip-bottom").tooltip({
				placement : "bottom"
			});
			$(".tooltip-left").tooltip({
				placement : "left"
			});

			$('.addpbn').click(function(e) {
				$el = document.getElementById("_AddDefDepartment");
				if ($el != null)
					$el.disabled = true;
			});
			$('#reset').click(function(e) {
				$el = document.getElementById("_AddDefDepartment");
				if ($el != null)
					$el.disabled = false;
			});
		});

function setDropDownList(elementRef, valueToSetTo) {
	elementRef = elementRef.get(0);
	for (var i = 0; i < elementRef.options.length; i++) {
		if (elementRef.options[i].text == valueToSetTo) {
			elementRef.options[i].selected = true;
			break;
		}
	}
}

function pGVfields(formid, fprefix, ob) {

	if (fprefix == undefined)
		fprefix = formid;
	reg = new RegExp('-map', 'i');
	var data = new Object;

	url = ob.href;
	$('#' + formid).attr('action', url);
	$(ob).parent().parent().children().each(function(key, el) {
		if (!$(el).hasClass('button-column')) {
			in_val = $(el).html();
			in_key = $(el).attr("class");
			if (reg.test(in_key)) {
				in_key = new String(in_key);
				in_key = in_key.split('-');
				in_key = in_key[0];
				in_val = htmlToValue($('#' + fprefix + in_key), $(el).html());
			}
			data[in_key] = in_val;
			/* data2 = "{"+$(el).attr("class")+" : "+$(el).html()+"}"; */
		}
	});

	try {
		for ( var key in data) {
			$('#' + fprefix + key).val(data[key]);
			$('#' + fprefix + key + '_label').html(data[key]);
		}
	} catch ($e) {
	}
	return data;
}

function htmlToValue(id, param) {
	var tmp = new Object;
	$(id).children().each(function(key, val) {
		tmp[$(val).html()] = $(val).val();
	});
	if (param != undefined) {
		return tmp[param];
	}
	return tmp;
}

$(window).load(function() {
	$('.modal-link').click(function(e) {
		e.preventDefault();
		var url = $(this).attr('href');

		$.ajax({
			url : url,
			type : 'get',
			dataType : 'html',
			success : function(data) {
				$(data).modal();
			},
			error : function(xhr, textStatus, errorThrown) {
				$.notify(xhr.responseText, "error");
			}
		});
	});
});

/*
 * Convert value from datePicker to dateTime sql type.
 * 
 * @param {String} obj @returns {String}
 */
function getDateTimeFormat(obj) {
	var date = $(obj).datepicker("getDate");
	if (date == null) {
		$(obj).next().removeAttr('value');
		return;
	}
	var datePieces = new Array(date.getFullYear(), date.getMonth() + 1, date
			.getDate());
	var timePieces = new Array(date.getHours(), date.getMinutes(), date
			.getSeconds())
	return datePieces.join("-") + " " + timePieces.join(":");
}

function setRowSelected(el) {
	if (el.checked) {
		$(el).parent().parent().addClass('selected');
	} else {
		$(el).parent().parent().removeClass('selected');
	}
}

jQuery.fn.reset = function() {
	$(this).each(function() {
		this.reset();
	});
}

jQuery.expr[':'].regex = function(elem, index, match) {
	var matchParams = match[3].split(','), validLabels = /^(data|css):/, attr = {
		method : matchParams[0].match(validLabels) ? matchParams[0].split(':')[0]
				: 'attr',
		property : matchParams.shift().replace(validLabels, '')
	}, regexFlags = 'ig', regex = new RegExp(matchParams.join('').replace(
			/^\s+|\s+$/g, ''), regexFlags);
	return regex.test(jQuery(elem)[attr.method](attr.property));
}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};