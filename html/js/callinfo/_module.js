$(window).load(function() {

    init();
    $('.hasDatepicker').on("change paste keyup", function() {
        if ($(this).val() === "")
            $(this).val($(this).prop('defaultValue'));
        $(this).next().val(getDateTimeFormat(this));
    });

    $('.hasDatepicker').on("change", function(e) {
        if ($(this).val() === "")
            $(this).val($(this).prop('defaultValue'));
    });

});

function init() {
    $('.hasDatepicker').each(function() {
        $(this).next().val(getDateTimeFormat(this));
    });
}

function ajaxSearch(form, data, hasError) {

    if (!hasError) {
        url = form.serialize();
        params = $.deparam.querystring('?' + url);
        window.History.pushState(null, document.title, decodeURIComponent($.param.querystring(window.location.pathname, params)));
        return false;
    }

}

$('#search-form :reset').click(function(evt) {
    var btn = $(this);
    btn.button('loading'); // call the loading function
    evt.preventDefault();
    var formAction = evt.target.form.action;
    var formName = evt.target.form.name;
    var view = formAction.substr(formAction.lastIndexOf('/') + 1);
    var servUrlPath = ["/callinfo", "ajax", "GetResetedForm"];
    var url = servUrlPath.join('/');
    $.post(url, {view: view, class: formName}, function(data) {
        var jsonData = $(data).serializeObject();
        var jsform = $("#" + "search-form").jsForm({prefix: ''}).jsForm('fill', jsonData);
        init(); //reinit
        btn.button('reset');
    });
});
