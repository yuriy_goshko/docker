$(document).ready(function() {

    $('#pexport-button').on('click', function() {
        var href = window.location.href;
        if (href.indexOf('?') == -1) {
            href = window.location.href + '?';
        }
        window.location.href = href + '&export=true';
    });

    function filterPosition() {
        if (botbarpos - $(document).scrollTop() <= 0) {
            $('.fhead').css('margin-bottom', $('#bottombar').height());
            $('#bottombar').css('position', 'fixed');
            $('#bottombar').css('z-index', '999');
            $('#bottombar').css('top', $('#topbar').height() - 1);
        }

        if (botbarpos - $(document).scrollTop() > 0) {
            $('.fhead').removeAttr('style');
            $('#bottombar').removeAttr('style');
        }
    }


    $(document).on('click', ':not(#anything)', function(e) {
        hidePopOvers(e);
    });

    $(document).on('show', '.gridplayer', function(e) {
        $("script").each(function() {
            if (/.*jouele\.js$/.test($(this).attr("src"))) {
                jQuery.getScript($(this).attr("src"), function(data) {
                    initPlayer();
                });
            }
        });

    })

    $(document).on('click', '.modalpop', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'html',
            success: function(data) {
                var data = jQuery.parseJSON(data);
                $('#' + data.modalId).find('.modal-header h4').html(data.header);
                if (data.body.forms != undefined) //populate forms
                    $.each(data.body.forms, function(id, val) {
                        $("#" + id).attr('action', url);
                        var jsform = $("#" + id).jsForm({prefix: ''}).jsForm('fill', val);
                    });

                $('#' + data.modalId).modal();
            },
            error: function(xhr, textStatus, errorThrown) {
                $.notify(xhr.responseText, "error");
            }
        });
    });



});

function submitModalForm(form, data, hasError)
{
    if (!hasError)
    {
        var url = form.attr('action');
        $.ajax({
            url: url,
            type: 'post',
            data: $(form).serialize(),
            dataType: 'html',
            success: function(data) {
                var data = jQuery.parseJSON(data);
                $('#' + data.modalId).find('.modal-header h4').html(data.header);
                if (data.body.forms != undefined) //populate forms
                    $.each(data.body.forms, function(id, val) {
                        $.fn.yiiGridView.update('calls');
                        $("#" + id).jsForm({prefix: ''}).jsForm('fill', val);
                    });
                $('#' + data.modalId).modal('hide');
            },
            error: function(xhr, textStatus, errorThrown) {
                $.notify(xhr.responseText, "error");
            }
        });
    }
}

function setVisibleReason() {
    if ($('#PhoneBookForm_pbBlacked').val() == 'no') {
        $('#PhoneBookForm_pbReason').parent().parent().addClass('hidden');
    } else {
        $('#PhoneBookForm_pbReason').parent().parent().removeClass('hidden');
    }
}
