/*Only for front layout js file*/

//all js is invoked
$(window).load(function() {

    //customize selectpicker
    $('.designed-select button.selectpicker').removeClass('btn dropdown-toggle');
    $('.designed-select button.selectpicker').find('.caret').remove();
    
    //attach event handlers
     $('.bootstrap-select .checkall').click(function(e){
        e.preventDefault();
        var id = $(e.target).closest('.bootstrap-select').find('button.selectpicker').data('id');
        $('#' + id).selectpicker('selectAll');
    });

     $('.bootstrap-select .uncheckall').click(function(e){
        e.preventDefault();
        var id = $(e.target).closest('.bootstrap-select').find('button.selectpicker').data('id');
        $('#' + id).selectpicker('deselectAll');
    });
});

$(document).ready(function() {

    if ($('.jouele').length > 0)
        initPlayer();

    $('.cust tbody tr').click(function(e) {
        $('.cust tbody tr').removeClass('selected');
        $(this).addClass('selected ');
    });
    /*Customize exporting buttons*/
    $('.chartToolTips a').removeClass('btn');

//CSV export function
    $('#export-button').click(function(e) {
        var headers = [];
        $('.items thead tr').find('th').each(function(el, val) {
            if (!$(val).hasClass('csvhide'))
                headers[el] = $(val).text();
        });
        var csv_value = $('.items').table2CSV({
            delivery: 'value',
            header: headers,
        });
        var f = document.createElement("form");
        f.setAttribute('method', "post");
        f.setAttribute('action', "/callInfo/default/makecsv");
        var i = document.createElement("input"); //input element, text
        i.setAttribute('type', "text");
        i.setAttribute('name', "data");
        i.setAttribute('value', encodeURIComponent(csv_value));
        f.appendChild(i);
        f.submit();

    });


//Show/hide block    
    $('.toggled-title >:first-child').click(function(e) {
        var content = $(this).parent().next('.toggled-content');
        var that = $(this).parent();
        if (that.hasClass('active')) {
            $(content).addClass('collapsed');
            that.removeClass('active');
            that.find('span').attr('class', 'icon-chevron-down');
            $('#bottombar').removeClass('active');
        } else {
            $(content).removeClass('collapsed');
            that.addClass('active');
            that.find('span').attr('class', 'icon-chevron-up');
            $('#bottombar').addClass('active');
        }
    });
    //configure navbar items before iocns
    $(".nav > li > a").append("<span class='navico'></span>");
});

function initPlayer() {

    /*Customize JPlayer*/
    if ($('.jouele-play-lift').length > 0)
        $(function() {
            $('.jouele-play-lift').remove();

            $(".jouele").bind($.jPlayer.event.timeupdate, function(event) {
                var id = this.id;
                var idSelector = '#' + id;

                $(idSelector).find('.jouele-play-bar-after').css('left', $(idSelector).find('.jouele-play-bar').width() - 1 + 'px');
            });
            $(".jouele").bind($.jPlayer.event.play, function(event) {
                var id = this.id;
                var idSelector = '#' + id;
                if ($(idSelector).find(".jouele-play-bar").length > 0)
                    $(idSelector).find(".jouele-play-bar").css('display', 'block');
                if (!$(idSelector).find(".jouele-play-bar-before").length > 0)
                    $(idSelector).find(".jouele-play-bar").before('<div class="jouele-play-bar-before"></div>');
                if (!$(idSelector).find(".jouele-play-bar-after").length > 0)
                    $(idSelector).find(".jouele-play-bar").after('<div class="jouele-play-bar-after"></div>');
            });
        });


}


function highchartCallback(chart) {
    var chart = chart; // chart object
    var mainid = chart.options.chart.renderTo; //chart id

    var print = $('#' + mainid).parent().find("#btnPrint");
    var exprt = $('#' + mainid).parent().find("#btnExport").find('.dropdown-menu a');

    $(print).bind('click', function(event) {
        chart.print();
    });

    $(exprt).bind('click', function(event) {
        event.preventDefault();
        var expType = $(event.target).attr('href');
        var mimeType = '';
        switch (expType) {
            case '#jpeg':
                mimeType = 'image/jpeg';
                break;
            case '#png':
                mimeType = 'image/png';
                break;
            case '#svg':
                mimeType = 'svg';
                break;
            case '#pdf':
                mimeType = 'application/pdf';
                break;
        }
        if (mimeType != false)
            chart.exportChart({type: mimeType});

    });
}

function hidePopOvers(e) {
    $('.link-popover').each(function() {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons and other elements within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
            return;
        }
    });
}
